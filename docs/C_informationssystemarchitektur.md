Phase C: Informationssystemarchitektur
======================================

Modelle, Tools und Techniken der Anwendungsarchitektur
------------------------------------------------------

Der SouvAP soll der öffentlichen Verwaltung in ihren Rollen als Nutzer,
Bereitsteller und Auftraggeber von Digitalen Technologien die
Anforderungen Wechselmöglichkeit, Gestaltungsfähigkeit und Einfluss auf
Anbieter befriedigen. Dazu wird der Ansatz verfolgt,
herstellerunabhängige Open Source Produkte zu einer universell
Softwareplattform zu verschmelzen. Mit der Bereitstellung aller
Komponenten auf Open CoDE wird die Offenheit des Gesamtsystems
unterstrichen und es wird eine unbeschränkte Nachnutzbarkeit angestrebt.

Die Umsetzung soll darin bestehen, bereits verfügbare Softwareprodukte
passend zu vorhandenen Anforderungen zu identifizieren und diese
entsprechend den Voraussetzungen für Open CoDE aufzubereiten. Zusätzlich
sind die Produkte auf einheitliche Bedienung und durchgehende
Kompatibilität mit Datenformaten und Informationsaustauschprotokollen
über alle Schichten zu ertüchtigen. Letzteres greift die Anforderung
nach medienbruchfreier Interoperabilität auf. Falls ein Produkt zum
SouvAP keines der SouvAP-Standardprotokolle unterstützt, ist zunächst
der Hersteller auf erforderliche Anpassungen hin anzusprechen. Ist die
Anpassung nicht möglich, dann soll die Kopplung über verlustfrei
arbeitende Protokollwandler (API-Gateways) erfolgen.

Die Architekturarbeit in diesem Kapitel betrachtet schwerpunktmäßig die
möglichen Modelle für die Anwendungsentwicklung, relevante Kriterien für
das Gesamtsystem, den Mindeststandard für abzudeckende Funktionen sowie
Rahmenbedingungen für technisch geeignete Produkte und ihre Integration
in das Gesamtsystem. Anschließende Diskussionen fächern Anforderungen an
das so geschaffene Servicemodell auf und betrachten Sicherheitsaspekte
zum Servicemodell. Das Kapitel mündet in eine Festlegung der
einzusetzenden Techniken zur Ausgestaltung der Anwendungsarchitektur.

### Anwendungsentwicklungsmodell

Der Auftrag sieht eine Migration bereits vorhandener
Open-Source-Softwareschichten zu einem Gesamtsystem vor. Gemäß
punktueller Analyse und allgemeine Einschätzung entstanden diese
mehrheitlich aus individuell festgelegter Anwendungsmodellierung.

Es wird davon ausgegangen, dass jedes infrage kommende
Open-Source-Produkt als Schichtenmodell aufgefasst werden kann, dass im
weitesten Sinne

-   eine Datenzugriffsschicht,

-   eine funktionelle Datenverarbeitungsschicht

-   und i.d.R., aber optional eine Präsentationsschicht

enthält. Eine Überführung aller Einzelkomponenten des SouvAP in ein
einheitliches Architekturentwicklungsmodell ist nicht beabsichtigt.

Im Zuge der Migration (Phase F) sind die genannten 3 Schichten zu
identifizieren und entsprechend den Anforderungen anzupassen:

-   Die Datenzugriffsschichten sind im Gesamtsystems zu harmonisieren.
    Dies soll über die bereits genannten API-Gateways gelingen.

-   Die funktionelle Datenverarbeitungsschicht ist anzupassen bzw. zu
    ergänzen, wenn ein Produkt zum Release-Kandidaten erhoben werden
    soll, es aber die funktionellen Anforderungen nicht vollumfänglich
    abdeckt.

-   Die Präsentationsschicht ist mit den Anforderungen nach
    einheitlichen Bedienkonzepten und barrierefreier Bedienung hin
    auszugestalten.

### Systemmodell

Die Modellierung der leistungserbringenden Komponenten kann direkt das
geforderte Leistungsspektrum aufgreifen, der grob umrissen in

-   synchroner und

-   asynchroner

**Kommunikation** zwischen Bediensteten der öffentlichen Verwaltung in

-   Text,

-   Bild,

-   Ton und

-   Fachdaten,

**Zusammenarbeit** der Bediensteten der öffentlichen Verwaltung auf

-   Fachebene und

-   Ebenen-übergreifend

und **Einbindung von fachlichen Diensten** wie E-Akte usw. umfasst.

Die Ein-/Ausgabebeziehungen müssen im Verbund die Anforderung
**Medienbruchfreiheit** befriedigen. Aufgrund der Vielzahl möglicher
Beziehungen zwischen Benutzer und System (Applikation, Service) sowie
Applikationen und Services untereinander sind dazu die erforderlichen
**Kommunikationsbeziehungen** zu analysieren und es sind die jeweils
zulässigen Verbindungsarten (verbindungslos, statuslos, zustandslos,
sitzungsorientiert) festzulegen. Des Weiteren sind **standardisierte
Kommunikationsprotokolle** für den Nachrichtenaustausch zu benennen. Ist
dies nicht möglich, dann sind Gateways einzuziehen, die Aufgaben der
verlustfreien Datenformatkonvertierung bewältigen. Ein weiteres
Augenmerk ist auf **Sicherheitsfunktionen** zu lenken, die die
Einhaltung der **Schutzziele gemäß unternehmerischen und regulatorischen
Vorgaben** adressieren.

Das Gesamtmodell enthält somit ein **Bündel von Modulen**, die in eine
**übergreifende Struktur einzubetten** sind. Diese Gesamtstruktur muss
die **Schnittstelle hin zur darunter liegenden Infrastruktur** liefern.
Mit der Anforderung „DVS-konform“ liegt dafür ein gut vorbereiteter
Zielkorridor vor. Umsetzungsrelevante Entscheidungen sind im Zuge der
Umsetzung zu treffen. Die Architektur geht davon aus, dass das logische
Systemmodell darauf vorbereitend keine Einschränkungen erzeugt.

#### Komponentenmodell

Das Diagramm Zielbild Basisfunktionen des SouvAP gemäß der
Leistungsbeschreibung LB22 zum SouvAP illustriert den Mindestbedarf an
geschäftsprozessunterstützenden Werkzeugen und das Zusammenspiel dieser
Komponenten. Zusätzlich sind dort Anforderungen dargestellt, die jede
Komponente des SouvAP berücksichtigen muss.

![](media/file21.png)

Abbildung : Zielbild Basisfunktionen des SouvAP

Die Architektur verfolgt den Ansatz, voneinander trennbare
Aufgabenfelder in Module zu bündeln. Dies entspricht der in der Praxis
verbreiteten Ausgestaltung geschäftsprozessunterstützender digitaler
Werkzeuge als anwendungsorientierte Softwareprodukte. Dieser Ansatz
bereitet außerdem die Austauschbarkeit von Komponenten zum SouvAP vor.

In der Leistungsbeschreibung LB23 zum SouvAP wurden zahlreiche
Produkttypen aufgelistet, die die Basisfunktionen des SouvAP bewältigen
können. Diese sind aus Sicht der Architektur den Aufgabenfeldern
Produktion, Kollaboration und Kommunikation wie folgt zuzuordnen
(Mehrfachnennungen möglich):

**Produktion –** Texteditor, Textverarbeitung, Tabellenkalkulation,
Gemeinsame Nutzung von Daten, Grafik, Kennwortverwaltung.

**Kommunikation –** Kurznachrichten, E-Mail und Kalender, Video- und
Telefonkonferenzen, Gemeinsame Nutzung von Daten, PDF-Anzeige,
Medienwiedergabe, Umfrage- und Abstimmungsprogramm, Live Streaming,
Visualisierungstool, Lernmanagementsystem.

**Kollaboration –** Whiteboard, Wissensmanagement, Projektmanagement,
Video- und Telefonkonferenzen, Gemeinsame Nutzung von Daten, Umfrage-
und Abstimmungsprogramm, Live Streaming, Lernmanagementsystem.

Ergänzende Anforderungen an das Leistungsspektrum des SouvAP und seine
Konformität zu geltenden Sicherheitsvorschriften begründen die
Erweiterung des Leistungsschnitts:

**Übergreifend –** Einbindung fachlicher Verwaltungs-Dienste.

**Sonstiges –** Dateikompression, Anti-Virus, Passwort Tresor

#### Kommunikationsmodell

Der SouvAP soll als Zusammenschluss von Modulen entstehen, die
untereinander medienbruchfrei Nachrichten austauschen können. Jedes
Modul soll vollumfänglich auf eine konkrete Aufgabenstellung (fachliche
Domäne) hin ausgerichtet sein. Das heißt, das Modul enthält alle
erforderlichen Funktionen zur Bewältigung der Aufgabe und benötigt dazu
keine Leistungen anderer Module (Self-Contained System).

Die Realisierung eines Moduls ist ein Produkt zum SouvAP. Die
Austauschbarkeit eines Produkts wird über den funktionellen
Leistungsschnitt festgeschrieben, der als Mindestanforderung des
jeweiligen Moduls festgelegt wurde: Ein Produkt ist austauschbar, wenn
ein Alternativprodukt mit gleichwertigem Funktionsvorrat existiert. Des
Weiteren ist der Leistungsvorrat jedes Moduls auf den tatsächlich
benötigten Leistungsumfang zu beschränken und es sind eventuell
enthaltene und nicht benötigte Funktionen abzuschalten (Härtung).

Den Nachrichtenaustausch betreffend sind Fragen zum Wer, Was und Wie zu
klären: Wer darf mit Wem kommunizieren, Was ist zulässiger
Kommunikationsinhalt und Wie sind die Inhalte zu formatieren. Daraus
resultieren die folgenden technischen und sicherheitsrelevanten
Anforderungen:

-   Authentifizierung: Der Zugriff auf Informationen ist über
    Berechtigungsschemata zu steuern. Die Authentifizierung prüft, ob
    der Zugriff von einer registrierten Quelle aus erfolgt.

-   Autorisierung: Die Autorisierung prüft, ob die authentifizierte
    Quelle auf die angefragten Informationen zugriffsberechtigt ist.
    Fällt dieser Test negativ aus, dann wird die Kommunikation mit
    technischen Mitteln unterbunden.

-   Datenformatierung: Falls der Nachrichtenaustausch konform zu
    geltenden Sicherheitsschemata erfolgt, ist eventuell eine
    Transformation der Nachrichten zwischen unterschiedlichen Ein- und
    Ausgabeprotokollen der Kommunikationsteilnehmer durchzuführen.

-   Verschlüsselung: Die verschlüsselte Datenübertragung sensibler Daten
    ist obligatorisch.

-   Überwachung: Jedwede Kommunikation wird über die Anwendung von
    Richtlinien geschützt. Die Überwachung weist die Anwendung der
    Richtlinien nach und protokolliert dies in besonders
    schutzbedürftigen Fällen.

Die Folgeabschnitte dieses Kapitels listen den Minimalvorrat an
Standardprotokollen, mit denen die Module des SouvAP kommunizieren
sollen.

Zusätzlich ist der Anforderung nach Austauschbarkeit der Module zu
entsprechen. Aus Sicht der Architektur kann dies sinnvoll über Gateways
gelingen, die die fachlich und sicherheitstechnisch erforderlichen
Anforderungen umsetzen. Diese Gateways leiten die Zugriffsberechtigung
eines Anforderers auf eine Ressource aus Sicherheitsrichtlinien ab, die
das Gesamtsystem in zentralen Speichern verwahrt. Formatkonvertierungen
führen sie unter Bezugnahme auf Protokollspezifikationen zu Ein- und
Ausgabeanforderungen der korrespondierenden Module durch.

##### Standardisierte Protokolle für Nachrichtenaustausch

**Nachrichtenaustausch**

Der Austausch von Nachrichten zwischen Produkten des SouvAP muss
standardisierte Schnittstellen verwenden, die auf offenen Standards
basieren. Dies ist eine Voraussetzung für Herstellerunabhängigkeit.
Beispiele für zulässige Schnittstellen zum Nachrichtenaustausch
innerhalb des SouvAP bilden SOAP und REST. Der Nachrichtenaustausch muss
ein zuverlässiges Übertragungsprotokoll verwenden.

##### Kommunikationsprotokoll-Standards für den SouvAP

**AMQP (**Advanced Message Queuing Protocol) ist ein auf sowohl
synchrone als auch asynchrone Nachrichtenübertragung zwischen
Anwendungen zugeschnittenes Protokoll. Es unterstützt eine Vielzahl weit
verbreiteter Transportprotokolle einschließlich TCP/IP und http.

**GraphQL** ist eine Abfragesprache für APIs, mit der Entwickler
ausgewählte Dateninhalte abrufen können. GraphQL fordert nur die
benötigten Felder an. GraphQL beschränkt damit die Bandbreite der
Nachrichtenübermittlung auf das Notwendige und steigert implizit die
Durchsatzleistung der damit arbeitenden API.

**gRPC** ist ein von Google entwickeltes Open-Source-RPC-Framework
(Remote Procedure Call). gRPC-basierte APIs übertragen Anfragen und
Antworten als Protokoll-Puffer (Protobuf-basierte Nachrichten).
gRPC-APIS arbeiten ähnlich, aber effizienter als RESTful APIs.
Gegenwärtig favorisieren Entwickler gRPC gegenüber SOAP.

**MQTT (**Message Queuing Telemetry Transport) wurde für die
Nachrichtenübermittlung zwischen Geräten in einem IoT-Netzwerk
entwickelt. Es ist ein auf TCP/IP aufbauendes leichtgewichtiges
Protokoll, das auf hohe Zuverlässigkeit und hohe Skalierbarkeit hin
optimiert wurde.

**OpenAPI** ist eine Spezifikation, gemäß der Entwickler RESTful APIs
dokumentieren können. Diese Herangehensweise soll Entwicklern helfen,
OpenAPI-basierte API-Funktionalitäten schnell zu verstehen. Dies soll
die Basis dafür legen, effiziente Anwendungen in kurzer Zeit fertigen zu
können, die OpenAPI verwenden.

**REST (**Representational State Transfer) ist ein Architekturstil, der
bei der Entwicklung von APIs und insbesondere innerhalb von
Webanwendungen weit verbreitet ist. RESTful APIs verwenden die
HTTP-Methoden GET, POST, PUT und DELETE zum Lesen, Erstellen,
Aktualisieren und Löschen von Nachrichten.

**SOAP (**Simple Object Access Protocol) ist ein weit verbreitetes
Protokoll insbesondere innerhalb von Webservices. SOAP-basierte APIs
übertragen Anfragen und Antworten in XML-Notation.

**WSDL (**Web Services Description Language) ist eine vom World Wide Web
Consortium (W3C) entwickelte standardisierte XML-basierte Sprache. Sie
ist für die Beschreibung von Netzwerkdiensten und zur
Nachrichtenübermittlung nutzbar. WSDL kann Serviceeigenschaften
(angebotene Datentypen und Datenformate) und Servicefähigkeiten
(Funktionen, Austauschprotokolle) kapseln.

##### Standardisierte Protokolle für Datenaustausch

Die Schnittstellen zum Datenaustausch zwischen Komponenten des SouvAP
müssen auf offenen und interoperablen Datenformat-Standards wie z.B. XML
oder JSON basieren. Der Datenaustausch muss fehlerfrei gelingen.
Informationsverluste oder -verfälschungen im Datenaustausch sind nicht
zulässig. Es sind bei entsprechenden Sicherheitsanforderungen die zuvor
genannten Kriterien zum sicheren Datenzugriff anzuwenden.

##### Datenaustausch-Standards für den SouvAP

**HAL (**Hypertext Application Language) ist ein Format, das den Einsatz
von Hyperlinks zum Verweis auf Ressourcen unterstützt. Dies kann die
Navigation durch die API signifikant erleichtern. HAL wird bevorzugt bei
der Entwicklung von REST-basierten APIs verwendet.

**HTTP** (Hypertext Transport Protocol) wurde ursprünglich für den
Transport von Dokumenten von einem Web-Server zu einem Web-Client
entwickelt. Heute wird http für die Übertragung allgemeiner Medien
einschließlich Audio und Video eingesetzt. Mittlerweile ist die
verschlüsselte Variante HTTPS das hauptsächlich verwendete Protokoll für
die Übertragung allgemeiner Daten zwischen vernetzten Endpunkten.

**JWT** (JSON Web Tokens) sind genormte Zugriffs-Token, die zum Nachweis
der Identität eines Benutzers im Zusammenhang mit der sicheren
Übertragung von Informationen zwischen Anwendungen verwendet werden.

**OpenDocument-**Format (ODF) ist ein offenes Dateiformat für
Bürodokumente. ODF wurde von der Organization for the Advancement of
Structured Information Standards (OASIS) entwickelt. ODF spezifiziert
Dokumenttypen wie Textverarbeitung, Tabellenkalkulation und
Präsentationen.

**OOXML** Office Open XML (OOXML) beschreibt von Microsoft entwickelte
Dateiformate zur Speicherung von Bürodokumenten auf XML-Basis, die den
Daten- beziehungsweise Dateiaustausch zwischen verschiedenen
Büroanwendungspaketen ermöglichen sollen.

**SPML** (Service Provisioning Markup Language) dient der Verwaltung von
Identitätsinformationen und Zugriffsberechtigungen über verschiedene
Systeme hinweg. Außerdem ist es mit SPML möglich, Provisioning-Prozesse
automatisiert durchzuführen, z.B. die Einrichtung, Aktualisierung und
Löschung von Benutzerkonten und -rechten in verschiedenen Systemen und
Anwendungen. SPML basiert auf XML und verwendet standardisierte Befehle
und Schnittstellen zur Automatisierung von Identitäts- und
Zugriffsverwaltungsvorgängen.

**XML** (Extensible Markup Language) ist ein Datenformat für die
Übertragung von strukturierten Daten zwischen Anwendungen. XML ist
praktisch der Standard für die Datenübermittlung in SOAP-basierten APIs.

##### Sicherer Datenzugriff

Der Datenzugriff seitens der Komponenten des SouvAP muss
Sicherheitsanforderungen berücksichtigen, die einerseits durch geltende
Datenschutzgesetze wie DSGVO oder im EU Kontext GDPR, andererseits aus
Vorgaben zum sicheren Betrieb vernetzter Informationssysteme
resultieren. Dazu benötigt der SouvAP Sicherheitsfunktionen, die
insbesondere den gesicherten Datenaustausch zwischen den Komponenten des
SouvAP steuern. Die nachfolgende Diskussion fokussiert auf die
Schutzziele Vertraulichkeit und Integrität.

Datenzugriffe sind über Richtlinien zu steuern, die die Kombination aus
Identität des Anforderers und Zugriffsrechte auf die angefragte
Ressource auswerten. Es bietet sich an, Identitäten über Token zu
repräsentieren (Kerberos, SAML, OID) und die Zugriffsrechte mit Mitteln
von Role-based Access Control (RBAC) bzw. Attribute-based Access Control
(ABAC) zu kodieren. Diese Herangehensweise schützt die Vertraulichkeit
über Authentisierung und Autorisierung. Es sollte eine Protokollierung
von Datenzugriffen aktivierbar sein (Accounting).

Eine weitere Methode zum Schutz der Vertraulichkeit besteht in der
Verschlüsselung der Daten. Diese ist auf Datenträgern (encryption of
data at rest) sowie beim Datenzugriff und im Zuge der
Interprozesskommunikation möglich (encryption of data in motion). Es
sind starke Verschlüsselungsmethoden (AES, RSA, ECC) entsprechend den
Empfehlungen des BSI anzuwenden. Zusätzlich sollte es möglich sein
mittels Schnittstellen zu Fremdsysteme zusätzliche Entropie zu
injizieren für höhere Schutzstufen. Die Verschlüsselung selbst wird
dabei aus Zertifikaten mit einer Schlüssellänge von 2048 Bit oder mehr
abgeleitet. Die Arbeit mit unternehmerischen (selbst erstellten)
Zertifikaten erfordert eine Public Key Infrastruktur.

Derartige Zertifikate können auch zum Nachweis der Integrität von Daten
herangezogen werden. Die Technik nutzt ein asymmetrisches Verfahren, das
einen privaten Schlüssel auf die Daten anwendet und daraus eine Signatur
erzeugt. Auf der Seite des Empfängers kommt ein öffentlicher Schlüssel
zum Einsatz. Sind die Daten unverfälscht, dann wird die Signatur als
korrekt erkannt. Mit dieser Methode ist außerdem der Nachweis des
Urhebers möglich. In der Praxis kommen alternativ symmetrische Verfahren
zum Nachweis der der Integrität zum Einsatz, die beispielsweise
Prüfsummen zum Nachweis der Integrität nutzen (MD5 sums). Im SouvAP
sollen alle genannten Sicherheitsfunktionen zur Anwendung kommen.

##### Authentikationsprotokoll-Standards für den SouvAP

**Kerberos** ist ein Netzwerk-Authentifizierungsprotokoll, das es
Benutzern ermöglicht, sichere Verbindungen zu Servern herzustellen, ohne
ihre Anmeldeinformationen offenzulegen.

**OAuth** (Open Authorization) ist ein Protokoll zur Autorisierung von
(Web-) Anwendungen für Zugriffe auf geschützte Daten. OAuth liefert
einen Mechanismus zur sicheren Autorisierung für den Zugriff auf eigene
Daten und Dienste seitens Serversystemen und Services, die mit anderen
Identitätskennungen als der des Anwenders arbeiten. Der Anwender muss
dazu seine Anmeldedaten nicht preisgeben.

**OpenID Connect** ist eine auf OAuth 2.0 aufbauende Authentifizierungs-
und Autorisierungs-Plattform. OpenID Connect ist heute der meist
verbreitete Standard zur Authentifizierung von Benutzern in
Webanwendungen und APIs.

**SAML (**Security Assertion Markup Language) ist ein XML-basiertes
Protokoll, das bei der föderierten Authentifizierung von Benutzern
verwendet wird. Anwendungen können darin enthaltene Attribute mit
anwendungsspezifischen Zugriffrechteschemen verknüpfen.

##### Konfigurations- und Anwendungssteuerung

Vernetzte System dienen häufig als diensterbringende Automaten, die
Services unter Auswertung vordefinierter Anwendungsprotokolle erbringen.
Typische Beispiele bilden etwa Mail-Services, Datei-Services oder
Streaming-Services. Derartige Anwendungsprotokolle sind universell, da
sie auf praktisch jeder Plattform einsetzbar sind und ihre Auswertung
auch nicht an eine konkrete Programmiersprache gebunden ist.

In vernetzten Systemlandschaften sind außerdem Verfahren der
Fernadministration gebräuchlich, die Grundkonfigurationen ausrollen oder
vorhandene Konfigurationen verändern. Ein zyklischer Abgleich der Soll-
mit der Ist-Konfiguration ermöglicht das Durchsetzen von
Soll-Konfigurationen, um das System in einen bekannten
Soll-Konfiguration zu setzen

##### Konfigurations- und Anwendungsprotokoll-Standards für den SouvAP

**CalDAV** (Calendaring Extensions to WebDAV) ist ein Protokoll zur
Verwaltung von Kalenderdaten. WebOffice-Anwendungen nutzen CalDAV zum
Synchronisieren und Teilen von Kalenderdaten.

**CardDAV** vCard Extensions to WebDAV (CardDAV) ist ein
Client-Server-Protokoll für Adressbücher, um eine Speicherung und
Freigabe von Kontaktdaten auf einem Server zu ermöglichen.

**CMIS** Content Management Interoperability Services (CMIS) ist ein
offener und herstellerunabhängiger Standard zur Anbindung von
Content-Management-Systemen.

**iCal** iCalendar (iCal) ist ein Datenformat zum Austausch von
Kalenderinhalten, welches in RFC 5545 standardisiert ist.

**IMAP** (Internet Message Access Protocol) verbindet Endpunkte mit
Mail-Servern. Das Protokoll enthält eine textbasierte Abfragesprache,
mit der im Endpunkt platzierte Mail User Agents vom Mail-Server
Nachrichten abrufen können. Die im Mail-Server verwahrte (persönliche)
Mailbox wird durch IMAP quasi als eine Art Dateisystem präsentiert.

**RTSP** (Real-Time Streaming Protocol) wurde von der Internet
Engineering Task Force als offener Standard für die kontinuierliche
Übertragung binärer Daten über IP-Netzwerke entwickelt. RTSP wird
vorrangig für die Aussendung von audiovisuellen Daten seitens eines
Servers genutzt, ist aber auch zur Softwareverteilung einsetzbar. RTSP
ist für verbindungsorientierte (TCP/IP) und für verbindungslose (UDP)
Kommunikation spezifiziert.

**SCIM** (System for Cross-domain Identity Management) wurde für die
Verwaltung und Synchronisation von Identitätsdaten und
Zugriffsberechtigungen für Benutzer und Ressourcen (z.B. Anwendungen,
Dateien, E-Mail-Postfächer) über verschiedene Systeme hinweg entwickelt.
SCIM speichert Identitätsinformationen wie Benutzernamen,
E-Mail-Adressen, Telefonnummern und Gruppenmitgliedschaften in einer
zentralen Identitätsquelle. Änderungen an diesen Identitätsinformationen
synchronisiert SCIM automatisch auf alle SCIM-Endpunkte, die dieser
Identitätsquelle zugeordnet sind.

**SIP** Session Initiation Protocol (SIP) ist ein Netzprotokoll zum
Aufbau, zur Steuerung und zum Abbau einer Kommunikationssitzung zwischen
zwei und mehr Teilnehmern.

**Sieve** Sieve ist eine domänenspezifische Sprache, die zum
Konfigurieren von Mailfiltern auf Mailservern durch Benutzer konzipiert
wurde.

**SMTP** (Simple Mail Transfer Protocol) ist das Standardprotokoll für
die Übertragung von E-Mails über das Internet. WebOffice-Anwendungen
verwenden SMTP zum Senden und Empfangen E-Mails.

**SPML** (Service Provisioning Markup Language) dient der Verwaltung von
Identitätsinformationen und Zugriffsberechtigungen über verschiedene
Systeme hinweg. Außerdem ist es mit SPML möglich, Provisioning-Prozesse
automatisiert durchzuführen, z.B. die Einrichtung, Aktualisierung und
Löschung von Benutzerkonten und -rechten in verschiedenen Systemen und
Anwendungen. SPML basiert auf XML und verwendet standardisierte Befehle
und Schnittstellen zur Automatisierung von Identitäts- und
Zugriffsverwaltungsvorgängen.

**WebDAV** (Web-based Distributed Authoring and Versioning) wird zur
Verwaltung von Dateien über das Internet verwendet. WebDAV ermöglicht
es, Dateien über das Internet zu bearbeiten und zu teilen.

**WebRTC** (Web Real-Time Communication) definiert
Kommunikationsprotokolle und Programmierschnittstellen, die den
Datenaustausch zwischen beliebigen vernetzten Endpunkten in Echtzeit
ermöglichen. Auf dieser Grundlage gelingt die Integration
anspruchsvoller Multimedia-Anwendungen im Web-Browser, z.B.
Videokonferenzen, Dateitransfer bzw. Datenübertragung, Chat und
Desktop-Sharing.

##### Protokoll-Standards der öffentlichen Verwaltungen

Der IT-Planungsrat hat die besondere Bedeutung von Standards in der
öffentlichen Verwaltung über ihre Relevanz für IT-Interoperabilität und
Sicherheit betont. In enger Anlehnung an die Digitalisierungsstrategie
des IT-Planungsrats wurde deshalb eine Standardisierungsstrategie
entwickelt und diese wird aktuell von der Föderativen IT-Kooperation
FITKO als Standardisierungsagenda gepflegt und vorangetrieben.

Tatsächlich sind die Bemühungen um Standardisierungen in der deutschen
IT der öffentlichen Verwaltung schon mehr als eine Dekade gereift. Die
bereits langjährig erprobten XÖV-Standards z.B. definieren
Spezifikationen zum Datenaustausch innerhalb der öffentlichen Verwaltung
und zwischen der öffentlichen Verwaltung und ihren Kunden.

Der länderübergreifende Standard European Interoperability Framework EIF
adressiert das Ziel, über den direkten Datenaustausch zwischen
EU-Mitgliedsstaaten die Zusammenarbeit der öffentlichen Verwaltungen auf
europäischer Ebene zu ermöglichen. Diese Entwicklungen gehen auf die
E-Government-Aktionspläne zurück, die seit 2011 begleitend zur Digitalen
Agenda für Europa entwickelt wurden.

Der SouvAP wird zunächst als Arbeitsplatz für täglich anfallende
Aufgaben der Kommunikation und der Zusammenarbeit zwischen Beschäftigten
der öffentlichen Verwaltung ausgestaltet. Eine Überführung von
Verfahrensdaten zum Informationsaustausch über den SouvAP bleibt künftig
möglich. Sollten im späteren Verlauf Anwendungsfälle sichtbar werden,
die dies erfordern, dann ist dies als Aufgabe zur Integration in den
SouvAP aufzufassen und die Datenanbindung ist über ein API-Gateway zu
filtern.

#### Gesamtmodell

Das Gesamtmodell koppelt Module des SouvAP. Dies wird im nachfolgenden
Abschnitt <span class="underline">6.1.3</span> <span
class="underline">Funktionsmodell</span> im Detail beschriebenen. Der
Abschnitt Gesamtmodell liefert eine integrierende Sicht auf die
Anwendungs- und Datenarchitektur, die den Geschäftsanforderungen und
-zielen des SouvAP gerecht werden.

Das Betriebskonzept zum SouvAP folgt im Wesentlichen dem Schema, dass
ein Anwender zunächst das Portal zum SouvAP kontaktiert. Nach
erfolgreicher Authentikation und Autorisierung verweist das Portal auf
Module des SouvAP.

Auf der Infrastruktur operieren die Module in Containern gekapselt. Die
Infrastruktur selbst ist eine DVS-konforme Cloud Plattform mit
Kubernetes-Laufzeitumgebung. Die Module des SouvAP operieren darauf im
Multiuser-Modus.

Diese SouvAP-Module müssen mit zunehmender Anzahl der Anwender
horizontal skalieren. Dazu sind Steuerungsprozesse benötigt, die
Container instanziieren bzw. entfernen. Dabei sind Datenzugriffe
entsprechend definierter Zugriffsrechteschemen umzusetzen.

Den Datenaustausch zwischen Portal und SouvAP-Modulen steuern
API-Gateways. Diese sollen einerseit den sicheren und effizienten
Datenaustausch von und zu einzelnen Modulen regeln, andererseits den
Ressourcenbedarf aufgrund schwankender Anforderungen erkennen und darauf
zugeschnittene Skalierungsfunktionen steuern.

Übergreifend soll ein API-Management die API-Gateways verwalten,
überwachen, darin anzuwendende Richtlinien durchsetzen und ihre
Aktivitäten bei Bedarf protokollieren. Eine übergreifende
Berichtsfunktion soll Sicherheitsvorfälle protokollieren und
Nutzungsstatistiken aufbereiten können.

Innerhalb des Gesamtsystems werden API-Gateways mit zwei funktionellen
Schwerpunkten eingezogen. Edge-API-Gateways sind an der Schnittstelle
zwischen internen und externen Netzwerken platziert. Sie fungieren als
Übergangspunkt auf die Schnittstellen der SouvAP-Module. Ihre Aufgben
umfassen z.B. die Steuerung des Datenverkehrs, die Authentifizierung und
Autorisierung von Benutzern und die Transformation von Datenformaten.

Micro-Gateways sind in Abgrenzung zu den Edge-Gateways darauf
ausgerichtet, Modul-spezifische Microservices oder APIs zu kapseln und
zu schützen. Sie steuern die Kommunikation sowie etwa erforderliche
Protokoll- bzw. Formatwandlungen zwischen den einzelnen SouvAP-Services.
Außerdem ermöglichen sie eine granulare Sicherheitssteuerung über den
Datenverkehr. Ihr Betrieb ist "leichtgewichtig".

Um komplexe Geschäftsprozesse automatisieren zu können, ist ein
Orchestrierungstool erforderlich, das die Integration verschiedener
Anwendungen und Dienste verantwortet und eine nahtlose Zusammenarbeit
gewährleistet. Darüber hinaus dient eine Service Registry als zentraler
Ort, an dem alle verfügbaren IT-Services registriert und abgefragt
werden können. Durch die Integration von Service-Management-Tools können
die Dienste verwaltet und optimiert werden.

![](media/file22.png)

Abbildung : SouvAP

Insgesamt stuft die Architektur eine gut durchdachte Integration der
Komponenten als entscheidend dafür ein, dass der Aufbau einer effektiven
und sicheren Daten- und Informationsaustauschplattform gelingt. Die
Integration der Komponenten in API-Gateways, API-Management,
Orchestrierungstools, Service-Registry und Service-Management-Tools soll
die Plattform-Architektur optimieren, die Flexibilität und die
Skalierbarkeit des SouvAP maximieren und so in Summe auf den Nutzen für
die Anwender fokussieren.

#### SouvAP-Portal

Das SouvAP-Portal realisiert den Zugangspunkt zum SouvAP. Es bringt
Anwendern und Administratoren die jeweils auf ihre Aufgaben und
Nutzungsrechte zugeschnittenen Komponenten des SouvAP in den Zugriff.
Initial präsentiert das SouvAP-Portal dem Anwender eine grafische
Oberfläche. Dort sind ein Authentikationspunkt und ein Self-Service
bereitgestellt. Letzterer bietet dem Anwender Funktionen zur Pflege
persönlicher Kontoeinstellungen und zur gesicherten Wiederherstellung
verlorengegangener oder deaktivierter Zugangskennungen an.

Im Zuge der Anmeldung liefert das SouvAP-Portal an den Anwender ein
Benutzer- und Endpunkt-individuelles Sicherheits-Token aus, mit dem der
Anwender fortan transparenten Zugriff auf alle Anwendungen und Daten
entsprechend seiner Autorisierung im Gesamtsystem erhält. Während des
Dialogbetriebs mit dem SouvAP bildet das SouvAP-Portal den Übergabepunkt
jedweder Kommunikation zwischen Benutzer und SouvAP. Insbesondere
präsentiert das SouvAP-Portal über diesen Kanal intern generierte
Nachrichten, die z.B. über eingehende E-Mails informieren.

Auch technische und fachliche SouvAP-Administratoren sollen das
SouvAP-Portal als Zugangspunkt nutzen. Der administrative Zugang ist
über eine besonders starke Authentifizierungsmethode zu gewähren (2FA).
Administrative Werkzeuge sind in einem isolierten Bereich des
Gesamtsystems zu platzieren. Ihr Leistungsumfang soll mindestens die
Verwaltung von Benutzerkonten, Zugriffsberechtigungen und allgemeinen
Sicherheitseinstellungen abdecken, Policy Verwaltung und Zugriffe auf
Informationen zum Gesundheitszustand und zur Auslastung des
Gesamtsystems gewähren.

Die Datenübertragung zwischen Anwender und SouvAP-Portal sowie zwischen
Administrator und SouvAP-Portal ist in jedem Fall mittels HTTPS und
SSL/TLS zu verschlüsseln. Die Verwendung sicherer Passwörter und der
regelmäßige Passwortwechsel sind in einer Passwortrichtlinie zu
manifestieren. Ein daraus abgeleitetes Regelwerk ist im Gesamtsystem
technisch durchzusetzen. Sicherheitsrelevante Updates sind zeitnah
einzuspielen. Zu den Konfigurationsdaten sind zyklisch Sicherheitskopien
zu erstellen. Sicherungszyklen, Anzahl der Backups und Vorhaltezeiten
sind in einem Betriebsmodell festzuschreiben. Es wird empfohlen, jedwede
Kommunikation standardmäßig zu verschlüsseln.

#### API-Gateway

Das API-Gateway nimmt als Schnittstelle zwischen den Modulen und den
Endbenutzern eine zentrale Rolle bezüglich Kommunikations- und
Informationssicherheit ein. Es nimmt zentralisiert an einem
Durchsetzungspunkt Anfragen von Endbenutzern und anderen Modulen
entgegen. Dort veranlasst es unter Anwendung von Richtlinien die
Einhaltung von Sicherheitsanforderungen. Auf Basis dieser
Schutzmaßnahmen schaltet es System-zu-System-Interaktionen zwischen den
Komponenten und den Backend-Systemen des SouvAP frei.

Das API-Gateway des SouvAP ist mit Fokus auf den sicheren Austausch von
Daten und Diensten über APIs (Application Programming Interfaces)
konzipiert. Ein API-Gateway bietet Funktionen wie Authentifizierung,
Autorisierung, API-Management, Rate-Limiting und Schutz vor Angriffen.
Ein API-Gateway ermöglicht auch die Verwaltung von APIs, einschließlich
Versionierung, Dokumentation und Überwachung von API-Metriken.

Die Skalierungsfähigkeit eines API-Gateways ist entscheidend für die
Bewältigung von Lastspitzen und die Erweiterung der Infrastruktur. Ein
skalierbares API-Gateway kann automatisch zusätzliche Ressourcen
bereitstellen, um den steigenden Datenverkehr zu bewältigen und die
Performance aufrechtzuerhalten. Self-validating Tokens (z.B. JSON Web
Tokens JWT) ermöglichen es, die Identität von Benutzern ohne zusätzliche
Anfragen an den Authentifizierungsserver zu überprüfen. Dadurch wird die
Netzwerklast reduziert und umgekehrt die Performance erhöht.

Um die Anzahl der Anfragen zu begrenzen, die ein einzelner Benutzer oder
eine Gruppe von Benutzern innerhalb eines bestimmten Zeitraums stellen
kann, dienen lokalisierte Rate-Limits. Dies schützt die API vor
missbräuchlicher Nutzung und hilft, Verfügbarkeit und Stabilität der
Dienste aufrechtzuerhalten. Eine Unveränderlichkeit hilft dabei, die
Konsistenz der Daten und Prozesse zu gewährleisten, insbesondere in
verteilten Systemen. Anfragen an das System sollen keine Informationen
über den Zustand einer Anfrage oder eines Benutzers speichern. Dieses
begünstigt die Skalierbarkeit und die Fehlertoleranz, da jeder Server
die Anfrage unabhängig von anderen Servern verarbeiten kann.

Um Zugriffsrechte und Zugriffsbeschränkungen für bestimmte Ressourcen zu
definieren sollen daher Per-Ressource Endpoints eingesetzt werden. Dies
erhöht die Flexibilität und die Sicherheit des API-Gateways, indem es
fein abgestimmte Zugriffskontrolle für verschiedene Benutzer und Systeme
anwenden kann. Sowohl der Client als auch der Server können gegenseitig
ihre Identität durch digitale Zertifikate bestätigen. Dies stellt eine
zusätzliche Sicherheitsebene für die Kommunikation zwischen API-Gateway
und den angebundenen Diensten bereit. Das API Gateway ist damit in der
Lage, eingehende und ausgehende Nachrichten auf Basis von vordefinierten
Schemata zu validieren. Dies gewährleistet die Datenintegrität und
reduziert das Risiko von Fehlern oder Sicherheitsvorfällen.

Das API-Gateway des SouvAP bietet auf dieser Basis eine robuste,
resiliente und mit Circuit Breaker Mechanismen eine sichere Plattform
für den Austausch von Daten und Diensten über APIs. Es ermöglicht eine
nahtlose Integration zwischen Backend-Systemen der OSS-Hersteller und
gewährleistet gleichzeitig die Einhaltung von Sicherheitsrichtlinien und
die Anwendung darauf zugeschnittener Schutzmaßnahmen.

#### EdgeGateWay

Das Edge-Gateway im Kontext des SouvAP ist ein zusätzliches
Sicherheitselement, das vor den MicroGateways platziert wird. Es stellt
die erste Verteidigungslinie dar und adressiert damit den geschützten
Datenaustausch zwischen den Endbenutzern und den SouvAP-Modulen. Das
Edge-Gateway übernimmt z.B. die Aufgaben Authentifizierung,
Autorisierung, API-Verwaltung, Rate-Limiting und elementaren Schutz vor
Angriffen.

Der Einsatz von Self-validating Token wie JSON Web Token (JWT) reduziert
die Netzwerklast und verbessert die Performance. Darüber hinaus wollen
lokalisierte Rate-Limits gewährleisten, dass die API vor
missbräuchlicher Nutzung geschützt ist und die Dienste verfügbar und
stabil bleiben.

Per-Ressource Endpoints sollen eine fein abgestimmte Zugriffskontrolle
ermöglichen und die Flexibilität und Sicherheit des Edge-Gateways
erhöhen. Die Validierung eingehender und ausgehender Nachrichten auf
vordefinierte Schemata soll die Datenintegrität stabilisieren und das
Risiko von Fehlern oder Sicherheitsvorfällen reduzieren. Das
Edge-Gateway des SouvAP bietet somit eine robuste und sichere Plattform
für den Austausch von Daten und Diensten über APIs und verfolgt dabei
gleichzeitig die Einhaltung von Sicherheitsrichtlinien.

Ein Edge-Gateway muss somit immer als zusätzliche Sicherheitsschicht
dienen, indem es den Datenverkehr am Rand des Netzwerks überwacht und
potenziell schädliche Aktivitäten abwehrt. MicroGateways können dann für
die granulare Steuerung des Datenverkehrs und für die Verwaltung von
APIs innerhalb eines Microservice-Ökosystems eingesetzt werden.

#### API-MicroGateway

Ein API-MicroGateway ist eine kleine Softwarekomponenten, die als
Schutzschicht zwischen einer API und einer Anwendung dient.
API-MicroGateways kontrollieren und schützen den Zugriff auf die API.
Insbesondere In verteilten Systemen mit mehreren APIs von verschiedenen
Anwendungen kommt ihre Schutzwirkung besonders stark zur Geltung.

MicroGateways bieten zahlreiche Vorteile, die dazu beitragen, die
Sicherheit und Leistung von APIs zu verbessern. Durch den Einsatz von
MicroGateways als zusätzliche Sicherheitsschicht können Anwendungen den
Zugriff auf die API und die Daten, die sie bereitstellt, besser
kontrollieren und schützen. MicroGateways ermöglichen es,
Sicherheitsfunktionen wie Authentifizierung, Autorisierung und
Verschlüsselung bereits auf der Anwendungsebene zu implementieren.

Ein weiterer Vorteil von MicroGateways ist ihre Skalierbarkeit. Da
MicroGateways in der Regel deutlich kleiner und damit
„leichtgewichtiger“ sind als normale API Gateways, können sie effizient
horizontal in verteilten Systemen skalieren. Dies ermöglicht es, die
durch wenige API-Gateways bereitgestellten Services von verschiedenen
Anwendungen aus effizienter zu nutzen.

![](media/file23.png)

Abbildung 24: Edge-to-Endpoint Security für Microservices

#### Service Mediation

Dieser Abschnitt beschreibt Fähigkeiten der Steuerungsschicht zum
SouvAP, die auf die Dienstanforderungen angewendet werden können, bevor
der Anbieterdienst aufgerufen wird. Die Fähigkeiten umfassen

-   Transportprotokollumwandlung,

-   Datenformatkonvertierung,

-   Nachrichtenaufteilung und -zusammenführung sowie

-   Dienstaufruf.

Diese werden auch verwendet, um sich mit Legacy-Systemen zu integrieren.

Die Service-Mediationsplattform positioniert sich als Schicht zwischen
den verschiedenen Diensten. Dort wendet sie ihr Leistungsspektrum zur
Transformation von Inhalten, Umschaltung von Transportprotokolltypen,
Routing, Quality of Service, Dienstkomposition und asynchroner
Kommunikation an. Die Plattform unterstützt auch die Transformation
zwischen verschiedenen Authentifizierungs- und Autorisierungsmethoden.
Weitere Leistungsmerkmale dieser Komponente schließen Monitoring- und
Reportfunktionen ein mit dem Ziel der Überwachung und Verbesserung der
Leistung der Dienste. In Summe adressiert sie damit die flexible und
gleichzeitig optimale Integration von Diensten und fördert ihre
Interoperabilität.

#### Service Orchestrierung

Die Service-Orchestrierung, also die Kopplung aller Komponenten an
Gateways und Microservices, wird im Gesamtsystem zentral von einer
separaten Orchestrierungskomponente wahrgenommen. In diesem Modell
arbeitet jeder Microservice gemäß den Anweisungen, die er von der
Orchestrierungskomponente erhält. Die Orchestrierungskomponente
verantwortet somit die Steuerung des Datenflusses, die Koordination von
Aktivitäten und die Verwaltung von Fehlern.

![](media/file24.png)

Abbildung : Geschäftsprozesse

#### Service Registry

Die Service Registry innerhalb des Gesamtsystems zum SouvAP verwaltet
zentral ein Verzeichnis aller im Systemverbund verfügbaren Services.
Dort fungiert sie sozusagen als „Quelle der Wahrheit“ für alle Services
im System. Jeder Service muss sich bei der Service Registry
registrieren. Erst danach kann er im Gesamtsystem gefunden und im
Servicegeflecht genutzt werden.

Die Informationen innerhalb der Service Registry sind im Wesentlichen
Adressen (Netzwerkkennungen) und weitere Metadaten zu den registrierten
Services. Da diese Daten zentral verwaltet werden, profitiert das
Gesamtsystem von schneller Integration neuer Services, ohne dass dabei
andere, bereits registrierte Services davon beeinträchtigt werden. Somit
trägt die Service Registry zusätzlich zur Flexibilität und zur
Skalierbarkeit des Gesamtsystems bei. Aufgabenstellungen der
Autorisierung übernehmen die API-Gateways.

Mit diesen Fähigkeiten kann die Service Registry außerdem die
Ausfallsicherheit des Gesamtsystems steigern, indem sie automatisch auf
Änderungen in der Verfügbarkeit von Services reagiert und den
Datenverkehr auf alternative Services umleitet.

![](media/file25.png)

Abbildung : Service Registry

Eine weitere Fähigkeit der Service Registry ist die Verwaltung von
Richtlinien, die die Anforderungen festlegen, die erfüllt sein müssen,
damit ein Webdienst ausgeführt werden darf. Darüber hinaus bietet die
Service Registry auch die Möglichkeit, Dienstendpunkte mithilfe von
Proxydiensten zu virtualisieren.

Ein weiterer Aspekt ist die Integration von API-Katalogen in die Service
Registry. Dadurch wird eine zentrale Stelle zur Verwaltung und
Überwachung von APIs bereitgestellt und Entwicklern die Möglichkeit
gegeben, schnell und einfach auf APIs zuzugreifen und sie zu nutzen. Der
Einsatz von API-Katalogen kann die Nutzung von APIs verbessern, die
Sicherheit erhöhen und die Einhaltung von Richtlinien durchsetzen.

##### API-Management

Um eine erfolgreiche Implementierung und Verwaltung der APIs im
souveränen Arbeitsplatz zu erreichen, ist die Verwendung einer
API-Management-Plattform unerlässlich. Durch die Integration einer
API-Management-Plattform können die APIs der verschiedenen Module
dokumentiert, veröffentlicht und verwaltet werden. Eine solche Plattform
bietet auch Sicherheits- und Überwachungsfunktionen sowie Tools zur
Analyse und zum Management des gesamten API-Lebenszyklus.

Zusätzlich zu den Funktionen Veröffentlichung, Versionierung und
Dokumentation liefert eine API-Management-Plattform auch Fähigkeiten zur
Monetarisierung der APIs und zur nahtlosen Integration eines
Self-Service-Portals für Endbenutzer. Die API-Management-Plattform
unterstützt auch die Entwickler-Community mittels Funktionen zur
umfassenden API-Analyse und Maßnahmen zur Durchsetzung von Sicherheit in
der API-Nutzung.

Die Implementierung einer API-Management-Plattform ist daher ein
weiterer Bestandteil der Architektur des souveränen Arbeitsplatzes, um
eine erfolgreiche Integration, Verwaltung und Skalierbarkeit der APIs zu
gewährleisten.

API-Management ist somit ein essentieller Bestandteil der
SouvAP-Softwarearchitektur. Dies kommt insbesondere durch die Verwendung
von Microservices zum Ausdruck. Dabei übernehmen API-Management-Tools im
SouvAP eine Vielzahl von Aufgaben, die im Zusammenhang mit dem Betrieb
und der Wartung von APIs stehen.

In diesem Zusammenhang spielt die Verwaltung von API-Gateways und der
ServiceRegistry eine zentrale Rolle: API-Gateways sind dafür
verantwortlich, die eingehenden API-Anrufe zu koordinieren und sie an
die richtigen Dienste weiterzuleiten und die ServiceRegistry liefert den
zentralen Speicherort für Informationen über alle verfügbaren Services.

In Summe überwacht das API-Management im SouvAP den Zustand aller APIs,
setzt Sicherheitsrichtlinien durch, implementiert Traffic-Steuerung und
verbessert die Leistung ihrer APIs. Dies trägt zur Steigerung der
Effizienz und auch zur Verbesserung der Servicequalität bei.

##### Verwaltung der Servicenutzung

Die Verwaltung der Servicenutzung adressiert das Ziel, dass Anwender die
Informationsdienste des SouvAP effizient nutzen können und
Service-Provider transparente Werkzeuge zur Service-Optimierung und
-Abrechnung in den Zugriff bekommen. Das Leistungsspektrum umfasst
exemplarisch das Services Publishing, die Abonnementverwaltung,
Katalogverwaltung, Nutzungsprüfung und Nutzungsanalyse. Die
Self-Service-Ebene ermöglicht den Nutzern eine einfache und effiziente
Nutzung von Informationsdiensten und sie sorgt für Einhaltung von
Richtlinien und Sicherheit.

##### Service Management and Monitoring

Die Dienstverwaltung und -überwachung ist Bestandteil des SouvAPs. Sie
umfasst unter anderem die Funktionen Ereignisverwaltung sowie
Protokollierung und Überwachung.

Die Systemüberwachung bietet Überwachungsfunktionen, um sicherzustellen,
dass das System reibungslos und fehlerfrei läuft. Die Dienstüberwachung
bietet Fähigkeitsüberwachungsdienste und stellt Management- und
Governance-Methoden bereit, um die Kontrolle über die bereitgestellten
Dienste zu erlangen und flexibel in Bezug auf die Dienstbereitstellung
und -interaktionen zu sein, um die Geschäftsanforderungen zu erfüllen.

Die Prüfung/Protokollierungsfunktionen ermöglichen es den
Administratoren, die Nutzung von Diensten nachzuverfolgen und
Fehlverhalten zu identifizieren. Es ist erforderlich, dass alle
Aktivitäten, die auf der Plattform ausgeführt werden, sorgfältig
überwacht werden, um sicherzustellen, dass die Plattform ordnungsgemäß
funktioniert und die Geschäftsanforderungen erfüllt werden.

Ein weiterer Aspekt ist das Data Dictionary-System, das ein System zum
Speichern, Veröffentlichen und Pflegen von Datenaustauschspezifikationen
bereitstellt. Dies hilft Betreibern und Administratoren, die
Informationen und Daten im System besser zu verstehen und zu verwalten.
Durch die Verwendung eines Data Dictionaries können die Benutzer
sicherstellen, dass alle Beteiligten im System die gleichen Daten
verwenden und dass es nicht zu Verwirrungen oder Missverständnissen
kommt. Die Dienstverwaltung und -überwachung trägt als Bestandteil des
Informationsaustauschsystems dazu bei, dass das System effektiv und
effizient funktioniert.

##### Fachverfahrens-Integration/-Anbindung

Fachverfahren können über Konnektoren angebunden werden. Integration
bezieht sich auf das Zusammenführen und Kombinieren von Systemen oder
Anwendungen, um eine kohärente und einheitliche Funktionsverkettung zu
erreichen. Anbindung ermöglicht die Verbindung und Kommunikation
zwischen zwei oder mehr Systemen oder Anwendungen, ohne dass sie
notwendigerweise vollständig integriert sind.

Fachverfahrens- Integrationen bzw. -Anbindungen sollen im SouvAP mit
einem MicroGateway gekoppelt werden. Dabei übernimmt das MicroGateway
die Aufgabe, die erforderlichen Schnittstellen und Protokolle zu
bedienen, die von den Fachverfahren verwendet werden.

Im Falle von verschiedenen Sicherheitsstufen der Kommunizierenden
Netzwerke muss ein EdgGateway dem MicroGateway vorgeschaltet werden

In der Regel verwenden Fachverfahren spezielle Protokolle oder
Standards, die nicht von jeder API oder jedem Gateway unterstützt
werden. Um diese Integration zu erleichtern, kann ein MicroGateway sich
etwa benötigter Adapter bedienen, die speziell für die Integration mit
bestimmten Fachverfahren entwickelt wurden. Diese Adapter ermöglichen es
dem MicroGateway, die spezifischen Protokolle und Standards der
Fachverfahren zu verstehen und zu verarbeiten.

Das MicroGateway soll stets in der Lage sein, die
Sicherheitsanforderungen der Fachverfahren zu erfüllen. Hierzu muss das
MicroGateway alle benötigten Sicherheitsfunktionen wie z.B.
Authentifizierung, Autorisierung und Verschlüsselung bereitstellen.

Konnektoren ermöglichen es, die Verbindung zwischen zwei Systemen oder
Komponenten herzustellen und aufrechtzuerhalten, die aufeinander
abgestimmt sind. Im Gegensatz zum Adapter muss der Konnektor keine
Übersetzung oder Konvertierung von Daten durchführen, da die verbundenen
Systeme bereits aufeinander abgestimmt sind.

##### Austauschbarkeit der OSS

Der SouvAP bildet eine herstellerunabhängige, generische
Softwareplattform. Er soll zur Steuerung oder Vernetzung aller Arten von
OSS-Modulen eingesetzt werden und in deren fachlichen
Kernfunktionalitäten Austauschbarkeit ermöglichen. Für die erfolgreiche
Vernetzung ist keine Schnittstellenänderung auf Seite der OSS-Module
benötigt. Die Anforderungen an eine nahtlose Integration in das
Gesamtsystem sollen Adapter befriedigen, wenn der OSS-Hersteller diese
Anfoderung befriedigt. Die Zugänge zu den Adaptern können als
zentralisierte, versionierte Schnittstellen verstanden werden, auf denen
die einzelnen OSS-Hersteller den Zugriff auf bestimmte fachliche
Funktionen vermitteln oder von außen den abstrakten Zugriff auf
bestimmte Dienste ermöglichen.

Eine herausragende Fähigkeit des SouvAP soll darin bestehen,
kontrolliert und dynamisch Service-Anwendungen (sogenannte Module)
möglichst zur Laufzeit einspielen zu können und auch zu aktualisieren
und wieder zu entfernen. Der Gesamtsystem zum SouvAP gibt damit die
Möglichkeit, verschiedene, weitgehend unabhängige und modulare
Anwendungen parallel in derselben Instanz laufen zu lassen und diese
während des gesamten Lebenszyklus der Anwendung zu administrieren und zu
aktualisieren. Dabei steht ein intelligentes Versionsmanagement zur
Verfügung und es werden Abhängigkeiten zwischen OSS-Modulen aufgelöst.

![](media/file26.png)

Abbildung : OSS-Module Austauschbarkeit und OSS-Adapter

Um die Integration zu standardisieren, sind Adapter für das MicroGateway
zu entwickeln, die speziell für die Integration mit der OSS konzipiert
sind. Solche Adapter ermöglichen es dem MicroGateway, die spezifischen
Schnittstellen und Protokolle der OSS in breiter Vielfalt zu verstehen
und zu verarbeiten. Dabei ist darauf zu achten, dass das MicroGateway
die Sicherheitsanforderungen der OSS erfüllt. Hierzu sind im
MicroGateway verschiedene Sicherheitsfunktionen wie Authentifizierung,
Autorisierung und Verschlüsselung bereitzustellen.

Adapter ermöglichen somit die Konvertierung spezifischer
Kommunikationsprotokolle und Formate der externen Systeme in ein
einheitliches und konsistentes internes Format, das vom
SouvAP-Gesamtsystem interpretiert und verarbeitet werden kann. Dieser
Ansatz erlaubt es, Fachverfahren agil und unkompliziert zu integrieren,
ohne umfassende Änderungen an der bestehenden Architektur vornehmen zu
müssen.

![](media/file27.png)

Abbildung : Austauschbarkeit durch OSS-Adapter

Weitere Vorteile im Einsatz von Adaptern kommen durch die Isolierung von
Änderungen und Updates der OSS-Module zum Ausdruck. Dieser Sachverhalt
minimiert die Auswirkungen auf das Gesamtsystem und führt implizit zu
verbesserter Stabilität und Wartbarkeit aller Komponenten des SouvAP bei
gleichzeitigem Abfedern der Risiken Fehlerquellen, Versionskonflikte und
Inkompatibilitäten.

### Funktionsmodell

Das Funktionsmodell katalogisiert Anforderungen zu den Modulen gemäß
<span class="underline">6.1.2.1</span> <span
class="underline">Komponentenmodel</span>. Die bisher bekannten
Anforderungen sind in diesem Dokument als Kurztext aufgeführt. Details
zu den Anforderungen sind im Anforderungsmanagement begleitend zur
Architekturarbeit zu konkretisieren.

#### Portal

Das Portal ist ein Zentrales Anlaufstelle für verschiedentse Endbenutzer
aktionen mit der Übersicht über Personenbezogenen Informationen
visalisiert in einem einheitlichen Dashboard. Die folgende Tabelle nennt
die Anforderungen an das Portal und deren Verbindlichkeiten.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFPO-01</td>
<td>Login mit Benutzername und Kennwort</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFPO-02</td>
<td>2 Factor Authentification</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFPO-03</td>
<td>SelfService für Datenänderungen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFPO-04</td>
<td>SelfService Passwort Reset</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFPO-05</td>
<td>OSS-Modul Auflistung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFPO-06</td>
<td>DashBoard für Benutzer</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFPO-07</td>
<td>DashBoard für Fachliche Administratoren</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFPO-08</td>
<td>Zentralisierte Informationsdarstellung</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Cloud-Dateiablage

Die Cloud-Dateiablage ist der zentrale Datenraum, der alle Dokumente,
Bild- und Video-Medien sowie allgemein strukturierte und unstrukturierte
Daten in einem hierarchischen Dateisystem verwahrt. Die folgende Tabelle
nennt die Anforderungen an die Cloud-Dateiablage und deren
Verbindlichkeiten.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFCD-01</td>
<td>Dateisystem-Navigation mit Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-02</td>
<td>Identitätsverwaltung außerhalb der Cloud-Dateiablage</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-03</td>
<td>IAM-gestützte Zugriffsrechteverwaltung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-04</td>
<td>Zugriffsrechte: Personell/Rolle lesen/schreiben/löschen/ausführen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-05</td>
<td>Single Sign-on auf Cloud-Dateiablagedienst</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-06</td>
<td>Schutz von Daten mit Zweifaktor-Authentifizierung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-07</td>
<td>Versionierung von Dateien</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AFCD-08</td>
<td>Verschlüsselung von Dateien</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-10</td>
<td>Kontextbindung an Anwendung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-11</td>
<td>Teilungsreichweite Personen, Gruppen, Maschinen, Serviceprovider</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-12</td>
<td>Konfigurierbarer Malwareschutz</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-13</td>
<td>Malwareschutz getrennt aktivierbar für Lese- und Schreibzugriffe</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AFCD-14</td>
<td>Storage-Quotierung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-15</td>
<td>Log-Erstellung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFCD-16</td>
<td>Nutzungs-Berichterstattung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFCD-17</td>
<td>Volltextsuche in Ordner / Ordner-Hierarchie</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Web-Office

Das Web-Office im Kontext des SouvAP besteht aus einem oder mehreren
Softwareprodukten, die den Leistungsumfang typischer Softwareanwendungen
für Textverarbeitung, Tabellenkalkulation und Präsentationsgrafik
abbilden. Die folgende Tabelle nennt die übergreifenden Anforderungen an
das Web-Office.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFWOU-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOU-02</td>
<td>Benutzeroberfläche auf Landessprache anpassbar</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOU-03</td>
<td>Datei-Ein-/Ausgabe auf Cloud-Dateiablage</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOU-04</td>
<td>Single Sign-on</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWUO-05</td>
<td>Import- und Exportfilter für aktuelle Dateiformate gemäß<br />
Industriestandards</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOU-06</td>
<td>Druckausgabe in PDF-Datei</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOU-07</td>
<td>Ausgabe auf lokal angeschlossenen Drucker</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AFWOU-08</td>
<td>Ausgabe auf Netzwerkdrucker</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AFWOU-09</td>
<td>Kollaborative Erstellung von Dokumenten</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Die folgende Tabelle nennt die Anforderungen an die Funktion
Textverarbeitung.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFWOTV-01</td>
<td>Layout-Vorlagen vorhanden und frei erstellbar</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-02</td>
<td>Mehrspaltige Texterstellung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOTV-03</td>
<td>Grafiken frei positionierbar mit umlaufendem Text</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-04</td>
<td>Kommentarfunktion</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOTV-05</td>
<td>Rechtschreibprüfung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-06</td>
<td>Änderungsverfolgung</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Die folgende Tabelle nennt die Anforderungen an die Funktion
Tabellenkalkulation.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFWOTK-01</td>
<td>Ein-/Ausblenden von Zeilen und/oder Spalten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTK-02</td>
<td>Datentyp-Formatierung von Zellinhalten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOTK-03</td>
<td>Formatierung der Schriftart und Schriftausrichtung in Zellen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-04</td>
<td>Zusammenfügen von Zellen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOTV-05</td>
<td>Erzeugung von Zelleninhalten über numerische Funktionen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-06</td>
<td>Darstellung multipler Tabellen in einem Dokument</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOTV-07</td>
<td>Berechnung von Tabelleninhalten über Tabellenverweise</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOTV-08</td>
<td>Erstellung von Präsentationsgrafiken zu Tabelleninhalten</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Die folgende Tabelle nennt die Anforderungen an die Funktion
Präsentationsgrafik.

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AFWOP-01</td>
<td>Programmierbare Layout-Vorlagen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOP-02</td>
<td>Freie Kombination von Text und Grafik</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOP-03</td>
<td>Werkzeuge für Grafikerstellung enthalten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOP-04</td>
<td>Integration multimedialer Inhalte</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOP-05</td>
<td>Import von Text, Tabellen und Präsentationen aus anderen Web-Office-Komponenten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AFWOP-06</td>
<td>Animierte Aufbereitung von Präsentationsinhalten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AFWOP-07</td>
<td>Ausgabe von Präsentation und Notizen auf unterschiedliche<br />
Monitore</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AFWOP-08</td>
<td>Timer-Unterstützung bei der Wiedergabe von Präsentationen</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### PDF-Anzeige

Die PDF-Anzeige ist ein Werkzeug zur grafischen Darstellung von
PDF-Dokumenten. Die folgende Tabelle nennt die Anforderungen an die
Funktion PDF-Anzeige.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>APDF-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APDF-02</td>
<td>Dokumentenansicht frei skalierbar (Zoom)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>APDF-03</td>
<td>Dokumentenansicht in 90 Grad Schritten drehbar</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APDF-04</td>
<td>Textauswahl und Überführung in Zwischenspeicher (Clipboard) möglich</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>APDF-05</td>
<td>Suchfunktion</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APDF-05</td>
<td>Seitenansicht</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Medienwiedergabe

Die Medienwiedergabe ist ein Werkzeug zur Darstellung von Bilddaten,
Audiodaten und Videodaten. Die folgende Tabelle nennt die Anforderungen
an die Funktion Medienwiedergabe.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AMW-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AMW-02</td>
<td>Bildanzeige (mindestens GIF, PNG, JPEG)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AMW-03</td>
<td>Audio-Wiedergabe (mindestens MP3, MP4, WAV)</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AMW-04</td>
<td>Video-Wiedergabe (mindestens AVI, MP4)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AMW-05</td>
<td>Integrierter eBook-Reader</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>AMW-06</td>
<td>Integrierter Video-Converter (z.B. AVI&lt;-&gt;MP4)</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Dateikompression

Die Dateikompression verdichtet große Dateien bzw. umfangreiche
Ordnerstrukturen mit dem Ziel der Reduktion des Platzbedarfs auf der
Dateiablage oder im Zuge einer bandbreitenoptimierten Datenübertragung
über ein Netzwerk. Die folgende Tabelle nennt die Anforderungen an die
Funktion Dateikompression.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>ADK-01</td>
<td>Standardisiertes Zip-Format</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ADK-02</td>
<td>Passwortgeschützte verschlüsselte Dateiobjekte</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ADK-03</td>
<td>Segmentierung großer Dokumente</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ADK-04</td>
<td>Ordnerkompression</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Anti-Virus

Anti-Virus oder allgemeiner Anti-Malware filtert den Datenfluss mit dem
Ziel, Schadcode aufzudecken und zu isolieren. Die folgende Tabelle nennt
die Anforderungen an die Funktion Anti-Virus.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AAV-01</td>
<td>Prüfung von Dateiinhalten gegen bekannte Virus-Signaturen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AAV-02</td>
<td>Tägliche automatische Aktualisierung der Virus-Signaturen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AAV-03</td>
<td>Automatische Überführung erkannter Malware in eine Quarantäne</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AAV-04</td>
<td>Automatische und manuell abrufbare Aktivierung von selektiven und vollständigen Scans von Inhalten des Arbeitsspeichers sowie der persönlich zugänglichen Daten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AAV-05</td>
<td>Konfiguration nicht zu prüfender Dateien bzw. Ordnerhierarchien</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Passwort-Tresor (Optional)

Der Passwort-Tresor ist eine Passwort-Managementsoftware, die
Zugangskennungen zu unterschiedlichen Services mit jeweils individuell
einzusetzenden Credentials in einer geschützten persönlichen Datenbasis
verwahrt. Die Datenbasis ist mit einem benutzerspezifischen
Master-Passwort geschützt. Die folgende Tabelle nennt die Anforderungen
an die Funktion Passwort-Tresor.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>APT-01</td>
<td>Festlegung von Master-Passwörtern nach Richtlinienvorgabe</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APT-02</td>
<td>Multifaktor-Schutz des Passwort-Tresors</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>APT-03</td>
<td>Notfall-Zugang zum Passwort-Tresor</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>APT-04</td>
<td>Import/Export von Zugangskennungen</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>APT-05</td>
<td>Unterstützung der Übertragung von Zugangskennungen über Clipboard und/oder Auto-Type</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Gemeinsame Nutzung von Dateien

Die Funktion Gemeinsame Nutzung von Dateien wird durch die
Cloud-Dateiablage erbracht. Die Anforderungen an die Funktion Gemeinsame
Nutzung von Dateien sind in der Tabelle zu <span
class="underline">6.1.3.2</span> Cloud-Dateiablage aufgeführt.

#### Lernmanagementsystem (Optional)

Das Lernmanagementsystem verwaltet strukturierte Dokumente mit
multimedialen Inhalten, die ihre Autoren zum Zweck des Selbststudiums
zusammengestellt haben. Die folgende Tabelle nennt die Anforderungen an
die Funktion Lernmanagementsystem.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>ALM-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALM-02</td>
<td>Integriertes Autorensystem für die Erstellung von Lerninhalten, Aufgaben und Tests</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ALM-03</td>
<td>Strukturierte Ablage der Dokumente in einer internen Datenbank</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALM-04</td>
<td>Grafisch gestütztes Zusammenführen von Materialien zu Lektionen, Kapiteln sowie Kursen über Bibliotheksverweise</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ALM-05</td>
<td>Bereitstellung von Kursen in virtuellen Kursräumen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALM-06</td>
<td>Zugang zu Kursräumen gesteuert über individuelle Nutzerkonten und nach Rollenzugehörigkeit</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>ALM-07</td>
<td>E-Mail-gestützte Selbstregistrierung</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>ALM-08</td>
<td>Kontoaktivierung nach Reaktion auf Bestätigungsmail</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>ALM-08</td>
<td>Kursabonnement durch Benutzer</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALM-09</td>
<td>Kurszuweisung durch Mentoren</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ALM-10</td>
<td>Befristete Freischaltung zu Kursinhalten</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>ALM-11</td>
<td>Konfigurierbare automatisierte Benachrichtigung zu Fristablauf</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>ALM-12</td>
<td>Reports zu Lernaktivitäten abrufbar durch Benutzer</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALM-13</td>
<td>Benutzerreports zu Lernaktivitäten abrufbar durch Mentor</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Wissensmanagementsystem

Ein Wissensmanagementsystem verwaltet zentral internes und externes
Wissen zu Prozessen, fachlichen Inhalten, unternehmerischen Vorgaben,
Arbeitstechniken und so fort. Die folgende Tabelle nennt die
Anforderungen an die Funktion Wissensmanagementsystem.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AWM-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWM-02</td>
<td>Zentrale Verwaltung von multimedial aufbereitetem Unternehmenswissen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AWM-03</td>
<td>Internes Autorensystem für die Aufbereitung individuell und/oder gemeinschaftlich (kollaborativ) erzeugter Inhalte</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWM-04</td>
<td>Verknüpfung der Inhalte über Navigationsstrukturen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AWM-05</td>
<td>Versionierung der Inhalte</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWM-06</td>
<td>Schlagwortverwaltung / Schlagwortsuche</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AWM-07</td>
<td>Rollenkonzept für Erstellungs- und Bearbeitungsrechte</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWM-08</td>
<td>Suche nach Autoren</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AWM-09</td>
<td>Suche in Zeitintervall der Veröffentlichungen</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Projektmanagementsystem

Ein Projektmanagementsystem stellt Werkzeuge zur zentralen Verwaltung
aller relevanten Ausgangsgrößen und Zwischenergebnisse bereit, die bei
der Abwicklung eines Projekts von Bedeutung sind. Damit will das
Projektmanagement die Planung und Steuerung von Projekten optimieren,
sodass Projektrisiken begrenzt, Projektchancen genutzt und Projektziele
qualitativ, termingerecht und im Kostenrahmen erreicht werden. Die
folgende Tabelle nennt die Anforderungen an die Funktion
Projektmanagementsystem.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>APM-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APM-02</td>
<td>Zentrale Verwaltung aller Informationen und Aktivitäten zum Projektmanagement</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>APM-03</td>
<td>Planung, Überwachung und Steuerung von Projekten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>APM-04</td>
<td>Unterstützung agiler Projektmanagement-Prozesse (Scrum, Kanban)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>APM-05</td>
<td>Grafische Darstellung von Projektphasen, Meilensteinen und Arbeitslasten der Team-Mitglieder</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Umfrage- und Abstimmungssystem

Das Abstimmungssystem richtet konkrete Fragen an eine Gruppe von
Abstimmungsberechtigten, nimmt die Antworten entgegen und bereitet die
Rückläufe als Abstimmungsergebnis auf. Das Umfragesystem liefert
komplexe Fragebögen mit unterschiedlichen Frage- (geschlossene Fragen,
offene Fragen) und Antworttypen (Einfachauswahl, Mehrfachauswahl von
Antwortvorgaben, Rangbildung, relativer Eindruck, …). Die folgende
Tabelle nennt die Anforderungen an die Funktion Umfrage- und
Abstimmungssystem.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AUAS-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AUAS-02</td>
<td>Befristung von Umfrage- und Abstimmungszeitraum</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AUAS-03</td>
<td>Konfigurierbare Erinnerungen an Endtermin</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AUAS-04</td>
<td>Echtzeitauswertung der Rückläufe</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AUAS-05</td>
<td>Begrenzung des Antwortumfangs für offene Fragen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AUAS-06</td>
<td>Adaptive Umfragen (Antwortwerte bestimmen Inhalte von Folgefragen)</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AUAS-07</td>
<td>Fortsetzung der Umfrage nach Unterbrechung</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AUAS-08</td>
<td>Grafische Aufbereitung der Ergebnisse</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### E-Mail und Kalender

Die Komponente E- ist ein Benutzer-gerichtetes Werkzeug zur Verwaltung
von Postfächern und zusätzlich ein systemweit operierender Dienst, der
Postfachinhalte zwischen Sendern und Empfängern von E-Mails
transportiert. Die folgende Tabelle nennt die Anforderungen an die
Funktion E-Mail

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AEMA-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEMA-02</td>
<td>Nachrichtenverschlüsselung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEMA-03</td>
<td>Postfachpflege über Ordnerstruktur</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEMA-04</td>
<td>Suchfunktion</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEMA-05</td>
<td>Empfängergenerierung über Verteilerlisten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEMA-06</td>
<td>Anheften von Dringlichkeitskategorien</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEMA-07</td>
<td>Anforderung von Empfangs- sowie Lesebestätigung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEMA-08</td>
<td>Unterstützung von Nur-Text- und HTML-formatierten Nachrichten</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AEMA-09</td>
<td>Konfigurierbare Out-of-Office Nachricht bei Abwesenheit</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEMA-10</td>
<td>Konfigurierbare Postfachfreigabe</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Kalender

Die Komponente Kalender ist ein Benutzer-gerichtetes Werkzeug zur
Verwaltung von Termindaten und zusätzlich ein systemweit operierender
Dienst, der Kalenderinhalte der Anwender auswertet, synchronisiert und
daraus Aktionen ableitet, z.B. Terminerinnerungen generiert. Die
folgende Tabelle nennt die Anforderungen an die Funktion Kalender.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AEKA-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEKA-02</td>
<td>Alarmierung vor Kalendertermin</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEKA-03</td>
<td>Konfigurierbare Arbeitszeit für Terminverfügbarkeit</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AEKA-04</td>
<td>Sitzungsräume buchbar / reservierbar</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Adressbuch

Die Komponente Adressbuch ist ein Benutzer-gerichtetes Werkzeug zur
Organisationshilfe zur Verwaltung von Kontaktdaten und
Kontaktverzeichnissen und zusätzlich ein systemweit operierender Dienst,
der Postfachinhalte zwischen Sendern und Empfängern von E-Mails
transportiert und Kalenderinhalte der Anwender auswertet, synchronisiert
und daraus Aktionen ableitet, z.B. Terminerinnerungen generiert. Die
folgende Tabelle nennt die Anforderungen an die Funktion E-Mail und
Kalender.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AEKO-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEKO-02</td>
<td>Speichern von Kontaktdaten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEKO-03</td>
<td>Such- und Sortierfunktion</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEKO-04</td>
<td>Bearbeitung und Aktualisierung von Kontakten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AEKO-05</td>
<td>Synchronisation</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AEKO-06</td>
<td>Verknüpfung von Kontakten</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Video- und Telefonkonferenzen

Das Modul Video- und Telefonkonferenzen liefert die Informationstechnik
für die digitale audio-visuelle Zusammenarbeit in Echtzeit. Die folgende
Tabelle nennt die Anforderungen an die Funktion Video- und
Telefonkonferenzen.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AVUT-01</td>
<td>Einbindung vorhandener technischer Endgeräte</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-02</td>
<td>Konferenzschaltungen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVUT-03</td>
<td>Wahlwiederholungen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-04</td>
<td>Weiterleitungen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVUT-05</td>
<td>Aufzeichnungen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-06</td>
<td>Anrufbeantworter</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AVUT-07</td>
<td>Adressbuch / Kontaktliste</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AVUT-08</td>
<td>Computer-Telefonie-Integration (z.B. Erkennung von Telefonnummern in E-Mails und benutzerinitiierter Wahlvorgang)</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Kurznachrichten

Das Modul Kurznachrichten unterstützt den Versand und den Empfang von
Kurznachrichten auf mobilen Endgeräten. Die folgende Tabelle nennt die
Anforderungen an die Funktion Kurznachrichten.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AVUT-01</td>
<td>Auf stationären Systemen nutzbar</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-02</td>
<td>Automatische Synchronisation mit zentralem Speicher nach Offline-Betrieb</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVUT-03</td>
<td>Kompatibel mit modernen Nachrichtendiensten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-04</td>
<td>Verschlüsselte Nachrichtenübermittlung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVUT-05</td>
<td>Gruppen-Chats</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVUT-05</td>
<td>Adressbuch-Kopplung</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>AVUT-06</td>
<td>Empfangsbestätigung</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Live Streaming (Optional)

Das Modul Live Streaming realisiert einen zentralen Dienst, der die
Echtzeit-Übertragung von Audio- und Video-Signalen über das Internet
leistet. Mögliche Signalquellen für diesen Dienst können aus
Live-Ereignissen oder aus Aufzeichnungen abgeleitet werden. Die folgende
Tabelle nennt die Anforderungen an die Funktion Live Streaming.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>ALS-01</td>
<td>(Life-)Einbindung von Arbeitsplatz-Inhalten (Display, Kamera, Mikrofon)</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ALS-02</td>
<td>Verschlüsselte Datenübertragung</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>ALS-03</td>
<td>Datenzugriff Passwort-geschützt</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>ALS-04</td>
<td>Digitale Urheber-Stempel einblendbar</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ALS-05</td>
<td>Sprachinformationen automatisiert als Untertitel darstellbar</td>
<td>Soll</td>
</tr>
</tbody>
</table>

#### Whiteboard

Das Whiteboard liefert ein grafisches Zeichenbrett als Softwareanwendung
für den digitalen Arbeitsplatz. Die folgende Tabelle nennt die
Anforderungen an die Funktion Whiteboard.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AWB-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWB-02</td>
<td>händische Erstellung von Notizen und Skizzen auf virtuellem Whiteboard über Touchpad</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AWB-03</td>
<td>Werkzeugvorrat mindestens Pinselstärke, Farbauswahl, Radierer</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AWB-04</td>
<td>kollaborativ nutzbar</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AWB-05</td>
<td>Texterkennung in handschriftlichen Aufzeichnungen</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Visualisierungstools

Das Visualisierungstool liefert einen universellen Werkzeugkasten, der
dem Anwender die Erstellung allgemeiner und geschäftsspezifischer
Illustrationen ermöglicht. Die folgende Tabelle nennt die Anforderungen
an die Funktion Visualisierungstools.

<table>
<tbody>
<tr class="odd">
<td>Kennung</td>
<td>Titel</td>
<td>Verbindlichkeit</td>
</tr>
<tr class="even">
<td>AVT-01</td>
<td>Benutzerdialog über Web-Browser</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVT-02</td>
<td>Erstellung und Anzeige von Prozessdiagrammen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVT-03</td>
<td>Erstellung und Anzeige von Netzwerkdiagrammen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVT-04</td>
<td>Erstellung und Anzeige von Flussdiagrammen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVT-05</td>
<td>Erstellung und Anzeige von Mindmaps</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>AVT-05</td>
<td>Erstellung von Freiform-2D-Grafiken</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVT-06</td>
<td>Objektgruppierung</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>AVT-07</td>
<td>Unterstützung von Pixel- und Vektorgrafik</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>AVT-08</td>
<td>Unterstützung gängiger Image-Formate für Import/Export</td>
<td>Muss</td>
</tr>
</tbody>
</table>

### Produktmodell

Der SouvAP wird ausschließlich aus Produkten assembliert, die ihre
Autoren zusammen mit von Open CoDE akzeptierten Open Source Lizenzen
veröffentlichen. Diese Produkte sind distributionsneutral aufzubereiten
und anschließend in Quellform auf Open CoDE zu veröffentlichen.
Zusätzlich sind die Produkte auf Open CoDE in Binärform vorzuhalten, und
zwar als Artefakte (Container-Images) einschließlich aller genutzten
Abhängigkeiten in einem Format, das in einer DVS-konformen
Container-Infrastruktur lauffähig ist.

Das Gesamtwerk SouvAP enthält Produkte gemäß dem Komponentenmodell
(<span class="underline">6.1.2.1</span>) und weitere Artefakte, die das
medienbruchfreie Zusammenspiel der Komponenten steuern. Dazu zählen z.B.
die Schichten Registry, Orchestrierung, Container-Laufzeitsteuerung,
Überwachung und eine Interoperabilitätsschicht wie in <span
class="underline">Kapitel</span> 7. Zu diesen Bausteinen erforderlicher
Quellcode und daraus erzeugt DVS-konforme Artefakte werden ebenfalls auf
Open CoDE veröffentlicht.

Für die Praxis ist eine Neutralität des SouvAP gegenüber
Betriebssystemen, Plattformen und Cloud-Infrastrukturen angestrebt. Mit
dieser Eigenschaft ist der SouvAP auf vielzähligen Umgebungen mit
unterschiedlichen zugrunde liegenden Architekturen kompatibel. Das
Design liefert somit nicht nur das Fundament für maximale Flexibilität.
Auch eine Zukunftssicherheit kann der SouvAP mit diesen Merkmalen
glaubwürdig versprechen.

Die Veröffentlichung in Quellform soll Entwickler und Nutzer befähigen,
auf hochwertiger Basis aufbauend Anpassungen an individuelle Bedürfnisse
durchführen zu können. Perspektivisch soll die Freigabe des Quellcodes
außerdem zur Programmierung von Erweiterungen einladen. Die zusätzliche
Offenlegung der Schnittstellen schließlich soll dem Markt die
Möglichkeit geben, Eigenentwicklungen hin zu Open Source Alternativen
für bereits vorhandene Produkte vorzuschlagen.

Die Entscheidung pro Open Source Software liefert aufgrund der inhärent
garantierten Freiheiten ein solides Fundament für hohe Transparenz.
Daraus wird gemeinhin auch eine positive Auswirkung auf die Sicherheit
abgeleitet, da unerwünschte Datenabflüsse direkt im Quellcode blockiert
werden können und Ursachen für etwaiges Fehlverhalten praktisch jeder
Fachkundige in kurzer Zeit eingrenzen und ausmerzen kann.

### Integrationsmodell

Die betriebliche Bereitstellung des SouvAP erfordert im Zuge des
Erstaufbaus die Installation des Gesamtmodells, eine Verankerung der
Artefakte zu den Komponenten in der Registry des Gesamtmodells und einen
Qualitätsnachweis, der über die Bewältigung einer Testsuite zu erbringen
ist. Mit jeder Weiterentwicklung sind die jeweils betroffenen
Abhängigkeiten erneut zu validieren. Eine Weiterentwicklung gilt als
zulässiger Nachfolger, wenn der Qualitätsnachweis positiv ausfällt. Der
Ersatz eines Produkts aus dem Komponentenmodell ist wie eine
Weiterentwicklung zu behandeln.

#### API-Gateway als Integrationsschicht

Alle Produkte zum Komponentenmodell docken an das Gesamtmodell über eine
Interoperabilitätsschicht an. Diese wird als API-Gateway erstellt. Sie
stellt mindestens die Kopplungstechniken Webdienst, REST-API und
Nachrichten-Bus bereit. Die jeweils für eine konkrete Komponente zu
benutzende Kopplungstechnik wird auf ihre spezifischen Anforderungen
abgestimmt. Die weiteren Details zu der Integrationsschicht werden in
den Folgekapiteln E und F ausgearbeitet.

#### Einsatz von Schnittstellenstandards

Die Interaktion der Komponenten in das Gesamtmodell und die Interaktion
der Komponenten untereinander erfolgt auf Basis des in <span
class="underline">6.1.2.2</span> genannten Kommunikationsmodells. Das
API-Gateway muss in Summe die genannten Protokollstandards bedienen
können. Eine Komponente gemäß Komponentenmodell ist immer dann in das
Gesamtmodell integrierbar, wenn das API-Gateway den Nachrichtenaustausch
von und zur Komponente steuern kann.

#### Anpassung der Komponenten-Schnittstellen

Dieser Abschnitt betrachtet den, Fall, dass ein Modul-Kandidat keine der
zum Standard erklärten Kommunikationsprotokolle (siehe <span
class="underline">Fehler: Verweis nicht gefunden</span>) beherrscht. Für
diesen Fall soll folgender Prozess durchlaufen werden:

1.  Beim Hersteller der Software wird angfragt, ob dieser eine
    Kommunikationsschnittstelle gemäß SouvAP-Standard nachreichen kann.

2.  Sollte dies aus zeitlichen oder wirtschaftlichen Gründen nicht
    möglich erscheinen, ist zu prüfen, ob die Anbindung über einen
    bereits vorhandenen oder zusätzlichen Adapter gelingt.

3.  Ist keine der zuvor genannten Vorgehensweisen erfolgverversprechend,
    dann ist zu entscheiden, ob die betreffende Software weiterhin als
    Modul-Kandidat behandelt werden soll.  

#### Testen und Optimieren

Währen und nach der API-Kopplung bzw. der Anpassung von
Komponenten-Schnittstellen sollen umfassende automatische Tests
nachweisen, dass die Integration wie erwartet funktioniert. Darauf
zugeschnittene übergreifende Tests sind einmalig für
Schnittstellenstandards und zusätzlich für etwa benötigte Adapter zu
erstellen. Ergänzende Lasttests sollen Hinweise auf die Skalierbarkeit
liefern und den Bedarf für erforderliche Leistungsoptimierungen
aufdecken. Das Spektrum durchzuführender Tests soll das Vorgehensmodell
gemäß der MicroService Testpyramide umsetzen. Leistungsoptimierungen
sollen bekannte „best practises“ berücksichtigen.

![](media/file28.png)

Abbildung : Testpyramide

#### Bündelung der Applikation in Containerimage

Die Analyse des Bedarfs an digitaler Geschäftsprozessunterstützung
mündete in <span class="underline">6.1.2.1</span> in ein
Komponentenmodell, auf dessen Basis in <span
class="underline">6.1.3</span> Architekturmodule spezifiziert wurden.
Diese sollen gemäß <span class="underline">6.1.2.3</span> austauschbare
Produkte in einem Gesamtmodell bilden. Dazu müssen die Produkte mit den
Schnittstellenspezifikationen des Gesamtmodells kompatibel sein und sie
müssen auf der darunter liegenden Infrastrukturschicht lauffähig sein.

Von der Infrastrukturschicht ist die DVS-Konformität gefordert. Diese
legt fest, dass Anbieter von Services für die Deutsche Verwaltungscloud
Container-as-a-Service Plattformen auf der Basis von Kubernetes
bereitstellen müssen. Dem folgend sind die Architekturmodule zum SouvAP
als Container-Images aufzubereiten und es sind Konfigurationsdateien zu
erstellen, die die Ausführung der jeweiligen Anwendung im Container
beschreiben.

Container-Images sind als Dockerfile anzufertigen, die den Empfehlungen
des BSI entsprechen. Diese müssen die Applikation (das zum Modul
passende Produkt) und alle davon benötigten Laufzeitbibliotheken
enthalten. Dabei sollte es das Ziel sein, ausschließlich die tatsächlich
erforderliche Middleware in das Dockerfile einzubringen. Idealerweise
sind sämtliche Abhängigkeiten zu Härten, sodass eine minimale bis gar
keine Angriffsfläche für Angriffe oder missbräuchliche Nutzung des
Containers bereitsteht.

Es ist üblich, Dockerfiles mit Bezug auf vorhandene
„Integrationsschicht-Container-Images“ zu erstellen, die auf einem
zentralen Repository bereitgestellt sind. Für die Komponenten des SouvAP
sind ausschließlich auf Schadsoftware geprüfte und für sicher befundene
Images zu benutzen, die aus einem vertrauenswürdigen Repository stammen.

Vor der betrieblichen Bereitstellung sind die als Container-Image
aufbereiteten Betriebsleistungskomponenten entsprechend dem
Vorgehensmodell gemäß <span class="underline">6.1.5.4</span> zu testen.
Zur Bereitstellung in einer DVS-konformen Container-as-a-Service
Umgebung sind die Container in die dort genutzte Container-Registry zu
überführen. Die Orchestrierung der Container-Images, die Integration in
das Container-Laufzeitsystem und etwa benötigten Skalierungen,
Load-Balancing, Container-Mobilität und Failover liegen im
Aufgabenbereich der Container-Infrastruktur.

### Servicemodell

Das Servicemodell zerlegt das Anwendungsportfolio in Blackboxes. Dies
legt die Grundlage dafür, dass jeder Service über die geforderten
Eigenschaften Agilität, Automatisierbarkeit und die betrieblich und
sicherheitstechnisch relevanten Attribute bedienen kann:

![](media/file29.png)

Abbildung : Servicemodell

### Sicherheits-Servicemodell

Die Komponenten zum SouvAP können perspektivisch den Zugriff auf Daten
ersuchen, die aufgrund von gesetzlichen oder unternehmerischen
Regularien mit einer Schutzstufe assoziiert sind. In einigen Fällen sind
Zugriffe auf derartige Daten außerdem zu protokollieren.

Das Gesamtmodell verfolgt den Ansatz, einerseits die Daten zu schützen,
andererseits Zugriffsersuchende vor unerwünschten
Zugriffsrechtsverletzungen zu schützen. Je nach Kritikalität der Daten
nutzt das Service-Sicherheitsmodell dazu eine oder mehrere der
nachfolgend aufgezählten Techniken. Der SouvAP ist offen dafür,
Schutztechniken zukünftig mit neuen Innovationen fortzuschreiben.

#### Sicherheit AAA

Das Akronym AAA (oft gesprochen als Triple-A) steht für Authentication,
Authorization und Accounting. AAA wird bevorzugt von Netzbetreibern
eingesetzt, ist aber auch bei Service-Anbietern weit verbreitet. Salopp
formuliert prüft AAA den Ressourcenzugriff, fällt Entscheidungen zur
zulässigen Service-Nutzung und protokolliert Zugang zum und Abgang vom
Service mit Zeitstempel. Implizit errechnet der Service-Anbieter daraus
die Nutzungsdauer und diese können als Orientierungswert für etwaige
Abrechnungen dienen.

Mit diesem Herangehen liefert AAA dem Service-Anbieter Antworten auf die
Fragen „Wer“, „Was“ und „Wie viel“. Die technische Umsetzung leistet
aber noch mehr, nämlich sie implementiert einen Zugriffsfilter. Das
Ergebnis der Authentikation bestimmt, für welche Ressourcen (Datei,
Dokument, Dienst) das Gesamtsystem den Konsumenten (Mensch oder
Maschine) autorisiert.

#### Zero Trust

Zero Trust realisiert je nach Grad der Implementierung granulare und
dynamisch skalierende Schutztechniken zur Absicherung von Ressourcen.
Das Prinzip: Jeder Zugriffsquelle wird konsequent misstraut, vor jedem
Zugriffsversuch muss die Quelle ihr Vertrauensniveau darlegen und der
Zugriff auf die Ressource wird nur dann gewährt, wenn die Kombination
aus Vertrauensniveau der Quelle und Schutzbedarf der Ressource ein
tragbares Risiko bezüglich der Verletzung von Schutzzielen erwarten
lässt. Im Sitzungsbetrieb kann Zero Trust diesen Sachverhalt auch in
Echtzeit aufgrund von besonderen Ereignissen oder sich ändernden
Voraussetzungen neu bewerten und zuvor gewährte Zugriffsrechte
aberkennen, also die Verbindung kappen.

Das Vertrauensniveau der Quelle kann Zero Trust aus einer Kombination
aus Identität des Anwenders, Gesundheitszustand des genutzten Endgeräts
(Hard- und Software) und Typ und Qualität des Netzwerks (Funk oder
Kabel-gebunden, nicht oder schwach oder stark verschlüsselt) ableiten.
Damit hebt Zero Trust die in AAA zugrunde gelegte statische Auswertung
der Authentikation und die statische Zuordnung von Zugriffsrechten
(Authorization) auf eine dynamische Zuweisung von Zugriffsrechten, bei
der ergänzend Log-Daten (Accounting) in die Bewertung einfließen können.
In der Praxis kann eine Zero Trust Implementierung außerdem von der
Quelle zusätzliche Identitätsnachweise anfordern, z.B. zusätzliche
Identitätsnachweise (Multi-Faktor-Authentifizierung) oder API-Schlüssel.

Diese Herangehensweise wurde in der Industrie bereits in Teilen für
gesicherte Interprozesskommunikation implementiert. Authentication und
Authorization sind derart umgesetzt, dass die Identität der Quelle das
Zugriffsrecht auf die anwendungsseitige API begründet. Dazu kann die
Anwendung z.B. Single Sign-on oder Multi-Faktor-Authentifizierung als
fortschrittliche Techniken zur Identitätsabfrage einsetzen. Wird dies in
einem API-Gateway realisiert, erfolgt die Autorisation außerdem vor dem
Ressourcenzugriff.

Zero Trust im API-Gateway setzt voraus, dass neben AAA ergänzende
Regelwerke vorzuhalten sind, die die Kommunikation über verschlüsselte
Leitungen und/oder den Abruf verstärkter Authentikationsnachweise
anfordern, bevor das API-Gateway den Zugriff auf besonders
schützenswerte Ressourcen freischaltet. Diese Maßnahmen sollen den
Schutz der Integrität und der Vertraulichkeit von Daten maximieren.

Das Diagramm illustriert schematisch eine Zero Trust Authentikation über
ein API-Gateway.

![](media/file30.png)

Abbildung : API Security nach Zero Trust

### Mensch-zu-Maschine-Kommunikation

Die Mensch-Maschine-Kommunikation erfolgt sitzungsgestützt. Dem geht
eine Anmeldung des Benutzers am System voraus. Zu unterscheiden ist
zwischen der lokalen Anmeldung und der Netzwerkanmeldung. Bei der
lokalen Anmeldung erzeugt das lokale System ein Identitätstoken und der
Anwender erhält damit einen Schlüssel, zu dem das lokale System den
Zugriff auf lokale Ressourcen gewährt.

Bei der Netzwerkanmeldung bildet das System, auf dessen Ressourcen
(Dienste und Daten) der Anwender zugreifen möchte, einen
Ressourcenanbieter. Die Schlüsselvergabe übernimmt ein
Identitätsanbieter. Der Anwender kann den Schlüssel entweder direkt oder
indirekt (aufgrund einer Weiterleitung seitens des Ressourcenanbieters)
beim Identitätsanbieter abrufen. Derartige Schlüssel können auch den
Zugriff auf eine Gruppe von Ressourcenanbietern gewähren (Single
Sign-on).

In beiden Fällen erfolgt die Schlüsselausgabe aufgrund einer
erfolgreichen Auswertung von Zugangskennungen (Credentials), die
mindestens eine Identitätskennung und in sicheren Netzen zusätzliche
eine Art Authentizitätsnachweis (Wissen, Besitz, Sein) erfordern. Art
und Gestalt des Authentizitätsnachweises müssen heute üblicherweise die
Vorgaben einer diesbezüglichen unternehmerischen Richtline erfüllen.

#### Maschine-zu-Maschine-Kommunikation

**Die Maschine-zu-Maschine-Kommunikation (M2M) ist ein automatisierter
Daten- oder Nachrichtenaustausch zwischen Rechnersystemen. Eine
geschützte M2M ist in der Literatur vereinzelt erschöpfend dadurch
geprägt, dass die Nachrichtenübertragung verschlüsselt erfolgt. Es
scheint daher sinnvoll, eine gesicherte M2M-Kommunikation zu fordern
derart, dass die Kommunikationspartner sich vor dem Daten- sowie
Nachrichtenaustausch gegenseitig authentifizieren müssen.**

![](media/file31.png)

Abbildung : Maschine zu Maschine Kommunikation mit Token

**Analog der Mensch-zu-Maschine-Kommunikation kann die Authentifizierung
durch gegenseitigen Austausch von Zugangskennungen erfolgen, die den
jeweiligen Kommunikationspartnern ihre jeweilige Authentizität
nachweist. Auch hier sind Zugang zu Diensten und Zugriff auf Ressourcen
über Schlüssel steuerbar und die Schlüsselausgabe kann ein drittes
System leisten, das im Netzwerk die Rolle des Identitätsanbieters
übernimmt.**

#### Security Token

In der M2M-Kommunikation hat sich heute der Einsatz von Token für den
Nachweis der Authentizität durchgesetzt. Token können auch
anwenderspezifische Zugriffsrechte kodieren (siehe <span
class="underline">6.1.2.2.6</span>). Aufgrund der universellen
Einsatzmöglichkeit für Token und dem breiten Anwendungsbereich für den
SouvAP fächert dieses Kapitel einige sicherheitsrelevanten Aspekte zu
Token auf, die der SouvAP verpflichtend umsetzen soll.

##### Authentifizierungstoken

Authentifizierungstoken müssen verwendet werden, damit die
M2M-Kommunikationspartner die Identität von Geräten bzw. Anwendungen
überprüfen können, die an der M2M-Kommunikation beteiligt sind. Bevor
eine Kommunikation zustande kommt, müssen die Geräte bzw. Anwendungen
ein gültiges Token besitzen und dem Nachrichtenempfänger anliefern,
sodass der Empfänger prüfen kann, ob und wie der Sender für einen Daten-
oder Nachrichtenaustausch mit dem Empfänger autorisiert ist.

##### Kommunikation

**Die M2M-Kommunikation muss verschlüsselt sein. Dies wehrt die Gefahr
ab, dass Dritte die übertragenen Daten abfangen oder manipulieren
können. Eine aus heutiger Sicht hochgradig sichere Methode der
Verschlüsselung von M2M-Kommunikation besteht in der Verwendung von
SSL/TLS-Protokollen.**

##### Zugriffssteuerungstoken

**Zugriffssteuerungstoken haben ihren Einsatzbereich in der Steuerung
des Zugriffs auf eine abgegrenzte Anzahl der vorhandenen Ressourcen,
Daten oder Dienste. Es handelt sich quasi um ein Authentifizierungstoken
mit zusätzlichen Attributen.**

##### Begrenzte Gültigkeitsdauer von Tokens

**Alle innerhalb des SouvAP genutzten Token sind mit begrenzter
Gültigkeitsdauer auszustellen. Nicht mehr benutzte Token sind zu
sperren. Diese Maßnahmen sollen das Risiko minimieren, dass Angreifer
mit gestohlenen bzw. ungültigen Token auf geschützte Ressourcen des
SouvAP zuzugreifen können.**

##### Token-Rotation

**Die Technik Token-Rotation generiert in regelmäßigen Abständen neue
Token, aktualisiert den aktuell genutzten Token und erklärt die
ersetzten Token für ungültig. Auf diesem Weg steigert die Token-Rotation
die Sicherheit der M2M-Kommunikation. Diese Maßnahme leistet einen
weiteren Beitrag zur Minimierung des Risikos von Angriffen unter Einsatz
gestohlener oder abgelaufener Token.**

#### Sicherheitsvorfälle

**Nicht erfolgreiche Authentikationen und unzulässige Datenzugriffe sind
als Sicherheitsvorfälle einzustufen und zu protokollieren.
Sicherheitsrelevante Protokolle sind an eine zentrale Stelle zu
überführen und es ist eine Alarmierung zu veranlassen. Zu besonders
sensiblen Daten ist eine vollständige Überwachung zu aktivieren, die
jeden Datenzugriff einschließlich Audit-relevanter Attribute
dokumentiert. Die Überwachung darf keine Löschung von Audit-Daten
zulassen. Audit-Daten sind regelmäßig zu sichern und entsprechen einer
zu vereinbarenden Aufbewahrungsfrist für etwaige Sicherheitsanalysen
bereitzuhalten.**

Um als BSI-konform zu gelten, muss das API-Gateway jedoch bestimmte
Anforderungen erfüllen, wie z.B.:

-   Sicherstellung der Integrität und Vertraulichkeit der
    Zugriffsprotokolldaten,

-   Erfassung aller relevanten Ereignisse und Aktivitäten im
    Zusammenhang mit der API-Kommunikation,

-   Implementierung von Mechanismen zur Verhinderung von Manipulation
    oder Löschung von Protokolldaten und

-   Verwendung von kryptografischen Methoden zur Sicherstellung der
    Vertraulichkeit und Integrität der Daten während der Übertragung.

### Techniken der Anwendungsarchitektur

Eine effektive Strategie für die Verwendung von Open-Source-Software bei
der Entwicklung der Anwendungsarchitektur ist zu berücksichtigen, um
sicherzustellen, dass die Anforderungen des SouvAp erfüllt werden und
eine nahtlose Integration in die vorhandene IT-Infrastruktur ermöglicht
wird.

#### Identifizierung und Auswahl von Open-Source-Software

Die Identifizierung und Auswahl von Open-Source-Software, die den
Anforderungen der Anwendungsarchitektur entspricht, erfolgen nach den im
Kapitel Funktionsmodel gegebenen funktionalen und nichtfunktionalen
Anforderungen gemäß einem noch zu erstellenden Kriterienkatalog.

#### Integration von Open-Source-Software

Die Integration von Open-Source-Software-Komponenten in die
Anwendungsarchitektur ist eine Herausforderung, da verschiedene
Komponenten möglicherweise unterschiedliche Standards und Protokolle
verwenden. Es ist daher hilfreich, Tools und Methoden wie
Containerisierung, Self-Contained-Service und Microservices-Architektur
oder Docker-Container zu verwenden, um die Integration zu erleichtern.

#### Bewertung von Open-Source-Software-Komponenten

Die Bewertung von Open-Source-Software-Komponenten, um ihre Qualität,
Sicherheit und Zuverlässigkeit zu bewerten. Der Support und die Wartung
von Open-Source-Software-Komponenten zu berücksichtigen, da dies eine
entscheidende Rolle bei der Entscheidung spielt, welche Komponenten in
die Anwendungsarchitektur integriert werden sollen.

Beschreibung der Ausgangslage
-----------------------------

Die Architekturarbeit legt den Leistungsschnitt der dPhoenixSuite zum
Zeitpunkt des Projektbeginns als Ausgangslage fest.

Die dPhoenixSuite sind mehrere zum Verbund geschaltete Open Source
Softwares (sowie deren Enterprise-Pendants), um als SaaS eine
browserbasierte souveräne Office Suite für die öffentliche Verwaltung
bereitzustellen.

dPhoenixSuite wird bei Dataport als SaaS-Bündel mit folgenden
Zusatzleistungen angeboten:

-   Support

-   Bereitstellung und Betrieb von Applikationen und Infrastruktur

-   BSI Grundschutz

BarrierefreiheitI

### Funktionsmodell

In der dPhoenixSuite sind die Module zu folgenden Funktionsdomänen
zusammengefasst:

-   IAM – Identitäts- und Access-Management und Portal

-   MAV – Messaging, Audio, Video (Chat, Audiokonferenz, Videokonferenz)

-   FS/WO – File Share & Web Office (Cloud Share, Word Processor,
    Tabellenkalkulation)

-   GW – Groupware (Mail, Kalender, Adressbuch)

### Systemmodell

Das Systemmodell zum IST-Zustand der Ausgangslage dPhoenixSuite wurde
als Zusammenschluss mehrerer Produktbündel in ein Gesamtsystem
konzipiert. Aus Anwendersicht bildet das Portal den unitären
Zugangspunkt. Ein Identity- & Access-Management IAM steuert den
Anmeldevorgang mit Bezug auf einen internen Verzeichnisdiensts und einen
Federation-Service. Des Weiteren verbindet das IAM nach erfolgreicher
Anmeldung per Single Sign-on mit Modulen, die in die Leistungsbündel
dOnlineZusammenarbeit, dPhoenixOffice und dPhoenixMail gruppiert sind.

Das Diagramm illustriert das Zusammenspiel der Produktbündel mit dem
Gesamtsystem.

![](media/file32.png)

Abbildung : Systemmodell dPhoenixSuite IST-Zustand

### Bereitstellungsmodell

Zum Zeitpunkt dieser Ausarbeitung wird die dPhoenixSuite gemäß einem
hybriden Integrationsmodell bereitgestellt. Die Auslieferung der
Services erfolgt anteilig containerisiert und VM-basiert. Aktuell wird
eine Transformation auf eine DVS-konforme containerisierte
Bereitstellung vorbereitet.

Das Applikations-Rollout erfolgt automatisiert über Continuous
Integration / Continuos Deployment (CI/CD) Pipelines. Je nach
Ziel-Rechenzentrum (on Premise, Cloud) wird Installation wird mit darauf
zugeschnittenen Infrastrukturbereitstellungstools vorbereitet und mit
Automatisierungswerkzeugen konfiguriert. Tools, Pipelines und
Kundenkonfigurationen sind zentral versioniert vorgehalten. Binärpakete,
Images und Softwareartefakte werden in zentralen Repositories gepflegt.

Beschreibung der Zielarchitektur
--------------------------------

Die in diesem Konzept vorbereitete Zielarchitektur enthält eine Reihe
von Schlüsselkomponenten, die darauf abzielen, eine hochmoderne,
flexible und sichere Plattform konform zu den Anforderungen des
Bundesamtes für Sicherheit in der Informationstechnik (BSI)
bereitzustellen. Diese Architektur verbindet Edge-Gateways,
Microgateways, austauschbare Open-Source-Software und
fachverfahrensbezogene Integrationen. Etwa erforderliche Anpassungen
einzelner OSS-Produkte an die innerhalb des SouvAP unterstützten
Protokolle (siehe <span class="underline">6.1.2.2</span>) sind über
produktspezifische Adapter zu schalten.

Edge-Gateways realisieren eine „erste Verteidigungslinie“ am
Netzwerkrand. Microgateways ermöglichen granulare API-Kontrolle und
effiziente Kommunikation zwischen Microservices. Diese Grundstruktur
will das Fundament für einer skalierbare und anpassungsfähige
Architektur legen, die schnell auf veränderte Anforderungen anpassbar
ist.

Die Ausrichtung auf Modularität, konkret auf austauschbare
Open-Source-Software, adressiert die operativen bzw.
betriebswirtschaftlichen Ziele Flexibilität und Kosteneffizienz.
Adapter-gestützte Fachverfahrensintegrationen ermöglichen eine nahtlose
Anbindung verwalterischer Fachanwendungen. Dafür benötigte
Voraussetzungen sind bereits im Gesamtsystem berücksichtigt und
derartige Erweiterungen bedingen keine umfangreichen Änderungen an der
zugrunde liegenden Infrastruktur.

Der SouvAP muss in einer DVS-konformen Infrastrukturbasis lauffähig
sein. Seine Bestandteile werden als Open-Source-Artefakte auf Open CoDE
veröffentlicht. Darauf basierend können Entwickler den SouvAP betreffend
Leistung und Sicherheit verbessern, also die Rolle eines Maintainers
ausgestalten. Zusätzlich können Entwickler das Leistungsspektrum des
SouvAP an ihre Bedürfnisse anpassen. In der Summe ist der SouvAP damit
maximal transparent.

![](media/file33.png)

Abbildung : Zielarchitektur SouvAP

Security by Design ist ein wesentliches Prinzip das im SouvAP eingesetzt
wird und das darauf abzielt, Informationssicherheit von Beginn an in die
Entwicklung und Implementierung von Systemen und Anwendungen zu
integrieren, wobei auf die jahrelangen Erfahrungen des Bundesamtes für
Sicherheit in der Informationstechnik (BSI) zurückgegriffen wird. In
diesem Zusammenhang werden sowohl die BSI-Leitlinien als auch die
Betriebsstabilität gemäß den IT Infrastructure Library (ITIL) und SDLC
Best Practices für den SouvAP beachtet. Zero Trust spielt eine zentrale
Rolle im Rahmen des SouvAP, da es einen restriktiven Ansatz verfolgt,
bei dem keinem Benutzer oder Gerät standardmäßig Vertrauen gewährt wird
und Zugriffsrechte basierend auf Bedarf und Verifikation erteilt werden.
Edge-Gateways und Microgateways unterstützen die Realisierung dieses
Konzepts, indem sie die Sicherheit an Netzwerkgrenzen und auf Mikroebene
verstärken, um den Datenverkehr effektiv zu steuern, überwachen und
potenzielle Bedrohungen abzuwehren. Die Integration von Security by
Design in die Architektur SouvAP unterstützt das Ziel. ein hohes
Sicherheitsniveau zu erreichen und gleichzeitig die Integrität,
Verfügbarkeit und Vertraulichkeit der Daten zu stabilisieren. Security
by Design muss daher in allen Aspekten zum SouvAP einfließen.

Analyse des Transformationsbedarfs
----------------------------------

> Das in der dPhoenixSuite derzeit genutzte Anwendungsentwicklungsmodell
> ist zunächst auf das hier zugrunde gelegte Schichtenmodell bestehend
> aus Datenzugriffsschicht, Funktionsschicht und Präsentationsschicht zu
> heben. Das Systemmodell ist vom bisherigen Produktmodell in das hier
> aufgezeigte Komponentenmodell zu überführen. Die dabei erzeugten
> Module sind um weitere Module zu ergänzen, sodass der mit dem SouvAP
> angestrebte Leistungsschnitt abgebildet werden kann.
>
> Anschließend sind bereits in der dPhoenixSuite genutzte Produkte
> betreffend ihrer Kommunikationsmodelle zu bewerten und diese sind auf
> die Zielarchitektur zu transformieren. Dazu sind vorhanden und neue
> Modulen auf dieses Kommunikationsmodell hin zu ertüchtigen und das
> soll über dafür individuell gefertigte Adapter gelingen. Schließlich
> ist das Gesamtmodell im Detail zu konzipieren, es sind API-Gateways
> entsprechend die Anforderungen an Nachrichtenaustausch, Datenaustausch
> und Sicherheitsanforderungen zu erstellen und die einzelnen Module
> sind in das Gesamtmodell zu integrieren. Die Abnahmefähigkeit des so
> erzeugten SouvAP ist mit automatisierbaren Tests nachzuweisen und
> dafür geeignete Testsuiten sind parallel zur eigentlichen
> Entwicklungsarbeit anzufertigen.
>
> Ergänzend zur fachlichen und technischen Transformation sind die
> Vorgaben an den SouvAP abzubilden. Dazu liegen aktuell funktionelle
> und nicht-funktionelle Anforderungen vor. Die Anforderungen sind nach
> Einschätzung der Architektur noch vollständig zu erfassen und zu
> dokumentieren, sodass die Zielerreichung zum Bereich
> Informationssystemarchitektur tabellarisch bestätigt werden kann.

### OSS Kriterien

> Zu den Modulen des SouvAP ausgewählte Produkte innerhalb der
> dPhoenixSuite ist aktuell die Kompatibilität mit den Anforderungen von
> Open CoDE nicht nachgewiesen. In Zeilen sind dort sogar einige
> Enterprise-Features eingeflossen, die eine Verträglichkeit mit Open
> CoDE perspektivisch ausschließen. In jedem Fall ist der Sachstand zum
> Thema Open CoDE zu jedem SouvAP-Produkt bzw. jedem
> OSS-Modul-Kandidaten für den SouvAP im Detail zu prüfen und zu
> attestieren.
>
> Von OSS-Kandidaten für den SouvAP wird generell gefordert, dass die
> Software einer akzeptierten Open-Source-Lizenz unterliegt. Durch den
> Gebrauch der Software dürfen keine Verletzungen von Patentrechten,
> Markenrechten oder Urheberrechten entstehen. Außerdem darf der
> Quellcode keine Viren bzw. Schadsoftware enthalten.

Der aus dem Quellcode abgeleitete Binärcode muss in einer DVS-konformen
Betriebsumgebung qualitativ hochwertig und stabil operieren können und
ihr Funktionsumfang muss die Anforderungen an die dadurch adressierte
Komponente zum SouvAP abdecken. Ergänzend soll die Software auf anderen
Plattformen und Betriebssystemen lauffähig sein, sodass die digitale
Souveränität auch über die Grenzen von Prozessor- und
Betriebssystemhersteller reicht.

### Medienbruchfreie Interoperabilität

> Die Integrationsschicht bildet die Schaltstelle für medienbruchfreie
> Interoperabilität. Sie sorgt für eine nahtlose, effiziente und sichere
> Kommunikation zwischen verschiedenen Softwaresystemen sowie den sowie
> den Datenaustausch zwischen Anwendungen bzw. technischen Komponenten.
> Zusätzlich unterhält die Integrationsschicht Sicherheitstechniken, die
> Unterbrechungen, Verzögerungen und Informationsverluste vermeiden.
> Idealerweise deckt sie proaktiv drohende Engpässe auf und federt diese
> mit dynamisch generierten Leistungsreserven ab.

### Funktionsumfang

<span class="underline">6.1.3</span> enthält die aktuell bekannten
funktionellen Anforderungen an Leistungsschnitte zu den einzelnen
Modulen des SouvAP. <span class="underline">6.1.1</span> stellt heraus,
dass der Funktionsumfang von Produkten daraufhin anzupassen ist, die als
Release-Kandidaten ausgewählt wurden, ihr Funktionsumfang die
Anforderungen aber nicht vollumfänglich abbildet. In <span
class="underline">6.1.5.3</span> wurde beworben, dass ein hoher Aufwand
für die Schnittstellenanpassung für ein konkretes Produkt eine
Entscheidung gegen das Produkt begründen kann. Diese Entscheidung sollte
auch getroffen werden, wenn der Aufwand für eine Ergänzung des
Funktionsumfangs unwirtschaftlich erscheint.

In jedem Fall sind Vorhaben der funktionellen Anpassung sorgfältig zu
Planen und es sind Aufwandsabschätzungen vor der Umsetzung der
Transformation durchzuführen. Die Transformation selbst ist mit einer
Projektleitung auszustatten, die jedwede Aktivitäten insbesondere
zwischen den beteiligten Entwickler-Teams gemäß Umsetzungsplan
koordiniert. Etwaige Engpässe oder Probleme sind frühzeitig an die
Stakeholder zu berichten und es sind Gegenmaßnahmen zu ergreifen.

Die Transformation des Funktionsumfangs zu einem Produkt endet mit einer
Abnahme durch die Stakeholder. Der angeforderte Funktionsumfang ist mit
Ergebnisprotokollen zu automatisch durchführbaren Testsequenzen
nachzuweisen. Der Umfang des Transformationsbedarfs wird vor Beginn der
Transformation festgelegt. Eine Erweiterung der Anforderungen während
der Transformation begründet keine Anpassung des mit der Transformation
adressierten Funktionsumfangs.

### Barrierefreiheit

> Die Transformation zur Barrierefreiheit soll Menschen mit
> Behinderungen einen gleichberechtigten Zugang zum SouvAP ermöglichen.
> Mit Bezug auf den Standard Web Content Accessibility Guidelines (WCAG)
> soll der SouvAP visuelle, auditive und physische Zugänge und
> Bedienungsoptionen anbieten. Dazu sind alle Benutzer-gerichteten
> Systeme, Anwendungen und Umgebungen auf eine digitale Inklusion hin zu
> erweitern.

### Übergreifendes einheitliches Bedienkonzept

> Ein UI-System ermöglicht ein einheitliches Bedienkonzept durch die
> Verwendung von wiederverwendbaren UI-Komponenten in verschiedenen
> Teilen der Software. Es enthält eine Design-Systembibliothek,
> einheitliche Design-Richtlinien und konsistente Interaktionen, um
> sicherzustellen, dass alle Benutzeroberflächen-Elemente einheitlich
> gestaltet sind. Dies spart Zeit und Ressourcen, da Entwickler und
> Designer die wiederverwendbaren Komponenten des Systems nutzen können,
> anstatt jedes Mal neue Elemente zu entwerfen. Das UI-System soll
> skalierbar und flexibel und sich leicht an veränderte Anforderungen
> anpassen können. Insgesamt verbessert es die Benutzererfahrung und
> Zufriedenheit durch ein einheitliches Bedienkonzept.

Entwicklung von Arbeitspaketen für die Transformation
-----------------------------------------------------

Die Transformation eines Informationssystems erfordert eine sorgfältige
Planung und Umsetzung von Arbeitspaketen, die sicherstellen, dass alle
benötigten Informationssystemkomponenten identifiziert werden, die den
Anforderungen der Funktionsmodellierung entsprechen, die Vorgaben des
Servicemodells und des Produktmodells erfüllen und über das
Integrationsmodell bereitgestellt werden können. In diesem Zusammenhang
ist eine genaue Analyse durchzuführen und geeignete Strategien zu
entwickeln, um die Transformation des SouvAP zu sichern. Allerdings kann
die Einbindung von OSS-Modulen auch zu Unsicherheiten führen,
insbesondere bei der Schätzung von Arbeitspaketen. Externe Faktoren wie
unerwartete Updates oder Änderungen an den OSS-Modulen können die
ursprüngliche Planung beeinträchtigen und die Schätzung von
Arbeitspaketen erschweren. Um diesem Risiko zu begegnen, empfiehlt es
sich, einen iterativen, agilen Ansatz zu wählen und die Arbeitspakete in
kleinere Schritte aufzuteilen. Dies ermöglicht es, schneller auf
Veränderungen und neue Anforderungen zu reagieren, ohne den Fokus auf
das Gesamtziel des SouvAP zu verlieren.

1.  Identifikation der Kernfunktionalitäten

    1.  Analyse der bestehenden Legacy-Software, um Kernfunktionalitäten
        zu identifizieren

    <!-- -->

    1.  Dokumentation der Kernfunktionalitäten in einem zentralen
        Dokument

<!-- -->

1.  Definition des Walking Skeletons

    1.  Analyse der Kernfunktionalitäten, um eine minimale Version der
        neuen Architektur zu definieren

    <!-- -->

    1.  Definition der Architekturkomponenten, die für das Walking
        Skeleton erforderlich sind

    <!-- -->

    1.  Dokumentation des Walking Skeletons in einem zentralen Dokument

<!-- -->

1.  Implementierung des Walking Skeletons

    1.  Implementierung der erforderlichen Architekturkomponenten für
        das Walking Skeleton

    <!-- -->

    1.  Integration des Walking Skeletons in die Legacy-Software

    <!-- -->

    1.  Tests zur Überprüfung der Funktionalität des Walking Skeletons

<!-- -->

1.  Iterative Verbesserung

    1.  Analyse der Anforderungen für die Erweiterung der Funktionalität

    <!-- -->

    1.  Implementierung der erforderlichen Erweiterungen in die
        Architektur

    <!-- -->

    1.  Tests zur Überprüfung der Funktionalität der Erweiterungen

<!-- -->

1.  Testen und Validieren

    1.  Durchführung von Tests, um sicherzustellen, dass das System den
        Anforderungen entspricht

    <!-- -->

    1.  Überprüfung der Leistung und Skalierbarkeit des Systems

    <!-- -->

    1.  Durchführung von Benutzertests, um sicherzustellen, dass das
        System für den Benutzer zugänglich und einfach zu bedienen ist

<!-- -->

1.  Schulung und Unterstützung

    1.  Schulung des Entwicklerteams in der neuen Architektur und deren
        Funktionen

    <!-- -->

    1.  Bereitstellung von Dokumentation zur Unterstützung des
        Entwicklerteams

    <!-- -->

    1.  Bereitstellung von Support und technischem Support, um
        sicherzustellen, dass das System optimal funktioniert
