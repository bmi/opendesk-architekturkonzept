Phase D: Technologiearchitektur
===============================

<table>
<tbody>
<tr class="odd">
<td><strong>Kurzbeschreibung:</strong> Die <strong>Technologiearchitektur</strong> (Phase D) beschreibt die Architekturelemente für Aufbau und Betrieb der IT-Infrastruktur. Sie definiert die Basis, auf der Anwendungen beschafft, integriert und betrieben werden können und ist somit das Fundament der technischen Realisierung.</td>
</tr>
</tbody>
</table>

Als Referenzarchitektur(en) wird eine Auswahl von weit verbreiteten
Kubernetes Distributionen festgelegt. Die Implementierung auf anderen
von der CNCF zertifizierten Kubernetes
Distributionen<a href="#fn1" id="fnref1" class="footnote-ref">1</a>
muss auch einfach möglich sein.

Betriebsumgebungen müssen vollständig automatisch erstellt werden
können. Im Sinne von DVS Rahmenplan 2.0.1 werden als Applikationen
Kubernetes Pods durch Helm Charts ausgerollt. Die Helm Charts sind als
Teil der Applikation anzusehen und müssen auch durch die CI/CD Pipeline
transportiert werden und auch auditiert werden.  
  
Kubernetes Cluster MÜSSEN automatisch erstellt werden können. Z.B. mit
Terraform oder durch andere Skripte. Die VMS oder
Hardwarekonfigurationen müssen vollautomatisch aus eine Repository
ausgerollt werden. Es DARF in Produktion KEINE Zugänge zu Kubernetes
Hosts geben. Kein Login, kein Ssh oder ähnliches.

Alle Dienste, für die ausreichend Compute, Storage und Netzwerk Resource
zur Verfügung stehen MÜSSEN initiert werden können.

Es gibt keine Backups von Hosts. Der Umgang mit Logging wird in D+S
beschrieben. Hosts MÜSSEN automatisch hinzugefügt werden können.

Schnittstellen
--------------

Schnittstellen zur Erzeugung Kubernetes Hosts MÜSSEN vollautomatisch
betreibbar sein, z.B. durch Terraform. Schnittstellen zu Kubernetes
verwenden den API Server.  
  
Es muss Container Storage
Interface<a href="#fn2" id="fnref2" class="footnote-ref">2</a> (CSI)
Schnittstellen zu mindestens einem Storage System geben um eine Standard
Storage Klasse festzulegen.

Einer oder mehrere Ingress
Controller<a href="#fn3" id="fnref3" class="footnote-ref">3</a> erlauben
einen netzwerkseitigen Zugang. Der Ingress Controller implementiert
einen Service, der auf Dienste innerhalb des Clusters verweist.

Es werden bis auf weitere Anforderungen ausschließlich Linux AMD64
Container verwendet.

DevOps Phasen
-------------

Die DevOps Phasen beschreiben den Weg einer Applikation von Source
Code bis zum Betrieb. Wir orientieren uns an diesen Phasen, auch wenn
wir kein DevOps im eigentlichen
Sinne<a href="#fn4" id="fnref4" class="footnote-ref">4</a> machen.
Die Phasen 3-8 sind weitestgehend automatisch durchzühren. Ausnahmen
sind nicht automatisierbare Tests (User Interfaces, Barrierefreiheit)
und ein manueller Trigger des Deployments Phase 6.

Tabelle: DevOps Phasen

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th>#</th>
<th>Durchführung</th>
<th>Phase</th>
<th>Hauptverantwortung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>1</td>
<td>manuell</td>
<td><p><strong>Planen</strong></p>
<p>Beschreibung der Funktionen der Anwendung</p></td>
<td>Dev</td>
</tr>
<tr class="even">
<td>2</td>
<td>manuell</td>
<td><p><strong>Codieren</strong></p>
<p>Erstellen des Programm Codes, lokale Tests</p></td>
<td>Dev</td>
</tr>
<tr class="odd">
<td>3</td>
<td>automatisch</td>
<td><p><strong>Bauen</strong></p>
<p>Unabhängiger Build aus einem Repository, Automatische Tests</p></td>
<td>Dev, Infra</td>
</tr>
<tr class="even">
<td>4</td>
<td>weitgehend automatisch</td>
<td><p><strong>Test</strong></p>
<p>Weitergehende Tests, Usability, Integrationstests</p></td>
<td>Dev, Test, Infra</td>
</tr>
<tr class="odd">
<td>5</td>
<td>automatisch</td>
<td><p><strong>Release</strong></p>
<p>Veröffentlichung aller Artefakte für den Einsatz</p></td>
<td>Dev, Ops</td>
</tr>
<tr class="even">
<td>6</td>
<td>Automatisch, manuell getriggert</td>
<td><p><strong>Deployment</strong></p>
<p>Ausrollen in Produktion, unterbrechungsfrei</p></td>
<td>Dev, Ops</td>
</tr>
<tr class="odd">
<td>7</td>
<td>automatisch</td>
<td><p><strong>Operations</strong></p>
<p>Live Betrieb</p></td>
<td>Ops</td>
</tr>
<tr class="even">
<td>8</td>
<td>automatisch</td>
<td><p><strong>Monitor</strong></p>
<p>Messung von Betriebs Kennzahlen, Last, Latenz, Bandbreite</p></td>
<td>Ops</td>
</tr>
</tbody>
</table>

### Digitale Souveränität

Oberstes Ziel bei der Technologiearchitektur ist es, souveräne
Entscheidungen für den Aufbau und Betrieb zu treffen. Es muss möglich
sein, auch komplexe Technologien stabil und schnell unabhängig vom
Betreiber zu implementieren. Dabei werden etablierte Standard
Technologien eingesetzt. Eine sicherer Betrieb MUSS einfach möglich
sein. Die Grundeinstellungen MÜSSEN bereits eine einfache Basis
Sicherheit erlauben, Security by
Default<a href="#fn5" id="fnref5" class="footnote-ref">5</a> MUSS
einfach erreicht werden
können.<a href="#fn6" id="fnref6" class="footnote-ref">6</a>

Es sollen Microservice basierte Technologien verwendet werden, für die
es Open Source Komponenten mit einer von Open CoDE akzeptierten
OSS-Lizenz gibt. Dabei sind permissive Lizenzen zu bevorzugen. Der
Einsatz von Lizenzen mit Zusatzanforderung „copyleft“ ist auf den
benötigten Sicherheitsbedarf (z.B. durch Notwendigkeit der Geheimhaltung
des Codes) abzustimmen.

Bevorzugt werden Projekte der Cloud Native Computing
Foundation<a href="#fn7" id="fnref7" class="footnote-ref">7</a>. Es
sollen die Projekte mit höheren Reifegraden bevorzugt werden, in der
Reihenfolge Graduated, Incubating vor Sandbox. Sobald Projekte
abgekündigt werden („archived“) MUSS nach einer alternativen
Implementierung gesucht werden.

Dabei ist unerlässlich, eine hohe Geschwindigkeit und eine hohe
Zuverlässigkeit gleichzeitig zu implementieren. Dieses Vorgehen verlangt
eine vollständige Automatisierung, in der nur Entscheidungen über
Releases manuell getroffen werden. In keiner Phase werden manuelle
Konfigurationen vorgenommen.

In Anbetracht der großen Zahl von Komponenten und der daraus
resultierenden möglichen Kombinationen ist ein Automatisierung
unerlässlich, sowohl im Hinblick auf die Geschwindigkeit des Rollout
Prozesses als auch der Zuverlässigkeit. Nur automatisierte
Installationen können nachvollzogen und wiederholt werden.

#### Digitale Performance Metriken

Wir folgen dabei den Definitionen aus der DevOps Welt wie den Schlüssel
Metriken zur Messung der DevOps
Performance<a href="#fn8" id="fnref8" class="footnote-ref">8</a> und dem
„2022 Accelerate State of DevOps Report“ von
Google<a href="#fn9" id="fnref9" class="footnote-ref">9</a>, weil damit
Schlüsselgrößen für die Aktivität verbunden sind. Diese MÜSSEN von
kontinuierlich erfasst und aktuell auf einem Dashboard veröffentlicht
und bewertet werden.

Alle Kennzahlen in fünf Größen zusammengezogen:

1.  Bereitstellungshäufigkeit - Wie oft ein Unternehmen oder ein Projekt
    erfolgreich für die Produktion freigibt

2.  Vorlaufzeit für Änderungen - Die Zeit, die ein Commit benötigt, um
    in Produktion zu gehen

3.  Change Failure Rate - Der Prozentsatz der Implementierungen, die zu
    einem Ausfall in der Produktion führen

4.  Mean Time to Restore Service (MTtR, Zeit bis zur Wiederherstellung
    des Dienstes) - Wie lange ein Unternehmen braucht, um sich von einem
    Ausfall in der Produktion zu erholen.

5.  Zuverlässigkeit  
    Wir folgen der Definition von SRE (Site Reliability
    Engineering)<a href="#fn10" id="fnref10" class="footnote-ref">10</a>
    und messen Verfügbarkeit, Latenz und Ressourcen Verbrauch

Der „primäre Dienst“ ist in diesem Fall der souveräne Arbeitsplatz, der
aus einer zweistelligen Zahl von Komponenten besteht. Da diese
Komponenten häufig aktualisiert werden, ist mit einer mittleren bis
hohen Änderungsrate zu rechnen.

Für die Zeit von der Übermittlung des Codes bis zum Einsatz in der
Produktion wird mit einer mittleren „Lead Time“ für Änderungsraten von
einer Woche bis einem Monat gerechnet. Um Sicherheitsupdates schnell
auszurollen, muss auch eine Änderung innerhalb eines Tages möglich sein.

Weil mit dem Ausfall des Dienstes ein hoher Reputationsverlust verbunden
ist, muss die darf Zeit für die Wiederherstellung einen Tag nicht
überschreiten.

Die Fehlerquote bei Änderungen muss sehr niedrig sein und ist durch
geeignete Maßnahmen so nah wie möglich an 0% zu drücken.

Die Metriken werden wie folgt
bewertet:<a href="#fn11" id="fnref11" class="footnote-ref">11</a>

Tabelle: Metriken für Software Auslieferung

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th><blockquote>
<p><strong>Software Auslieferungs Performance Metrik</strong></p>
</blockquote></th>
<th><blockquote>
<p><strong>Niedrig</strong></p>
</blockquote></th>
<th><blockquote>
<p><strong>Mittel</strong></p>
</blockquote></th>
<th><blockquote>
<p><strong>Hoch</strong></p>
</blockquote></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><blockquote>
<p><strong>Deployment frequency</strong></p>
<p>Wie oft wird Code für die wichtigsten Anwendungen oder den primären Dienst, an dem Sie arbeiten, für die Produktion bereitgestellt oder für Endbenutzer freigegeben?</p>
</blockquote></td>
<td>Weniger als einmal pro Monat</td>
<td>Einmal pro Woche bis einmal im Monat</td>
<td>Auf Anforderung, mehr als einmal pro Tag</td>
</tr>
<tr class="even">
<td><blockquote>
<p><strong>Lead time für Änderungen</strong></p>
<p>Wie lange dauert es bei der Hauptanwendungen oder Diensten, bis Änderungen vorgenommen werden (d. h. wie lange dauert es von der Übermittlung des Codes bis zum erfolgreichen Einsatz des Codes in der Produktion)?</p>
</blockquote></td>
<td><blockquote>
<p>Mehr als ein Monat</p>
</blockquote></td>
<td><blockquote>
<p>Eine Woche bis ein Monat</p>
</blockquote></td>
<td><blockquote>
<p>Ein Tag bis eine Woche</p>
</blockquote></td>
</tr>
<tr class="odd">
<td><blockquote>
<p><strong>Time to restore service</strong></p>
<p>Wie lange dauert es bei den Hauptanwendung oder den Hauptdiensten, wenn ein Vorfall oder ein Defekt auftritt, der sich auf die Benutzer auswirkt (z. B. ein ungeplanter Ausfall oder eine Beeinträchtigung des Dienstes)?</p>
</blockquote></td>
<td><blockquote>
<p>Eine Woche bis ein Monat</p>
</blockquote></td>
<td><blockquote>
<p>Ein Tag bis eine Woche</p>
</blockquote></td>
<td><blockquote>
<p>Weniger als ein Tag</p>
</blockquote></td>
</tr>
<tr class="even">
<td><blockquote>
<p><strong>Fehlerquote bei Änderungen</strong></p>
<p>Wie viel Prozent der Änderungen, die Sie an der primären Anwendung oder dem primären Dienst vornehmen, führen zu einer Beeinträchtigung des Dienstes (z. B. zu einer Beeinträchtigung oder einem Ausfall des Dienstes) und erfordern anschließend eine Korrektur (z. B. einen Hotfix, Rollback, Fix Forward, Patch)</p>
</blockquote></td>
<td><blockquote>
<p>46%-60%</p>
</blockquote></td>
<td><blockquote>
<p>16%-30%</p>
</blockquote></td>
<td><blockquote>
<p>0%-15%</p>
</blockquote></td>
</tr>
</tbody>
</table>

### Implementierung von Applikationen

Unter einer Applikation verstehen wir einen oder mehrere Microservices, implementiert als Container, die zusammenarbeiten.

Die Konfiguration erfolgt durch Helm Charts.

#### Versionierung

Alle Helm Charts, Container und Konfigurationen MÜSSEN eindeutig
versioniert und signiert werden.

#### Container

Container SOLLEN nach 12Factor<a href="#fn12" id="fnref12" class="footnote-ref">12</a>
Kriterien entwickelt werden. Es DÜRFEN nur die Anwendungen im
Container enthalten sein, die für den Betrieb des Containers gebraucht
werden. Es SOLL nur EIN PROZESS pro Container gestartet werden. Es
DÜRFEN keine für einen Angriff verwendbare
Werkzeuge<a href="#fn13" id="fnref13" class="footnote-ref">13</a> vorhanden sein.

Idealerweise wird dafür ein statische Binärdatei, verwendet. Auch eine
minimale
Umgebung<a href="#fn14" id="fnref14" class="footnote-ref">14</a> ist
noch erlaubt.

12Factor IX
„Disposability“<a href="#fn15" id="fnref15" class="footnote-ref">15</a>
verpflichtet zu schnellen Starts und Stops. Das schließt Prozesse mit
Just in Time (JIT) Compilern eigentlich aus. Deswegen ist der Einsatz
nur in Ausnahmefällen möglich. Vorzuziehen sind für Programmiersprachen
die auf der Java Virtual Machine beruhen die Übersetzung in Native
Executables<a href="#fn16" id="fnref16" class="footnote-ref">16</a>.
Entsprechende Frameworks sind einzusetzen. Als Obergrenze bis zum Start
eines Containers werden 60s festgelegt.

#### Helm Charts

Der Paketmanager
Helm<a href="#fn17" id="fnref17" class="footnote-ref">17</a>
verbindet die Container mit Templates und Variablen zu einer
vollständigen Applikation. Dabei werden vollständige Kubernetes Objekte
erzeugt. Der gesamte Souveräne Arbeitsplatz wird als ein
konfigurierbarer Helm Chart ausgeliefert.

Alle verwendeten Applikationen werden durch
Abhängigkeiten<a href="#fn18" id="fnref18" class="footnote-ref">18</a>
auf andere Helm Charts implementiert. Schalter legen fest, welche
Anwendungen und Features tatsächlich ausgeliefert werden.

Alle Charts MÜSSEN auch die Anbindung an die Peripherie wie Netzwerke,
Ingress, Loadbalancer, Networkpolicies, Storage, Zertifikate und
ähnliches konfigurieren.

Alle Helm Charts MÜSSEN elementare Tests vorsehen, die zeigen, ob alle
Container im Zustand READY sind.

#### Rollout Prozess

Der Prozess des Rollouts MUSS vollautomatisch nur aus Images, Charts
und Git Commits durch einen
Gitops<a href="#fn19" id="fnref19" class="footnote-ref">19</a>
Prozess geschehen. Ein Rollback MUSS möglich sein.

Alle Helm Charts MÜSSEN automatisiert ausrollbar sein.
Hooks<a href="#fn20" id="fnref20" class="footnote-ref">20</a> MÜSSEN
so eingesetzt werden, dass das Deployment durch automatisierte Tools
möglich
ist<a href="#fn21" id="fnref21" class="footnote-ref">21</a>.

##### Lizenz Compliance

Bis zum Erscheinen eines deutschen oder europäischen Rahmens wird der
Standard des US „Department of Commerce - The Minimum Elements for an
SBOM“
verwendet<a href="#fn22" id="fnref22" class="footnote-ref">22</a>.
Für jeden Container MUSS eine
SBOM<a href="#fn23" id="fnref23" class="footnote-ref">23</a> im SPDX
oder CycloneDX Format erstellt werden. SBOMs MÜSSEN signiert werden.

### Segmentierung im Schichtenmodell

Jede Anwendung ist in geeignete Schichten
aufzuteilen<a href="#fn24" id="fnref24" class="footnote-ref">24</a>.
Die Schichten bilden eine Hierarchie, vom Frontend bis zur
Persistenzschicht und dürfen nur eine Kommunikation von oben innerhalb
einer Schicht und in die unteren Schichten aufbauen.

### Netzwerk

Die Netzwerkschicht MUSS auf die Anforderungen der umgebenden virtuellen
oder on Premises Implementierung der Nodes und der aufnehmenden
Rechenzentren angepasst sein. Die Architektur MUSS zusammen zusammen mit
der Einbettung konzipiert und bewertet werden. Dies gilt für virtuelle
Umgebungen als auch im Zusammenhang mit Software Defined Networks.

Nicht betrachtet werden die Implementierung außerhalb des Kubernetes
Stacks, also Hypervisor, Hardware und Netzwerkimplementierung. Für alle
diese Komponenten SOLLEN Open Source Implementierungen verwendet werden,
soweit sie
vorliegen.<a href="#fn25" id="fnref25" class="footnote-ref">25</a>

### Implementierung

Zur Netzwerkschicht zählen alle Komponenten, die das Kubernetes Network
Modell<a href="#fn26" id="fnref26" class="footnote-ref">26</a>
implementieren.

Die Netzwerkschicht muss durch den Plattformbetreiber implementiert
werden. Dabei ist darauf zu achten, dass die Vorgaben der DVS
Zielarchitektur 2.0 Verschlüsselung vorsehen. Es gibt zahlreiche
Implementierungen<a href="#fn27" id="fnref27" class="footnote-ref">27</a>,
es SOLLEN nur solche ausgewählt werden, die CNCF graduiert sind. Von
einem Hersteller in der ganzen Deployment Chain zur Verfügung stehen.

### Datenschicht

Die Daten werden in einem oder mehreren Kubernetes Storage durch das
Container Storage Interface
(CSI)<a href="#fn28" id="fnref28" class="footnote-ref">28</a> mit einem
der Treiber
implementiert.<a href="#fn29" id="fnref29" class="footnote-ref">29</a>
Die Storageschicht muss ausreichend dimensoniert sein. Die
Dimensionierung ist durch geeignete Last Tests zu überprüfen

#### Logging

Aus dem Logging werden Analysen über den aktuellen und historischen
Zustand des Clusters und der Applikationen abgeleitet. Damit wird das
Monitoring des Betriebszustandes, Alarmierungen im Falle einer Störung
und Trendanalysen für Planungen abgeleitet. Es werden auch hier Projekte
aus dem Bereich der Cloud Native Observability and Analysis
Frameworks<a href="#fn30" id="fnref30" class="footnote-ref">30</a>
eingesetzt, z.B.
Fluentd<a href="#fn31" id="fnref31" class="footnote-ref">31</a>, der
sicherstellt, dass die Logdaten verschiedener Quellen in verschiedenen
Endpunkten gespeichert werden können.

##### Kubernetes Logs

Die Logdaten der Kubernetes Infrastruktur sind für den Betrieb
notwendig, müssen aber über die betrieblichen Erfordernisse nicht
aufbewahrt werden. Für Trendanalyse reicht eine Aggregation.

##### Application Logs

Applikationen haben u.U. individuelle Anforderungen an das Logging. Es
MUSS möglich die Logfiles verschiedener Applikationen individuell zu
trennen und in verschiedene Endpunkte zu schreiben.

##### Monitoring

Für das Monitoring sind Dashboards anzulegen, die den Betriebszustand
angemessen
visualisieren.<a href="#fn32" id="fnref32" class="footnote-ref">32</a>
Dazu gehört neben dem Ist-Zustand auch eine Visualierung der Historie
und über eine Trendanalyse eine Projektion in die Zukunft.

### Continuous Integration

Eine CI Pipeline<a href="#fn33" id="fnref33" class="footnote-ref">33</a>
für den SouvAp wird eingerichtet. Alle Quellen einschließlich der Build
Anweisungen für alle Artefakte sowie der Tests werden im OpenCoDE
Repository abgelegt.

Binäre Artefakte wie Pakete und Container Images werden in Repositories
abgelegt<a href="#fn34" id="fnref34" class="footnote-ref">34</a>. Die
Repositories müssen nicht öffentlich zugänglich sein.

![](media/file34.png)

Abbildung : Continuous Integration

Die Abbildung zeigt eine minimale Deployment Pipeline. Der weiße Pfad
definiert die Build-, Test- und Deploymentschritte zum Ausrollen einer
Applikation. Die roten Schritte definieren zusätzliche
Sicherheitsprüfungen, die nicht in jedem Commit durchlaufen werden
müssen, aber nächtlich oder minimal wöchentlich ausgeführt werden
müssen.

#### Verteilung der Pipeline

Die Pipeline ist nicht als eine einheitliche Applikation zu verstehen,
die an einem zentralen Ort abläuft. Die Übergabepunkte werden durch
Trigger
angesprochen<a href="#fn35" id="fnref35" class="footnote-ref">35</a>,
wie sie z.B. OpenCode.de und jede Git Implementierung über Web
Hooks<a href="#fn36" id="fnref36" class="footnote-ref">36</a> auslösen
kann. Sind Web Hooks aus Sicherheitsgründen nicht möglich, treten
periodische Pull Prozesse an ihre Stelle.

Es gibt mindestens Prod/Staging/Dev. Darüber hinaus können weitere
Umgebungen aufgebaut werden, z.B. für Last Tests. Die
Verantwortlichkeiten und Übergabepunkte sind noch zu definieren.

Bei Übergabe sind Signierte Container und Helm Charts mit eindeutiger
Versionierung zu übergeben.

#### Freigaben

Technisch werden Freigaben über einen Git Commit realisiert. Durch Web
Hooks können beliebige Prozesse angestoßen werden.

#### GitOps Rollout

Für das Rollout der Applikation wird ein GitOps
Prozess<a href="#fn37" id="fnref37" class="footnote-ref">37</a>
verwendet. Die Zielkonfiguration wird in einem Git Repository beim
Betreiber erfasst. Wenn durch einen neuen Commit im Repository eine
Abweichung entsteht, wird automatisch eine Synchronisation den Ziel- auf
den Ist-Zustand durchgeführt.

Das Rollout der Konfiguration erfolgt durch eine Web-Anwendung, die
diesen Prozess
visualisiert<a href="#fn38" id="fnref38" class="footnote-ref">38</a>.
Ein Rollback muss möglich sein.

### Modularität

Die Phasen 1-7 MÜSSEN durch die Entwicklung abgedeckt werden.

Alle Beteiligten im Projekt müssen in die Lage versetzt werden, alle
Phasen des Projektes in eigener Souveränität durchzuführen. Für die
automatischen Schritte ist es notwendig, dass neben dem Programmcode
auch Bauprozesse und die Elemente der Build Pipeline offengelegt wird.
Dazu gehören

1.  Build Scripte

2.  Bauanleitungen für die Container

    1.  Dockerfiles

    2.  oder ähnliches

3.  Härtungsskripte

4.  Test Suiten<a href="#fn39" id="fnref39" class="footnote-ref">39</a>

5.  Deployment

    1.  Basis Funktionalität

    2.  Spezielle Anforderungen wie Tests mit Netzwerk oder Storage
        Schichten.

Es ist nicht notwendig, dass Test Suites in beliebiger Tiefe ausgeführt
werden. Für die Entwicklung durch Zendis wird eine minimale Pipeline
realisiert. Das Deployment wird nur in einer Basisvariante durchgeführt,
nicht mit z.B speziellen Netzwerkschichten. Diese können auf Anforderung
durch Erweiterung der Beauftragung hinzu genommen werden. Gleiches gilt
für Lasttests.

#### Registry

Es MUSS eine Registry für die Container Images verwendet werden.

### Föderierung

Eine Föderierung von Diensten über Cluster Grenzen muss möglich sein,
das bedeutet nicht eine Föderierung von Clustern (siehe D+S).

Phase D+S: Sicherheitsarchitektur
---------------------------------

<table>
<tbody>
<tr class="odd">
<td><strong>Kurzbeschreibung:</strong> Die <strong>Sicherheitsarchitektur</strong> (Phase D+S) beschreibt die Sicherheitsmassnahmen für Aufbau und Betrieb der IT-Infrastruktur. Sie definiert die Anforderungen und die Infrastruktur, auf der Anwendungen von den Betreibern abgesichert werden müssen sowie die Anforderungen an die Applikationen um sicher betreibbar zu sein.</td>
</tr>
</tbody>
</table>

Eine wesentliche Voraussetzung für Souveränität ist Sicherheit. Unter
Sicherheit werden viele Aspekte zusammengefasst, die im einzelnen
detailliert ausgeführt werden.

### DevSecOps Phasen und Sicherheitsaspekte

Um aus einem DevOps Ansatz einen DevSecOps Prozess zu machen, orientiert
sich an den Prozessen von Clound Native Computing Foundation (CNCF) und
dem US Department of Defence (US
DoD)<a href="#fn40" id="fnref40" class="footnote-ref">40</a>,
insbesondere an den DoD Enterprise DevSecOps
Fundamentals<a href="#fn41" id="fnref41" class="footnote-ref">41</a>.

Hinzu kommen 14 Sicherheitsaspekte in den verschiedenen Phasen. Ziel
aller Phasen ist zu jeder Zeit und in jedem Cluster nur bekannte
Artefakte und Konfigurationen zu haben.

Tabelle 1: DevSecOps Phasen und Sicherheitsaspekte

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>#</td>
<td>Durchführung</td>
<td>Phase</td>
<td>Hauptverantwortung</td>
</tr>
<tr class="even">
<td>1</td>
<td>manuell</td>
<td><p><strong>Planen</strong></p>
<p>Bedrohungsanalyse für jede Komponente</p></td>
<td>Dev, Sec</td>
</tr>
<tr class="odd">
<td>2</td>
<td>manuell</td>
<td><p><strong>Codieren</strong></p>
<p>Secure Coding<a href="#fn42" id="fnref42" class="footnote-ref">42</a>, den Code nach sicheren Prinzipien bauen</p></td>
<td>Dev</td>
</tr>
<tr class="even">
<td>2S</td>
<td>automatisch</td>
<td>Security as Code: Automatisierung von Scans und Sicherheitstests, Ausführen von Skripttests, Implementierung von Überwachungsfunktionen, Durchführung von routinemäßigen Prüfungen gegen die Sicherheitsrichtlinien</td>
<td>Dev</td>
</tr>
<tr class="odd">
<td>3</td>
<td>automatisch</td>
<td><strong>Bauen</strong></td>
<td>Dev, Infra</td>
</tr>
<tr class="even">
<td>3S</td>
<td>automatisch</td>
<td><p><strong>Static Application Security Testing (SAST)</strong></p>
<p>Aufdecken von Speicherlecks, Cross-Site-Scripting (XSS), SQL-Injection, Authentifizierung und Fehlkonfigurationen der Zugriffskontrolle und der meisten OWASP Sicherheits probleme<a href="#fn43" id="fnref43" class="footnote-ref">43</a></p></td>
<td>Dev</td>
</tr>
<tr class="odd">
<td>3D</td>
<td>automatisch</td>
<td><p><strong>Dynamic Application Security Testing (DAST)</strong></p>
<p>Laufzeitanalyse innerhalb der Systemumgebung, Verschlüsselungsalgorithmen von außen brechen, Berechtigungsanalyse zur Überprüfung der Isolierung der Berechtigungsstufen, Prüfung auf Cross-Site-Scripting (XSS), SQL-Injection und andere Software-Sicherheitsschwachstellen, Sicherheitslücken in Schnittstellen von Drittanbietern, Aufzeichnung für Post-Mortem-Testfehleranalyse auf, schwere Anwendungsfehler ab.</p></td>
<td>Test, Sec</td>
</tr>
<tr class="even">
<td>4</td>
<td>weitgehend automatisch</td>
<td><strong>Test</strong></td>
<td>Dev, Test, Infra</td>
</tr>
<tr class="odd">
<td>4P</td>
<td>manuell</td>
<td><p><strong>Penetration Testing</strong></p>
<p>Manuelle Tool unterstützte Schwachstellenanalyse an laufenden Systemen</p></td>
<td>Sec</td>
</tr>
<tr class="even">
<td>4S</td>
<td>automatisch</td>
<td><p><strong>Digitales Signieren</strong></p>
<p>Automatisiertes Signieren der Artefakte, garantiert das nur Software eingesetzt wird, die aus einer sicheren Build Umgebung kommt und dass keine Artefakte signiert werden, die nicht bekannt sind.</p>
<p>Der Signierprozess MUSS in einer abgesichtern Netzwerkumgebung stattfinden.</p></td>
<td>Infa</td>
</tr>
<tr class="odd">
<td>5</td>
<td>automatisch</td>
<td><strong>Release</strong></td>
<td>Dev, Ops</td>
</tr>
<tr class="even">
<td>5D</td>
<td>automatisch</td>
<td><p><strong>Deliver</strong></p>
<p>Es muss ein sicher Weg der Auslieferung gewählt werden. Zusammen mit Signaturen ist das durch den Einsatz eine Managementservers<a href="#fn44" id="fnref44" class="footnote-ref">44</a> gewärhleistet. Signiert werden Images, Git Konfigurationen und Helm Charts (als Images oder durch Git)</p></td>
<td>Dev, Ops, Sec</td>
</tr>
<tr class="odd">
<td>6</td>
<td>Automatisch, manuell getriggert</td>
<td><p><strong>Deployment</strong></p>
<p>Die Konfigurationen werden aus einem revisionssicheren Git Repository ausgerollt. Die Git Signaturen werden beim Ausrollen geprüft. Die Signaturen der Images werden in der Registry überprüft.</p></td>
<td>Dev, Ops</td>
</tr>
<tr class="even">
<td>6S</td>
<td>automatisch</td>
<td><p><strong>Security Scans</strong></p>
<p>Container Images und die Konfiguration werde auf Schwachstellen geprüft. Die Images in der Registry<a href="#fn45" id="fnref45" class="footnote-ref">45</a>, die Helm Charts im Git Repository<a href="#fn46" id="fnref46" class="footnote-ref">46</a>.</p></td>
<td>Ops, Sec</td>
</tr>
<tr class="odd">
<td>7</td>
<td>automatisch</td>
<td><strong>Operations</strong></td>
<td>Ops</td>
</tr>
<tr class="even">
<td>7P</td>
<td>automatisch</td>
<td><p><strong>Patches</strong></p>
<p>Im live Betrieb werden Sicherheitsupdates unterbrechungsfrei und zeitnah durchgeführt.</p></td>
<td></td>
</tr>
<tr class="odd">
<td>7A</td>
<td>manuell</td>
<td><p><strong>Security Audits</strong></p>
<p>Die Cluster werden nach den vorgegebenen Standards auditiert. Dazu werden Audit Logs außerhalb des Clusters gespeichert<a href="#fn47" id="fnref47" class="footnote-ref">47</a>.</p></td>
<td>Sec</td>
</tr>
<tr class="even">
<td>8</td>
<td>automatisch</td>
<td><strong>Monitor</strong></td>
<td>Ops</td>
</tr>
<tr class="odd">
<td>8M</td>
<td>automatisch</td>
<td><p><strong>Security Monitor</strong></p>
<p>Während des Betriebs werden die Cluster und die laufenden Applikationen und Konfigurationen auch regelmässig gescannt. Die Ergebnisse der Scans werden geloggt und mit Monitoring und Alerting überwacht.</p></td>
<td></td>
</tr>
<tr class="even">
<td>8A</td>
<td>manuell</td>
<td><p><strong>Security Analysis</strong></p>
<p>Die Ergebnisse der automatischen Schritte werden gesammelt und bewertet. Unvermeidliche Schwachstellen können durch Ausnahmelisten zugelassen werden.<a href="#fn48" id="fnref48" class="footnote-ref">48</a></p></td>
<td></td>
</tr>
</tbody>
</table>

#### Signaturen

Dadurch dass sie signiert sind, und die Signaturen geprüft werden, kann
garantiert werden, dass es keine ungewünschten Programme und
Konfigurationen im System gibt. Die Erfassung der Signaturen in einem
Kontobuch garantiert zudem, dass keine Artefakte im Umlauf sind, die
eine Signatur tragen und nicht legitimiert sind. Dazu ist der Betrieb
eines oder mehrere souveränen Kontobücher
notwendig<a href="#fn49" id="fnref49" class="footnote-ref">49</a>. Ein
öffentliches Kontobuch erfasst alle für den souveränen Arbeitsplatz aus
Open Source Komponenten erzeugten Signaturen. Für Erweiterungen in einem
nichtöffentlichen Prozess ist der Betrieb eines privaten Kontobuches
innerhalb der jeweiligen Organisation notwendig.

#### Metriken für die Umsetzung des DevSecOps Prozesses

Das „DevSecOps Fundamental Playbook des
US-DoD“<a href="#fn50" id="fnref50" class="footnote-ref">50</a>
empfiehlt die Übernahme der Google Metriken statt eines Reife
Modells<a href="#fn51" id="fnref51" class="footnote-ref">51</a> wie es
bereits in Kapitel D eingeführt wird.

### Tiefe Sicherheit

Es werden gestaffelte Schutzmassnahmen, „Security in Depth“ verwendet.
Selbst wenn die Applikation im Container verwundbar ist, wird ein
Ausbruch in das System durch andere Massnahmen wie die Container Runtime
oder Networkpolicies verhindert.

Das Konzept zieht sich durch alle Aspekte. Eine Sicherheitslücke in
einem Aspekt muss durch andere Aspekte aufgehalten werden. Wenn z.B.
eine Library in einer Applikation verwundbar ist, muss es unmöglich
gemacht werden, diese Lücke auszunutzen. Es dürfen weder Tools für
Laterale Angriffe im Container vorhanden sein noch dürfen Dienste
erreichbar sein, die der Container nicht braucht.

Es MÜSSEN alle Sicherheitsvorkehrungen getroffen werden, die
Kubernetes
mitbringt<a href="#fn52" id="fnref52" class="footnote-ref">52</a>
und die die eingesetzten Produkte erlauben. Es auf der Applikationen,
diese Einstellungen zu definieren. Im Sinne des DevSecOps Prozesses
werden diese Einstellungen auditiert und bewertet.

### Applikationen

#### Sicherer Auslieferung und Betrieb von Applikationen

Eine Applikation ist nur dann sicher betreibbar wenn sie einer
eindeutig nachvollziehbare Lieferkette stammt. Neben der sicheren
Grundkonfiguration beinhaltet das eine Versionierung einer
Applikation. Für alle Applikationen ist eine SBOM zu liefern, für alle
Container mit eine nachvollziehbaren Liste aller verwendeten Libraries.
Es DÜRFEN KEINE Libraries enthalten sein, die nicht von der SBOM erfasst
sind.

##### Container

Es werden gestaffelte Schutzmassnahmen, „Security in Depth“ verwendet.
Selbst wenn die Applikation im Container verwundbar ist, wird ein
Ausbruch in das System durch andere Massnahmen wie die Container Runtime
oder Networkpolicies verhindert. Wenn Container mit mehreren Prozessen,
also ähnlich wie virtuelle Maschinen betrieben werden
müssen<a href="#fn53" id="fnref53" class="footnote-ref">53</a>, ist
eine gesonderte Betrachtung durchzuführen.

Es dürfen keine persistenten Daten in Containern gehalten werden.
Sollen Datenbanken betrieben werden, muss die Speicherung auf externem
Storage erfolgen.

Es sind grundsätzlich Datenbanken in Clustern auf verschiedenen Nodes
verteilt zu verwenden. Jeder Container ist als vergänglich (ephemeral)
zu betrachten und kann durch den Betrieb jederzeit ohne Ankündigung
beendet werden. Alle davon abhängigen Dienste MÜSSEN dessen ungeachtet
unterbrechungsfrei weiterlaufen, es gibt grundsätzlich keine
Wartungszeiten.

Eine Zusammenfassung findet sich im verpflichtenden BSI Standard SYS
1.6.

##### Stateless Containers

Es dürfen nur zustandslose Container verwendet werden. Das schliesst
Datenbanken im Cluster mit ein, sofern sie sich auf Storage abstützen
und vollautomatisch Backup-Restore fähig sind. Der Zyklus ist
automatisiert zu testen.

##### Container Images

Es SOLLEN, ab Q1 2024 MÜSSEN nur gehärtete Container ausgerollt werden
mit idealerweise statisch gelinkten
Binaries<a href="#fn54" id="fnref54" class="footnote-ref">54</a>.
Container SOLLEN, ab Q1 2024 DÜRFEN im Gegensatz zur Praxis auf
Dockerhub oder auch bei Red Hat
<a href="#fn55" id="fnref55" class="footnote-ref">55</a>KEINE
Betriebssystem Komponenten enthalten (curl, wget, aber auch echo, cat)  
Ausnahmen sind zu vermeiden und MÜSSEN unter Vorlage einer Risikoanalyse
genehmigt werden. Dabei sind die Security Scans der Container mit
einzubeziehen.

##### Nutzungstypen

Der Nutzungstype eines Cluster hängt von der Bedrohungsanalyse der
Anwendungen ab. Wenn nur eigener Code verwendet und ausgeschlossen
werden kann, das Sidechannel Attacks durchgeführt werden können, DARF
Hardware geteilt werden (Shared).  
Sobald laut Risiko Analyse Code verwendet wird, der nicht
vertrauenswürdig ist, MUSS die Isolation auf eigenen Hardware Nodes
erfolgen., nicht nur VMs als NodePool sondern auf einem eigenem Cluster.

##### Helm Charts

Helm Charts implementieren „Configuration as Code“ und sind als Teil
des Lieferprozesses auf Sicherheitslücken zu überprüfen, zu auditieren
und zu scannen.

Die Charts DÜRFEN KEINE Secrets enthalten. Secrets werden immer
gesondert ausgerollt. In den Helm Charts werden die Namen der Secrets
referenziert.

##### Secret Management

Ein geeignetes Secret Management ist zu etablieren.

##### Git Repository

Es wird von Kerckhoffs’
Prinzip<a href="#fn56" id="fnref56" class="footnote-ref">56</a>
ausgegangen. Alle Konfigurationen, Charts, Build Umgebungen müssen im
OpenCode Repository des Bundes öffentlich zur Verfügung stehen.

Alle Secrets, Passwörter, Credentials DÜRFEN NICHT öffentlich sein und
MÜSSEN auf den Betreiber beschränkt werden.

Alle Git Commits MÜSSEN
signiert<a href="#fn57" id="fnref57" class="footnote-ref">57</a>
werden, die Signaturen MÜSSEN vor der Weiterverarbeitung geprüft
werden.

##### Rollout Prozess

Alle Artefakte für den Rollout müssen signiert werden. Das betrifft
Images, Charts und Git Commits.

##### SBOM

Die SBOM muss automatisch auf Verwundbarkeiten überprüft werden.

#### Segmentierung im Schichtenmodell

Die Isolation im Schichtenmodell ist durch
Microsegmentation<a href="#fn58" id="fnref58" class="footnote-ref">58</a>
auf der Netzwerkschicht zu implementieren.

#### Frontend

Frontend Anwendungen sind von außen erreichbar. Aus diesem Grund MÜSSEN
sie besonders gehärtet werden.

#### Netzwerk

Die Sicherheit der Netzwerkschicht MUSS zusammen zusammen mit der
Umgebung implementiert werden. Es ist von aussen durchgehend HTTPS zu
verwenden. Sie muss an die Anforderungen der umgebenden virtuellen oder
on Premises Implementierung der Nodes und der aufnehmenden Rechenzentren
angepasst sein. Die Architektur MUSS zusammen zusammen mit der
Einbettung konzipiert und bewertet werden. Dies gilt für virtuelle
Umgebungen als auch im Zusammenhang mit Software Defined Networks.

![](media/file35.png)

Abbildung :Netzwerk

Nicht betrachtet werden die Implementierung außerhalb des Kubernetes
Stacks, also Hypervisor, Hardware und Netzwerkimplementierung. Für alle
diese Komponenten SOLLEN Open Source Implementierungen verwendet werden,
soweit sie
vorliegen.<a href="#fn59" id="fnref59" class="footnote-ref">59</a>

Die Loadbalancer sind außerhalb des Clusters. Die Netzwerkschicht MUSS
Microsegmenation durch Networkpolicies oder im Falle des Einsatzes von
ServiceMeshs eine vergleichbare Isolation erlauben.

Eingehende Anfragen werden in der Ingress
Schicht<a href="#fn60" id="fnref60" class="footnote-ref">60</a>
autorisiert und authentifiziert. Dazu ist eine Anbindung an das ein
Identity and Access Management System notwendig. Auch ein zukünftiges
API Gateway wird in diese Schicht
integriert<a href="#fn61" id="fnref61" class="footnote-ref">61</a>.

##### Verschlüsselung auf der Netzwerkschicht

Verschlüsselung auf der Netzwerkschicht ist eine Vorgabe aus der DVS
Zielarchiektur. Aus diesem Grunde MUSS ein Produkt ausgewählt
werden<a href="#fn62" id="fnref62" class="footnote-ref">62</a>, dass
diese Eigenschaft auf eine Weise implementiert, die nicht umgangen
werden kann<a href="#fn63" id="fnref63" class="footnote-ref">63</a>. Das
kann durch VPN Tunnel oder mTLS Proxies in Service Meshs implementiert
werden.

Der Standard für VPN Tunnel unter Kubernetes ist
Wireguard<a href="#fn64" id="fnref64" class="footnote-ref">64</a>.
Sobald eine Implementierung von Post Quantum Key Exchange mit Wireguard
unter Kubernetes vorliegt, MUSS sie eingesetzt
werden<a href="#fn65" id="fnref65" class="footnote-ref">65</a>.

##### Servicemesh

Ein Servicemesh<a href="#fn66" id="fnref66" class="footnote-ref">66</a>
KANN eingesetzt werden, um die Vorgaben zu
zentralisieren<a href="#fn67" id="fnref67" class="footnote-ref">67</a>.
Der Einsatz stellt einen tiefen Eingriff in die Netzwerkarchitektur und
-sicherheit dar.

Bei Einsatz eines Servicemeshs MUSS die Mikrosegmentierung mit den
Mitteln des Servicemeshs umgesetzt werden. Die Implementierung muss
einer eigenen Sicherheitsbetrachtung unterzogen werden. Es ist darauf
dazu zu achten, dass das Sidecar
Pattern<a href="#fn68" id="fnref68" class="footnote-ref">68</a> alleine
nicht geeignet ist, eine ausreichende Trennung zwischen
Netzwerksegmentierung und Applikation zu
gewährleisten<a href="#fn69" id="fnref69" class="footnote-ref">69</a>.
Es MUSS eine Implementierung über das Container Network Interface
(CNI)<a href="#fn70" id="fnref70" class="footnote-ref">70</a>, oder bei
entsprechender Reife, über das Service Mesh Interface
(SMI)<a href="#fn71" id="fnref71" class="footnote-ref">71</a> gewählt
werden.

##### Verschlüsselung

Es ist Aufgabe der Plattformbetreiber, nachzuweisen, dass die Vorgabe
zur Hoheit über Krypto-Module und –Schlüssel eingehalten
wird.<a href="#fn72" id="fnref72" class="footnote-ref">72</a>

##### Isolation P-A-P

Es eine Kombination von Paketfiltern und Web-Applikationsfiltern (WAF)
einzusetzen. Die erste Schicht der Paketfilterung wird bereits vom
Loadbalancer implementiert, die WAF SOLLTE in die Ingressschicht
integriert
werden.<a href="#fn73" id="fnref73" class="footnote-ref">73</a>
Real-Time Audio Video Kommunikation muss gesondert betrachtet werden.

##### Microsegmentation

Die zweite Paketfilterschicht MUSS durch die Basis
Netzwerkimplementierung in die Beziehungen der Applikationen eingreifen
und darf nur notwendige Verbindungen zulassen. Im Sinne einer minimalen
Rechtevergabe<a href="#fn74" id="fnref74" class="footnote-ref">74</a>
MUSS durch Massnahmen der Netzwerkschicht sichergestellt werden, dass
sich nur Anwendungen miteinander verbinden können, die diesen Kontakt
benötigen. Die Sicherstellung durch Networkpolicies oder entsprechende
Service Mesh Regeln sind Teil der Applikation und durch einen Helm Chart
auszuliefern. Um zu erschweren, dass die Regeln durch Applikationen
ausser Kraft gesetzt werden können, MUSS eine Implementierung über das
CNI oder CMI erfolgen. Sidecars, die durch Admission
Controller<a href="#fn75" id="fnref75" class="footnote-ref">75</a>
erzeugt werden, können umgangen werden und sind als nicht ausreichend
anzusehen.

##### Verschlüsselung der Datenschicht (Encryption on Rest)

Es SOLLTE eine Implementierung mit
Encryption<a href="#fn76" id="fnref76" class="footnote-ref">76</a> on
Rest eingesetzt werden, sobald für diese Implementierung allgemein
verfügbar ist.<a href="#fn77" id="fnref77" class="footnote-ref">77</a>
Dies gilt besonders für Umgebungen mit hohem Schutzbedarf.

##### Datenbanken

Datenbanken in Kubernetes MÜSSEN grundsätzlich als verteilter Datenbank
Cluster aufgesetzt
werden.<a href="#fn78" id="fnref78" class="footnote-ref">78</a> Einzelne
Datenbank-Nodes sind ephemeral, dafür MUSS die Synchronisation überwacht
werden. Datenbank Cluster MÜSSEN so dimensioniert werden, dass die Last
durch das Aufsynchronisieren eines neuen Nodes nach Ausfall eines Nodes
zu keinen Performance Einschränkungen führen
kann.<a href="#fn79" id="fnref79" class="footnote-ref">79</a>

##### Backup-Restore

Für die Persistenzschicht MUSS ein vollständiger Backup-Restore Zyklus
aufgesetzt
werden.<a href="#fn80" id="fnref80" class="footnote-ref">80</a> Die
Restore Fähigkeit der Daten ist automatisiert regelmäßig, mindestens
wöchentlich, zu
testen.<a href="#fn81" id="fnref81" class="footnote-ref">81</a> Der
Backup Prozess DARF die Daten NICHT im gleichen Storage System ablegen
auf dem die Daten lagern. Es ist zu prüfen, ob weitere Anforderungen an
Wiederherstellung nach Katastrophen eingehalten werden müssen.

Die Daten müssen verschlüsselt gespeichert werden. Der Schlüssel muss
separat von den Daten abgelegt werden.

##### Logging

Für alle Arten von Logging müssen angemessene Aufbewahrungsfristen
definiert werden.

###### Audit Logs

Kubernetes Audit
Logs<a href="#fn82" id="fnref82" class="footnote-ref">82</a> dienen der
Aufzeichnung sicherheitsrelevanter Änderungen am Cluster. Sie MÜSSEN
außerhalb des Clusters in einer eigenen Datenbank gespeichert werden.
Sie erfassen die Daten des Betriebs, sowohl der Änderungen die
automatisch durchgeführt werden als auch die durch Betriebspersonal
veranlassten Änderungen des Clusterzustandes. Sie werden für Sicherheits
Audits herangezogen.

Die Erfassung der Daten des Betriebspersonals kann die Zustimmung der
Personalvertretung erfordern. Es eine geeignete Audit Log
Policy<a href="#fn83" id="fnref83" class="footnote-ref">83</a> definiert
werden.

###### Application Logs

Es DÜRFEN KEINE sicherheitsrelevanten Informationen in den Logs erfasst
werden, auch nicht während einer Fehleranalyse.

###### Monitoring

Für das Monitoring sind Dashboards anzulegen, die den Betriebszustand
angemessen
visualisieren<a href="#fn84" id="fnref84" class="footnote-ref">84</a>.

##### Alerting

Aus dem Monitoring System MÜSSEN Alarme abgeleitet werden, die im Fall
einer Störung das Betriebspersonal benachrichtigen.

##### Management Cluster

Für die Steuerung der GitOps Prozess MUSS ein separater Management
Cluster verwendet werden. Der Cluster steuert die Arbeitscluster und hat
deshalb eine hohe Vertrauensstellung. Auf dem Management Cluster darf
keine andere Arbeitslast installiert sein, um Supply Chain Attacks zu
verhindern<a href="#fn85" id="fnref85" class="footnote-ref">85</a>.

Es wird eine Verbindung zu einer Registry und einem Git Repository
benötigt. Eine Installation z.B. mit Harbor, Gitea und
ArgoCD<a href="#fn86" id="fnref86" class="footnote-ref">86</a> kann auch
so eingerichtet werden, dass Produktionssysteme ohne Verbindung zum
Internet betrieben werden können, z.B. als Container Proxy
Cache<a href="#fn87" id="fnref87" class="footnote-ref">87</a> und Git
Mirror<a href="#fn88" id="fnref88" class="footnote-ref">88</a>. Die
Komponenten können durch andere Produkte ersetzt werden.

Der Management Cluster DARF NICHT von den Produktionsclustern gesteuert
werden. Er MUSS so eingerichtet werden, dass nur signierte und
überprüfte Artefakte ausgerollt werden können.

Er braucht nicht permanent zu Verfügung stehen, wenn eine andere
Registry verwendet wird.

Alle Logging Anforderungen für Kubernetes Cluster gelten auch für den
Management Cluster. Insbesondere die Ergebnisse der Vulnerability Scans
MÜSSEN zeitnah ausgewertet werden und in angemessene Reaktionen von
Updates bis zum Abschalten der Applikationen umgesetzt werden.

#### Continuous Integration

Binäre Artefakte wie Pakete und Container Images werden in Repositories
abgelegt<a href="#fn89" id="fnref89" class="footnote-ref">89</a>. Die
Repositories müssen nicht öffentlich zugänglich sein. Die Pakete und
Images sind mit
GPG<a href="#fn90" id="fnref90" class="footnote-ref">90</a> und
Sigstore<a href="#fn91" id="fnref91" class="footnote-ref">91</a> zu
signieren.

1.  Die Artefakte MÜSSEN auf einem Build Server aus dem Source Code
    erzeugt und paketetiert werden.

<!-- -->

1.  Der Source Code MUSS auf Verwundbarkeiten gescannt werden.

<!-- -->

1.  Die Berichte dazu MÜSSEN vorgelegt werden.

<!-- -->

1.  Eine Software Bill of Material MUSS erzeugt werden.

<!-- -->

1.  Die Container müssen aus den Paketen erzeugt, signiert und in die
    Container Registry geladen werden.

<!-- -->

1.  Ein Security Scanner MUSS die Container auf Verwundbarkeiten
    untersuchen.

<!-- -->

1.  Die Container werden auf einem Testcluster mit einer
    Testkonfiguration ausgerollt und automatisch und manuell getestet.

<!-- -->

1.  Auf dem Testcluster MUSS ein vollständiger Integrationstest
    durchgeführt werden.

<!-- -->

1.  Die Freigabe erfolgt über eine weitere Signatur, die in einer
    geeigneten abgeschotteten Umgebung durchgeführt werden MUSS.

<!-- -->

1.  Zusätzlich werden weitere Sicherheitstests an der Applikation
    durchgeführt<a href="#fn92" id="fnref92" class="footnote-ref">92</a>.

Die Cluster werden permanent durch geeignete Scanner auf
Sicherheitslücken und Fehlkonfigurationen
geprüft<a href="#fn93" id="fnref93" class="footnote-ref">93</a>.

##### Registry

In der Registry MÜSSEN alle Container auf Sicherheitslücken überprüft
werden.<a href="#fn94" id="fnref94" class="footnote-ref">94</a> Ein
Schwachstellenmanagement und ein automatisierter Export von CVEs MUSS
möglich sein.

##### Scannen

Die Überprüfung muss mindestens täglich gegen die Common Vulnerabilities
and Exposure
(CVE)<a href="#fn95" id="fnref95" class="footnote-ref">95</a> Datenbank
erfolgen, die Ergebnisse der Scans sind zu loggen und bei Auffinden
einer Sicherheitslücke ist ein Alarm auszulösen. Als Scanner kommen
verschiedene Scanner in
Frage.<a href="#fn96" id="fnref96" class="footnote-ref">96</a>

#### Policies

Es ist zu erwarten, dass im Falle einer Sicherheitslücke das Abschalten
eines unsicheren Containers nicht ohne weiteres möglich ist. Für diesen
Fall muss ein Prozess aufgesetzt werden, der eine Entscheidung
vorbereitet, wann

1.  Entscheidung Weiterbetrieb eines verwundbaren Containers oder
    Abschalten

<!-- -->

1.  Anstoßen eines Bug Fix Prozesses

<!-- -->

1.  Ausrollen der gefixten Version

Die Aktionen sind angemessen zu dokumentieren.

#### Signieren

Container MÜSSEN signiert werden. Der Signaturprozesse MUSS in einer
sicheren Umgebung erfolgen. Schlüssellängen MÜSSEN den BSI
Vorgaben<a href="#fn97" id="fnref97" class="footnote-ref">97</a> für
kryptographische Verfahren entsprechen.

Alle Container, Helm Charts und Git Konfigurationen, von denen ein
Deployment abhängt, MÜSSEN signiert sein. Der Prozess MUSS so
aufgesetzt, dass in der CI/CD Pipeline keine Container, Helm Charts und
Konfigurationen zum Deployment eingesetzt werden, die nicht signiert
werden.

1.  Die Registry muss die Signaturen der Container überprüfen.

<!-- -->

1.  Deployment Tools müssen die Signaturen der Helm Charts und des Git
    Commits prüfen.

<!-- -->

1.  Es dürfen nur Kubernetes Versionen verwendet werden, die sich so
    konfigurieren lassen, dass nur signierte Container zu Ausführung
    kommen. Das geschieht idealerweise durch die Kubernetes
    Runtime<a href="#fn98" id="fnref98" class="footnote-ref">98</a>.
    Wird ein
    Admission-Controller<a href="#fn99" id="fnref99" class="footnote-ref">99</a>
    eingesetzt, ist er so zu konfigurieren, dass keine unsignierten
    Container im ganzen Cluster eingesetzt werden.

##### Keyless Signature

Es MUSS eine Infrastruktur für Keyless
Signing<a href="#fn100" id="fnref100" class="footnote-ref">100</a> mit
einer Certificate Authority (CA) aufgesetzt werden, die Keyless Signing
unterstützt<a href="#fn101" id="fnref101" class="footnote-ref">101</a>.
Über die Signaturen ist ein Kontobuch zu führen, dass die Signaturen
aller Images
erfasst<a href="#fn102" id="fnref102" class="footnote-ref">102</a>.
Insbesondere ist die zeitliche Gültigkeit der Schlüssel zu überprüfen.

Produktions Images MÜSSEN mit dieser Infrastruktur signiert werden.

#### Sicherheit

Es wird „Defense in Depth“
durchgeführt<a href="#fn103" id="fnref103" class="footnote-ref">103</a>.
Das bedeutet, dass verschiedene Sicherheitsmaßnahmen eingeführt werden,
die sich ergänzen. Die Sicherheit des Gesamtsystems muss gewährleistet
sein, auch wenn einzelne Teile kompromittiert sind.

Im Einzelnen müssen die folgenden Maßnahmen ergriffen werden.

##### Container

Es MÜSSEN minimale, signierte Container wie oben definiert verwendet
werden.

##### Microsegmentierung durch Networkpolicies

Die Container MÜSSEN durch Networkpolicies wie oben definiert isoliert
werden.

#### Secret Management

Ein eigenes Secret Management ist einzuführen, um Credentials unabhängig
von Git Repositories aufbewahren zu können.

#### Kubernetes-spezifische Sicherheitsmaßnahmen

In den Clustern muss der Zugriff von Anwendungen auf Host, Netz und
Cluster Ressourcen auf das notwendige Maß beschränkt werden. Die
aktuellen CIS und Nist Standards sind einzuhalten, eine aktuelle
Übersicht<a href="#fn104" id="fnref104" class="footnote-ref">104</a>
verweist auf die weitergehenden Sicherheitsmassnahmen.

Darüber ist die Architektur so auszulegen, dass privilegierte Container
keinen Zugang vom und zum Internet haben
dürfen.<a href="#fn105" id="fnref105" class="footnote-ref">105</a>

#### Netzwerk Zonen

Innerhalb des Clusters MÜSSEN Schichten und Namespaces durch
Microsegmentation isoliert sein. Egress Traffic MUSS auch über einen
Loadbalancer/Firewall geleitet werden.

Die Frontend Netze können bei Vorliegen einer entsprechenden
Risikoanalyse auf gemeinsamer Hardware betrieben werden. Für höhere
Sicherheitsanforderungen MUSS die Loadbalancer Hardware getrennt werden.

Die Netze der Hosts werden aufgeteilt. Es können dafür mehrere Ingress
Controller<a href="#fn106" id="fnref106" class="footnote-ref">106</a>
eingerichtet werden, die nur aus jeweils einer Zone erreichbar sind.

1.  Externe Frontends  
    Anbindung an Loadbalancer Richtung Internet

2.  Föderierte Frontend  
    Anbindung an Loadbalancer Richtung Föderierte Netze

3.  Internes Frontend  
    Anbindung an Interne Netze der Betreiber

4.  Ggf. weiter Frontend Netze

5.  Kubernetes Nodes  
    Eigene Schicht, verschlüsselter Traffic (Wireguard) zwischen Nodes

    1.  Internets Netz in Kubernetes  
        nicht nach außen sichtbar ist. Zugriff nur über Proxies Richtung
        Frontend. Es MUSS eine Implementierung gewählt werden, die
        Microsegmentation erlaubt, ggf als Service Mesh

    2.  Storage Schicht  
        Anbindung an Storage Systeme

#### Föderierung

Unter Föderierung wird die Integration zweier oder mehr Cluster zu einer
Einheit, verstanden. Nach außen wirkt die Föderierung wie eine
Anwendung. Sie wird eingerichtet, um Latenz, Performance und/oder die
Verfügbarkeit zu verbessern. Kritischer Punkt ist Verteilung der
Datenschicht. Um die Konsistenz der Daten zu erreichen, muss bei
schreibenden Zugriff eine Synchronisation der Daten erreicht werden.
Vorteile für die Latenz werden dabei wieder zunichte gemacht. Neben der
Verbindung der Netzwerke üblicherweise durch Service
Meshs<a href="#fn107" id="fnref107" class="footnote-ref">107</a> muss
eine verteilte Datenhaltung über die
Datenbank<a href="#fn108" id="fnref108" class="footnote-ref">108</a> und
das Storage System eingerichtet
werden<a href="#fn109" id="fnref109" class="footnote-ref">109</a>.

Die Ingress Schicht wird durch einen Reverse Proxy implementiert. In
dieser Schicht MUSS Authorisierung und Authentifizierung durch Anfrage
an einen Authentifizierungsserver hergestellt werden. Grundsätzlich
können auf dieser Ebene durch Redirects auch Dienste an andere Cluster
durchgeführt werden. Der Cluster, der einen Request empfängt, MUSS die
Autorisierung und Authentifizierung sicherstellen.

Eine Föderierung von Diensten ist möglich. Es muss die Authentifizierung
und Autorisierung so eingerichtet werden, das ein Zugriff auf den
Service einer anderen Behörde, mit dem Daten ausgetauscht werden sollen,
wie ein externer Zugriff behandelt wird.

Zwischen Behörden wird Ingress Traffic über über ein Frontend mit
jeweiligen Regeln geleitet. Egress Traffic wird mit Firewall Regeln
limiiert.

Bei hohen Performance Anforderungen KANN nach Risikoanalyse und Prüfung
ein gemeinsames VLAN über eine verschlüsseltes VPN zur Verbindung
verwendet
werden<a href="#fn110" id="fnref110" class="footnote-ref">110</a>.

Multi-Cluster
Federation<a href="#fn111" id="fnref111" class="footnote-ref">111</a>
ist möglich, verkompliziert aber das Netzwerk, das Sicherheitsmodell und
engt die Update Möglichkeiten so stark ein, dass davon abgeraten wird.
Deswegen SOLL Multi Cluster Federation NICHT eingesetzt werden.

Es gibt keine direkte Kopplung zwischen Behördennetzen über VPN oder
VLAN. Behördennetze werden als nicht sicher
eingestuft.<a href="#fn112" id="fnref112" class="footnote-ref">112</a>

Alle föderierten Dienste MÜSSEN als WebServices angesprochen werden. TCP
oder UDP, die in Ausnahmefällen notwendig werden, MÜSSEN um die
Authentifizierung in Kubernetes verwenden zu können, über einen
kube-proxy durchgeführt werden. Damit sind sie auch über TLS
abgesichert.  
Das schliesst auch alle Dienste über REST API ein, wie z.B. Nextcloud
Federation<a href="#fn113" id="fnref113" class="footnote-ref">113</a>.

Alle REST API Schnittstellen MÜSSEN abgesichert werden. Die Regeln sind
als RBAC Regeln zu dokumentieren.
<a href="#fn114" id="fnref114" class="footnote-ref">114</a>

#### Transport Layer Zertifikate

Aufgrund der großen Menge der anfallenden Domains und der damit
verbundenen Zertifikate SOLLTE ein Automatisches Zertifikatsmanagement
nach RFC 8555<a href="#fn115" id="fnref115" class="footnote-ref">115</a>
eingeführt werden. In diesem Fall SOLLTE ein Zertifikatsmanagement
Dienst<a href="#fn116" id="fnref116" class="footnote-ref">116</a> die
Zertifikate automatisch aktualisieren.

#### Weitere notwendige sicherheitrelevante Dienste

##### ACME Server RFC 8555

Es SOLLTE ein ACME
Server<a href="#fn117" id="fnref117" class="footnote-ref">117</a>
verwendet werden. Alle Dienste MÜSSEN über kurzlebige Zertifkate
abgesichert werden. Zertifikate MÜSSEN über einen den
Cert-Manager<a href="#fn118" id="fnref118" class="footnote-ref">118</a>
automatisch ausgerollt werden können.

##### Logging, Monitoring, Alerting

Es MÜSSEN von den Betreibern geeignete Logging, Monitoring und Alerting
Systeme aufgebaut und Betrieben werden (Audit Logging, Cluster
Monitoring und Applikations Logging). Diese MÜSSEN über ein Fluentd
Output Plugin<a href="#fn119" id="fnref119" class="footnote-ref">119</a>
ansprechbar sein.

##### Scanning im Betrieb

Betreiber MÜSSEN die Sicherheit Ihrer Cluster täglich scannen und das
Ergebnis dokumentieren.

##### Sigstore Transparency Log

Es SOLLTE ein eigenes Rekor Transparency Log geführt
werden.<a href="#fn120" id="fnref120" class="footnote-ref">120</a>

Es DÜRFEN KEINE Container ausgführt werden, die nicht signiert werden.
Es DÜRFEN KEINE Signaturen von Containern im Rekor Log existieren, zu
denen keine Container auffindbar sind.

##### Signaturprüfung

Die Signaturen aller Container und Helm Charts MÜSSEN überprüft werden.
Bei der Ausführung durch einen Admission
Controller<a href="#fn121" id="fnref121" class="footnote-ref">121</a>
oder durch die Container Runtime (bevorzugt, sobald verfügbar)

##### Zero Trust

Aufgrund der Findings des
Rechnungshofes<a href="#fn122" id="fnref122" class="footnote-ref">122</a>
kann nicht mehr davon ausgegangen werden, dass Verwaltungsnetze sicher
sind. Deshalb sind alle Verbindung von und zum SouvAP und allen
Anwendungen durch Authorisierung abzusichern. Dafür bietet sich eine
Zero Trust Implementierung an.

#### Standards

Die folgenden Standards sind für das Projekt als verbindlich anzusehen.
Dies sind weitestgehend Standards der Deutschen Verwaltungscloud, des
BSI aber auch internationale Standards, die in der Cloud Native
Community anerkannt sind.

Soweit keine deutschen oder EU Standards vorhanden sind, wird auf
internationale Standards zurückgegriffen.

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Standard</td>
<td>Thema</td>
<td>Urheber</td>
<td>Link, Kommentar</td>
</tr>
<tr class="even">
<td>Deutsche Verwaltungscloud Strategie Leitfaden 2.0, August 2022</td>
<td></td>
<td>IT Planungsrat</td>
<td><p>Muss noch verabschiedet werden</p>
<p><a href="https://fs.bmi.app.bmi.dphoenixsuite.de/s/i68LnPdHRBQ9B5B"><span class="underline">https://fs.bmi.app.bmi.dphoenixsuite.de/s/i68LnPdHRBQ9B5B</span></a></p></td>
</tr>
<tr class="odd">
<td>Grundschutz</td>
<td></td>
<td>BSI</td>
<td>Alle anwendbaren Dokumente</td>
</tr>
<tr class="even">
<td></td>
<td>Container</td>
<td></td>
<td>SYS 1.6</td>
</tr>
<tr class="odd">
<td></td>
<td>Kubernetes</td>
<td></td>
<td>APP 4.4</td>
</tr>
<tr class="even">
<td></td>
<td>Höchstverfügbarkeit<a href="#fn123" id="fnref123" class="footnote-ref">123</a></td>
<td></td>
<td><p>Hochverfügbarkeit G1</p>
<p>wenn anzuwenden</p></td>
</tr>
<tr class="odd">
<td></td>
<td>C5</td>
<td></td>
<td><a href="https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/kriterienkatalog-c5_node.html"><span class="underline">https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/kriterienkatalog-c5_node.html</span></a></td>
</tr>
<tr class="even">
<td>OWASP</td>
<td>Top 10</td>
<td>OWASP</td>
<td><a href="https://owasp.org/www-project-kubernetes-top-ten/"><span class="underline">https://owasp.org/www-project-kubernetes-top-ten/</span></a></td>
</tr>
<tr class="odd">
<td></td>
<td>Kubernetes Top 10</td>
<td></td>
<td><a href="https://owasp.org/www-project-kubernetes-top-ten/"><span class="underline">https://owasp.org/www-project-kubernetes-top-ten/</span></a></td>
</tr>
<tr class="even">
<td></td>
<td>DevSecOps Maturity Model</td>
<td></td>
<td><p><span class="underline">https://owasp.org/www-project-devsecops-maturity-model/</span></p>
<p>Maturity Level 2 bis Ende 2022, Level 4 bis Ende 2023</p></td>
</tr>
<tr class="odd">
<td>12Factor</td>
<td></td>
<td>Addam Wiggins</td>
<td><p><span class="underline">https://12factor.net/</span></p>
<p>community accepted definition of a Cloud Native Application</p></td>
</tr>
<tr class="even">
<td>NIST Application Security Guide</td>
<td></td>
<td>NIST</td>
<td><span class="underline">https://nvlpubs.nist.gov/nistpubs/specialpublications/nist.sp.800-190.pdf</span></td>
</tr>
<tr class="odd">
<td>Kubernetes STIG Ver 1, Rel 6</td>
<td></td>
<td>NIST</td>
<td><a href="https://ncp.nist.gov/checklist/996/download/8655"><span class="underline">https://ncp.nist.gov/checklist/996/download/8655</span></a></td>
</tr>
<tr class="even">
<td>Minimale Container</td>
<td>abgeleitet vom Minimalitätsprinzip Least Privileges</td>
<td>CIS</td>
<td><p>Minimal Container FROM SCRATCH</p>
<p>best practice</p>
<p>SOLL wo immer möglich</p>
<p>MUSS wenn Frontend oder wenn Container mit Privilegien laufen müssen</p></td>
</tr>
<tr class="odd">
<td>Container Benchmarks</td>
<td></td>
<td></td>
<td><a href="https://www.cisecurity.org/benchmark/docker"><span class="underline">https://www.cisecurity.org/benchmark/docker</span></a></td>
</tr>
<tr class="even">
<td>Kubernetes Benchmarks</td>
<td></td>
<td></td>
<td><a href="https://www.cisecurity.org/benchmark/kubernetes"><span class="underline">https://www.cisecurity.org/benchmark/kubernetes</span></a></td>
</tr>
<tr class="odd">
<td>Hardening</td>
<td></td>
<td></td>
<td><a href="https://www.cisecurity.org/insights/blog/using-hardened-container-image-secure-applications-cloud"><span class="underline">https://www.cisecurity.org/insights/blog/using-hardened-container-image-secure-applications-cloud</span></a></td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td>NSA</td>
<td><p><a href="httpsmedia.defense.gov/2021/Aug/03/2002820425/-1/-1/1/CTR_KUBERNETESHARDENINGGUIDANCE.PDF"><span class="underline">httpsmedia.defense.gov/2021/Aug/03/2002820425/-1/-1/1/CTR_KUBERNETESHARDENINGGUIDANCE.PDF</span></a></p>
<p>Kommentare</p>
<p><a href="https://kubernetes.io/blog/2021/10/05/nsa-cisa-kubernetes-hardening-guidance/"><span class="underline">https://kubernetes.io/blog/2021/10/05/nsa-cisa-kubernetes-hardening-guidance/</span></a></p>
<p><a href="https://blog.aquasec.com/nsa-kubernetes-hardening-guide"><span class="underline">https://blog.aquasec.com/nsa-kubernetes-hardening-guide</span></a></p></td>
</tr>
<tr class="odd">
<td>Enterprise Container Hardening</td>
<td></td>
<td><p>CNCF</p>
<p>US DoD</p></td>
<td><a href="https://dl.dod.cyber.mil/wp-content/uploads/devsecops/pdf/Final_DevSecOps_Enterprise_Container_Hardening_Guide_1.1.pdf"><span class="underline">https://dl.dod.cyber.mil/wp-content/uploads/devsecops/pdf/Final_DevSecOps_Enterprise_Container_Hardening_Guide_1.1.pdf</span></a></td>
</tr>
<tr class="even">
<td>CNCF and DoD Enterprise DevSecOps Reference Design</td>
<td></td>
<td></td>
<td><a href="https://dodcio.defense.gov/Portals/0/Documents/Library/DevSecOpsReferenceDesign.pdf"><span class="underline">https://dodcio.defense.gov/Portals/0/Documents/Library/DevSecOpsReferenceDesign.pdf</span></a></td>
</tr>
<tr class="odd">
<td>Distroless</td>
<td>Google Distroless Container Tools</td>
<td>Google</td>
<td><a href="https://github.com/GoogleContainerTools/distroless"><span class="underline">https://github.com/GoogleContainerTools/distroless</span></a></td>
</tr>
<tr class="even">
<td>Best Practices</td>
<td></td>
<td></td>
<td><a href="https://cloud.google.com/solutions/best-practices-for-building-containers"><span class="underline">https://cloud.google.com/solutions/best-practices-for-building-containers</span></a></td>
</tr>
<tr class="odd">
<td>Signaturen</td>
<td></td>
<td>Industriekonsortium</td>
<td><a href="https://www.sigstore.dev/"><span class="underline">https://www.sigstore.dev/</span></a></td>
</tr>
<tr class="even">
<td>Container Security Scanner</td>
<td></td>
<td>Verschiedene Anbieter</td>
<td><p><span class="underline">https://geekflare.com/container-security-scanners/</span></p>
<p>z.B. Trivy https://github.com/aquasecurity/trivy</p></td>
</tr>
<tr class="odd">
<td>Cluster Configuration Scanner</td>
<td></td>
<td></td>
<td><a href="https://thechief.io/c/editorial/7-kubernetes-security-scanners-to-use-in-your-devsecops-pipeline/"><span class="underline">https://thechief.io/c/editorial/7-kubernetes-security-scanners-to-use-in-your-devsecops-pipeline/</span></a></td>
</tr>
<tr class="even">
<td>SLSA</td>
<td></td>
<td>Linux Foundation</td>
<td><p><span class="underline">https://slsa.dev/</span></p>
<p>Level 2 bis Ende 2022, Level 4 bis Ende 2023</p></td>
</tr>
<tr class="odd">
<td>Architektur best practices</td>
<td>Keine Verbindung von privilegierten Containern mit den Netzwerk</td>
<td></td>
<td><a href="https://www.heise.de/hintergrund/Kubernetes-Security-Teil-3-Im-Spannungsfeld-von-Komplexitaet-und-Sicherheit-4862263.html?seite=all"><span class="underline">https://www.heise.de/hintergrund/Kubernetes-Security-Teil-3-Im-Spannungsfeld-von-Komplexitaet-und-Sicherheit-4862263.html?seite=all</span></a></td>
</tr>
</tbody>
</table>

Phase D +M: Betrieb des Souveränen Arbeitsplatzes in einer Multicluster Umgebung
--------------------------------------------------------------------------------

Der Betrieb des SouvAp in einer verteilten Umgebung, die mehrere Backend
Standorte umfasst, muss grundsätzlich möglich sein. Alle Module des
Souveränen Arbeitsplatzes sollten an jedem Standort installiert sein.

Die Replikation der Backend Infrastruktur an einen oder mehrere weitere
Standorte erhöht grundsätzlich die Verfügbarkeit und liefert damit einen
wesentlichen Beitrag zur Resilienz. Damit ist grundsätzlich eine
Active-Active Replikation gemeint. Die dazu notwendige Synchronisation
der Datenhaltung muss über Datenbank oder Storage Replikation erfolgen.

Durch die Möglichkeit, sich mit dem jeweils nächsten Cluster im Sinne
der geringsten Latenz zu verbinden, erhöht dich die Benutzbarkeit und
damit die Akzeptanz der User. Die Entwicklung in Kubernetes ist sehr
dynamisch, vor einer konkreten Implementierung sind die existierenden
Lösungen zu vergleichen. Eine Übersicht findet sich in „Simplifying
multi-clusters in Kubernetes“
<a href="#fn124" id="fnref124" class="footnote-ref">124</a>

### Controlplane

Beide Cluster müssen durch die gleiche Git Konfiguration betrieben
werden. Das Einrichten einer virtuellen Controlplane wird
empfohlen<a href="#fn125" id="fnref125" class="footnote-ref">125</a>, es
gibt aber noch viele weiter Optionen, um Cluster nur für ausgewählte
Namespaces<a href="#fn126" id="fnref126" class="footnote-ref">126</a>
oder nur auf Netzwerkschicht
7<a href="#fn127" id="fnref127" class="footnote-ref">127</a> zu
synchronisieren. In diesem Fall müssen alle Namespaces aller Module des
Souveränen Arbeitsplatzes synchronisiert werden.

### Anforderung

Der Betrieb einer Multiclusterumgebung stellt einen erheblichen Aufwand
da. Aus diesem Grund MUSS in der Bedrohungsanalyse eine
Höchstverfügbarkeit oder eine entsprechende Lastabschätzung explizit
verlangt werden. In ersten Fall sind die Kriterien für die Standortwahl
von
Rechenzentren<a href="#fn128" id="fnref128" class="footnote-ref">128</a>
einzuhalten.

Es wird davon ausgegangen, dass zwei oder mehr Rechenzentren sich
gegenseitig unterbrechungsfrei vertreten und im Regelfall gemeinsam ihre
Leistung zur Verfügung stellen. Ein reiner Standby Betrieb ist auch
möglich, lässt aber große Teile der Kapazität brach liegen.

Aufgrund der Entfernungsanforderungen von im Regelfall mindestens 200km,
bei Betrieb in zwei unterschiedlichen Flusssystemen mindestens 100km
DÜRFEN nur asynchrone Methoden zur Datensynchronisation verwendet
werden.

### Netzwerk

Alle Beschreibungen für Kubernetes gehen davon aus, dass die Cluster
sich ein Netzwerk als IP Subnetz teilen. Aufgrund der Anforderungen an
die Verschlüsselung aus der DVS kommen dafür nur verschlüsselte
Verbindungen in Frage. Durch die Kopplung des Netzwerkes an mehreren
Standorten sind damit alle Netzwerke in der selben Sicherheitszone.

### Kopplung durch Service Mesh

Es ist ein Deployment
Modell<a href="#fn129" id="fnref129" class="footnote-ref">129</a> zu
definieren. In den Hyperscaler Clouds wird üblicherweise ein
Servicemesh<a href="#fn130" id="fnref130" class="footnote-ref">130</a>
zugrunde gelegt. Dabei ist darauf zu achten, dass bei Updates die
installierten Versionen einem genauen Upgradepfad folgen.

### Loadbalancer

Üblicherweise sollen die Anwendung nach außen einheitlich erscheinen und
für die Anwender nicht sichtbar sein, in welchem Standort die Anwendung
läuft.

Dazu wird ein Globaler Loadbalancer so eingerichtet, dass er die
Anfragen und den Traffic auf die Standort lokalen Loadbalancer umlenkt.
Diese können die Daten mit verschiedenen Methoden, über ein Service
Mesh, einen Ingress Controller oder bei hohen Lastanforderungen auch
unter Umgehung des Linux Kernels direkt in die Container des Pod leiten.

### Storage

Der Pod verarbeitet die Daten und speichert sie in eine lokale Datenbank
oder über einen Persistent Volume Claim in ein Persistent Volume. Das
Storage System, das das PV bereitstellt wird üblicherweise als Block
Storage implementiert.

### Synchronisation

Eine Datensynchronisation kann entweder über die Replikationsmechanismen
der Datenbank oder des Blockstorage erfolgen.

#### Datenbanken

Datenbankreplikationen sind sehr speziell, deswegen muss für jedes
Datenbank Management System (DBMS) die dazu passende Replikationsart
gewählt und automatisiert eingerichtet werden. Im Zweifel ist die
Datenbanksynchronisation performanter und deswegen vorzuziehen, weil der
Automatisierungsaufwand nur einmal geleistet werden muss.

#### Blockspeicher

Einfacher einzurichten sind Replikationen durch Blockspeicher
(Blockstorage). Die Methode eignet sich im wesentlichen für Dateien, bei
Datenbanken ist eine Active-Active Replikation der verwendeten Blöcke,
in diesen Fall ist eine Active-Passive Synchronisation sinnvoller. Es
ist darauf zu achten, dass die Replikation zur Clustertopologie passt
und lokal synchrone und für entfernte Replikationen asynchrone
Replikation ausgewählt wird.

#### Verschlüsselung des Filesystems

Die Daten auf den verwendeten Filesystemen sind grundsätzlich zu
verschlüsseln. Die dafür im BSI Grundschutz vorgesehenen Schlüssellängen
sind
einzuhalten<a href="#fn131" id="fnref131" class="footnote-ref">131</a>.

### Monitoring

Zusätzlich zu den anderen Parametern sind noch die die Replikationslücke
(replication lag) bei den Datenbanken und dem Speicher zu erfassen und
zu bewerten. Der Betrieb ist so auszulegen, dass sich die Lücke nicht
auswirkt<a href="#fn132" id="fnref132" class="footnote-ref">132</a>. Es
gibt dafür verschiedene
Strategien<a href="#fn133" id="fnref133" class="footnote-ref">133</a>.

### Vertrauenszone

Durch die Netzwerkkopplung der Cluster sind die Netze beider Standorte
direkt gekoppelt. Dadurch ist in der Sicherheitsbetrachtung immer darauf
zu achten, dass beide Cluster eine gemeinsame Sicherheitszone bilden,
die sich über zwei oder mehr Standorte erstreckt. Sind die einzelnen
Cluster unterschiedlich eingestuft, wird der gekoppelte Verbund mit der
niedrigeren Stufe bewertet.

![](media/file36.png)

------------------------------------------------------------------------

1.  Vgl. [<span
    class="underline">https://www.cncf.io/certification/software-conformance/</span>](https://www.cncf.io/certification/software-conformance/)<a href="#fnref1" class="footnote-back">↩︎</a>

2.  <https://kubernetes.io/blog/2019/01/15/container-storage-interface-ga/><a href="#fnref2" class="footnote-back">↩︎</a>

3.  <https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/><a href="#fnref3" class="footnote-back">↩︎</a>

4.  Vgl.:
    <https://en.wikipedia.org/wiki/DevOps><a href="#fnref4" class="footnote-back">↩︎</a>

5.  <https://www.ncsc.gov.uk/information/secure-default><a href="#fnref5" class="footnote-back">↩︎</a>

6.  Beispiel Web Server: http ist unsicher, https mit Installation von
    Zertifikaten ist sicher, aber komplex, anzustreben ist automatisches
    Ausrollen von Zertifikaten, z.B. mit ACME RFC
    8555<a href="#fnref6" class="footnote-back">↩︎</a>

7.  <https://www.cncf.io/projects/><a href="#fnref7" class="footnote-back">↩︎</a>

8.  <https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance?hl=en><a href="#fnref8" class="footnote-back">↩︎</a>

9.  <https://services.google.com/fh/files/misc/2022_state_of_devops_report.pdf?hl=de><a href="#fnref9" class="footnote-back">↩︎</a>

10. <https://de.wikipedia.org/wiki/Site_Reliability_Engineering><a href="#fnref10" class="footnote-back">↩︎</a>

11. Zahlen sind eventuell
    anzupassen<a href="#fnref11" class="footnote-back">↩︎</a>

12. Vgl. [<span
    class="underline">https://12factor.net</span>](https://12factor.net/)<a href="#fnref12" class="footnote-back">↩︎</a>

13. Vgl. Curl: [<span
    class="underline">https://github.com/thomasfricke/training-kubernetes-security/blob/main/OpenShift/ImageTragickExploit.ipynb</span>](https://github.com/thomasfricke/training-kubernetes-security/blob/main/OpenShift/ImageTragickExploit.ipynb)
    oder auch ein simples cat und echo: [<span
    class="underline">https://andreafortuna.org/2021/03/06/some-useful-tips-about-dev-tcp/</span>](https://andreafortuna.org/2021/03/06/some-useful-tips-about-dev-tcp/)<a href="#fnref13" class="footnote-back">↩︎</a>

14. Vgl. Google best practices for building containers: [<span
    class="underline">https://cloud.google.com/solutions/best-practices-for-building-containers</span>](https://cloud.google.com/solutions/best-practices-for-building-containers);
    Minimal go container from scratch: [<span
    class="underline">https://github.com/endocode/minimal-go-container-from-scratch</span>](https://github.com/endocode/minimal-go-container-from-scratch);
    Google Distroless: [<span
    class="underline">https://github.com/GoogleContainerTools/distroless</span>](https://github.com/GoogleContainerTools/distroless);
    Alpine: [<span
    class="underline">https://www.alpinelinux.org/</span>](https://www.alpinelinux.org/)<a href="#fnref14" class="footnote-back">↩︎</a>

15. Vgl. [<span
    class="underline">https://12factor.net/disposability</span>](https://12factor.net/disposability)<a href="#fnref15" class="footnote-back">↩︎</a>

16. Vgl. [<span
    class="underline">https://en.wikipedia.org/wiki/Quarkus</span>](https://en.wikipedia.org/wiki/Quarkus);
    [<span
    class="underline">https://quarkus.io/</span>](https://quarkus.io/);
    [<span
    class="underline">https://micronaut.io/</span>](https://micronaut.io/)<a href="#fnref16" class="footnote-back">↩︎</a>

17. Vgl. <span class="underline"> </span> [<span
    class="underline">https://helm.sh/</span>](https://helm.sh/)<a href="#fnref17" class="footnote-back">↩︎</a>

18. Vgl. [<span
    class="underline">https://helm.sh/docs/helm/helm\_dependency/</span>](https://helm.sh/docs/helm/helm_dependency/)<a href="#fnref18" class="footnote-back">↩︎</a>

19. Vgl. [<span
    class="underline">https://de.wikipedia.org/wiki/DevOps\#GitOps,https://www.redhat.com/en/topics/devops/what-is-gitops</span>](https://de.wikipedia.org/wiki/DevOps#GitOps,https://www.redhat.com/en/topics/devops/what-is-gitops)<a href="#fnref19" class="footnote-back">↩︎</a>

20. Vgl. [<span
    class="underline">https://helm.sh/docs/topics/charts\_hooks/</span>](https://helm.sh/docs/topics/charts_hooks/)<a href="#fnref20" class="footnote-back">↩︎</a>

21. Vgl. z. B. [<span
    class="underline">https://argo-cd.readthedocs.io/en/stable/user-guide/helm/</span>](https://argo-cd.readthedocs.io/en/stable/user-guide/helm/)<a href="#fnref21" class="footnote-back">↩︎</a>

22. Vgl. [<span
    class="underline">https://www.ntia.doc.gov/files/ntia/publications/sbom\_minimum\_elements\_report.pdf</span>](https://www.ntia.doc.gov/files/ntia/publications/sbom_minimum_elements_report.pdf)<a href="#fnref22" class="footnote-back">↩︎</a>

23. Vgl. [<span
    class="underline">https://www.cisa.gov/sbom</span>](https://www.cisa.gov/sbom);
    [<span
    class="underline">https://spdx.dev/</span>](https://spdx.dev/);
    [<span
    class="underline">https://github.com/anchore/syft</span>](https://github.com/anchore/syft);
    [<span
    class="underline">https://cyclonedx.org/</span>](https://cyclonedx.org/)<a href="#fnref23" class="footnote-back">↩︎</a>

24. Vgl. [<span
    class="underline">https://de.wikipedia.org/wiki/Schichtenarchitektur</span>](https://de.wikipedia.org/wiki/Schichtenarchitektur);
    [<span
    class="underline">https://www.oreilly.com/library/view/software-architecture-patterns/9781491971437/ch01.html</span>](https://www.oreilly.com/library/view/software-architecture-patterns/9781491971437/ch01.html)<a href="#fnref24" class="footnote-back">↩︎</a>

25. Vgl. z.B. Hypervisor KVM: [<span
    class="underline">https://www.linux-kvm.org/page/Main\_Page</span>](https://www.linux-kvm.org/page/Main_Page);
    Virtualisierungsmanagement [<span
    class="underline">https://scs.community/de/</span>](https://scs.community/de/);
    offene Hardware: [<span
    class="underline">https://www.opencompute.org/</span>](https://www.opencompute.org/);
    Sonic: [<span
    class="underline">https://azure.microsoft.com/de-de/blog/sonic-the-networking-switch-software-that-powers-the-microsoft-global-cloud/</span>](https://azure.microsoft.com/de-de/blog/sonic-the-networking-switch-software-that-powers-the-microsoft-global-cloud/)<a href="#fnref25" class="footnote-back">↩︎</a>

26. <https://kubernetes.io/docs/concepts/services-networking/#the-kubernetes-network-model><a href="#fnref26" class="footnote-back">↩︎</a>

27. Vgl. [<span
    class="underline">https://kubernetes.io/docs/concepts/cluster-administration/addons/\#networking-and-network-policy</span>](https://kubernetes.io/docs/concepts/cluster-administration/addons/#networking-and-network-policy)<a href="#fnref27" class="footnote-back">↩︎</a>

28. Vgl. [<span
    class="underline">https://kubernetes.io/blog/2019/01/15/container-storage-interface-ga/</span>](https://kubernetes.io/blog/2019/01/15/container-storage-interface-ga/)<a href="#fnref28" class="footnote-back">↩︎</a>

29. Vgl. [<span
    class="underline">https://kubernetes-csi.github.io/docs/drivers.html</span>](https://kubernetes-csi.github.io/docs/drivers.html)<a href="#fnref29" class="footnote-back">↩︎</a>

30. <https://landscape.cncf.io/card-mode?category=observability-and-analysis&grouping=category><a href="#fnref30" class="footnote-back">↩︎</a>

31. Vgl. [<span
    class="underline">https://www.fluentd.org/</span>](https://www.fluentd.org/)<a href="#fnref31" class="footnote-back">↩︎</a>

32. Vgl. z. B. [<span
    class="underline">https://grafana.com/grafana/dashboards/</span>](https://grafana.com/grafana/dashboards/)<a href="#fnref32" class="footnote-back">↩︎</a>

33. Vgl. [<span
    class="underline">https://opensource.com/article/19/7/cicd-pipeline-rule-them-all</span>](https://opensource.com/article/19/7/cicd-pipeline-rule-them-all)<a href="#fnref33" class="footnote-back">↩︎</a>

34. Vgl. [<span
    class="underline">https://www.networkworld.com/article/3305810/how-to-list-repositories-on-linux.html\#:~:text=A%20Linux%20repository%20is%20a,software%20packages%20on%20Linux%20systems</span>](https://www.networkworld.com/article/3305810/how-to-list-repositories-on-linux.html#:~:text=A%20Linux%20repository%20is%20a,software%20packages%20on%20Linux%20systems)<a href="#fnref34" class="footnote-back">↩︎</a>

35. <https://docs.gitlab.com/ee/ci/triggers/><a href="#fnref35" class="footnote-back">↩︎</a>

36. <https://en.wikipedia.org/wiki/Webhook><a href="#fnref36" class="footnote-back">↩︎</a>

37. Vgl. [<span
    class="underline">https://www.redhat.com/en/topics/devops/what-is-gitops</span>](https://www.redhat.com/en/topics/devops/what-is-gitops)<a href="#fnref37" class="footnote-back">↩︎</a>

38. Vgl. z.B.: [<span
    class="underline">https://github.com/argoproj/argo-cd</span>](https://github.com/argoproj/argo-cd);
    [<span
    class="underline">https://fluxcd.io/</span>](https://fluxcd.io/)<a href="#fnref38" class="footnote-back">↩︎</a>

39. [https://landscape.cncf.io/card-mode?category=cloud-native-network](https://landscape.cncf.io/card-mode?category=cloud-native-network&grouping=category)
    und
    <https://landscape.cncf.io/card-mode?category=service-mesh><a href="#fnref39" class="footnote-back">↩︎</a>

40. https://public.cyber.mil/devsecops/<a href="#fnref40" class="footnote-back">↩︎</a>

41. https://software.af.mil/wp-content/uploads/2021/05/DoD-Enterprise-DevSecOps-2.0-Fundamentals.pdf<a href="#fnref41" class="footnote-back">↩︎</a>

42. https://en.wikipedia.org/wiki/Secure\_coding<a href="#fnref42" class="footnote-back">↩︎</a>

43. https://owasp.org<a href="#fnref43" class="footnote-back">↩︎</a>

44. <https://github.com/thomasfricke/notebooks-management-cluster><a href="#fnref44" class="footnote-back">↩︎</a>

45. <https://goharbor.io/docs/2.0.0/administration/vulnerability-scanning/><a href="#fnref45" class="footnote-back">↩︎</a>

46. <https://github.com/kubescape/kubescape><a href="#fnref46" class="footnote-back">↩︎</a>

47. <https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/><a href="#fnref47" class="footnote-back">↩︎</a>

48. <https://goharbor.io/docs/2.1.0/administration/vulnerability-scanning/configure-system-allowlist/><a href="#fnref48" class="footnote-back">↩︎</a>

49. <https://blog.sigstore.dev/a-guide-to-running-sigstore-locally-f312dfac0682/><a href="#fnref49" class="footnote-back">↩︎</a>

50. <https://dodcio.defense.gov/Portals/0/Documents/Library/DevSecOpsFundamentalsPlaybook.pdf><a href="#fnref50" class="footnote-back">↩︎</a>

51. <https://dsomm.timo-pagel.de/> und
    <https://www.datadoghq.com/resources/devsecops-maturity-model/><a href="#fnref51" class="footnote-back">↩︎</a>

52. <https://kubernetes.io/docs/concepts/security/><a href="#fnref52" class="footnote-back">↩︎</a>

53. Vgl. [<span
    class="underline">https://www.commitstrip.com/en/2016/06/24/how-to-host-a-coder-dinner-party/</span>](https://www.commitstrip.com/en/2016/06/24/how-to-host-a-coder-dinner-party/)<a href="#fnref53" class="footnote-back">↩︎</a>

54. distroless<a href="#fnref54" class="footnote-back">↩︎</a>

55. <https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image><a href="#fnref55" class="footnote-back">↩︎</a>

56. Vgl. Kerckhoffs’ Prinzip: [<span
    class="underline">https://de.wikipedia.org/wiki/Kerckhoffs%E2%80%99\_Prinzip</span>](https://de.wikipedia.org/wiki/Kerckhoffs’_Prinzip)<a href="#fnref56" class="footnote-back">↩︎</a>

57. Vgl. [<span
    class="underline">https://docs.gitlab.com/ee/user/project/repository/gpg\_signed\_commits/</span>](https://docs.gitlab.com/ee/user/project/repository/gpg\_signed\_commits/)<a href="#fnref57" class="footnote-back">↩︎</a>

58. Vgl. [<span
    class="underline">https://cloud.redhat.com/blog/networkpolicies-and-microsegmentation</span>](https://cloud.redhat.com/blog/networkpolicies-and-microsegmentation)<a href="#fnref58" class="footnote-back">↩︎</a>

59. Vgl. z.B. Hypervisor KVM: [<span
    class="underline">https://www.linux-kvm.org/page/Main\_Page</span>](https://www.linux-kvm.org/page/Main_Page);
    Virtualisierungsmanagement [<span
    class="underline">https://scs.community/de/</span>](https://scs.community/de/);
    offene Hardware: [<span
    class="underline">https://www.opencompute.org/</span>](https://www.opencompute.org/);
    Sonic: [<span
    class="underline">https://azure.microsoft.com/de-de/blog/sonic-the-networking-switch-software-that-powers-the-microsoft-global-cloud/</span>](https://azure.microsoft.com/de-de/blog/sonic-the-networking-switch-software-that-powers-the-microsoft-global-cloud/)<a href="#fnref59" class="footnote-back">↩︎</a>

60. <span class="underline"> </span> Vgl. <span class="underline">
    <https://kubernetes.io/docs/concepts/services-networking/ingress/></span><a href="#fnref60" class="footnote-back">↩︎</a>

61. [<span class="underline">Vgl.
    https://apisix.apache.org/docs/apisix/customize-nginx-configuration/</span>](https://apisix.apache.org/docs/apisix/customize-nginx-configuration/)<a href="#fnref61" class="footnote-back">↩︎</a>

62. Vgl. z.B., aber ohne Anspruch auf Vollständigkeit,  
    Tigera Calica: [<span
    class="underline">https://www.tigera.io/project-calico/</span>](https://www.tigera.io/project-calico/);
    WeaveNet: [<span
    class="underline">https://www.weave.works/oss/net/</span>](https://www.weave.works/oss/net/);
    Cilium: [<span
    class="underline">https://cilium.io/</span>](https://cilium.io/)<a href="#fnref62" class="footnote-back">↩︎</a>

63. Das schließt Sidecars aus. Es muss nach das CNI verwendet werden:
    [<span
    class="underline">https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/</span>](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/network-plugins/)<a href="#fnref63" class="footnote-back">↩︎</a>

64. <https://www.wireguard.com/>, security fixed durch STF
    <https://sovereigntechfund.de/wireguard.html><a href="#fnref64" class="footnote-back">↩︎</a>

65. Kandidat ist Projekt Rosenpass
    <https://nlnet.nl/project/Rosenpass/><a href="#fnref65" class="footnote-back">↩︎</a>

66. Vgl. Einführung: [<span
    class="underline">https://www.redhat.com/en/topics/microservices/what-is-a-service-mesh</span>](https://www.redhat.com/en/topics/microservices/what-is-a-service-mesh);
    Auswahl einiger Produkte: [<span
    class="underline">https://thechief.io/c/editorial/top-14-kubernetes-service-meshes/</span>](https://thechief.io/c/editorial/top-14-kubernetes-service-meshes/);
    ohne Anspruch der
    Vollständigkeit<a href="#fnref66" class="footnote-back">↩︎</a>

67. Der Reifegrad von Service Meshs unterliegt einer sehr dynamischen
    Entwicklung. Es ist zu erwarten, das im Laufe des Jahres 2023 eine
    oder mehrere Implentierungen einen Reifegrad erreichen, der eine
    Implementierung als Standard Netzwerkschicht erreicht. Dann wird die
    Verschlüsselung ausschließlich durch Service Meshs
    implementiert.<a href="#fnref67" class="footnote-back">↩︎</a>

68. Vgl. [<span
    class="underline">https://istio.io/latest/docs/reference/config/networking/sidecar/</span>](https://istio.io/latest/docs/reference/config/networking/sidecar/)<a href="#fnref68" class="footnote-back">↩︎</a>

69. Vgl. Angriff auf Istio Sidecars: [<span
    class="underline">https://github.com/thomasfricke/training-kubernetes-security/blob/main/IstioHack.ipynb</span>](https://github.com/thomasfricke/training-kubernetes-security/blob/main/IstioHack.ipynb)<a href="#fnref69" class="footnote-back">↩︎</a>

70. Vgl. Container Network Interface: [<span
    class="underline">https://github.com/containernetworking/cni</span>](https://github.com/containernetworking/cni)<a href="#fnref70" class="footnote-back">↩︎</a>

71. Vgl. [<span
    class="underline">https://www.cncf.io/projects/service-mesh-interface-smi/</span>](https://www.cncf.io/projects/service-mesh-interface-smi/)<a href="#fnref71" class="footnote-back">↩︎</a>

72. Vgl. DVS-007-R01<a href="#fnref72" class="footnote-back">↩︎</a>

73. Vgl. [<span
    class="underline">https://en.wikipedia.org/wiki/ModSecurity</span>](https://en.wikipedia.org/wiki/ModSecurity)<a href="#fnref73" class="footnote-back">↩︎</a>

74. Vgl. [<span
    class="underline">https://en.wikipedia.org/wiki/Principle\_of\_least\_privilege</span>](https://en.wikipedia.org/wiki/Principle_of_least_privilege)<a href="#fnref74" class="footnote-back">↩︎</a>

75. Vgl. [<span
    class="underline">https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/</span>](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/)<a href="#fnref75" class="footnote-back">↩︎</a>

76. Es gibt dazu zahlreiche Standards vom BSI, die einzuhalten
    sind.<a href="#fnref76" class="footnote-back">↩︎</a>

77. Vgl. [<span
    class="underline">https://github.com/kubernetes-sigs/secrets-store-csi-driver/issues/906</span>](https://github.com/kubernetes-sigs/secrets-store-csi-driver/issues/906)<a href="#fnref77" class="footnote-back">↩︎</a>

78. Vgl. z.B. [<span
    class="underline">https://github.com/zalando/postgres-operator</span>](https://github.com/zalando/postgres-operator)<a href="#fnref78" class="footnote-back">↩︎</a>

79. Also mindestens N+2<a href="#fnref79" class="footnote-back">↩︎</a>

80. Vgl. z.B. mit [<span
    class="underline">https://velero.io/</span>](https://velero.io/)<a href="#fnref80" class="footnote-back">↩︎</a>

81. Level of Done: Das Backup ist fertig, wenn der Restore getestet
    ist.<a href="#fnref81" class="footnote-back">↩︎</a>

82. Vgl. [<span
    class="underline">https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/</span>](https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/)<a href="#fnref82" class="footnote-back">↩︎</a>

83. Vgl. [<span
    class="underline">https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/\#audit-policy</span>](https://kubernetes.io/docs/tasks/debug/debug-cluster/audit/#audit-policy)<a href="#fnref83" class="footnote-back">↩︎</a>

84. Vgl. z. B. [<span
    class="underline">https://grafana.com/grafana/dashboards/</span>](https://grafana.com/grafana/dashboards/)<a href="#fnref84" class="footnote-back">↩︎</a>

85. Vgl. [<span
    class="underline">https://www.trendmicro.com/vinfo/be/security/news/vulnerabilities-and-exploits/abusing-argo-cd-helm-and-artifact-hub-an-analysis-of-supply-chain-attacks-in-cloud-native-applications</span>](https://www.trendmicro.com/vinfo/be/security/news/vulnerabilities-and-exploits/abusing-argo-cd-helm-and-artifact-hub-an-analysis-of-supply-chain-attacks-in-cloud-native-applications)<a href="#fnref85" class="footnote-back">↩︎</a>

86. Vgl. [<span
    class="underline">https://github.com/thomasfricke/notebooks-management-cluster</span>](https://github.com/thomasfricke/notebooks-management-cluster)<a href="#fnref86" class="footnote-back">↩︎</a>

87. Vgl. [<span
    class="underline">https://goharbor.io/docs/2.1.0/administration/configure-proxy-cache/</span>](https://goharbor.io/docs/2.1.0/administration/configure-proxy-cache/)<a href="#fnref87" class="footnote-back">↩︎</a>

88. Vgl. [<span
    class="underline">https://docs.gitea.io/en-us/repo-mirror/</span>](https://docs.gitea.io/en-us/repo-mirror/)<a href="#fnref88" class="footnote-back">↩︎</a>

89. Vgl. [<span
    class="underline">https://www.networkworld.com/article/3305810/how-to-list-repositories-on-linux.html\#:~:text=A%20Linux%20repository%20is%20a,software%20packages%20on%20Linux%20systems</span>](https://www.networkworld.com/article/3305810/how-to-list-repositories-on-linux.html#:~:text=A%20Linux%20repository%20is%20a,software%20packages%20on%20Linux%20systems)<a href="#fnref89" class="footnote-back">↩︎</a>

90. Vgl. [<span
    class="underline">https://gnupg.org/</span>](https://gnupg.org/)<a href="#fnref90" class="footnote-back">↩︎</a>

91. Vgl. [<span
    class="underline">https://www.sigstore.dev/</span>](https://www.sigstore.dev/)<a href="#fnref91" class="footnote-back">↩︎</a>

92. Vgl. z.B. OWASP Zap: [<span
    class="underline">https://owasp.org/www-project-zap/</span>](https://owasp.org/www-project-zap/)<a href="#fnref92" class="footnote-back">↩︎</a>

93. Vgl. z.B., aber nicht nur mit: [<span
    class="underline">https://github.com/aquasecurity/kube-bench</span>](https://github.com/aquasecurity/kube-bench)<a href="#fnref93" class="footnote-back">↩︎</a>

94. Vgl. z.B. Harbor: [<span
    class="underline">https://goharbor.io/</span>](https://goharbor.io/)<a href="#fnref94" class="footnote-back">↩︎</a>

95. Vgl. [<span
    class="underline">https://cve.mitre.org/cve/</span>](https://cve.mitre.org/cve/)<a href="#fnref95" class="footnote-back">↩︎</a>

96. Vgl. [<span
    class="underline">https://geekflare.com/container-security-scanners/</span>](https://geekflare.com/container-security-scanners/);
    z.B. Trivy: [<span
    class="underline">https://trivy.dev/</span>](https://trivy.dev/)<a href="#fnref96" class="footnote-back">↩︎</a>

97. Vgl. BSI TR-02102-1<a href="#fnref97" class="footnote-back">↩︎</a>

98. Vgl. [<span
    class="underline">https://kubernetes.io/docs/setup/production-environment/container-runtimes/</span>](https://kubernetes.io/docs/setup/production-environment/container-runtimes/);
    z.B. [<span
    class="underline">https://cri-o.io/</span>](https://cri-o.io/)<a href="#fnref98" class="footnote-back">↩︎</a>

99. Vgl. [<span
    class="underline">https://docs.sigstore.dev/policy-controller/overview/</span>](https://docs.sigstore.dev/policy-controller/overview/)<a href="#fnref99" class="footnote-back">↩︎</a>

100. Vgl. [<span
     class="underline">https://github.com/sigstore/cosign/blob/main/KEYLESS.md</span>](https://github.com/sigstore/cosign/blob/main/KEYLESS.md)<a href="#fnref100" class="footnote-back">↩︎</a>

101. Vgl. [<span
     class="underline">https://github.com/sigstore/fulcio</span>](https://github.com/sigstore/fulcio)<a href="#fnref101" class="footnote-back">↩︎</a>

102. Vgl. [<span
     class="underline">https://github.com/sigstore/rekor</span>](https://github.com/sigstore/rekor)<a href="#fnref102" class="footnote-back">↩︎</a>

103. Vgl. [<span
     class="underline">https://www.cisa.gov/uscert/bsi/articles/knowledge/principles/defense-in-depth</span>](https://www.cisa.gov/uscert/bsi/articles/knowledge/principles/defense-in-depth)<a href="#fnref103" class="footnote-back">↩︎</a>

104. Vgl. [<span
     class="underline">https://kubernetes.io/docs/concepts/security/</span>](https://kubernetes.io/docs/concepts/security/)<a href="#fnref104" class="footnote-back">↩︎</a>

105. Das ist ein Grund, warum ein Management Cluster isoliert von den
     anderen Clustern installiert werden muss, obwohl die Funktionalität
     natürlich mitlaufen
     kann.<a href="#fnref105" class="footnote-back">↩︎</a>

106. <https://kubernetes.io/docs/concepts/services-networking/ingress/#the-ingress-resource><a href="#fnref106" class="footnote-back">↩︎</a>

107. <https://www.cncf.io/blog/2021/04/12/simplifying-multi-clusters-in-kubernetes/><a href="#fnref107" class="footnote-back">↩︎</a>

108. z.B. Vitess <https://vitess.io/> auf der Basis von MySQL oder
     <https://github.com/zalando/postgres-operator> mit
     <https://b-peng.blogspot.com/2021/07/postgres-disaster-recovery-on-k8s-zalando.html>
     auf der Basis von
     Postgres<a href="#fnref108" class="footnote-back">↩︎</a>

109. <https://docs.ceph.com/en/quincy/radosgw/multisite/><a href="#fnref109" class="footnote-back">↩︎</a>

110. z.B. mit Multus
     <https://github.com/k8snetworkplumbingwg/multus-cni><a href="#fnref110" class="footnote-back">↩︎</a>

111. [<span
     class="underline">https://www.cncf.io/blog/2021/04/12/simplifying-multi-clusters-in-kubernetes/</span>](https://www.cncf.io/blog/2021/04/12/simplifying-multi-clusters-in-kubernetes/)<a href="#fnref111" class="footnote-back">↩︎</a>

112. Nach [<span
     class="underline">https://background.tagesspiegel.de/cybersecurity/bundesrechnungshof-sieht-gravierende-defizite-bei-it-sicherheit</span>](https://background.tagesspiegel.de/cybersecurity/bundesrechnungshof-sieht-gravierende-defizite-bei-it-sicherheit)<a href="#fnref112" class="footnote-back">↩︎</a>

113. <https://nextcloud.cmedia/wp135098u/Architecture-Whitepaper-WebVersion-072018.pdf><a href="#fnref113" class="footnote-back">↩︎</a>

114. Nach Meinung der Entwickler gibt es nicht genug Interesse an der
     Cluster Federation
     <https://groups.google.com/g/kubernetes-sig-multicluster/c/lciAVj-_ShE>

     Das Kubefed Projekt wurde eingestellt
     <https://github.com/kubernetes-sigs/kubefed>,
     <https://www.heise.de/select/ix/2019/3/1551686036454559>

     Dies deckt sich mit der Erfahrung des Autors, der über das rein
     technische Interesse keinen realen Usecase gefunden
     hat.<a href="#fnref114" class="footnote-back">↩︎</a>

115. Automatic Certificate Management Environment (ACME) [<span
     class="underline">https://datatracker.ietf.org/doc/html/rfc8555</span>](https://datatracker.ietf.org/doc/html/rfc8555)<a href="#fnref115" class="footnote-back">↩︎</a>

116. https://www.cncf.io/blog/2022/10/19/cert-manager-becomes-a-cncf-incubating-project/<a href="#fnref116" class="footnote-back">↩︎</a>

117. <https://www.rfc-editor.org/rfc/rfc8555>, z.B.
     <https://github.com/letsencrypt/boulder><a href="#fnref117" class="footnote-back">↩︎</a>

118. <span class="underline">
     <https://cert-manager.io/docs/></span><a href="#fnref118" class="footnote-back">↩︎</a>

119. [<span
     class="underline">https://docs.fluentd.org/v/0.12/output</span>](https://docs.fluentd.org/v/0.12/output)<a href="#fnref119" class="footnote-back">↩︎</a>

120. [<span
     class="underline">https://blog.sigstore.dev/a-guide-to-running-sigstore-locally-f312dfac0682/</span>](https://blog.sigstore.dev/a-guide-to-running-sigstore-locally-f312dfac0682/)<a href="#fnref120" class="footnote-back">↩︎</a>

121. [<span
     class="underline">https://github.com/sigstore/policy-controller</span>](https://github.com/sigstore/policy-controller)<a href="#fnref121" class="footnote-back">↩︎</a>

122. Nach [<span
     class="underline">https://background.tagesspiegel.de/cybersecurity/bundesrechnungshof-sieht-gravierende-defizite-bei-it-sicherheit</span>](https://background.tagesspiegel.de/cybersecurity/bundesrechnungshof-sieht-gravierende-defizite-bei-it-sicherheit)<a href="#fnref122" class="footnote-back">↩︎</a>

123. BSI Kriterien für die Standortwahl von Rechenzentren,
     Standort-Kriterien RZ Version
     2.0<a href="#fnref123" class="footnote-back">↩︎</a>

124. <https://www.cncf.io/blog/2021/04/12/simplifying-multi-clusters-in-kubernetes/><a href="#fnref124" class="footnote-back">↩︎</a>

125. <https://github.com/virtual-kubelet/virtual-kubelet><a href="#fnref125" class="footnote-back">↩︎</a>

126. <https://submariner.io/>,<a href="#fnref126" class="footnote-back">↩︎</a>

127. <https://skupper.io/><a href="#fnref127" class="footnote-back">↩︎</a>

128. <https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/RZ-Sicherheit/Standort-Kriterien_Rechenzentren.pdf?__blob=publicationFile&v=1><a href="#fnref128" class="footnote-back">↩︎</a>

129. <https://istio.io/latest/docs/ops/deployment/deployment-models/><a href="#fnref129" class="footnote-back">↩︎</a>

130. https://cloud.google.com/service-mesh/docs/unified-install/gke-install-multi-cluster<a href="#fnref130" class="footnote-back">↩︎</a>

131. Relevante Grundschutz Bausteine: CON.1 Kryptokonzept,  
     SYS.1.1 Allgemeiner Server – Container kann ein Server sein, sowie
     APP.4.4.A20 verschlüsselte Datenhaltung bei
     Pods<a href="#fnref131" class="footnote-back">↩︎</a>

132. Innerhalb Deutschlands unter
     10ms<a href="#fnref132" class="footnote-back">↩︎</a>

133. <https://github.blog/2017-10-13-mitigating-replication-lag-and-reducing-read-load-with-freno/><a href="#fnref133" class="footnote-back">↩︎</a>
