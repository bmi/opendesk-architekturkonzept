Inhaltsübersicht
================

[1 Inhaltsübersicht ](0_inhaltsverzeichnis.md#inhaltsubersicht)

[2 Einleitung ](0_einleitung.md#einleitung)

[3 Vorbereitungen ](0_vorbereitungen.md#vorbereitungen)

[3.1 Ausgangslage ](0_vorbereitungen.md#ausgangslage)

[3.1.1 Verfügbare Vorgehensmodelle für die Architekturarbeit](0_vorbereitungen.md#verfugbare-vorgehensmodelle-für-die-architekturarbeit)

[3.1.2 Einflussnehmende strategische Dokumente](0_vorbereitungen.md#einflussnehmende-strategische-dokumente)

[3.1.3 Geschäftsprinzipien, Geschäftliche Ziele](0_vorbereitungen.md#geschaftsprinzipien-geschaftliche-ziele)

[3.1.4 Übergreifende Rahmenbedingungen](0_vorbereitungen.md#ubergreifende-rahmenbedingungen)

[3.1.5 Anvisierter Geschäftsnutzen der Architekturarbeit](0_vorbereitungen.md#anvisierter-geschaftsnutzen-der-architekturarbeit)

[3.1.6 Beteiligte Unternehmen und Partner](0_vorbereitungen.md#beteiligte-unternehmen-und-partner)

[3.1.7 Organisationsmodell für Architekturarbeit](0_vorbereitungen.md#organisationsmodell-fur-architekturarbeit)

[3.2 Architekturvorhaben ](0_vorbereitungen.md#architekturvorhaben)

[3.2.1 Abgrenzung beteiligter Organisationseinheiten](0_vorbereitungen.md#abgrenzung-beteiligter-organisationseinheiten)

[3.2.2 Einzusetzende Rahmenwerke und Steuerung der Architekturarbeit](0_vorbereitungen.md#einzusetzende-rahmenwerke-und-steuerung-der-architekturarbeit)

[3.2.3 Architekturteam ](0_vorbereitungen.md#architekturteam)

[3.2.4 Architekturprinzipien ](0_vorbereitungen.md#architekturprinzipien)

[3.2.5 Architekturrichtlinie des Bundes](0_vorbereitungen.md#architekturrichtlinie-des-bundes)

[3.2.5.1 Standards der Deutschen Verwaltungscloud-Strategie](0_vorbereitungen.md#standards-der-deutschen-verwaltungscloud-strategie)

[3.2.5.2 Föderale IT-Richtlinien](0_vorbereitungen.md#foderale-it-richtlinien)

[3.2.6 Konfektionierung von Architektur-Frameworks für SouvAP](0_vorbereitungen.md#konfektionierung-von-architektur-frameworks-fur-souvap)

[4 Phase A: Architekturvision ](A_architekturvision.md#phase-a-architekturvision)

[4.1 Projektinitiierung ](A_architekturvision.md#projektinitiierung)

[4.2 Anspruchsgruppen, Interessen und Geschäftsanforderungen](A_architekturvision.md#anspruchsgruppen-interessen-und-geschaftsanforderungen)

[4.2.1 Anspruchsgruppen ](A_architekturvision.md#anspruchsgruppen)

[4.2.2 Interessen ](A_architekturvision.md#interessen)

[4.2.3 Geschäftsanforderungen ](A_architekturvision.md#geschaftsanforderungen)

[4.2.4 Kommunikationsbedarf ](A_architekturvision.md#kommunikationsbedarf)

[4.2.5 Informationsspeicher ](A_architekturvision.md#informationsspeicher)

[4.3 Geschäftstreiber, Rahmenbedingungen und Geschäftsziele](A_architekturvision.md#geschaftstreiber-rahmenbedingungen-und-geschaftsziele)

[4.3.1 Geschäftstreiber ](A_architekturvision.md#geschaftstreiber)

[4.3.2 Rahmenbedingungen ](A_architekturvision.md#rahmenbedingungen)

[4.3.3 Geschäftsziele ](A_architekturvision.md#geschaftsziele)

[4.4 Bewertung vorhandener Fähigkeiten](A_architekturvision.md#bewertung-vorhandener-fahigkeiten)

[4.5 Prognose zu zukünftigen Fähigkeiten](A_architekturvision.md#prognose-zu-zukunftigen-fahigkeiten)

[4.6 Analyse und Bewertung der Reife für eine Transformation](A_architekturvision.md#analyse-und-bewertung-der-reife-fur-eine-transformation)

[4.7 Abgrenzung des Umfangs der Architekturarbeit](A_architekturvision.md#abgrenzung-des-umfangs-der-architekturarbeit)

[4.8 Entwicklung von Architekturprinzipien und Geschäftsprinzipien](A_architekturvision.md#entwicklung-von-architekturprinzipien-und-geschaftsprinzipien)

[4.8.1 Übergeordnete Architekturprinzipien](A_architekturvision.md#ubergeordnete-architekturprinzipien)

[4.8.2 Geschäftsprinzipien ](A_architekturvision.md#geschaftsprinzipien)

[4.8.3 Informationsprinzipien ](A_architekturvision.md#informationsprinzipien)

[4.8.4 Anwendungsprinzipien ](A_architekturvision.md#anwendungsprinzipien)

[4.8.5 Infrastrukturprinzipien ](A_architekturvision.md#infrastrukturprinzipien)

[4.8.6 Sicherheitsprinzipien ](A_architekturvision.md#sicherheitsprinzipien)

[4.9 Architekturvision ](A_architekturvision.md#architekturvision)

[4.10 Mögliche Risiken, Maßnahmen zur Schadensbegrenzung](A_architekturvision.md#mogliche-risiken-maßnahmen-zur-schadensbegrenzung)

[5.1 Überblick ](B_geschäftsarchitektur.md#uberblick)

[5.2 Geschäftsstrategie ](B_geschäftsarchitektur.md#geschaftsstrategie)

[5.2.1 Geschäftsprinzipien ](B_geschäftsarchitektur.md#geschaftsprinzipien-1)

[5.2.2 Treiber für die Entwicklung des SouvAP](B_geschäftsarchitektur.md#treiber-fur-die-entwicklung-des-souvap)

[5.2.2.1 Zu berücksichtigende digitale Trends in der IT](B_geschäftsarchitektur.md#zu-berucksichtigende-digitale-trends-in-der-it)

[5.2.2.2 Zu berücksichtigende Trends in der Verwaltungsarbeit](B_geschäftsarchitektur.md#zu-berucksichtigende-trends-in-der-verwaltungsarbeit)

[5.2.2.3 Gesellschaftspolitische Ableitungen](B_geschäftsarchitektur.md#gesellschaftspolitische-ableitungen)

[5.2.2.4 Schlussfolgerungen und Akzeptanzkriterien für den SouvAP](B_geschäftsarchitektur.md#schlussfolgerungen-und-akzeptanzkriterien-fur-den-souvap)

[5.2.3 Organisationssicht ](B_geschäftsarchitektur.md#organisationssicht)

[5.2.3.1 Beteiligte Organisationen, Akteure](B_geschäftsarchitektur.md#beteiligte-organisationen-akteure)

[5.2.3.2 Beteiligte Rollen im Rahmen des SouvAP](B_geschäftsarchitektur.md#beteiligte-rollen-im-rahmen-des-souvap)

[5.2.3.3 Motivation der Beteiligten Akteure](B_geschäftsarchitektur.md#motivation-der-beteiligten-akteure)

[5.2.4 Betriebsmodell ](B_geschäftsarchitektur.md#betriebsmodell)

[5.2.5 Lösungsidee ](B_geschäftsarchitektur.md#losungsidee)

[5.2.6 Legale und kommerzielle Geschäftsaspekte](B_geschäftsarchitektur.md#legale-und-kommerzielle-geschaftsaspekte)

[5.2.6.1 Gesetze und Verwaltungsvorschriften](B_geschäftsarchitektur.md#gesetze-und-verwaltungsvorschriften)

[5.2.6.2 Software-Nutzungsbedingungen](B_geschäftsarchitektur.md#software-nutzungsbedingungen)

[5.2.6.3 Abrechnungsmodelle ](B_geschäftsarchitektur.md#abrechnungsmodelle)

[5.3 Geschäftsdesign ](B_geschäftsarchitektur.md#geschaftsdesign)

[5.3.1 Beschreibung der zu entwickelnden Business Capabilities](B_geschäftsarchitektur.md#beschreibung-der-zu-entwickelnden-business-capabilities)

[5.3.1.1 Capabilities, den SouvAP selbst betreffend](B_geschäftsarchitektur.md#capabilities-den-souvap-selbst-betreffend)

[5.3.1.2 Capabilities, die Bereitstellung des SouvAP betreffend](B_geschäftsarchitektur.md#capabilities-die-bereitstellung-des-souvap-betreffend)

[5.3.1.3 Capabilities der Steuerung und Produktentwicklung](B_geschäftsarchitektur.md#capabilities-der-steuerung-und-produktentwicklung)

[5.3.2 Zuordnung der Rollen zu den Akteuren und Capabilities](B_geschäftsarchitektur.md#zuordnung-der-rollen-zu-den-akteuren-und-capabilities)

[5.3.3 Beschreibung der Liefergegenstände durch die beteiligten Partner](B_geschäftsarchitektur.md#beschreibung-der-liefergegenstande-durch-die-beteiligten-partner)

[5.3.4 Werteströme des SouvAP ](B_geschäftsarchitektur.md#wertestrome-des-souvap)

[5.3.4.1 Überblick über die Werteströme](B_geschäftsarchitektur.md#uberblick-über-die-wertestrome)

[5.3.4.2 Zuordnung der Werteströme zu den beteiligten Rollen](B_geschäftsarchitektur.md#zuordnung-der-wertestrome-zu-den-beteiligten-rollen)

[5.3.4.3 Zuordnung der Werteströme zu den Liefergegenständen](B_geschäftsarchitektur.md#zuordnung-der-wertestrome-zu-den-liefergegenstanden)

[5.3.5 Geschäftsmodell und Lieferketten](B_geschäftsarchitektur.md#geschaftsmodell-und-lieferketten)

[5.3.5.1 Liefergegenstände und Schnittstellen](B_geschäftsarchitektur.md#liefergegenstande-und-schnittstellen)

[5.3.5.2 Geschäftsmodelle der Beteiligten](B_geschäftsarchitektur.md#geschaftsmodelle-der-beteiligten)

[5.3.5.3 Implikationen aus den Geschäftsmodellen](B_geschäftsarchitektur.md#implikationen-aus-den-geschaftsmodellen)

[5.3.6 Abgeleitete Prozesse und Beispielworkflows](B_geschäftsarchitektur.md#abgeleitete-prozesse-und-beispielworkflows)

[5.3.6.1 Nutzungsszenarien der SouvAP Nutzer](B_geschäftsarchitektur.md#nutzungsszenarien-der-souvap-nutzer)

[5.3.6.2 Nutzungsszenarien der Entwicklung und Bereitstellung](B_geschäftsarchitektur.md#nutzungsszenarien-der-entwicklung-und-bereitstellung)

[5.3.7 Lösungsdesign ](B_geschäftsarchitektur.md#losungsdesign)

[5.3.7.1 Geräteklassen und Nutzungsprofile der IT-Lösung](B_geschäftsarchitektur.md#gerateklassen-und-nutzungsprofile-der-it-losung)

[5.3.7.2 Zuordnung der Nutzerprofile zu den Geräteklassen](B_geschäftsarchitektur.md#zuordnung-der-nutzerprofile-zu-den-gerateklassen)

[5.3.8 Übersicht der zu liefernden Funktionalitäten](B_geschäftsarchitektur.md#ubersicht-der-zu-liefernden-funktionalitaten)

[5.3.8.1 Priorisierung Tätigkeitsspektrum der Verwaltung](B_geschäftsarchitektur.md#priorisierung-tatigkeitsspektrum-der-verwaltung)

[5.3.8.2 Priorisierung der Business-Capabilities](B_geschäftsarchitektur.md#priorisierung-der-business-capabilities)

[5.3.8.3 Ableitung der bereitzustellenden Lösungskomponenten](B_geschäftsarchitektur.md#ableitung-der-bereitzustellenden-losungskomponenten)

[5.3.9 Beschreibung der relevanten Informationsdomänen](B_geschäftsarchitektur.md#beschreibung-der-relevanten-informationsdomanen)

[5.4 Geschäftstransformation ](B_geschäftsarchitektur.md#geschaftstransformation)

[5.4.1 Priorisierung der Aktivitäten](B_geschäftsarchitektur.md#priorisierung-der-aktivitaten)

[5.4.2 Implikationen auf die bestehenden Geschäftsmodelle der Beteiligten](B_geschäftsarchitektur.md#implikationen-auf-die-bestehenden-geschaftsmodelle-der-beteiligten)

[5.4.3 Geschäftsrisiken ](B_geschäftsarchitektur.md#geschaftsrisiken)

[5.4.3.1 Einführung des SouvAP als Teil der Digitalisierung](B_geschäftsarchitektur.md#einfuhrung-des-souvap-als-teil-der-digitalisierung)

[5.4.3.2 Risiken im Kontext der Nutzung von Software](B_geschäftsarchitektur.md#risiken-im-kontext-der-nutzung-von-software)

[5.4.3.3 Allgemeine Risiken in der Einführung des SouvAP](B_geschäftsarchitektur.md#allgemeine-risiken-in-der-einfuhrung-des-souvap)

[5.4.3.4 Spezifische Risiken mit der Einführung des SouvAP](B_geschäftsarchitektur.md#spezifische-risiken-mit-der-einfuhrung-des-souvap)

[6 Phase C: Informationssystemarchitektur](C_informationssystemarchitektur.md#phase-c-informationssystemarchitektur)

[6.1 Modelle, Tools und Techniken der Anwendungsarchitektur](C_informationssystemarchitektur.md#modelle-tools-und-techniken-der-anwendungsarchitektur)

[6.1.1 Anwendungsentwicklungsmodell](C_informationssystemarchitektur.md#anwendungsentwicklungsmodell)

[6.1.2 Systemmodell ](C_informationssystemarchitektur.md#systemmodell)

[6.1.2.1 Komponentenmodell ](C_informationssystemarchitektur.md#komponentenmodell)

[6.1.2.2 Kommunikationsmodell ](C_informationssystemarchitektur.md#kommunikationsmodell)

[6.1.2.2.1 Standardisierte Protokolle für Nachrichtenaustausch](C_informationssystemarchitektur.md#standardisierte-protokolle-fur-nachrichtenaustausch)

[6.1.2.2.2 Kommunikationsprotokoll-Standards für den SouvAP](C_informationssystemarchitektur.md#kommunikationsprotokoll-standards-fur-den-souvap)

[6.1.2.2.3 Standardisierte Protokolle für Datenaustausch](C_informationssystemarchitektur.md#standardisierte-protokolle-fur-datenaustausch)

[6.1.2.2.4 Datenaustausch-Standards für den SouvAP](C_informationssystemarchitektur.md#datenaustausch-standards-fur-den-souvap)

[6.1.2.2.5 Sicherer Datenzugriff ](C_informationssystemarchitektur.md#sicherer-datenzugriff)

[6.1.2.2.6 Authentikationsprotokoll-Standards für den SouvAP](C_informationssystemarchitektur.md#authentikationsprotokoll-standards-fur-den-souvap)

[6.1.2.2.7 Konfigurations- und Anwendungssteuerung](C_informationssystemarchitektur.md#konfigurations--und-anwendungssteuerung)

[6.1.2.2.8 Konfigurations- und Anwendungsprotokoll-Standards für den SouvAP](C_informationssystemarchitektur.md#konfigurations--und-anwendungsprotokoll-standards-fur-den-souvap)

[6.1.2.2.9 Protokoll-Standards der öffentlichen Verwaltungen](C_informationssystemarchitektur.md#protokoll-standards-der-offentlichen-verwaltungen)

[6.1.2.3 Gesamtmodell ](C_informationssystemarchitektur.md#gesamtmodell)

[6.1.2.4 SouvAP-Portal ](C_informationssystemarchitektur.md#souvap-portal)

[6.1.2.5 API-Gateway ](C_informationssystemarchitektur.md#api-gateway)

[6.1.2.6 EdgeGateWay ](C_informationssystemarchitektur.md#edgegateway)

[6.1.2.7 API-MicroGateway ](C_informationssystemarchitektur.md#api-microgateway)

[6.1.2.8 Service Mediation ](C_informationssystemarchitektur.md#service-mediation)

[6.1.2.9 Service Orchestrierung ](C_informationssystemarchitektur.md#service-orchestrierung)

[6.1.2.10 Service Registry ](C_informationssystemarchitektur.md#service-registry)

[6.1.2.10.1 API-Management ](C_informationssystemarchitektur.md#api-management)

[6.1.2.10.2 Verwaltung der Servicenutzung](C_informationssystemarchitektur.md#verwaltung-der-servicenutzung)

[6.1.2.10.3 Service Management and Monitoring](C_informationssystemarchitektur.md#service-management-and-monitoring)

[6.1.2.10.4 Fachverfahrens-Integration/-Anbindung](C_informationssystemarchitektur.md#fachverfahrens-integration-anbindung)

[6.1.2.10.5 Austauschbarkeit der OSS](C_informationssystemarchitektur.md#austauschbarkeit-der-oss)

[6.1.3 Funktionsmodell ](C_informationssystemarchitektur.md#funktionsmodell)

[6.1.3.1 Portal ](C_informationssystemarchitektur.md#portal)

[6.1.3.2 Cloud-Dateiablage ](C_informationssystemarchitektur.md#cloud-dateiablage)

[6.1.3.3 Web-Office ](C_informationssystemarchitektur.md#web-office)

[6.1.3.4 PDF-Anzeige ](C_informationssystemarchitektur.md#pdf-anzeige)

[6.1.3.5 Medienwiedergabe ](C_informationssystemarchitektur.md#medienwiedergabe)

[6.1.3.6 Dateikompression ](C_informationssystemarchitektur.md#dateikompression)

[6.1.3.7 Anti-Virus ](C_informationssystemarchitektur.md#anti-virus)

[6.1.3.8 Passwort-Tresor (Optional)](C_informationssystemarchitektur.md#passwort-tresor-optional)

[6.1.3.9 Gemeinsame Nutzung von Dateien](C_informationssystemarchitektur.md#gemeinsame-nutzung-von-dateien)

[6.1.3.10 Lernmanagementsystem (Optional)](C_informationssystemarchitektur.md#lernmanagementsystem-optional)

[6.1.3.11 Wissensmanagementsystem](C_informationssystemarchitektur.md#wissensmanagementsystem)

[6.1.3.12 Projektmanagementsystem](C_informationssystemarchitektur.md#projektmanagementsystem)

[6.1.3.13 Umfrage- und Abstimmungssystem](C_informationssystemarchitektur.md#umfrage--und-abstimmungssystem)

[6.1.3.14 E-Mail und Kalender ](C_informationssystemarchitektur.md#e-mail-und-kalender)

[6.1.3.15 Kalender ](C_informationssystemarchitektur.md#kalender)

[6.1.3.16 Adressbuch ](C_informationssystemarchitektur.md#adressbuch)

[6.1.3.17 Video- und Telefonkonferenzen](C_informationssystemarchitektur.md#video--und-telefonkonferenzen)

[6.1.3.18 Kurznachrichten ](C_informationssystemarchitektur.md#kurznachrichten)

[6.1.3.19 Live Streaming (Optional)](C_informationssystemarchitektur.md#live-streaming-optional)

[6.1.3.20 Whiteboard ](C_informationssystemarchitektur.md#whiteboard)

[6.1.3.21 Visualisierungstools ](C_informationssystemarchitektur.md#visualisierungstools)

[6.1.4 Produktmodell ](C_informationssystemarchitektur.md#produktmodell)

[6.1.5 Integrationsmodell ](C_informationssystemarchitektur.md#integrationsmodell)

[6.1.5.1 API-Gateway als Integrationsschicht](C_informationssystemarchitektur.md#api-gateway-als-integrationsschicht)

[6.1.5.2 Einsatz von Schnittstellenstandards](C_informationssystemarchitektur.md#einsatz-von-schnittstellenstandards)

[6.1.5.3 Anpassung der Komponenten-Schnittstellen](C_informationssystemarchitektur.md#anpassung-der-komponenten-schnittstellen)

[6.1.5.4 Testen und Optimieren ](C_informationssystemarchitektur.md#testen-und-optimieren)

[6.1.5.5 Bündelung der Applikation in Containerimage](C_informationssystemarchitektur.md#bundelung-der-applikation-in-containerimage)

[6.1.6 Servicemodell ](C_informationssystemarchitektur.md#servicemodell)

[6.1.7 Sicherheits-Servicemodell](C_informationssystemarchitektur.md#sicherheits-servicemodell)

[6.1.7.1 Sicherheit AAA ](C_informationssystemarchitektur.md#sicherheit-aaa)

[6.1.7.2 Zero Trust ](C_informationssystemarchitektur.md#zero-trust)

[6.1.8 Mensch-zu-Maschine-Kommunikation](C_informationssystemarchitektur.md#mensch-zu-maschine-kommunikation)

[6.1.8.1 Maschine-zu-Maschine-Kommunikation](C_informationssystemarchitektur.md#maschine-zu-maschine-kommunikation)

[6.1.8.2 Security Token ](C_informationssystemarchitektur.md#security-token)

[6.1.8.2.1 Authentifizierungstoken](C_informationssystemarchitektur.md#authentifizierungstoken)

[6.1.8.2.2 Kommunikation ](C_informationssystemarchitektur.md#kommunikation)

[6.1.8.2.3 Zugriffssteuerungstoken](C_informationssystemarchitektur.md#zugriffssteuerungstoken)

[6.1.8.2.4 Begrenzte Gültigkeitsdauer von Tokens](C_informationssystemarchitektur.md#begrenzte-gultigkeitsdauer-von-tokens)

[6.1.8.2.5 Token-Rotation ](C_informationssystemarchitektur.md#token-rotation)

[6.1.8.3 Sicherheitsvorfälle ](C_informationssystemarchitektur.md#sicherheitsvorfalle)

[6.1.9 Techniken der Anwendungsarchitektur](C_informationssystemarchitektur.md#techniken-der-anwendungsarchitektur)

[6.1.9.1 Identifizierung und Auswahl von Open-Source-Software](C_informationssystemarchitektur.md#identifizierung-und-auswahl-von-open-source-software)

[6.1.9.2 Integration von Open-Source-Software](C_informationssystemarchitektur.md#integration-von-open-source-software)

[6.1.9.3 Bewertung von Open-Source-Software-Komponenten](C_informationssystemarchitektur.md#bewertung-von-open-source-software-komponenten)

[6.2 Beschreibung der Ausgangslage](C_informationssystemarchitektur.md#beschreibung-der-ausgangslage)

[6.2.1 Funktionsmodell ](C_informationssystemarchitektur.md#funktionsmodell-1)

[6.2.2 Systemmodell ](C_informationssystemarchitektur.md#systemmodell-1)

[6.2.3 Bereitstellungsmodell ](C_informationssystemarchitektur.md#bereitstellungsmodell)

[6.3 Beschreibung der Zielarchitektur](C_informationssystemarchitektur.md#beschreibung-der-zielarchitektur)

[6.4 Analyse des Transformationsbedarfs](C_informationssystemarchitektur.md#analyse-des-transformationsbedarfs)

[6.4.1 OSS Kriterien ](C_informationssystemarchitektur.md#oss-kriterien)

[6.4.2 Medienbruchfreie Interoperabilität](C_informationssystemarchitektur.md#medienbruchfreie-interoperabilitat)

[6.4.3 Funktionsumfang ](C_informationssystemarchitektur.md#funktionsumfang)

[6.4.4 Barrierefreiheit ](C_informationssystemarchitektur.md#barrierefreiheit)

[6.4.5 Übergreifendes einheitliches Bedienkonzept](C_informationssystemarchitektur.md#ubergreifendes-einheitliches-bedienkonzept)

[6.5 Entwicklung von Arbeitspaketen für die Transformation](C_informationssystemarchitektur.md#entwicklung-von-arbeitspaketen-fur-die-transformation)

[7 Phase D: Technologiearchitektur](D_technologiearchitektur.md#phase-d-technologiearchitektur)

[7.1 Schnittstellen ](D_technologiearchitektur.md#schnittstellen)

[7.2 DevOps Phasen ](D_technologiearchitektur.md#devops-phasen)

[7.2.1 Digitale Souveränität ](D_technologiearchitektur.md#digitale-souveranitat)

[7.2.1.1 Digitale Performance Metriken](D_technologiearchitektur.md#digitale-performance-metriken)

[7.2.2 Implementierung von Applikationen](D_technologiearchitektur.md#implementierung-von-applikationen)

[7.2.2.1 Versionierung ](D_technologiearchitektur.md#versionierung)

[7.2.2.2 Container ](D_technologiearchitektur.md#container)

[7.2.2.3 Helm Charts ](D_technologiearchitektur.md#helm-charts)

[7.2.2.4 Rollout Prozess ](D_technologiearchitektur.md#rollout-prozess)

[7.2.2.4.1 Lizenz Compliance ](D_technologiearchitektur.md#lizenz-compliance)

[7.2.3 Segmentierung im Schichtenmodell](D_technologiearchitektur.md#segmentierung-im-schichtenmodell)

[7.2.4 Netzwerk ](D_technologiearchitektur.md#netzwerk)

[7.2.5 Implementierung ](D_technologiearchitektur.md#implementierung)

[7.2.6 Datenschicht ](D_technologiearchitektur.md#datenschicht)

[7.2.6.1 Logging ](D_technologiearchitektur.md#logging)

[7.2.6.1.1 Kubernetes Logs ](D_technologiearchitektur.md#kubernetes-logs)

[7.2.6.1.2 Application Logs ](D_technologiearchitektur.md#application-logs)

[7.2.6.1.3 Monitoring ](D_technologiearchitektur.md#monitoring)

[7.2.7 Continuous Integration ](D_technologiearchitektur.md#continuous-integration)

[7.2.7.1 Verteilung der Pipeline](D_technologiearchitektur.md#verteilung-der-pipeline)

[7.2.7.2 Freigaben ](D_technologiearchitektur.md#freigaben)

[7.2.7.3 GitOps Rollout ](D_technologiearchitektur.md#gitops-rollout)

[7.2.8 Modularität ](D_technologiearchitektur.md#modularitat)

[7.2.8.1 Registry ](D_technologiearchitektur.md#registry)

[7.2.9 Föderierung ](D_technologiearchitektur.md#foderierung)

[7.3 Phase D+S: Sicherheitsarchitektur](D_technologiearchitektur.md#phase-ds-sicherheitsarchitektur)

[7.3.1 DevSecOps Phasen und Sicherheitsaspekte](D_technologiearchitektur.md#devsecops-phasen-und-sicherheitsaspekte)

[7.3.1.1 Signaturen ](D_technologiearchitektur.md#signaturen)

[7.3.1.2 Metriken für die Umsetzung des DevSecOps Prozesses](D_technologiearchitektur.md#metriken-fur-die-umsetzung-des-devsecops-prozesses)

[7.3.2 Tiefe Sicherheit ](D_technologiearchitektur.md#tiefe-sicherheit)

[7.3.3 Applikationen ](D_technologiearchitektur.md#applikationen)

[7.3.3.1 Sicherer Auslieferung und Betrieb von Applikationen](D_technologiearchitektur.md#sicherer-auslieferung-und-betrieb-von-applikationen)

[7.3.3.1.1 Container ](D_technologiearchitektur.md#container-1)

[7.3.3.1.2 Stateless Containers ](D_technologiearchitektur.md#stateless-containers)

[7.3.3.1.3 Container Images ](D_technologiearchitektur.md#container-images)

[7.3.3.1.4 Nutzungstypen ](D_technologiearchitektur.md#nutzungstypen)

[7.3.3.1.5 Helm Charts ](D_technologiearchitektur.md#helm-charts-1)

[7.3.3.1.6 Secret Management ](D_technologiearchitektur.md#secret-management)

[7.3.3.1.7 Git Repository ](D_technologiearchitektur.md#git-repository)

[7.3.3.1.8 Rollout Prozess ](D_technologiearchitektur.md#rollout-prozess-1)

[7.3.3.1.9 SBOM ](D_technologiearchitektur.md#sbom)

[7.3.3.2 Segmentierung im Schichtenmodell](D_technologiearchitektur.md#segmentierung-im-schichtenmodell-1)

[7.3.3.3 Frontend ](D_technologiearchitektur.md#frontend)

[7.3.3.4 Netzwerk ](D_technologiearchitektur.md#netzwerk-1)

[7.3.3.4.1 Verschlüsselung auf der Netzwerkschicht](D_technologiearchitektur.md#verschlusselung-auf-der-netzwerkschicht)

[7.3.3.4.2 Servicemesh ](D_technologiearchitektur.md#servicemesh)

[7.3.3.4.3 Verschlüsselung ](D_technologiearchitektur.md#verschlusselung)

[7.3.3.4.4 Isolation P-A-P ](D_technologiearchitektur.md#isolation-p-a-p)

[7.3.3.4.5 Microsegmentation ](D_technologiearchitektur.md#microsegmentation)

[7.3.3.4.6 Verschlüsselung der Datenschicht (Encryption on Rest)](D_technologiearchitektur.md#verschlusselung-der-datenschicht-encryption-on-rest)

[7.3.3.4.7 Datenbanken ](D_technologiearchitektur.md#datenbanken)

[7.3.3.4.8 Backup-Restore ](D_technologiearchitektur.md#backup-restore)

[7.3.3.4.9 Logging ](D_technologiearchitektur.md#logging-1)

[7.3.3.4.9.1 Audit Logs ](D_technologiearchitektur.md#audit-logs)

[7.3.3.4.9.2 Application Logs ](D_technologiearchitektur.md#application-logs-1)

[7.3.3.4.9.3 Monitoring ](D_technologiearchitektur.md#monitoring-1)

[7.3.3.4.10 Alerting ](D_technologiearchitektur.md#alerting)

[7.3.3.4.11 Management Cluster ](D_technologiearchitektur.md#management-cluster)

[7.3.3.5 Continuous Integration](D_technologiearchitektur.md#continuous-integration-1)

[7.3.3.5.1 Registry ](D_technologiearchitektur.md#registry-1)

[7.3.3.5.2 Scannen ](D_technologiearchitektur.md#scannen)

[7.3.3.6 Policies ](#_Toc148432216)

[7.3.3.7 Signieren ](D_technologiearchitektur.md#signieren)

[7.3.3.7.1 Keyless Signature ](D_technologiearchitektur.md#keyless-signature)

[7.3.3.8 Sicherheit ](#_Toc148432219)

[7.3.3.8.1 Container ](D_technologiearchitektur.md#container-2)

[7.3.3.8.2 Microsegmentierung durch Networkpolicies](D_technologiearchitektur.md#microsegmentierung-durch-networkpolicies)

[7.3.3.9 Secret Management ](D_technologiearchitektur.md#secret-management-1)

[7.3.3.10 Kubernetes-spezifische Sicherheitsmaßnahmen](D_technologiearchitektur.md#kubernetes-spezifische-sicherheitsmaßnahmen)

[7.3.3.11 Netzwerk Zonen ](D_technologiearchitektur.md#netzwerk-zonen)

[7.3.3.12 Föderierung ](D_technologiearchitektur.md#foderierung-1)

[7.3.3.13 Transport Layer Zertifikate](D_technologiearchitektur.md#transport-layer-zertifikate)

[7.3.3.14 Weitere notwendige sicherheitrelevante Dienste](D_technologiearchitektur.md#weitere-notwendige-sicherheitrelevante-dienste)

[7.3.3.14.1 ACME Server RFC 8555 ](D_technologiearchitektur.md#acme-server-rfc-8555)

[7.3.3.14.2 Logging, Monitoring, Alerting](D_technologiearchitektur.md#logging-monitoring-alerting)

[7.3.3.14.3 Scanning im Betrieb ](D_technologiearchitektur.md#scanning-im-betrieb)

[7.3.3.14.4 Sigstore Transparency Log](D_technologiearchitektur.md#sigstore-transparency-log)

[7.3.3.14.5 Signaturprüfung ](D_technologiearchitektur.md#signaturprufung)

[7.3.3.14.6 Zero Trust ](D_technologiearchitektur.md#zero-trust-1)

[7.3.3.15 Standards ](D_technologiearchitektur.md#standards)

[7.4 Phase D +M: Betrieb des Souveränen Arbeitsplatzes in einer Multicluster Umgebung](D_technologiearchitektur.md#phase-d-m-betrieb-des-souveranen-arbeitsplatzes-in-einer-multicluster-umgebung)

[7.4.1 Controlplane ](D_technologiearchitektur.md#controlplane)

[7.4.2 Anforderung ](D_technologiearchitektur.md#anforderung)

[7.4.3 Netzwerk ](D_technologiearchitektur.md#netzwerk-2)

[7.4.4 Kopplung durch Service Mesh](D_technologiearchitektur.md#kopplung-durch-service-mesh)

[7.4.5 Loadbalancer ](D_technologiearchitektur.md#loadbalancer)

[7.4.6 Storage ](D_technologiearchitektur.md#storage)

[7.4.7 Synchronisation ](D_technologiearchitektur.md#synchronisation)

[7.4.7.1 Datenbanken ](D_technologiearchitektur.md#datenbanken-1)

[7.4.7.2 Blockspeicher ](D_technologiearchitektur.md#blockspeicher)

[7.4.7.3 Verschlüsselung des Filesystems](D_technologiearchitektur.md#verschlusselung-des-filesystems)

[7.4.8 Monitoring ](D_technologiearchitektur.md#monitoring-2)

[7.4.9 Vertrauenszone ](D_technologiearchitektur.md#vertrauenszone)

[8 Phase E: Opportunities und Lösungen](E_opportunities.md#phase-e-opportunities-und-losungen)

[8.1 Einfluss der Architektur auf unternehmerische Standards](E_opportunities.md#einfluss-der-architektur-auf-unternehmerische-standards)

[8.1.1 Identifikation und Bewertung transformationsbedingter Faktoren](E_opportunities.md#identifikation-und-bewertung-transformationsbedingter-faktoren)

[8.1.1.1 Transformationsbedingte Faktoren: Nutzer](E_opportunities.md#transformationsbedingte-faktoren-nutzer)

[8.1.1.2 Transformationsbedingte Faktoren: Software-Lieferanten](E_opportunities.md#transformationsbedingte-faktoren-software-lieferanten)

[8.1.1.3 Transformationsbedingte Faktoren: Plattform-/Software-Betreiber](E_opportunities.md#transformationsbedingte-faktoren-plattform-software-betreiber)

[8.1.2 Bewertung der Transformationsreife der beteiligten Organisationen](E_opportunities.md#bewertung-der-transformationsreife-der-beteiligten-organisationen)

[8.2 Identifikation transformationsbeeinflussender geschäftlicher Rahmbedingungen](E_opportunities.md#identifikation-transformationsbeeinflussender-geschaftlicher-rahmbedingungen)

[8.2.1 Transformation unterbrechungsfrei](E_opportunities.md#transformation-unterbrechungsfrei)

[8.2.2 Transformation schrittweise](E_opportunities.md#transformation-schrittweise)

[8.2.3 Transformation wirtschaftlich optimiert](E_opportunities.md#transformation-wirtschaftlich-optimiert)

[8.2.4 Transformation risikoorientiert](E_opportunities.md#transformation-risikoorientiert)

[8.2.5 Transformation planungssicher](E_opportunities.md#transformation-planungssicher)

[8.3 Zusammenführung des Transformationsbedarfs gemäß Phase B, C und D](E_opportunities.md#zusammenfuhrung-des-transformationsbedarfs-gemaß-phase-b-c-und-d)

[8.3.1 Transformation der Geschäftsarchitektur](E_opportunities.md#transformation-der-geschaftsarchitektur)

[8.3.1.1 Ausgangslage ](E_opportunities.md#ausgangslage-1)

[8.3.1.2 Rollengruppe Nutzer ](E_opportunities.md#rollengruppe-nutzer)

[8.3.1.3 Rollengruppe OS-Ökosystem](E_opportunities.md#rollengruppe-os-okosystem)

[8.3.1.4 Rollengruppe Standort-Betreiber](E_opportunities.md#rollengruppe-standort-betreiber)

[8.3.2 Transformation der Informationssystemarchitektur](E_opportunities.md#transformation-der-informationssystemarchitektur)

[8.3.3 Transformation der Infrastrukturarchitektur](E_opportunities.md#transformation-der-infrastrukturarchitektur)

[8.4 Zusammenführung und Abgleich der Anforderungen an Interoperabilität](E_opportunities.md#zusammenfuhrung-und-abgleich-der-anforderungen-an-interoperabilitat)

[8.4.1 Geschäftsprozesse ](E_opportunities.md#geschaftsprozesse)

[8.4.2 Informationssysteme ](E_opportunities.md#informationssysteme)

[8.4.3 Infrastruktur ](E_opportunities.md#infrastruktur)

[8.5 Aufdecken und Bewerten von Abhängigkeiten](E_opportunities.md#aufdecken-und-bewerten-von-abhangigkeiten)

[8.5.1 Nutzer ](E_opportunities.md#nutzer)

[8.5.2 Software-Lieferanten ](E_opportunities.md#software-lieferanten)

[8.5.3 Plattform-/Software-Betreiber](E_opportunities.md#plattform-software-betreiber)

[8.5.4 Betriebliche Aspekte ](E_opportunities.md#betriebliche-aspekte)

[8.6 Prüfung auf Transformationsreife, Bewertung von Transformations-Risiken](E_opportunities.md#prufung-auf-transformationsreife-bewertung-von-transformations-risiken)

[8.7 Festlegung der Strategie für Implementierung/Migration](E_opportunities.md#festlegung-der-strategie-fur-implementierungmigration)

[8.8 Festlegung der benötigten Arbeitspakete](E_opportunities.md#festlegung-der-benotigten-arbeitspakete)

[8.9 Benötigte Transitions-Architekturen](E_opportunities.md#benotigte-transitions-architekturen)

[8.10 Architektur-Roadmap: Pläne für Implementierung und Migration](E_opportunities.md#architektur-roadmap-plane-fur-implementierung-und-migration)

[8.10.1 Rahmenbedingungen und Zielsetzung](E_opportunities.md#rahmenbedingungen-und-zielsetzung)

[8.10.2 Architektur-Roadmap ](E_opportunities.md#architektur-roadmap)

[8.10.3 Stolpersteine ](E_opportunities.md#stolpersteine)

[9 Phase F: Integrations-/ Migrationsplan](F_integrationsplan.md)

[10 Phase G: Governance Struktur](G_governance.md)

[11 Phase H: Architektur Change Management](H_changemanagement.md)

[12 Appendix](0_appendix.md#appendix)
