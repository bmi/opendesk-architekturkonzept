Phase E: Opportunities und Lösungen
===================================

Einfluss der Architektur auf unternehmerische Standards
-------------------------------------------------------

Die Architekturarbeit transformiert das Unternehmen vom Ist-Stand hin zu
einer Zielarchitektur, die weitestgehend durch die Architekturvision
beeinflusst ist. Das hat Auswirkungen auf Faktoren, die im Wesentlichen
Risiken, potenzielle Probleme, Annahmen, Abhängigkeiten, Tätigkeiten und
Auswirkungen umfassen. Die folgenden Unterkapitel identifizieren und
bewerten transformationsbedingte Faktoren und bewerten die Fähigkeit der
beteiligten Organisationen, die Auswirkungen der Transformation tragen
zu können.

### Identifikation und Bewertung transformationsbedingter Faktoren

Die Architekturarbeit erfordert im Ergebnis Transformationen der
Arbeitsweisen

-   der Nutzer der geplanten Zielarchitektur,

-   der beteiligten Software-Lieferanten (Integratoren) im Kontext der
    neuen Informationssystemarchitektur und

-   der Betreiber der zukünftigen Infrastruktur, die ebenfalls durch die
    Architekturarbeit Wandlungen unterworfen ist.

Dieser Abschnitt notiert tabellarisch die bisher bekannten
transformationsspezifischen Faktoren nebst einer Kurzbeschreibung. Zu
jedem Faktor sind ergänzend Schlussfolgerungen erarbeitet, die bei der
Umsetzung der Architekturarbeit Berücksichtigung erfahren sollen. Die
Faktoren werden in Gruppen gegliedert, sodass ihre Relevanz bezogen auf
die jeweils betroffenen Nutzer, Hersteller und Betreiber differenziert
ist.

#### Transformationsbedingte Faktoren: Nutzer

Die nachstehenden Tabellen beschreiben Faktoren, die aus der Perspektive
der Benutzer im Vorfeld der Transformation zu berücksichtigen sind. Die
jeweils ausgewiesenen Schlussfolgerungen beschreiben Auswirkungen, die
die genannten Faktoren auslösen können.

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Der SouvAP ist Cloud-nativ.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Der SouvAP ist ein Cloud-gebundenes Werkzeugbündel. Jedwede Interaktion mit den Werkzeugen setzt eine funktionierende Internetverbindung voraus.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Es bestehen Risiken bezüglich erhöhter Latenz sowie Service-Verlust aufgrund von Verbindungsabbrüchen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Veränderte Dateiablage.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Der SouvAP ist ein Cloud-Arbeitsplatz. In der ersten Implementierung ist die Datenablage auf lokalen Medien nicht unterstützt.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Nutzer sind mit Hinweisen auf die Vorteile der Datenablage in Cloud-Speichern dahingehend zu motivieren, dass sie die Datenablage in Cloud-Speichern als wertvolle Neuerung akzeptieren.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Wechsel des Betriebssystems.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Erfahrene Windows-Benutzer müssen sich in Teilen an andersartige Bedienkonzepte gewöhnen.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Nutzer sind frühzeitig mit den Bedienkonzepten der neuen Betriebssystembasis vertraut zu machen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Neue Werkzeuge.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Vertraute Werkzeuge werden durch alternative Produkte ersetzt, die mehrheitlich abweichende Bedienkonzepte umsetzen.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Nutzer sind frühzeitig und schrittweise auf den Umgang mit den neuen Werkzeugen vorzubereiten.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Bedenken bezüglich Privatsphäre.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Der Cloud-Arbeitsplatz nutzt anteilige Ressourcen innerhalb eines großen Rechnerverbunds. Gleiches gilt für die Datenablege, die einerseits dem Nutzer den Schutz vertraulicher Daten zusichert, ihm andererseits verbesserte Zusammenarbeit in der Datenverarbeitung verspricht. Diese gegensätzlichen Ziele lassen den Nutzer eventuell zweifeln, dass die Vertraulichkeit von Daten anforderungsgerecht eingehalten wird.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Nutzer sind dahingehend aufzuklären, dass der Betreiber des SouvAP an gesetzliche Vorgaben gebunden ist, die ihn zur Umsetzung von Maßnahmen zum Schutz insbesondere personenbezogener Daten verpflichtet. Gleichwohl ist der Anwender über seine Mitwirkungspflicht aufzuklären, die vorhandenen Schutztechniken auf seine persönlichen Daten anforderungsgerecht anzuwenden.</td>
</tr>
</tbody>
</table>

#### Transformationsbedingte Faktoren: Software-Lieferanten

Die nachstehenden Tabellen beschreiben Faktoren, die aus der Perspektive
der Software-Lieferanten im Vorfeld der Transformation zu
berücksichtigen sind. Die jeweils ausgewiesenen Schlussfolgerungen
beschreiben Auswirkungen, die die genannten Faktoren auslösen können.

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>SouvAP ist Container-nativ.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Die Software muss die essenziellen Fähigkeiten Cloud-nativer Software (Elastizität, Self-Service und nutzungsorientierte Abrechenbarkeit) beherrschen. Außerdem müssen die SouvAP-Module für den Einsatz in einer DVS-konformen Cloud vorbereitet sein.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Es sind die DVS-Anforderungen an Container umzusetzen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Erweiterte Sicherheitsanforderungen.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>In Cloud-Umgebungen sind Sicherheitsmaßnahmen betreffend Datensicherheit, Zugriffsschutz und unternehmerischer / regulatorischer Compliance bereits innerhalb der Software zu leisten. Traditionelle Schutzmethoden innerhalb des Netzwerks stehen hier eventuell nicht im Zugriff.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Im Zuge der Erstintegration sowie bei Migrationsvorhaben sind Sicherheitsanforderungen zu erheben und dazu passende Maßnahmen sind in die Software zu integrieren. Die Wirksamkeit der Maßnahmen ist mit austomatisierten Tests nachzuweisen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Vermeiden von Herstellerbindungen.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Moderne Entwicklungsprojekte greifen oft in großem Stil auf bereits vorhandene Software-Bibliotheken zu. Ist die Nutzung solcher Bibliotheken an kostenpflichtige Lizenzen gebunden, dann entsteht daraus eventuell eine Herstellerbindung. Im Einsatz auf Seite eines Dienstleisters ist die Lizenz außerdem meist auf Seite des Dienstleisters zu beschaffen. Im Ergebnis kann daraus eine Bindung an den Dienstleister entstehen, wenn kein oder nur wenige alternative Dienstleister entsprechende Lizenzverträge unterhalten.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Software-Lieferanten müssen etwa verwendete Software-Bibliotheken von Drittanbietern auf die Gefahr einer Herstellerbindung hin prüfen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Service-Zuverlässigkeit.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Die Verfügbarkeit von Cloud-Services steht und fällt mit der Verfügbarkeit des Netzwerks „Ende-zu-Ende“. Dialog-orientierte Services müssen robust gegen Netzwerkunterbrechungen und daraus resultierenden Sitzungsabbrüchen sein.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Anwendungen müssen Sitzungsabbrüche kompensieren können und beim Sitzungsneuaufbau den Weiterbetrieb ab einem Wiederanlaufpunkt ermöglichen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Mehrschichtinstanziierung in Anlehnung an die DVS.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Die DVS empfiehlt eine Trennung der Programmbestandteile Präsentationsschicht, Datenverarbeitungsschicht und Datenverwahrschicht in eigenständige Ablaufumgebungen, die gegeneinander mindestens mit Portfiltern zu schützen sind. Die einzelnen Ablaufumgebungen sollen jeweils in voneinander abgegrenzten Sicherheitsbereichen operieren.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Software ist bezüglich der jeweiligen Schichten zu separieren. Es sind Kommunikationsprotokolle zu entwickeln und zu integrieren, die erforderliche Interprozesskommunikationen und Ablaufsteuerungen kapseln.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Technische Evolution.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Mit zunehmender Akzeptanz von Cloud-Computing haben Hardware-Hersteller das technische Fundament zyklisch überarbeitet und ehemals in Software auf Standardhardware abgewickelte Funktionen in dedizierter Hardware realisiert. Das hat teils erhebliche Leistungssprünge ermöglicht. Allerdings mussten Software-Entwickler stets ihre Software auf die neuen Möglichkeiten hin anpassen, um diese nutzen zu können. Es ist zu erwarten, dass diese technische Evolution unaufhaltsam fortschreitet.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Die Software-Entwickler müssen ihre Produkte zyklisch auf neue technische Möglichkeiten hin zuschneiden. Dies kann eine Überarbeitung von Schnittstellen und Funktionen erfordern.</td>
</tr>
</tbody>
</table>

#### Transformationsbedingte Faktoren: Plattform-/Software-Betreiber

Die nachstehenden Tabellen beschreiben Faktoren, die aus der Perspektive
der Plattform-/Software-Betreiber im Vorfeld der Transformation zu
berücksichtigen sind. Die jeweils ausgewiesenen Schlussfolgerungen
beschreiben Auswirkungen, die die genannten Faktoren auslösen können.

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Die Plattform ist konform zur DVS aufzubauen.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Die DVS sieht mindestens die Bereitstellung von Container-Clustern in den Sicherheitszonen DMZ, Intranet und Schutzbedarf hoch vor.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Es sind 3 separate Produktivumgebungen aufzubauen und diese sind jeweilig anforderungsgerecht mit Sicherheitsmaßnahmen auszustatten und entsprechend der organisatorisch erforderlichen Sicherheitsvorkehrungen zu betreiben.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Der Serviceumfang ist noch unklar.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Bisher liegen keine Muster für vertragliche Dienstqualitätsvereinbarungen vor. Ferner sind bisher keine Kundenanforderungen bekannt, die den Leistungsschnitt für betriebliche Services qualifizieren.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Es ist davon auszugehen, dass der Plattform/Software-Betreiber aus Sicht der Nutzer erster Ansprechpartner für etwa auftretende Störungen, aber auch für nutzungsunterstützende Anfragen wird. Es sind diesbezügliche Leistungsschnitte (eventuell mit gestaffeltem Leistungsumfang) sowie Zeiten für personell begleiteten Betrieb festzulegen. Zu den daraus entwickelten Leistungsbündeln sind Preisindikationen festzulegen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Rollen für Image-Bereitstellung, -Betrieb und -Fortschreibung.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>In einer ersten Überprüfung der technischen und operativen Aufgaben innerhalb eines DVS-konformen Ökosystems hat die DVS ein Modell ausgearbeitet, das die Bereitstellung und Prüfung von Standard-Images auf die Rollen Image-Hersteller und Standard-Image-Entwickler verteilt. Dabei wurde festgelegt, dass auf Images für den produktiven Einsatz abschließend eine Sicherheitsüberprüfung mit einem Image-Scanner anzuwenden ist und zum Abschluss das Image zu signieren ist. Damit obliegen Image-Erstellung und Image-Fortschreibung alleine dem Software-Lieferanten. Der Image-Betrieb wiederum fällt in den Aufgabenbereich des Plattform-/Software-Betreibers.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Vor dem Image-Betrieb sind Images auf Betreiberseite einer Qualitätssicherung zu unterziehen. Anschließend ist das Image in das lokale Repository des Plattform-Betreibers zu überführen. Der Betreiber muss zyklisch auf Open CoDE die Verfügbarkeit von Updates prüfen und diese zeitnah dem vorab beschriebenen Bereitstellungsprozess unterwerfen.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Kurze Innovationszyklen bei Kubernetes.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Gemäß der Roadmap der Kubernetes Community beträgt der Lebenszyklus zu jedem Kubernetes-Release 6 Monate. Die Wartungsleistungen übernimmt die Kubernetes Community nach Freigabe eines neuen Release für das Vorgängerrelease zu 100%. Für den Vorvorgänger sind Wartungsleistungen auf 2 Monate beschränkt. Die Wartungsleistung zu einem neuen Release ist somit auf 14 Monate beschränkt.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Plattformbetreiber müssen ihre Kubernetes-Installationen zyklisch auffrischen bzw. erneuern. Dazu ist parallel zum Wirkbetrieb der Aufbau eines Folgerelease erforderlich, der zusätzliche technische und personelle Ressourcen bindet. Bisher liegen keine Erfahrungen vor, ob eine Migration zu einem Folgerelease unterbrechungsfrei gelingt und welcher Aufwand insgesamt mit einer Migration verbunden ist.</td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th>Faktor</th>
<th>Plattform-/Software-Betreiber müssen CI/CD-Pipeline integrieren.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Erklärung</td>
<td>Zuvor wurde bereits herausgestellt, dass jeder Software-Betreiber die Überführung von auf Open CoDE abgelegten Images in das lokale Repository der zugrunde liegenden Plattform über eine lokale CI/CD-Pipeline abwickeln muss. Demnach ist die Ergänzung der Infrastruktur um eine CI/CD-Pipeline unverzichtbar.</td>
</tr>
<tr class="even">
<td>Schlussfolgerung</td>
<td>Es sind notwendige und mögliche Aufgabenstellungen zu erörtern, die die CI/CD-Pipeline automatisiert umsetzen muss. Im Ergebnis ist ein Lösungsdesign für eine betreiberseitige CI/CD-Pipeline zu fertigen, die insbesondere die unternehmerischen Richtlinien des Plattformbetreibers abbildet.</td>
</tr>
</tbody>
</table>

### Bewertung der Transformationsreife der beteiligten Organisationen

Der Reifegrad der öffentlichen Verwaltung für eine architekturbegründete
Transformation wurde in Phase A Architekturvision (Kapitel 4.6) als mehr
als hinreichend qualifiziert. Dies wurde unter anderem damit begründet,
dass regelmäßige strategische Planungen, eventuell begleitet durch
Beratungsleistungen namhafter professioneller Unternehmen, die
öffentlichen Verwaltungen auf ein hohes Innovationsniveau und damit auf
maximale Handlungsfähigkeit heben können.

Die Transformation der Informationssystemarchitektur innerhalb des
Architekturvorhabens sollen namhafte Mitgliedern des OS-Ökosystems
vorantreiben. Alle Beteiligten kennen die mit dem Projekt verbundenen
Leistungsbeschreibungen. Vor diesem Hintergrund ist davon auszugehen,
dass die projektbeteiligten Hersteller bereits mit Projektbeginn über
den benötigten Reifegrad verfügt haben oder dies im Laufe der
Projektarbeit erreichen wollen.

Identifikation transformationsbeeinflussender geschäftlicher Rahmbedingungen
----------------------------------------------------------------------------

Der Souveräne Arbeitsplatz SouvAP soll die digitale Souveränität des
staatlichen Handels maximieren. In der Praxis soll er dazu die aktuell
im Einsatz befindlichen Werkzeuge zur Unterstützung des digitalen
Verwaltungshandelns ersetzen. Dies kann potenziell Disruptionen auf
Seite der Nutzer und der Betreiber der Werkzeuge auslösen. Nachstehend
sind geschäftliche Rahmenbedingungen aufgezählt, die bei der Umsetzung
er Transformation Berücksichtigung finden sollten.

### Transformation unterbrechungsfrei

Die betriebliche Bereitstellung des SouvAP soll zunächst in einer
Erprobungsphase auf maximale Nutzerakzeptanz hin weiterentwickelt werden
und perspektivisch eine hundertprozentige Ablösung derjenigen
proprietären Werkzeuge ermöglichen, deren Rahmenbedingungen im
Widerspruch zur europäischen Datenschutz-Grundverordnung stehen. Die
Erprobungsphase stellt keine Gefahr für den unterbrechungsfreien
Geschäftsbetrieb. Die anschließende Produktivsetzung muss
unterbrechungsfrei gelingen.

### Transformation schrittweise

Mit der vorliegenden Leistungsbeschreibung wurde ein zeitlicher und
wirtschaftlicher Planungshorizont für das Nahziel "Erprobung des SouvAP"
gelegt. Dem stehen bisher nicht bekannte monetäre und temporäre Aufwände
für Nutzer- und Betreiberschulungen entgegen. Auch zum zu erwartenden
Aufwand für Softwarepflege liegen bisher keine Erfahrungen vor, die eine
Hochrechnung hin zu verlässlichen und damit planbaren Kostenstrukturen
ermöglichen.

### Transformation wirtschaftlich optimiert

Für die öffentliche Verwaltung ist aus kaufmännischer Sicht eine zügige
Umsetzung der Transformation und dabei die zeitnahe Ablösung bestehender
Lizenzverträge anzustreben. Mit Produktivsetzung kann der prozentuale
Anteil der Nutzung der neuen Architektur schrittweise gesteigert und der
Anteil der alten Architektur im Gleichschritt zurückgefahren werden.

### Transformation risikoorientiert

Die Transformation kann über einen Parallelbetrieb neuer und alter
Werkzeuge respektive Plattformen erfolgen. Das bindet zwar zusätzliche
technische und personelle Ressourcen, schützt aber im Fall ungeplanter
Probleme durch das Vorhandensein einer Rückfalloption vor einem
potenziellen Totalausfall.

### Transformation planungssicher

Mit Bezug auf die zuvor genannten Rahmenbedingungen sollte die
Transformation durch eine Meilensteinplanung begleitet werden. Dabei ist
anhand einer Art Reifegradmodell zyklisch der Zielerreichungsgrad zu
bewerten und etwa erkannte Defizite gegenüber dem Zielbild sind
systematisch nachzusteuern.

Zusammenführung des Transformationsbedarfs gemäß Phase B, C und D
-----------------------------------------------------------------

In den Phasen B, C und D wurden die Zielbilder aus den Perspektiven der
jeweiligen Architekturdomänen entwickelt. Soweit möglich wurden diese
außerdem mit dem Ist-Stand verglichen und es wurde der
Transformationsbedarf herausgestellt. Gelegentlich wurde eine unbekannte
oder nicht transformierbare Ausgangslage gewählt, sodass die
Transformation eher einer Erstinstallation ohne Bezug zu einer
bestehenden Implementierung entspricht.

### Transformation der Geschäftsarchitektur

Die Geschäftsarchitektur zu diesem Architekturvorhaben wurde in Phase B
als Rollenmodell mit den Rollengruppen Auftraggeber, Nutzergruppen
(Bund, Länder, Kommunen), Betreiber, Entwickler, OS-Ökosystem,
Teilprojekte (TP0-3) und Gremien aufgefächert. Die Umsetzung der
Architekturarbeit obliegt den Rollengruppen Software-Lieferant,
Software-Konfektionierer, Software-Betreiber und Plattform-Betreiber.
Für den angestrebten Wirkbetrieb ist davon auszugehen, dass die Rolle
Software-Lieferant im weitesten Sinne durch Vertreter des OS-Ökosystems
umgesetzt und die Rollen Plattform-Betreiber, Software-Konfektionierer
und Software-Betreiber durch ein oder mehrere öffentliche
IT-Dienstleister erbracht werden. In der Folge wird die zuletzt genannte
Rolle als Standort-Betreiber bezeichnet.

Jede dieser transformationsumsetzenden Rollengruppen muss ihr
traditionelles Geschäftsmodell perspektivisch mindestens teilweise
überarbeiten oder umgestalten. In Einzelfällen können sogar komplett
neue Betriebsprozesse erforderlich sein, je nachdem, ob Cloud-Services
sowie Container-Techniken bereits zum unternehmerischen Geschäftsalltag
zählen oder dies auf eine neue und somit noch zu erkundende Betriebswelt
führt.

Ergänzend sind auf der Seite der Nutzer Geschäftsprozesse anzupassen.
Z.B. ist der Wechsel auf einen Cloud-Arbeitsplatz organisatorisch
vorzubereiten, die Endgeräte sind für diese Betriebsform auszulegen und
das Personal ist auf die neue Arbeitsumgebung hin zu schulen.

#### Ausgangslage

In Phase B wurden die verschiedenen Rollen und ihre Werteströme in ein
Lieferkettenmodell eingebettet. Die Themenfelder, die von
Überarbeitungen bzw. Neugestaltungen der Geschäftsprozesse betroffenen
sind, sind aus Sicht der Architektur

-   die Strategische Entwicklung,

-   die Produktentwicklung und

-   die Bereitstellung (des SouvAP).

Der Auftraggeber (BMI/ZENDIS) verantwortet dabei die Strategische
Entwicklung. Die Produktentwicklung wird in gemeinschaftlicher
Abstimmung zwischen Auftraggeber, (öffentlich-rechtliche)
IT-Dienstleister (des Bundes, der Länder und der Kommunen) und den
Software-Lieferanten (Privatwirtschaft, Community) angestoßen. Die
Umsetzung gemäß dem diesem Konzept zugrunde liegenden Modell leisten die
Software-Lieferanten bzw. die ihnen im Zugriff stehenden Entwickler. Die
Betriebsleistung, namentlich Leistungen betreffend Infrastruktur und
Betrieb, IT-Management und zentrale Dienste, erbringen vollumfänglich
die IT-Dienstleister. Die zentralen Dienste werden vom Auftraggeber über
die Rahmenbedingung „DVS-konform“ auf logischer Ebene vordefiniert.

Zusammenfassend beschreiben die folgenden Inhalte exemplarisch, welche
Themenfelder die einzelnen Rollengruppen im Zuge der Transformation
überarbeiten bzw. anpassen müssen und sie geben Hinweise auf
Schwerpunkte, die dabei zu beachten sind.

#### Rollengruppe Nutzer

Die nachstehende Aufzählung fasst Aufgabenstellungen zusammen, die die
Transformation der Nutzer begleiten sollen. Zu diesen Aufgabenstellungen
sind passende Geschäftsprozesse zu entwickeln.

-   Die Nutzer sind entsprechen ihrer Anwendungsschwerpunkte zu
    klassifizieren

-   Es sind Schulungen zum übergreifenden neuen Arbeitsstil
    „Cloud-Arbeitsplatz“ zu entwickeln.

-   Es sind Themen-bezogene Schulungen für die identifizierten
    Nutzergruppen zu entwickeln.

-   Die Schulungsinhalte sind für ein Selbststudium aufzubereiten.

-   Ergänzend ist Trainerpersonal aufzubauen, sodass die
    Schulungsinhalte in einführenden und/oder erweiternden
    „Classroom-Trainings“ vermittelbar sind.

#### Rollengruppe OS-Ökosystem

Die nachstehende Aufzählung fasst Aufgabenstellungen zusammen, die in
die Transformation der Anwendungen einfließen sollen. Zu diesen
Aufgabenstellungen sind passende Geschäftsprozesse zu entwickeln.

-   Die Software ist „Cloud-nativ“ zu entwickeln, also insbesondere auf
    horizontale Skalierbarkeit hin zu befähigen.

-   Es ist eine Trennung der Anwendungsschichten Datenhaltung,
    Datenverarbeitung und Datenpräsentation anzustreben und diese
    Schichten sind als Mikroservices in eigenständig lauffähigen
    Containern zu paketieren.

-   Die Software ist im Einklang mit den folgenden „Best Practices“ und
    sicherheitsrelevanten Empfehlungen anzufertigen:

-   BSI Grundschutz, insbesondere die Bausteine App.x und dort die
    Maßnahmen, die innerhalb der Anwendung umzusetzen sind sowie
    Baustein SYS.1.6 Containerisierung, hier: A11 Nur ein Dienst pro
    Container.

-   Umsetzung der Empfehlungen gemäß „Docker Best Practices“:

    -   Namespace Trennung,

    -   keine Schreibrechte auf das Container-Image (read-only
        Dateisysteme verwenden),

    -   Non-root-Benutzer verwenden,

    -   Erstellung von „Self-contained Apps“,

    -   Ermöglichen eines statuslosen Betriebs,

    -   Einsatz von externem persistenten Datenspeicher.

-   Umsetzung der Empfehlungen gemäß NIST SP 800-190:

    -   Verwendung einheitlicher Minimal-/Basis-Images,

    -   keine eingebetteten Sicherheitsrisiken wie Malware, unnötige
        Angriffsflächen, Verbindungsinformationen (connection strings),
        private SSH-Keys usw.,

    -   Festlegen von Obergrenzen für die Ressourcennutzung durch
        Container,

    -   ausschließlicher Einsatz von verschlüsselter Kommunikation,

    -   verschlüsselte Dateiablage,

    -   Rollengruppierung der Entwicklungs-Teams und Anwendung
        rollenbasierter Zugriffsrechte,

    -   Umsetzung von Multi-Stage-Builds,

    -   Kennzeichnung der Versionen über Tags.

#### Rollengruppe Standort-Betreiber

Die nachstehende Aufzählung fasst Aufgabenstellungen zusammen, die in
die Transformation der Plattform einfließen sollen. Zu diesen
Aufgabenstellungen sind passende Geschäftsprozesse zu entwickeln.

-   Die Infrastruktur ist im Einklang mit den folgenden
    sicherheitsrelevanten Empfehlungen und „Best Practices“ aufzubauen
    und zu betreiben:

    -   BSI Grundschutz, insbesondere die Bausteine APP.4.4 Kubernetes
        und SYS.1.6 Containerisierung,

    -   NIST SP 800-190,

    -   C5:

        -   Richtlinie Container-Sicherheit erstellen,

        -   Einsatz von starken Authentifizierungstechniken für
            Administrative Zugriffe,

        -   Auditieren administrativer Zugriffe,

        -   Auditieren der Benutzerzugriffe ab Schutzbedarf hoch,

        -   Berücksichtigung der Container-spezifischen Patch- und
            Update-Prozesse,

        -   Trusted Computing,

        -   Bare-Metal Container,

        -   Minimales Container-Betriebssystem,

        -   Betrieb ausschließlich vertrauenswürdiger Container,

        -   Realisierung von Sicherheitszonen über Cluster-Separierung,

        -   Container-Host-Bindung bei besonderen
            Sicherheitsanforderungen,

        -   Separieren der Anwendungen (Container) über Namensräume,

        -   konsequente Festlegung der maximalen Ressourcennutzung für
            jeden Container,

        -   Einsatz dynamischer Firewall-Regeln,

        -   Einsatz von App-sensitiven Sicherheitstechniken im Netzwerk.

-   Es ist ein Betriebshandbuch zu erstellen.

-   Es ist eine Sicherheitsrichtlinie für administrative Tätigkeiten zu
    erstellen.

-   Es sind Dienstgütevereinbarungen (SLAs) zu entwickeln.

### Transformation der Informationssystemarchitektur

Die nachstehende Aufzählung fasst technische Anforderungen zusammen, die
im Zuge der Aufbereitung der Komponenten der
Informationssystemarchitektur zu beachten sind.

-   Die Anwendungen sind bezüglich der folgenden Vorgaben und
    Richtlinien anzupassen:

    -   Behindertengleichstellungsgesetz,

    -   Barrierefreie Informationstechnologie-Verordnung (BITV 2.0),

    -   Harmonisierte Europäische Norm (EN 301 549),

    -   Web Content Accessibility Guidelines (WCAG 2.1),

    -   PDF/UA (DIN ISO-14289-1:2016-12),

    -   WCAG 2.1 Level A und AA.

<!-- -->

-   Die Anwendungen sind nach Möglichkeit in die mikrosegmentierten
    Schichten Datenzugriffsschicht, Datenverarbeitungsschicht und
    Präsentationsschicht zu kapseln.

-   Die Entwicklung ist formell zu standardisieren (Erstellen eines
    Ablaufplans).

-   Es ist eine Entwicklungs-Pipeline aufzubauen.

-   Der Build-Prozess ist zu automatisieren.

-   Die Anwendungen sind in Container zu paketieren.

-   Die Container-Images sind bezüglich allgemein bekannter
    Schwachstellen (CVE-Scan) zu überprüfen.

-   Containern, die in den Produktivbetrieb gehen sollen, sind zu
    härten.

-   Die Container sind zu signieren.

### Transformation der Infrastrukturarchitektur

Die nachstehende Aufzählung fasst technische Anforderungen zusammen, die
im Zuge des Aufbaus der Infrastruktur (Plattform) zu beachten sind.

-   Definition einer DVS-kompatiblen Zielkonfiguration:

    -   Berücksichtigung der Anforderungen gemäß BSI-Grundschutz (Die
        Sicherheitskonzeption der dPhoenixSuite kann als
        Orientierungspunkt dienen),

    -   Berücksichtigung der Anforderungen gemäß C5,

    -   Berücksichtigung der Anforderungen gemäß NIST SP 800-190.

-   Bereitstellung bedarfsgerechter Infrastrukturkomponenten.

-   Entwicklung eines Scripts, das die DVS-kompatible Zielkonfiguration
    automatisiert erzeugt.

-   Ausrollen der DVS-kompatible Zielkonfiguration.

-   Entwicklung von Prüfmodulen, die die DVS-kompatible
    Zielkonfiguration überwachen (Desired Configuration Management).

-   Bereitstellung von SouvAP-Images in lokaler Registry.

Zusammenführung und Abgleich der Anforderungen an Interoperabilität
-------------------------------------------------------------------

Die Interoperabilität innerhalb des SouvAP ist aus den
Geschäftsprozessen heraus benötigt. Dazu sind Daten zwischen
Verfahrensnutzern medienbruchfrei austauschbar sein und die Verfahren
untereinander müssen nahtlos miteinander interagieren können.

Die Interoperabilität muss somit über die folgenden logischen Kontexte
hergestellt werden:

-   Interoperabilität der Geschäftsprozesse,

-   Interoperabilität der Informationssysteme und

-   Interoperabilität der technischen Basis (Infrastruktur).

### Geschäftsprozesse

Zur Aufgabenstellung Interoperabilität der Geschäftsprozesse wurden in
Phase A eine Kommunikationsmatrix aufbereitet, die den
Kommunikationsbedarf der Stakeholder untereinander ausdefiniert. In
Phase B wurde der Bedarf an Informationsfluss zwischen
Geschäftsprozessen zusammengeführt. Im Zentrum der Anforderungen stand
dabei die medienbruchfreie Interoperabilität der
geschäftsprozessunterstützenden Informationssystemarchitektur.

### Informationssysteme

Phase C hat dies aufgegriffen und die Kommunikation zwischen den
Komponenten der Informationssystemarchitektur in die Klassen

-   Nachrichtenaustausch,

-   Datenaustausch,

-   Authentikation und

-   Anwendungssteuerung

aufgeteilt. Zu jeder der genannten Klassen wurde der ausschließliche
Einsatz von offenen Protokollstandards empfohlen, die innerhalb der
Informationsarchitektur zum SouvAP eingesetzt werden sollen. Diese
Vorgabe soll die Weichen für nahtlose Interoperabilität von Anwendungen
mit Daten, Anwendungen untereinander und Anwendungen mit der zugrunde
liegenden Infrastruktur stellen.

### Infrastruktur

Die Infrastruktur ist gemäß der Zielvorgabe „DVS-konform“ als
Container-Plattform aufzubauen. Bisher vorliegende Ergebnisse von zwei
nacheinander durchgeführten DVS-Machbarkeitsstudien haben belegt, dass
ein Plattformbetreiber-unabhängiger Betrieb gelingt, wenn die
Orchestrierung mit dem Produkt Kubernetes umgesetzt wird. Kubernetes
selbst ist ein Community-getriebenes Bündel von Open Source Software,
das standardisiert die automatisierte Bereitstellung von
Container-Instanzen nebst ihrem Management und ihrer automatisierten
horizontalen Skalierung steuern kann.

In Phase D zu diesem Architekturkonzept wurden Kriterien aufgefächert,
die den Einsatz des SouvAP auch Betreiber-übergreifend in einem Verbund
der Gestalt Multicluster ermöglichen. Um in einem Standort-übergreifend
aufgebauten Multicluster eine nahtlose Interoperabilität zwischen den
Standorten zu erreichen, sind dazu die folgenden Informationen zwischen
den Standorten zu synchronisieren:

-   Informationen zu aktiven Container-Instanzen, die im jeweils lokal
    gehaltenen Service-Netzwerk verwaltet werden.

-   Datenbanken, in denen Cluster-spezifische Laufzeitinformationen
    verwahrt wird.

-   (Persistenter) Storage, den Container-Instanzen zur Laufzeit
    benötigen.

Aufdecken und Bewerten von Abhängigkeiten
-----------------------------------------

Kapitel 3 Identifikation transformationsbeeinflussender geschäftlicher
Rahmbedingungen hat die Transformation den geschäftlichen
Rahmenbedingungen unterbrechungsfrei, schrittweise, wirtschaftlich
optimiert, risikoorientiert und planungssicher untergeordnet. Zu diesem
Ziel sind für jede der drei Gruppen Nutzer, Software-Entwickler und
Service-Betreiber (Plattform, Software) Strategien zu entwickeln, die
diese Rahmenbedingungen möglichst optimal adressieren.

### Nutzer

Die Nutzer sind im Zuge der Transformation voraussichtlich mit einem
Wechsel aller bisher eingesetzten Werkzeuge konfrontiert. Die
Neuausrichtung kann der Gefahr einer substanziellen Ablehnung
unterworfen sein. Die Gefahr der Ablehnung ist mit vorbereitenden
Motivationstrainings zu minimieren. Anschließend ist sukzessive ein
Ökosystem aufzubauen, das die folgenden Komponenten enthält:

-   Aufbau von Trainingspersonal, das die Nutzer im Zuge der
    Transformation ausbildet. In der Eingewöhnungsphase soll das
    Trainingspersonal den Nutzern mit Rat und Tat zur Seite stehen.
    Optional sukzessives Heranführen an die Benutzung der neuen
    Werkzeuge in anwendungsfallorientierten Workshops.

-   Aufbau von Lerninhalten, über die mittels geführter Trainings sowie
    im Selbststudium das effiziente Arbeiten mit den neuen Werkzeugen
    trainiert werden kann.

-   Aufbau von Hilfesystemen (typische Fragen mit Antworten), das den
    Nutzern eine Art Wissensbasis liefert.

Die Auflistung soll im ersten Schritt etwa benötigtes Trainingspersonal
ertüchtigen. Idealerweise kann das Personal bereits vorhandenes
Schulungsmaterial direkt für Trainings verwenden. Falls zu einem
konkreten Werkzeug kein Schulungsmaterial vorhanden ist, ist dies
zunächst zu erstellen. Das Hilfesystem soll das Schulungsmaterial um
eine Art Nachschlagewerk mit Suchfunktion ergänzen. Die genannte
Auflistung bildet somit ein System aufeinander aufbauender Lernhilfen.

### Software-Lieferanten

Die Software-Lieferanten müssen die SouvAP-Komponenten als sichere
Software ausliefern. Jede Komponente muss in ein oder (als Bündel von
Mikroservices) mehreren Container-Images paketierbar und oberhalb einer
DVS-konformen Infrastruktur lauffähig sein. Nach aktueller Einschätzung
wird der Modulbedarf zum SouvAP durch mehrere, voneinander unabhängige
Entwickler auf diese Anforderungen hin transformiert.

An Stelle einer schrittweisen ist daher eine parallele Transformation
der SouvAP-Module wahrscheinlich. Dabei ist jedoch zu berücksichtigen,
dass alle datenverarbeitenden Module den Zugriff auf zentrale
Datenträger benötigen. In der Informationssystemarchitektur zum SouvAP
sind dies die Cloud-Dateiablage sowie das Modul E-Mail und Kalender.
Darauf Bezug nehmende Module des SouvAP benötigen daher bereits im
Vorfeld der Transformation Schnittstellenspezifikationen, die während
der Projektlaufzeit allenfalls minimalen Veränderungen unterliegen
sollten. Zudem sind diese Komponenten spätestens im Teststadium als
ausführbare Objekte benötigt. Das Zielformat „containerisiert“ ist in
diesem Stadium jedoch voraussichtlich nicht benötigt.

Eine DVS-Konformität der Module zum SouvAP bedeutet für die
Software-Entwickler, sie müssen die Vorgaben an sichere Software
zumindest in Anlehnung an die BSI-Bausteine App.4.4 und Sys.1.6
umsetzen. Die Anforderung „Schutzbedarf HOCH“ an das Lieferergebnis
SouvAP 2023 impliziert dabei, dass die Basis-Anforderungen, die
Standard-Anforderungen und die Anforderungen bei erhöhtem Schutzbedarf
zu berücksichtigen sind.

Diese Anforderungen einleitend als „exemplarische Vorschläge“
gekennzeichnet und durchweg mit Verbindlichkeit „sollte“ geprägt. Auch
die Standard-Anforderungen haben zu diesen Bausteinen haben die
BSI-Autoren mit Verbindlichkeit "sollte" versehen mit dem Zusatz, die
Standard-Anforderungen sollten grundsätzlich erfüllt werden.

Zu einigen Anforderungen aus diesen Bausteinen muss die Umsetzung in der
Infrastruktur erfolgen, z.B. zu App.4.4.A7 Separierung von Netzwerken.
In anderen Fällen muss die Umsetzung durch die Software Entwicklung
erbracht werden, z.B. bei App.4.4.A18 Verwendung von Mikro-Segmentierung
und bei Sys.1.6.A11 nur ein Dienst pro Container. In weiteren Fällen
kann die Umsetzung von "Best Practices" die Grundlage für sichere
Software liefern. Darauf konzentrierte Beispiele enthalten u.a. die
bereits mehrfach genannten „Docker Best Practices“ und NIST SP 800-190.

Die nachfolgende Aufzählung liefert eine Orientierung, welche Maßnahmen
den sicheren Betrieb von Containern begünstigen:

-   Einsatz vertrauenswürdiger Basis-Images (Slim-Versionen für Pre-Prod
    und Prod).

-   Abstimmen und festlegen, welche Techniken für Monitoring und
    zentrale Protokollierung zur Anwendung kommen sollen.

-   Automatisiertes Build, Test, Paketieren, Überprüfen, Härten und
    Signieren der Images in einer CI/CD-Pipeline.

-   Minimierung von Angriffsflächen.

-   Begrenzen der Privilegien von Containern.

-   Einsatz von Zugriffsrechteschemen.

-   Sensibilisierung der Entwickler für sichere Container.

-   Regelmäßiges Scannen der Images auf bekannte Verwundbarkeiten.

-   Regelmäßige Aktualisierung der Image-Bestandteile.

### Plattform-/Software-Betreiber

Die ideale Welt für den SouvAP wäre, alle öffentlichen IT-Dienstleister
bilden ein Konsortium von Plattform-/Software-Betreibern mit
hochaktueller Infrastruktur und langjähriger Erfahrung mit Cloud- und
Container-Betrieb. Rechenzentren haben aber typischerweise Lebenszyklen
analog der Automobile im täglichen Straßenverkehr.
Infrastruktur-Konzepte leben häufig länger als ein Jahrzehnt. Darin
verbaute Hardware wird gegebenenfalls alle 3-5 Jahre gegen neuere
Technik mit höheren Leistungsmerkmalen getauscht. Diese müssen aber
rückwärtskompatibel sein, da anderenfalls gleichzeitig die Software
ersetzt werden muss. Den Bedarf an neuen Techniken und Konzepten wird
dabei eher durch Aufbau spezieller Silos Rechnung getragen. Entsprechend
sind eher parallel betriebene heterogene Technik-Bündel anzutreffen, die
ein breites Spektrum traditioneller, überalterter und moderner
IT-Landschaften bedienen.

Die Spezifikationen zu „DVS-konform“ sind aufgrund ihrer sehr jungen
Präsenz im Markt vermutlich noch eher als ein Konzept für
zukunftsgerichtete Verwaltungs-IT einzustufen. Mittlerweile liegen aber
erste Ergebnisse von DVS-Machbarkeitsstudien vor, aus deren
Dokumentation Hinweise auf erforderlich Umsetzungsschritte entnommen
werden können. Die nahstehende Auflistung ist der Versuch, die
Abhängigkeiten aufzufächern, die beim Aufbau einer DVS-konformen
Infrastruktur zu beachten sind.

-   Bestimmung der maximal benötigten Ressourcenbedarfs.

-   Bestimmung der Betreiber-spezifischen Verbindlichkeiten von
    relevanten BSI-Bausteinen (Ersatz von SOLLTE- durch
    MUSS-Anforderungen, wo unternehmerisch gewünscht).

-   Festlegung der Zielkonfiguration.

-   Entwicklung einer Automatisierungstechnik für den Aufbau der
    Zielkonfiguration.

-   Beschaffung der benötigten technischen Hardware.

-   Aufbau von Kubernetes-Clustern entsprechend dem Zonenbedarf (Stage,
    DMZ, Intranet, SB hoch).

-   Härtung der Kubernetes Cluster entsprechend der unternehmerischen
    Vorgaben.

-   Ausbau der Cluster mit Funktionen für Monitoring und zentraler
    Protokollierung.

-   Aufbau einer CI/CD-Pipeline.

-   Nachbereiten von Open-CoDE-Artefakten entsprechend unternehmerischer
    Sicherheitsanforderungen.

-   Ablage neu erstellter Images in lokaler Registry des Stage-Clusters.

-   Durchführen automatisierter Funktions- und Performance-Tests im
    Stage-Cluster.

-   Überführen getesteter Images in die Produktions-Cluster DMZ,
    Intranet und SB hoch.

-   Registrieren neuer Images im Portal

### Betriebliche Aspekte

In diesem Kapitel wurden bisher Abhängigkeiten aufgefächert, die
ausschließlich im Zusammenhang mit der Transformation selbst eine Rolle
spielen. Im anschließenden Wirkbetrieb sind ebenfalls Abhängigkeiten zu
erwarten. Z.B. dürfte eine effiziente Anwendungsnutzung von einem
regelmäßig gepflegten und weiterentwickelten Hilfesystem profitieren,
Wartungsarbeiten an den Anwendungen (etwa zur Beseitigung bekannt
gewordener Angriffsflächen) sind für den gesicherten Betrieb
unerlässlich und an der Infrastruktur sind ebenfalls regelmäßige
Wartungsaktivitäten unerlässlich, z.B. tägliches Scannen der
Container-Images gegen aktuelle CVE-Listen.

Entsprechend erforderliche Prozesse und daraus resultierende
Kostenstrukturen sind durch die jeweils betroffenen Rollengruppen
Nutzer, Software-Lieferanten und Plattform-/Software-Betreiber zu
erstellen. Das Architekturkonzept empfiehlt, dies transparent
aufzubereiten und in Form von Handreichungen, Wartungsverträgen und
Betriebsvereinbarungen zu veröffentlichen.

Prüfung auf Transformationsreife, Bewertung von Transformations-Risiken
-----------------------------------------------------------------------

Gemäß den bisherigen Ausarbeitungen zu Phase E berührt die
Transformation organisatorische und technische Aspekte. Damit verbunden
sind Risiken derart, dass die Zielarchitektur nicht oder nur teilweise
erreicht wird. Die Schlussfolgerungen zu den transformationsbedingten
Faktoren gemäß Kapitel 2.1 liefern Anhaltspunkte, wie diese Risiken
abgefedert werden können.

**Nutzer:** Die organisatorischen Aspekte können auf der Seite der
Nutzer bis hin zu einem Kulturwandel reichen: Bisherige Prozesse sind
zumindest in Teilen und die dabei eingesetzten Werkzeuge sind praktisch
vollständig zu überarbeiten bzw. zu ersetzen. Damit verbundene Risiken
wurden bereits in Phase A dahingehend bewertet, dass in einer
Übergangsphase Abstriche in der Wirtschaftlichkeit und in der
Kundenzufriedenheit zu erwarten sind.

**Software-Lieferanten:** Auf der Seite der Software-Lieferanten sind
einerseits Risiken zu bewerten, die durch unzureichend umgesetzten
Funktionsumfang in Anlehnung an die Funktionsmodellierung gemäß Phase C
Kapitel 2.3 begründet sind. Das Risiko fällt allerdings gering aus, wenn
OSS-Produkte mit zu geringem Abdeckungsgrad der Funktionsmodellierung
von einer Kandidatur für den SouvAP ausgeschlossen werden. Deutlich
höher dürfte andererseits das Risiko ausfallen, dass die Transformation
vorhandener und nach traditionellen Anwendungsmodellen entwickelter
Software die Anforderung nach Containerisierung, Gliederung in
Datenzugriffs-, Datenverarbeitungs- und Präsentationsschicht und
Umsetzung von Anforderungen an sichere Software für Container nicht
vollumfänglich gelingt. Entsprechend ist abzuschätzen wie eine nicht
oder nur teilweise erreichte Transformation die Nutzbarkeit des SouvAP
beeinträchtigt.

**Plattform-/Software-Betreiber:** Den Pilotbetrieb des SouvAP soll der
öffentliche IT-Dienstleister Dataport umsetzen. Dataport war an den
bisher durchgeführten zwei DVS-Machbarkeitsstudien aktiv beteiligt. Es
wird erwartet, dass die darin gewonnenen Erkenntnisse eine solide Basis
für die Planungen zum Erstaufbau der benötigten Infrastruktur gelegt
haben. Die weitaus größere Herausforderung dürfte jedoch in der
anschließend gefragten Rolle des Plattform-/Software-Betreibers
bestehen. Es ist noch nicht festgelegt, ob Dataport die benötigte
Infrastrukturtechnik selbst aufbaut oder ob diese von einem
Cloud-Serviceanbieter angemietet werden soll. Diese Entscheidung kann
wirtschaftliche Vorteile implizieren, jedoch kann sie umgekehrt bisher
nicht abschätzbare Diskussionen um Zuständigkeiten und Fristen für
Lieferleistungen auslösen. Es ist damit zu rechnen, dass der Wirkbetrieb
den Plattform-Betreiber und den Software-Betreiber in jeder dieser
Rollen mit neuen Herausforderungen konfrontiert. Um diese professionell
meistern zu können, dürfte außerdem der enge Dialog zwischen
Software-Betreiber und Software-Lieferant unerlässlich sein. Diese
Sachzwänge bergen Risiken derart, dass Lieferanten und Betreiber in
ihren Rollen zumindest in der Anfangsphase mit neuen organisatorischen
Herausforderungen konfrontiert werden.

**Zusammenfassung:** Die Ursachen für die aufgeführten Risiken sind aus
Sicht der Architektur für jede der beteiligten Rollengruppen absehbar.
Jede Rollengruppe sollte daher in der Pflicht stehen, zeitnah geeignete
Maßnahmen zu ergreifen, die den Ursachen entgegenwirken und damit die
Risiken auf ein tragbares Ausmaß begrenzen.

Festlegung der Strategie für Implementierung/Migration
------------------------------------------------------

Der bisher aufgezeigte Transformationsbedarf (Kapitel 4 Zusammenführung
des Transformationsbedarfs gemäß Phase B, C und D) hat eine scharfe
Aufgabentrennung der an der SouvAP-Transition beteiligten Rollengruppen
Nutzer, Software-Lieferanten und Plattform-/Software-Betreiber
herausgestellt. Die in Kapitel 6 Aufdecken und Bewerten von
Abhängigkeiten zusammengestellten Rangfolgen der Transitions-Ziele geben
eine Reihenfolge vor, in der die einzelnen Rollengruppen ihre dazu
erforderlichen Arbeitsschritte festlegen und umsetzen sollen.

Aufgrund der Vorgaben aus Kapitel 3 Identifikation
transformationsbeeinflussender geschäftlicher Rahmbedingungen ist eine
iterative Migration der Informationssystemarchitektur anzustreben. Die
betriebliche Nutzung jedes Moduls setzt voraus, dass ihr technisches
Fundament bereitsteht und, falls erforderlich, die Nutzer an die jeweils
iterativ erstellten Module sukzessiv angelernt werden können.

Die Migration der Informationssystemarchitektur ist in Teilen parallel
umsetzbar, da die Transition dieser Architekturdomäne von mehreren
Gewerken geleistet werden soll. Dabei sollten Synergien maximiert werden
derart, dass wiederkehrende Aufgaben innerhalb der Programmierung durch
gemeinsam genutzte Frameworks und Bibliotheken auf maximale
Standardisierung hin fokussieren.

Die parallele Ausgestaltung von Komponenten der Informationsarchitektur
muss sorgfältig geplant werden. Zunächst sind die übergreifenden
Komponenten des Gesamtsystems in einer mindestens rudimentären, aber für
den Probebetrieb erster Werkzeuge tauglichen Vollständigkeit zu
entwickeln. Anschließend sind die in Phase C Kapitel 3 Funktionsmodell
genannten Module in eine Rangordnung zu sortieren. Dazu sind zur
geschätzten Herstellungszeit jedes Moduls und zu ihrer Bedeutung für die
Geschäftsprozesse der Nutzer Wertziffern zu ermitteln. Die Rangbildung
ist über eine Gewichtung der so erzeugten Wertziffern festzulegen.

Während der Modulentwicklung sind regelmäßige Abstimmungstermine mit den
Arbeitsgruppen abzuhalten, die die Dokumentation bzw. den Aufbau von
Ausbildungsinhalten vorantreiben. Damit soll das Ziel erfüllt werden,
Module als Komplettpaket einschließlich Dokumentation und
Ausbildungsmaterial in den Verkehr bringen zu können

Auf Basis dieser Abhängigkeiten zwischen den Architekturdomänen der
Zielarchitektur, den darin jeweils enthaltenen und übergreifend
sichtbaren Abhängigkeiten und dem Wunsch nach maximaler Hebung möglicher
Synergien legt das Architekturkonzept zum SouvAP die Strategie für die
Implementierung/Migration stichwortartig wie folgt fest:

**Informationssystemarchitektur:**

-   Entwicklung standardisierter Container-Images für Entwicklungs- und
    > Produktiv-Umgebung.

-   Entwicklung der Kopplungsinfrastruktur (das sind die Komponenten des
    Gesamtsystems außerhalb der SouvAP-Module).

-   Rangbildung der SouvAP-Module nach betrieblicher Relevanz und
    frühestens möglichem Liefertermin.

-   Identifikation modulübergreifend benötigter Routinen, Ausbau zu
    Frameworks/Standard-Bibliotheken.

-   Realisierung der SouvAP-Module entsprechend den funktionellen
    Anforderungen.

-   Optimierung der SouvAP-Module entsprechend sicherheitsrelevanter
    Vorgaben und Empfehlungen (BSI-Grundschutz, C5, Docker Best
    Practices, NIST SP800-190).

-   Containerisierung der SouvAP-Module.

-   Partitionierung der Container in Anlehnung an das
    Mehrschichtenmodell mit Schichten Datenzugriffsschicht,
    Datenverarbeitungsschicht und Präsentationsschicht.

**Infrastrukturarchitektur:**

-   Aufbau einer Kubernetes-Plattform.

-   Implementierung der Kopplungsinfrastruktur.

-   Schrittweise Implementierung der sicherheitsrelevanten Maßnahmen
    gemäß BSI-IT-Grundschutz und C5.

-   Entwicklung von Betriebsmodellen in Anlehnung an ITIL, Dokumentation
    erforderlicher Arbeitsprozesse in einem Betriebshandbuch.

-   Festlegung buchbarer Betriebsleistungen, Bündelung dieser
    Betriebsleistungen in Dienstgütevereinbarungen.

**Geschäftsarchitektur:**

-   Zusammenstellung übergreifender motivierender und fachlich klärender
    > Inhalte zum SouvAP.

-   Abstimmung der zeitlichen Abfolge des modulspezifischen
    Dokumentations- und Schulungsbedarfs mit den Gewerken, die die
    Transition der Informationssystemarchitektur umsetzen.

-   Schrittweise Bewertung vorhandener Dokumentations- und
    Schulungsinhalte zu den SouvAP-Modulen in Anlehnung an die geplante
    Reihenfolge der Realisierung der Module, Ausbau der Dokumentations-
    und Schulungsinhalte entsprechend dem erwarteten Bedarf.

-   Integration der Dokumentations- und Schulungsinhalte in das geplante
    Wissens- und Lernmanagementsystem zum SouvAP.

Festlegung der benötigten Arbeitspakete
---------------------------------------

Die angedachten Arbeitspakete zur Transition des SouvAP wurden im Zuge
der Auftragserteilung (Leistungsschein zum SouvAP für das Jahr 2023) in
der Gestalt von mehr als 200 Steckbriefen zusammengeführt. Jeder
Steckbrief beschreibt Teilaufgaben zu einem übergeordneten Paket.
Entsprechend fällt die Zahl der Arbeitspakete deutlich geringer aus als
die Zahl der Steckbriefe.

Der Leistungsschein hat eine Projektplan vordefiniert, die im
Wesentlichen auch die benötigten Arbeitspakete zur Realisierung des
SouvAP beschreibt. Das nachfolgende Diagramm erklärt den in 4
Teilprojekten gegliederte Struktur.

![](media/file37.png)

Abbildung : Organisationsstruktur zur Realisierung des SouvAP

Die Realisierung des SouvAP sollen die Teilprojekte 1 – Technische
Entwicklung, 2 – Einbettung in Strukturen der ÖV und 3 – Erprobung
leisten. Dabei soll das Teilprojekt 1 die Transition von bereits
vorhandenen Open-Source-Produkten vorantreiben derart, dass
übergreifende Anforderungen wie z.B. medienbruchfreie Interoperabilität,
einheitliche und barrierefreie Bedienung, Compliance mindestens zu den
Sicherheitsanforderungen von BSI-IT-Grundschutz sowie C5 und
Containerisierung entsprechend der Zielsetzung der DVS integriert sind.
Diese Eigenschaften sollen harmonisch die Module des SouvAP und
übergreifend das Gesamtsystem liefern. Im Wesentlichen übernimmt das
Teilprojekt 1 somit Aufgaben der Transition der
Informationssystemarchitektur.

Das Teilprojekt 2 erzeugt dazu passend das technische Fundament
entsprechend den jeweils zeitgleich bekannten Attributen einer
DVS-konformen Betriebsplattform. Diese Lieferleistung ist im
Leistungsschein nicht direkt beauftragt, jedoch ist die Konzeption, die
Realisierung und der Test einer derartigen Infrastruktur verpflichtende
Voraussetzung einerseits für die im Teilprojekt 1 zu erzeugenden
SouvAP-Module bis zur Produktionsreife, für die anschließend im
Teilprojekt 3 durchzuführende Erprobung und für die Erstellung der
Betriebsdokumentation, die das Teilprojekt 3 erstellen soll.

Aufgrund dieser Erkenntnisse erscheint eine übergreifende Struktur der
Arbeitspakete mit den Teilaufgaben

-   Konzeption,

-   Realisierung,

-   Test und

-   Dokumentation

obligatorisch. Diese Struktur ist anzuwenden auf

-   die Identifikation von SouvAP-Modulkandidaten passend zum
    Funktionsmodell (Phase C),

-   die Erstellung des Gesamtsystems gemäß Systemmodell (Phase C),

-   die Anpassung der SouvAP-Modulkandidaten an die Vorgaben (z.B.
    medienbruchfreie Interoperabilität, einheitliches und barrierefreies
    Bedienkonzept und Containerisierung) uir Integration in das
    Gesamtsystem.

-   die Erstellung der Infrastruktur gemäß Phase D.

Zusätzliche Arbeitspakete sind aufgrund der Anforderung Bereitstellung
über / Pflege von Open CoDE (Teilprojekt 2) und Community Management
(Teilprojekt 3) aufzubereiten. Des Weiteren sind Arbeitspakete zu
schnüren, die die Nutzer bei der Transformation hin zum SouvAP begleiten
sollen, also

-   die Aufbereitung von Schulungsmaterialien,

-   den Aufbau eines Trainingsangebots und

-   das Zusammenführen von Betriebs- und Anwendungswissen in einer
    Wissensdatenbank.

Der Bedarf für die zuletzt genannten Arbeitspakete ergibt sich aus den
in Phase A herausgestellten Fähigkeiten, mit denen SouvAP die
Geschäftsziele des IT-gestützten Verwaltungshandelns auf einen insgesamt
höheren Reifegrad transformieren will.

Benötigte Transitions-Architekturen
-----------------------------------

Die Transition soll als Erstinstallation außerhalb der aktuell
betriebenen Rechenzentrumstechnik erfolgen. Ergänzend zur
Zielarchitektur sind keine weiteren Transitions-Architekturen benötigt.

Architektur-Roadmap: Pläne für Implementierung und Migration
------------------------------------------------------------

### Rahmenbedingungen und Zielsetzung

Das Architekturkonzept entsteht als Teilauftrag eines Leistungsscheins.
Letzterer hat ausgehend von Annahmen die Umsetzung des Zielbilds SouvAP
als Liste von Lieferleistungen vordefiniert und deren zeitliche
Abarbeitung über Meilensteine mit festen Lieferterminen und zugeordneten
Abnahmekriterien manifestiert. Diese Vorgehensweise sieht eine iterative
Umsetzung vor, mit der der SouvAP zeitnah produktionsreif aufbereitet
werden soll.

Im Zuge der Architekturarbeit wurden wünschenswerte sowie zwingend
erforderliche Arbeitspakete identifiziert, die erforderlich sind, um die
Fähigkeiten des Verwaltungshandelns auf den Reifegrad gemäß Phase A
Kapitel 4.5 zu transformieren. Diese betreffen vorrangig die
Rollengruppe der Nutzer. Zusätzlich hat Kapitel 4.1.4 den Bedarf nach
Abgrenzung der Betriebsleistungen auf der Seite der
Plattform-/Software-Betreiber betont. Somit ist der beauftragte
Leistungsschnitt um die folgenden Arbeitspakete zu ergänzen:

-   Die Nutzer benötigen transformationsbegleitende Schulungsmaterialien
    und eine Art digitales Nachschlagewerk für häufig gestellte Fragen.

-   Die Betreiber benötigen Abgrenzungen zu Anforderungen an den
    Wirkbetrieb, der in einem Betriebshandbuch festzuhalten und in
    Dienstgütevereinbarungen festzulegen ist.

Das Architekturkonzept empfiehlt, die Arbeitspakete in die thematischen
Gruppen

-   Technik,

-   Security (Maßnahmen zur Durchsetzung von IT-Sicherheit) und

-   Personal

zu clustern. Auf der Basis dieser Struktur sollen Zielerreichungsgrade
Metriken liefert, die eine Bewertung

-   der Nutzbarkeit,

-   der technischen Vollständigkeit/Nutzbarkeit und

-   des erreichten Sicherheitsniveaus

dokumentieren können.

### Architektur-Roadmap

Die folgende Aufzählung clustert Aufgabenstellungen zu den zuvor
genannten thematischen Gruppen und gibt damit eine Reihenfolge vor, in
der Arbeitspakete einen Fertigstellungsgrad erreichen sollen, der den
Start des Folgeschritts ermöglicht. Eine quantitative Zuordnung zu
Meilensteinterminen gemäß Leistungsschein zum SouvAP erfolgt nicht, da
die Aufgabenstellungen nicht eineindeutig auf die Steckbriefe gemäß
Leistungsschein abbildbar sind.

**Technik:**

-   Basis-Image fertigen (Dev, Prod);

-   Gesamtsystem realisieren (Backend, Gateways);

-   Modulkandidaten identifizieren;

    -   Abprüfen des Leistungsschnitts gegenüber dem Funktionsmodell;

    -   Kompatibilität mit aufgeschlüsselten Kommunikationsstandards
        verifizieren;

-   Funktionsumfang pro Modul vervollständigen;

-   eventuell Adapter erstellen/anpassen;

-   Anforderungen an Barrierefreiheit ergänzen;

-   technischer Aufbau Kubernetes-Cluster;

-   Bereitstellung einer CI/CD-Pipeline;

-   Härtung des Codes in Anlehnung an Docker Best Practices;

-   Containerisierung des Codes;

-   Härtung der Container-Images nach Vorgaben aus BSI-IT-Grundschutz,
    C5, NIST SP 800-190;

-   Überführung der Anwendungsschichten in Microservices.

**Security:**

-   Sicherheitskonzept entwickeln;

-   Sicherheitsarchitektur entwickeln;

-   Maßnahmenkatalog festlegen;

-   Security-Maßnahmen umsetzen an Modulkandidaten;

-   Security-Maßnahmen umsetzen am Kubernetes Cluster;

-   Dokumentation aller umgesetzten Security-Maßnahmen;

-   Installation von Infrastruktur und Komponenten des
    Informationssystems auditieren.

**Personal:**

-   übergreifende Akzeptanzschulungen entwickeln;

-   Vorlagen für Dienstgütevereinbarungen (SLAs) festlegen;

-   Bedarf an zusätzlichem modulspezifischen Ausbildungsmaterial
    ermitteln;

-   modulspezifisches Schulungsmaterial vervollständigen;

-   Wissensbasis/Wiki aufbauen;

-   bei Bedarf Trainer rekrutieren.

### Stolpersteine

Dieser Abschnitt betrachtet und bewertet Auswirkungen, die aufgrund von
teilweiser bzw. unvollständiger Umsetzung von Arbeitspaketen entstehen
können.

**Technik:**

<table>
<thead>
<tr class="header">
<th>Arbeitspaketziel nicht erreicht</th>
<th>Auswirkungen</th>
<th>Risiko</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Die benötigten Kubernetes-Cluster sind nicht betriebsbereit.</td>
<td>Der SouvAP ist weder in Teilen noch in Gänze einsetzbar.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Das Basis-Image ist nicht „Slim“ / nicht distroless.</td>
<td>Die containerisierte Software ist ausführbar, aber der Container ist nicht vollständig gehärtet. Der Container ist eventuell nicht vertrauenswürdig.</td>
<td>mittel</td>
</tr>
<tr class="odd">
<td>Das Gesamtsystem ist nicht vollständig.</td>
<td>Es sind vermutlich nur einige, aber nicht alle Module des SouvAP vollständig in das Gesamtsystem integrierbar. Die medienbruchfreie Interoperabilität ist eventuell nicht durchgängig erreicht.</td>
<td>mittel</td>
</tr>
<tr class="even">
<td>Der modulspezifische Funktionsumfang ist nicht passend zu den Anforderungen</td>
<td>Das Leistungsspektrum des SouvAP-Moduls ist nicht ausreichend für die Erfordernisse der Geschäftsprozesse der öffentlichen Verwaltung. Einige Aufgaben können nur mit Zugeständnissen bewältigt werden.</td>
<td>hoch</td>
</tr>
<tr class="odd">
<td>SouvAP-Modul ist nicht containerisiert.</td>
<td>Der zugehörige Modulkandidat ist nicht mit den Rahmenbedingungen zum SouvAP kompatibel und daher auch nicht einsetzbar. Ein Hybrid-Betrieb (teilweise virtuelle Maschinen) ist nicht vorgesehen.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Anforderungen an Barrierefreiheit sind nicht umgesetzt.</td>
<td>Die betroffenen Module sind durch den Anwenderkreis nicht nutzbar, die eine barrierefreie Bedienung benötigen.</td>
<td>hoch</td>
</tr>
<tr class="odd">
<td>CI/CD-Pipeline ist nicht vorhanden bzw. nur teilweise nutzbar.</td>
<td>Die (vollständig) automatisierte Erstellung von Container-Images ist nicht möglich. Die resultierenden Images sind eventuell nicht reproduzierbar. Die Images sind nicht automatisiert ausrollbar.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Ein Container-Image ist nicht gehärtet.</td>
<td>Das Container-Image enthält eine unbekannte Anzahl an Verwundbarkeiten. Der produktive Einsatz ist nicht zulässig.</td>
<td>sehr hoch</td>
</tr>
<tr class="odd">
<td>Ein SouvAP-Modul ist nur monolithisch, aber nicht als Bündel von Mikroservices verfügbar.</td>
<td>Der DVS-konforme Betrieb erfordert mindestens den Einsatz eines Web-Proxy, ist aber generell möglich.</td>
<td>gering</td>
</tr>
</tbody>
</table>

**Security:**

<table>
<thead>
<tr class="header">
<th>Arbeitspaketziel nicht erreicht</th>
<th>Auswirkungen</th>
<th>Risiko</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Das Sicherheitskonzept fehlt oder ist unvollständig.</td>
<td>Der SouvAP kann nicht entsprechend der Vorgabe BSI-zertifiziert betrieben werden.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Die Sicherheitsarchitektur fehlt oder ist unvollständig.</td>
<td>Ein passgenauer Zuschnitt von Security-Maßnahmen zum vorgesehen Schutzniveau des SouvAP ist nicht möglich. Eventuell wird ein hoher Aufwand für Security investiert, der wenig Sicherheitsgewinn bewirkt.</td>
<td>gering</td>
</tr>
<tr class="odd">
<td>Der Security-Maßnahmenkatalog fehlt oder ist unvollständig.</td>
<td>Die Audit-relevanten Security-Maßnahmen sind nicht vollständig bekannt und stehen daher nicht zur Umsetzung an. Die BSI-Zertifizierung des Informationsverbunds ist gefährdet. Der DVS-konforme-Betrieb ist nicht möglich.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Die Security-Maßnahmen wurden an einem oder mehreren SouvAP-Modulen nicht oder nur teilweise umgesetzt.</td>
<td>Die betroffenen SouvAP-Module enthalten nicht bekannte Angriffsflächen. Ein anforderungskonformer Betrieb der betroffenen SouvAP-Module ist nicht möglich. Es besteht Gefahr für die Integrität von Daten, Verfahren und Infrastruktur.</td>
<td>sehr hoch</td>
</tr>
<tr class="odd">
<td>Die Security-Maßnahmen wurden am Kubernetes-Cluster nicht oder nur teilweise umgesetzt.</td>
<td>Die BSI-Zertifizierung des Informationsverbunds ist gefährdet. Der SouvAP kann nicht entsprechend der Vorgabe BSI-zertifiziert betrieben werden.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Die Security-Dokumentation fehlt oder ist unvollständig.</td>
<td>Die BSI-Zertifizierung des Informationsverbunds ist ausgeschlossen. Der SouvAP kann nicht entsprechend der Vorgabe BSI-zertifiziert betrieben werden.</td>
<td>sehr hoch</td>
</tr>
<tr class="odd">
<td>Die Installation ist nicht auditiert.</td>
<td>Die Umsetzung der Security-Maßnahmen ist nicht protokolliert. Die BSI-Zertifizierung des Informationsverbunds ist gefährdet.</td>
<td>sehr hoch</td>
</tr>
</tbody>
</table>

**Personal:**

<table>
<thead>
<tr class="header">
<th>Arbeitspaketziel nicht erreicht</th>
<th>Auswirkungen</th>
<th>Risiko</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Übergreifende Akzeptanzschulungen sind nicht verfügbar.</td>
<td>Die Anwender sind auf die Transformation des traditionellen Werkzeugkoffers hin zu einem Cloud-Arbeitsplatz nicht vorbereitet. Es besteht Gefahr der grundsätzlichen Ablehnung des SouvAP.</td>
<td>sehr hoch</td>
</tr>
<tr class="even">
<td>Vorlagen für Dienstgütevereinbarungen sind nicht vorhanden.</td>
<td>Eine Zusicherung des Leistungsschnitts zum SouvAP ist gegenüber Kunden des SouvAP nicht vertraglich regelbar. Es besteht Gefahr, dass Kunden den Nutzen des SouvAP als wirtschaftlich unzureichend einstufen.</td>
<td>hoch</td>
</tr>
<tr class="odd">
<td>Modulspezifisches Schulungsmaterial fehlt oder es unvollständig.</td>
<td>Nutzer des SouvAP müssen den Umgang mit den betroffenen SouvAP-Modulen experimentell erlernen. Eine wirtschaftlich sinnvolle Produktivität ist erst nach einer Eingewöhnungszeit zu erwarten. Es besteht die Gefahr, dass der Einsatz des betroffenen Werkzeugs abgelehnt und auf alternative Werkzeuge zurückgegriffen wird (Schatten-IT).</td>
<td>hoch</td>
</tr>
<tr class="even">
<td>Wissensbasis/Wiki nicht vorhanden.</td>
<td>Die Nutzer des SouvAP haben kein Nachschlagewerk im Zugriff, in dem sie Antworten auf häufig gestellte Fragen zur Werkzeugnutzung nachschlagen können. Es besteht die Gefahr, dass Kollegen Beratungsleistung zu Lasten ihrer eigenen Produktivität erbringen müssen.</td>
<td>mittel</td>
</tr>
<tr class="odd">
<td>Moderierte Trainings nicht verfügbar.</td>
<td>Die Nutzer sind im Umgang mit Werkzeugen zur Geschäftsprozessunterstützung vertraut. Ein Werkzeugwechsel wird mit vertretbarem Aufwand gelingen, wenn den Nutzern umfassend Schulungsmaterial zum Selbststudium zur Verfügung steht.</td>
<td>gering</td>
</tr>
</tbody>
</table>
