Einleitung
==========

Eine im Jahr 2019 im Auftrag des Bundesministeriums des Innern, für Bau
und Heimat (BMI) erstellte [<span class="underline">Strategische
Marktanalyse zur Reduzierung von Abhängigkeiten von einzelnen
Software-Anbietern</span>](https://www.cio.bund.de/SharedDocs/downloads/Webs/CIO/DE/digitale-loesungen/marktanalyse-reduzierung-abhaengigkeit-software-anbieter.pdf?__blob=publicationFile&v=1)
hat unmissverständlich nachgewiesen, dass die Bundesverwaltung in allen
Schichten der eingesetzten geschäftsprozessunterstützenden Software von
nur wenigen Software-Anbietern abhängig ist. Das Dokument zeigt in
diesem Zusammenhang erhebliche Risiken betreffend der
Informationssicherheit auf und dies wird als Widerspruch zu den
strategischen Zielen der IT des Bundes bewertet.

Die Arbeitsgruppe „Informationssicherheit“ des IT-Planungsrats hat
diesen Sachverhalt umgehend aufgegriffen und in dem [<span
class="underline">Beschluss
2020/19</span>](https://www.it-planungsrat.de/fileadmin/beschluesse/2020/Beschluss2020-19_Entscheidungsniederschrift_Umlaufverfahren_Eckpunktepapier.pdf)
das Eckpunktepapier Stärkung der Digitalen Souveränität der Öffentlichen
Verwaltung veröffentlicht. Darin verkünden Bund, Länder und Kommunen das
gemeinsame Ziel, die Digitale Souveränität der Öffentlichen Verwaltung
in ihren Rollen als Nutzer, Bereitsteller und Auftraggeber von Digitalen
Technologien gemeinsam und kontinuierlich zu stärken.

Kurz darauf veröffentlichte der IT-Planungsrat die [<span
class="underline">Strategie zur Stärkung der Digitalen Souveränität für
die IT der Öffentlichen
Verwaltung</span>](https://www.it-planungsrat.de/fileadmin/beschluesse/2021/Beschluss2021-09_Strategie_zur_Staerkung_der_digitalen_Souveraenitaet.pdf).
Darin sind die strategischen Ziele Wechselmöglichkeit,
Gestaltungsfähigkeit und Einfluss auf Anbieter herausgestellt und es
sind diesbezügliche Umsetzungspläne konkretisiert. Die Steuerung von
Abhängigkeiten soll also über den Wechsel von Anbieter bzw. Produkt, die
(Mit-)Gestaltung alternativer Produkte, die Beauftragung eines Anbieters
oder eine Kombination aus diesen Aktionen geprägt sein.

Das Projekt Souveräner Arbeitsplatz (SouvAP) fokussiert auf den
Lösungsansatz 2 „Beschaffung bzw. Entwicklung alternativer IT-Lösungen“
der zuvor zitierten Strategie. Die Steuerung und die Koordination von
SouvAP obliegt der neu gegründeten Organisation Zentrum für Digitale
Souveränität ZenDiS. Gemäß dem [<span class="underline">Zielbild von
ZenDiS</span>](https://www.cio.bund.de/SharedDocs/downloads/Webs/CIO/DE/digitale-loesungen/organisationskonzept-zentrum-digitale-souveraenitaet.pdf?__blob=publicationFile&v=1)
soll der SouvAP in einer ersten Ausbaustufe das Thema
Standard-Arbeitsplatzumgebung in einer föderal übergreifenden Ausprägung
adressieren. Mit Blick auf kurzfristige Ergebnisse und gleichzeitig eine
Stärkung der Digitalen Souveränität soll das Vorhaben auf Open Source
Software fokussieren.

Am 30. August 2022 hat das BMI in einer Auftaktveranstaltung die
operative Umsetzung des SouvAP eröffnet. Zu diesem Termin wurden die
grundsätzliche Bedeutung des Projekts zur Stärkung der Digitalen
Souveränität, die Produkt-Vision, die zukünftige Zusammenarbeit mit dem
OS-Ökosystem und erste Erprobungsansätze vorgestellt. Des Weiteren wurde
beschlossen, die Realisierung durch eine kollaborative, offene und
effektive Zusammenarbeit innerhalb der ÖV und mit dem OS-Ökosystem zu
leisten. Das Architekturkonzept sollen Vertreter der öffentlichen
RZ-Dienstleister und zusätzliche Partner erstellen. Die technische
Umsetzung erfolgt durch den Dienstleister Dataport.
