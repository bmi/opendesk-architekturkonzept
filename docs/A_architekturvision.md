Phase A: Architekturvision
==========================

Projektinitiierung
------------------

Die Initiierung des Projektes wurde mit Stichtag 01.08.2022 in einem
Kickoff verkündet. Im Vorfeld dazu wurde die mit dem Auftrag verbundene
Leistungsbeschreibung analysiert und es wurden 4 Teilprojekte, 15
Aufgabenbereiche und 46 Arbeitspakete identifiziert. Für die Umsetzung
des Projekts wurde ein Volumen von 22 Mitarbeitern von Dataport und 70
oder mehr Beteiligten auf Seiten der Hersteller (Mitglieder des
OS-Ökosystems) geplant.

Zum Vorhaben wurde eine Projektstruktur über 3 Ebenen festgelegt:

-   **Entscheidungsebene** – Unter der Schirmherrschaft des
    IT-Planungsrats bilden Vertreter von BMI, BMF, AA, BMDV und der
    Länder Hessen, Berlin, Schleswig-Holstein, Brandenburg, Thüringen,
    Nordrhein-Westfalen und Bayern einen Lenkungskreis. Vertreter von
    Bund, Ländern und Kommunen innerhalb der AG Cloud liefern ergänzende
    Beiträge auf Entscheidungsebene.

-   **Steuerungsebene** – Hier gestalten Mitarbeiter von ZenDiS / BMI
    das Teilprojekt TP0 Projektsteuerung aus.

-   **Operative Ebene** – Die operativen Teilprojekte TP1 Technische
    Entwicklung, TP2 Einbettung in Strukturen der Öffentlichen
    Verwaltung und TP3 Erprobung sollen das Ziel Souveräner Arbeitsplatz
    arbeitsteilig und im Dialog seitens Mitarbeitern von Dataport und
    Mitarbeitern aus dem OS-Ökosystem umsetzen. Die Operative Ebene
    wurde zusätzlich um ein Produktboard, ein Architekturboard und ein
    User Experience-Board ergänzt. Letztere stehen beratend und
    unterstützend im ständigen Dialog mit allen 4 Teilprojekten des
    Vorhabens.

Anspruchsgruppen, Interessen und Geschäftsanforderungen
-------------------------------------------------------

### Anspruchsgruppen

Das Projekt SouvAP wurde vom Auftraggeber initiiert mit dem Ziel, einen
umfassenden Standard für die digitale Geschäftsprozessunterstützung
aller Bediensteten der öffentlichen Verwaltung zu erzeugen. Somit zählen

-   der **Auftraggeber** BMI,

-   weitere **oberste Bundesbehörden** (AA, BKAmt, BMF, BMJ BMVg, BMAS,
    BMEL, BMFSFJ etc.),

-   **nachgeordnete Bundesbehörden** (BeschA / ZIB, BSI),

-   **Steuerungsgremien der öffentlichen Verwaltung** (IT-PLR, KoITB /
    IT-Rat),

-   **parlamentarische IT-Verantwortliche** (CIO BB, CIO BE, CIO Bund,
    CIO BW, CIO BY, CIO HB etc.) und

-   **Träger der öffentlichen Verwaltung** (SprinD, Deutscher Städte-
    und Gemeindebund, Deutscher Landkreis, Digital / Services 4 Germany,
    Universitäten etc.)

naturgemäß zu den Anspruchsgruppen, die Einfluss auf die
Architekturarbeit nehmen.

Im Ergebnis soll die Architekturarbeit ein Bündel an Werkzeugen
spezifizieren, die in ein Betriebskonzept einzubetten sind. Diese sollen
mehrheitlich über die Veredelung bereits vorhandener Open Source
Softwarekomponenten entstehen. Zusätzlich sollen sie über den geplanten
Nutzungszeitraum gemäß einem Lebenszyklusmodell der beständigen Pflege
und Weiterentwicklung unterworfen sein. Daraus resultierend sind den
Anspruchsgruppen im Rahmen dieser Architekturarbeit ergänzend

-   **IT-Dienstleister** (Betreiber),

-   **Entwickler** und

-   **OS-Hersteller** (Teilnehmer am OS-Ökosystem)

für das Lebenszyklusmodell zuzuordnen.

Aufgrund der hohen politischen Bedeutung des Projekts sind ergänzend

-   **Interessensvertretungen** (Bitkom, govdigital, VITAKO, Voice e.V.)
    sowie

-   **Bund-Länder-Kommissionen** (AG Cloud Computing und Digitale
    Souveränität

als Anspruchsgruppe einzustufen. Die Ausrichtung „Open Source“
schließlich begründet eine Aufnahme von

-   **Open Source nahe Interessensverbände** (Apache Foundation, Chaos
    Computer Club e.V., Linux Foundation, GovTech Campus, Open Source
    Business Alliance OSBA)

in diese Liste.

Eine übergreifende Projektleitung nimmt während der gesamten
Projektlaufzeit und insbesondere im Rahmen der Architekturarbeit
Steuerungsaufgaben wahr. Diesbezügliche leistungsverantwortliche
Kapazitäten stellt das **ZenDiS** im Auftrag des BMI. Auf operativer
Ebene angesiedelte Teilprojekte und die projektspezifischen Gremien sind
zusätzlich als Anspruchsgruppen einzustufen. Die Liste der
Anspruchsgruppen komplettieren somit

-   **Projektbeteiligte** (BMI, Dataport, Element, HZD, IT.NRW,
    Nextcloud, OpenProject, Open-Xchange etc.) und

-   **Testpartner**.

### Interessen

Die folgende Tabelle benennt die Anspruchsgruppen nebst ihrer jeweiligen
Interessen im Zusammenhang mit dem Architekturvorhaben.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th>Anspruchsgruppe</th>
<th>Interessen</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Auftraggeber</td>
<td><ul>
<li><p>Abbau von Herstellerabhängigkeiten</p></li>
<li><p>Entwicklung eines DVS-konformen Cloud-Arbeitsplatzes unter Einsatz von Open Source</p></li>
</ul>
<ul>
<li><p>Grundschutzkonformität und Barrierefreiheit</p></li>
</ul></td>
</tr>
<tr class="even">
<td>oberste Bundesbehörden</td>
<td><ul>
<li><p>Nutzung des SouvAP als zukünftige Arbeitsplatz-Alternative</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>nachgeordnete Bundesbehörden</td>
<td><ul>
<li><p>Nutzung des SouvAP als zukünftige Arbeitsplatz-Alternative</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Steuerungsgremien der öffentlichen Verwaltung</td>
<td><ul>
<li><p>Mitgestaltung der Projektinhalte und -ziele</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>parlamentarische IT-Verantwortliche</td>
<td><ul>
<li><p>Mitgestaltung der Projektinhalte und -ziele</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Träger der öffentlichen Verwaltung</td>
<td><ul>
<li><p>Nutzung des SouvAP als zukünftige Arbeitsplatz-Alternative</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>IT-Dienstleister</td>
<td><ul>
<li><p>Erfolgreicher Betrieb des SouvAP</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Entwickler</td>
<td><ul>
<li><p>Herstellung und Pflege von OS-Software</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>OS-Hersteller</td>
<td><ul>
<li><p>Lieferung und Pflege von Software-Produkten für den SouvAP</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Interessensvertretungen</td>
<td><ul>
<li><p>Mitgestaltung der Projektinhalte und -ziele</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Bund-Länder-Kommissionen</td>
<td><ul>
<li><p>Mitgestaltung der Projektinhalte und -ziele</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Open Source nahe Interessensverbände</td>
<td><ul>
<li><p>Wissensaustausch mit Projektbeteiligten und Herstellern</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Projektbeteiligte</td>
<td><ul>
<li><p>Realisierung des SouvAP</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Testpartner</td>
<td><ul>
<li><p>Ertüchtigung des SouvAP als zukünftige Arbeitsplatz-Alternative</p></li>
</ul></td>
</tr>
</tbody>
</table>

### Geschäftsanforderungen

Die folgende Tabelle benennt die Anspruchsgruppen nebst ihrer jeweiligen
Geschäftsanforderungen im Zusammenhang mit dem Architekturvorhaben.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Anspruchsgruppe</td>
<td>Geschäftsanforderungen</td>
</tr>
<tr class="even">
<td>Auftraggeber</td>
<td><ul>
<li><p>Verfügbarmachung eines modularen und skalierbaren Standards für allgemeine Verwaltungs-IT</p></li>
<li><p>Synergiehebung durch Verschmelzung aller föderalen Ebenen innerhalb der Bundesrepublik Deutschland</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>oberste Bundesbehörden</td>
<td><ul>
<li><p>Verfügbarmachung einer OS-Komplettlösung mit sämtlichen notwendigen IT-Anwendungen für die Arbeit in der ÖV</p></li>
<li><p>Einbettung des SouvAP in die Strukturen des Bundes</p></li>
<li><p>Kompatibilität mit dem Bundesclient</p></li>
</ul></td>
</tr>
<tr class="even">
<td>nachgeordnete Bundesbehörden</td>
<td><ul>
<li><p>Verfügbarmachung einer OS-Komplettlösung mit sämtlichen notwendigen IT-Anwendungen für die Arbeit in der ÖV</p></li>
<li><p>Einbettung des SouvAP in die Strukturen des Bundes</p></li>
<li><p>Berücksichtigung und Umsetzung der Anforderungen der nachgeordneten Bundesbehörden</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Steuerungsgremien der öffentlichen Verwaltung</td>
<td><ul>
<li><p>Moderation zwischen Bund und Ländern</p></li>
<li><p>Wahrnehmung der Entscheidungsautorität</p></li>
</ul></td>
</tr>
<tr class="even">
<td>parlamentarische IT-Verantwortliche</td>
<td><ul>
<li><p>Berücksichtigung und Einbindung der Forderungen und Änderungswünsche der CIOs</p></li>
<li><p>regelmäßige Information zu Implementierungsfortschritten</p></li>
<li><p>Erstellung eines SouvAP als erfolgreiche Arbeitsplatz-Alternative</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Träger der öffentlichen Verwaltung</td>
<td><ul>
<li><p>Abdeckung der speziellen strukturellen Anforderungen der Träger der ÖV mit OS-Produkten</p></li>
</ul></td>
</tr>
<tr class="even">
<td>IT-Dienstleister</td>
<td><ul>
<li><p>Implementierung, Betriebssteuerung und Überwachung des SouvAP</p></li>
<li><p>Evaluation des Betriebs und Aufdecken von Optimierungspotenzialen</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Entwickler</td>
<td><ul>
<li><p>Erstellung, Weiterentwicklung und Pflege von OS-Software</p></li>
</ul></td>
</tr>
<tr class="even">
<td>OS-Hersteller</td>
<td><ul>
<li><p>Erstellung eines vollständigen, sicheren und modernen Arbeitsplatzsystems</p></li>
<li><p>Reduktion von kritischen Abhängigkeiten zu einzelnen Herstellern</p></li>
<li><p>Stärkung des ÖV-Marktpotenzials für Unternehmen unterschiedlicher Größenordnungen</p></li>
<li><p>Stärkung der Reputation dieser Unternehmen mit dem Ergebnis langfristiger Aufträge seitens der ÖV</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Interessensvertretungen</td>
<td><ul>
<li><p>Gewährleistung von Funktionsfähigkeit und digitaler Souveränität der ÖV und des Staates</p></li>
<li><p>Realisierung des SouvAP zwecks Schaffung der seit langem diskutierten Vorteile eines SouvAP</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Bund-Länder-Kommissionen</td>
<td><ul>
<li><p>Berücksichtigung und Einbindung der Forderungen und Änderungswünsche der BLK-Organisationen</p></li>
<li><p>gemeinschaftliche Förderung des Erfolgs über abgestimmte und einheitliche Kommunikations- und Abstimmungsstrukturen</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Open Source nahe Interessensverbände</td>
<td><ul>
<li><p>Abdecken der gesamten Bandbreite an Funktions- und Anwendungsbedarf für Aufgaben der ÖV mit OS</p></li>
<li><p>Signifikante Reduktion der Abhängigkeiten zu einzelnen Anbietern und Produkten</p></li>
<li><p>Nutzung der Security-Erfahrungen der OS-Interessensverbände</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Projektbeteiligte</td>
<td><ul>
<li><p>Abbau bestehender kritischer Abhängigkeiten zu Herstellern</p></li>
<li><p>Implementierung von Open-Source-Alternativprodukten</p></li>
<li><p>Stärkung der digitalen Souveränität von Staat und ÖV</p></li>
<li><p>Beteiligung an der Umsetzung mit intern erzeugten Architekturbausteinen, Empfehlungen und Entscheidungsvorlagen</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Testpartner</td>
<td><ul>
<li><p>Erprobung des SouvAP und Nachweis der Zielerreichung, insbesondere des Funktionsumfangs, der Nutzerfreundlichkeit, der Modularität, der Austauschbarkeit, der Interoperabilität und der Wechselfähigkeit zwischen Komponenten mit vergleichbaren Leistungsmerkmalen</p></li>
</ul></td>
</tr>
</tbody>
</table>

### Kommunikationsbedarf

Die Projektbeteiligten tauschen sich untereinander zyklisch zu
organisatorischen, fachlichen und technischen Fragen aus. Dabei
festgehaltene Vereinbarungen sind zu dokumentieren.

Die folgende Tabelle fächert die Anspruchsgruppen und ihre jeweils
benötigten Projektinformationen auf:

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Anspruchsgruppe</td>
<td><blockquote>
<p>Statusreport</p>
</blockquote></td>
<td><blockquote>
<p>Meeting-Agenda</p>
</blockquote></td>
<td><blockquote>
<p>Meeting-Protokoll</p>
</blockquote></td>
<td><blockquote>
<p>Architektur-Vision</p>
</blockquote></td>
<td><blockquote>
<p>Anforderungen</p>
</blockquote></td>
<td><blockquote>
<p>Modell der Geschäftsarchitektur</p>
</blockquote></td>
<td><blockquote>
<p>Modell der Datenarchitektur</p>
</blockquote></td>
<td><blockquote>
<p>Modell der Informationssystemarchitektur</p>
</blockquote></td>
<td><blockquote>
<p>Modell der Infrastrukturarchitektur</p>
</blockquote></td>
</tr>
<tr class="even">
<td>Auftraggeber</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>oberste Bundesbehörden</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="even">
<td>nachgeordnete Bundesbehörden</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Steuerungsgremien der öffentlichen Verwaltung</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="even">
<td>parlamentarische IT-Verantwortliche</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Träger der öffentlichen Verwaltung</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="even">
<td>IT-Dienstleister</td>
<td>X</td>
<td></td>
<td></td>
<td></td>
<td>X</td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Entwickler</td>
<td>X</td>
<td></td>
<td></td>
<td></td>
<td>X</td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="even">
<td>OS-Hersteller</td>
<td>X</td>
<td></td>
<td></td>
<td></td>
<td>X</td>
<td></td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Interessensvertretungen</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Bund-Länder-Kommissionen</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Open Source nahe Interessensverbände</td>
<td>X</td>
<td></td>
<td></td>
<td>X</td>
<td>X</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr class="even">
<td>Projektbeteiligte</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
<tr class="odd">
<td>Testpartner</td>
<td>X</td>
<td>X</td>
<td>X*</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
<td>X</td>
</tr>
</tbody>
</table>

\*: keine internen/vertraulichen Protokollinhalte

### Informationsspeicher

Die folgende Tabelle benennt die Liefertermine und die Ablageorte der
Projekt-Informationen:

<table>
<tbody>
<tr class="odd">
<td>Information</td>
<td>Frequenz</td>
<td>Ablageort</td>
</tr>
<tr class="even">
<td>Statusreport</td>
<td>wöchentlich</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="odd">
<td>Meeting-Agenda</td>
<td>wöchentlich</td>
<td>Projektordner des Teilprojekts/des Gremiums</td>
</tr>
<tr class="even">
<td>Meeting-Protokoll</td>
<td>wöchentlich</td>
<td>Projektordner des Teilprojekts/des Gremiums</td>
</tr>
<tr class="odd">
<td>Architektur-Vision</td>
<td>zum Ende von Phase A</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="even">
<td>Anforderungen</td>
<td>wöchentlich</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="odd">
<td>Modell der Geschäftsarchitektur</td>
<td>zum Ende von Phase B</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="even">
<td>Modell der Datenarchitektur</td>
<td>zum Ende von Phase C</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="odd">
<td>Modell der Informationssystemarchitektur</td>
<td>zum Ende von Phase C</td>
<td>übergreifender Projektordner</td>
</tr>
<tr class="even">
<td>Modell der Infrastrukturarchitektur</td>
<td>zum Ende von Phase D</td>
<td>übergreifender Projektordner</td>
</tr>
</tbody>
</table>

Geschäftstreiber, Rahmenbedingungen und Geschäftsziele
------------------------------------------------------

Einige der diese Architekturarbeit begründenden Geschäftstreiber sind in
der Leistungsbeschreibung im Detail benannt. Weitere sind in
übergreifenden IT-Strategien explizit oder implizit aufgeführt. An
dieser Stelle sei explizit auf die Nationale E-Government Strategie NEGS
des IT-Planungsrats von 2013 sowie auf die IT-Strategien der Länder
verwiesen.

### Geschäftstreiber

Die dominierenden Geschäftstreiber sind

-   Bedarf an zeitgemäßer IT-Unterstützung für effizientes
    Verwaltungshandeln,

-   Bedarf an standardisierter digitaler Geschäftsprozessunterstützung,

-   Medienbruchfreie ebenen-übergreifende Zusammenarbeit,

-   Knappe Kassen,

-   Planungssicherheit,

-   Abbau von Herstellerbindungen,

-   Green-IT,

-   Hohes Aufkommen an Cybercrime,

-   Bedarf an gesteigerter Resilienz,

-   Cloudifizierung.

### Rahmenbedingungen

Von der Architektur zu berücksichtigende Rahmenbedingungen sind
einerseits Gesetze und Verordnungen, andererseits dienstliche
Anordnungen und Vorgaben. Dazu zählen

-   BSI-Grundschutz,

-   Datenschutzgrundverordnung DSGVO,

-   Standard Datenschutzmodell SDM,

-   IT-Sicherheitsgesetz,

-   Online Zugangsgesetz OZG,

-   IT-Strategien von Bund und Ländern,

-   Staatsverträge der AöR-Träger,

-   Zielbilder der Verwaltungs-Rechenzentren.

### Geschäftsziele

Die nachstehenden Geschäftsziele verdichten die Geschäftstreiber gemäß
Kapitel 4.3.1 und die Geschäftsanforderungen gemäß Kapitel 4.2.3 unter
Einbezug der in Kapitel 4.3.2 aufgezählten Rahmenbedingungen.

-   Steigerung des Nutzens für Bürger, Unternehmen, Verwaltung,

-   Sicherstellung der Digitale Souveränität,

-   Maximieren der Wirtschaftlichkeit der Geschäftstätigkeiten der ÖV,

-   Maximieren der Effizienz der Geschäftstätigkeiten der ÖV,

-   Ausbau der Leistungsfähigkeit der ÖV,

-   Gewährleistung der Informationssicherheit,

-   Konformität mit den Vorgaben zu Geheim- und Datenschutz,

-   Berücksichtigung von Vorgaben zu Transparenz und gesellschaftliche
    Teilhabe,

-   Ausrichtung auf Zukunftsfähigkeit,

-   Ausrichtung auf Nachhaltigkeit,

-   Maximieren von Kundenorientierung/Kundenzufriedenheit.

Bewertung vorhandener Fähigkeiten
---------------------------------

Derzeit nutzen Bedienstete der öffentlichen Verwaltung für ihre Aufgaben
überwiegend ressortspezifische IT-Werkzeuge. Ihr Betrieb steht
mehrheitlich im Einklang mit geltenden Vorgaben zum Datenschutz. Die
konsequente Anwendung von Maßnahmen zur Informationssicherheit hingegen
ist eventuell nur dann umgesetzt, wenn dies aufgrund gesetzlicher oder
dienstlicher Regelungen vorgeschrieben ist.

Des Weiteren bergen die Effizienz und die Wirtschaftlichkeit bei
ebenen-übergreifender Zusammenarbeit oft deutliche
Optimierungspotenziale, z.B. den Abbau von Medienbrüchen im
Informationsaustausch oder die Homogenisierung und Standardisierung der
Systemlandschaften sowie der Arbeitsprozesse.

Diese sind weitgehend erkannt. Ihr Abbau gelingt aber in der Regel nur
punktuell: Föderale Strukturen in den Verwaltungen erlauben
landesspezifische Gesetzgebungen und Prozesswelten, die voneinander
losgelöst sind und gegebenenfalls zum Zweck der landesübergreifenden
Zusammenarbeit aufeinander anzugleichen sind.

Tatsächlich sieht sich die öffentliche Verwaltung zusätzlich mit
Disruptionen konfrontiert: Beständig steigende Anforderungen sind nur
mit höheren Taktraten zu bewältigen. Kurze Innovationszyklen erfordern
mehr Flexibilität und Agilität im Verwaltungshandeln. Internationale
verwalterische Trends wie Smart Cities und Green IT erfordern zudem eine
kurzfristig umsetzbare Agenda, die die Wandlung hin zu einem modernen,
zukunftsfähigen und bürgernahen Staat auf den Weg bringt.

Ziel dieser Architekturarbeit ist es, die Geschäftsziele gemäß Kapitel
4.3.3 zu adressieren. Die zugrunde liegende aktuelle Architektur und die
daraus abgeleitete technische Implementierung werden bezüglich dieser
Geschäftsziele quantitativ wie folgt eingestuft:

![](media/file1.png)

Abbildung : Bewertung vorhandener Fähigkeiten

Anmerkungen: 0=Ziel nicht erreicht, 10=Ziel vollständig erreicht. Die
Einschätzungen stammen aus dem Projektteam.

Prognose zu zukünftigen Fähigkeiten
-----------------------------------

Der Souveräne Arbeitsplatz soll die aktuellen Bewertungen idealerweise
übertreffen. Es wird vermutet, dass dies zum Thema Kundenzufriedenheit
in einer Übergangsphase nur bedingt gelingt, da neue Werkzeuge eine
Eingewöhnungsphase erfordern. Das wird vermutlich zunächst auch die
Leistungsfähigkeit, die Effizienz und die Wirtschaftlichkeit tangieren.

In Summe soll der Souveräne Arbeitsplatz dennoch die
IT-Geschäftsprozessunterstützung öffentlich Bediensteter auf ein
fortschrittliches, zukunftsfähiges und digital souveränes Niveau heben
und dieser Zustand soll spätestens mittelfristig erreicht sein. Es wird
erwartet, dass der Souveräne Arbeitsplatz bei der Sicherstellung der
Souveränität den größten Mehrwert erwirken kann und dieser Sachverhalt
wird automatisch mit seiner Produktivsetzung erreicht.

Die Grade der Zielerreichung mit dem Souveränen Arbeitsplatz werden in
der ersten Iteration der Architekturarbeit bewusst konservativ geschätzt
und es werden geringfügige Abstriche in den Punkten Kundenzufriedenheit
und Wirtschaftlichkeit geschätzt. Die Auswirkungen der Architekturarbeit
im Vergleich zur Ausgangslage:

![](media/file2.png)

Abbildung : Auswirkungen der Architekturarbeit auf die Geschäftsziele

Anmerkungen: 0=Ziel nicht erreicht, 10=Ziel vollständig erreicht. Die
Einschätzungen stammen aus dem Projektteam.

Die starken Gewinne bei Informationssicherheit, Geheim- und Datenschutz
werden mit der konsequenten Umsetzung einer Sicherheitsarchitektur zum
SouvAP begründet. Zukunftsfähigkeit und Nachhaltigkeit resultieren aus
der Verwendung von Open Source als Basis. Damit werden die
Voraussetzungen für agile Anpassungen an neue Anforderungen befriedigt.
Für die Zugewinne bei der Effizienz und der Leistungsfähigkeit zeichnen
die durchgehende Standardisierung und die darin enthaltene auch
ebenen-übergreifende Interoperabilität verantwortlich.

Analyse und Bewertung der Reife für eine Transformation
-------------------------------------------------------

Die Informations- und Kommunikationstechnik der öffentlichen Verwaltung
in Deutschland ist seit Jahren durch strategische Planungen geprägt.
Bund und Länder fertigen zyklisch IT-Strategien für einen Zeithorizont
von 5-8 Jahren. Öffentliche IT-Dienstleister manifestieren ihre
Leitlinien und Ziele in Zielbildern. Den Kommunen steht bei
Ausarbeitungen und Fortschreibungen von IT-Strategien bei Bedarf unter
anderem die Kommunale Gemeinschaftsstelle für Verwaltungsmanagement zur
Seite. Zahlreiche professionelle Unternehmen liefern ergänzende
Beratungsleistungen mit dem Ziel, die öffentlichen Verwaltungen auf ein
hohes Innovationsniveau und damit auf maximale Handlungsfähigkeit zu
heben.

Vor diesem Hintergrund liefern die Fähigkeiten aller am Projekt
beteiligten Organisationen zweifelsfrei den erforderlichen Reifegrad für
die mit dieser Architekturarbeit vorzubereitende Transformation. Sollten
dennoch punktuell Auffrischungen erforderlich sein, dann kann eine etwa
erforderliche Ertüchtigung spätestens über Zuarbeiten des insgesamt
verfügbaren Ökosystems effizient und zeitnah erreicht werden.

Abgrenzung des Umfangs der Architekturarbeit
--------------------------------------------

Die Architekturarbeit nach der Methode von TOGAF betrachtet die
Architekturdomänen

-   Geschäftsarchitektur,

-   Informationssystemarchitektur,

-   Datenarchitektur,

-   Infrastrukturarchitektur

und ergänzend

-   Sicherheitsarchitektur.

Das Ziel des Auftrags „Souveräner Arbeitsplatz“ adressiert im Kern die
Architekturdomänen Informationssystemarchitektur, Datenarchitektur und
Sicherheitsarchitektur.

Die Geschäftsarchitektur ist vorgegeben und soll im Wesentlichen
erhalten bleiben. Es wird dennoch erwartet, dass einige Prozesse
entweder an neue Betriebskonzepte angepasst werden müssen oder im
späteren Verlauf eine Umgestaltung von Geschäftsprozessen angeraten ist.
Dies kann daraus resultieren, dass neue Optimierungspotenziale erkennbar
werden und eine Veränderung von Geschäftsprozessen Verbesserungen in den
Punkten Wirtschaftlichkeit und/oder Effizienz erwarten lassen. Die
angestrebte Harmonisierung der verwalterischen Geschäftstätigkeiten von
Bund, Ländern und Kommunen lässt zusätzlich eine Umgestaltung der
Geschäftsprozesse in einigen Teilbereichen erwarten. Hier ist aber keine
disruptive Veränderung zu erwarten, sondern eine Verschlankung der
Prozesslandkarte aufgrund von iterativer Standardisierung.

Auf der Seite der Infrastrukturarchitektur lässt die Anforderung
„DVS-konform“ auf den ersten Blick den Bedarf für eine fundamentale
Neugestaltung von Rechenzentren und den darauf zugeschnittenen Betrieb
erwarten. Dies sollte aber kaum eine Rolle spielen, da die öffentlichen
IT-Dienstleister mindestens informell und in der Praxis auch gestaltend
in die Entwicklung der DVS eingebunden sind. Erste Benchmarks haben
bereits die DVS-Konformität ausgewählter Implementierungen in
öffentlichen Rechenzentren bestätigt. Mit Blick auf die Notwendigkeit
für eine Integration in die deutsche Verwaltungscloud dürfte es daher
allenfalls eine Frage der Zeit sein, bis die relevanten IT-Dienstleister
diese Anpassungen über ihre zyklischen Fortschreibungen in eigenen
Reihen umgesetzt haben.

Entwicklung von Architekturprinzipien und Geschäftsprinzipien
-------------------------------------------------------------

Architekturprinzipien liefern Leitplanken für die technisch orientierten
Architekturdomänen. Geschäftsprinzipien fokussieren auf strategische
Erfolgsfaktoren. Im Detail kann die Ausarbeitung solcher Prinzipien sehr
granular ausfallen und es sind genau genommen Prinzipien für jede der
einflussnehmenden Architekturdomänen zu entwickeln:

-   Geschäftsprinzipien

-   Informationssystemprinzipien

-   Anwendungsprinzipien

-   Infrastrukturprinzipien

-   Sicherheitsprinzipien

Der Erfahrung nach nehmen strategische Erfolgsfaktoren oft auf mehrere
dieser Architekturdomänen Einfluss. Um Wiederholungen zu vermeiden,
werden diese in übergeordnete Prinzipien gebündelt.

### Übergeordnete Architekturprinzipien

-   Konsequente Ausrichtung auf Sicherheit

-   Ausrichtung auf Anwenderzufriedenheit

-   Ökologisches Denken

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Konsequente Ausrichtung auf Sicherheit</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die Ausgestaltung des Souveränen Arbeitsplatzes muss alle regulatorisch vorgeschriebenen und alle aus Wissenschaft und Industrie empfohlenen Maßnahmen zum Schutz der benötigten Informationstechnik einschließlich der zu verarbeitenden Daten berücksichtigen und passende Maßnahmen entsprechend dem Bedarf umsetzen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Der Souveräne Arbeitsplatz ist auf die Geschäftsprozesse der öffentlichen Verwaltung zugeschnitten. Diese verarbeiten überwiegend sensible Daten, die die Regularien für den Schutz von personenbezogenen Informationen und/oder die für den Umgang mit Verschlusssachen befolgen müssen. Mit Bezug auf die <a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2019/Beschluss2019-04_TOP12_Anlage_Leitlinie.pdf"><span class="underline">Leitlinie für Informationssicherheit der öffentlichen Verwaltung</span></a> ist ein Sicherheitsniveau anzustreben, das keine hohen Risiken akzeptiert. Dazu passende Maßnahmen sind schon in der Designphase einzuplanen, da etwa benötigte Nachbesserungen erfahrungsgemäß nur mit deutlich erhöhtem Aufwand gelingen.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Der Souveräne Arbeitsplatz ist als geschlossener Informationsverbund schon in der Designphase bezüglich möglicher Risiken zu betrachten. Im Zuge seiner Realisierung sind begleitend Risiko-Analysen durchzuführen. Vor der Produktivsetzung sind Maßnahmen einzuziehen, die etwaige Risiken geeignet beschränken.</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Ausrichtung auf Anwenderzufriedenheit</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die Architekturarbeit ist mit Fokus auf optimale Akzeptanz der Ergebnisse über alle Architekturdomänen zu gestalten. Ziel muss es sein, die Anwenderzufriedenheit zu maximieren.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Die Anwenderzufriedenheit ist ein Stimmungsbild der Nutzer der Informationstechnik. Sie entsteht aus dem Zusammenspiel von Verfügbarkeit der IT-Werkzeuge, Zufriedenheit mit Ausstattung und Bedienbarkeit und Wirksamkeit in der Geschäftsprozessunterstützung. Im Zusammenspiel mit anderen IT-Werkzeugen nehmen zusätzlich die Interoperabilität der Werkzeuge untereinander und der medienbruchfreie Datenaustausch Einfluss auf die Anwenderzufriedenheit.</p>
<p>Es ist beabsichtigt, dass die Architekturarbeit aktuell genutzte durch neue IT-Werkzeuge ersetzt. Wenn dies zu einer reduzierten Anwenderzufriedenheit führt, dann sind gemäß typischer Studien zur Anwenderzufriedenheit mit IT negative Auswirkungen auf die Arbeitsleistung zu erwarten. Dies kann im Extremfall zur Ablehnung der IT-Werkzeuge führen mit dem Ergebnis, dass die Geschäftsprozesse mit vertrauten oder anderen IT-Werkzeugen bewältigt werden (→ Schatten-IT).</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Es sind Kriterien und Kennzahlen zu entwickeln, die die Anwenderzufriedenheit treffsicher bewerten können. Die Anwenderzufriedenheit ist zyklisch zu prüfen und es sind Nachbesserungen umzusetzen, wenn der erwartete Grad der Anwenderzufriedenheit signifikant unterschritten wird. Idealerweise werden die Anwender frühzeitig in die Entwicklung neuer Werkzeuge eingebunden und nehmen über subjektive Bewertungen diese Kriterien Einfluss auf die Produktentwicklung.</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Ökologisches Denken</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Der Souveräne Arbeitsplatz muss das gesellschaftliche Ziel nach mehr Nachhaltigkeit unterstützen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Die deutsche Nachhaltigkeitsstrategie manifestiert das Ziel, bei öffentlichen Beschaffungen insbesondere auch im Bereich der Informationstechnik Kriterien der Nachhaltigkeit zu berücksichtigen. Dazu sind z.B. bei Beschaffungen energieeffiziente Produkte und Dienstleistungen zu bevorzugen und es ist das Ziel zu adressieren, den Anfall von Elektronikabfall (u.a. durch längerfristige Nutzung) zu reduzieren.</p>
<p>Die heute verfügbaren Leitfäden zur umweltfreundlichen Beschaffung von Informations- und Kommunikationstechnik adressieren Kriterien, die die Architekturdomäne Infrastrukturarchitektur direkt verwenden kann. Auf der Seite der Software ist bekannt, dass zu den heute gebräuchlichen Programmiersprachen eine Rangbildung zur Energieeffizienz möglich ist. Es sollte ein vorrangiges Ziel der Architekturarbeit sein, die Energieeffizienz als Entscheidungskriterium bei der Auswahl von Softwarekomponenten heranzuziehen.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Bei der Ausgestaltung der Infrastruktur ist der <a href="https://www.umweltbundesamt.de/sites/default/files/medien/479/publikationen/uba_leitfaden_zur_umweltfreundlichen_oeffentlichen_beschaffung_server_und_datenspeicherprodukte.pdf"><span class="underline">Leitfaden zur umweltfreundlichen öffentlichen Beschaffung: Server und Datenspeicherprodukte</span></a> zu berücksichtigen. Die Nutzung der Infrastruktur ist mit Techniken der Virtualisierung auf maximale Ressourcennutzung hin zu optimieren. Bei der Auswahl von Softwareschichten sind Aspekte der Energieeffizienz zu eruieren. Bei der Neuentwicklung von Software ist die Energieeffizienz als mitgeltende Vorgabe zu betrachten.</td>
</tr>
</tbody>
</table>

### Geschäftsprinzipien

-   Wirtschaftlichkeit (keine Umsetzung um jeden Preis)

-   unterbrechungsfreier Geschäftsbetrieb wenn nötig

-   Orientierung an internationalen Verwaltungsstandards

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Wirtschaftlichkeit (keine Umsetzung um jeden Preis)</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Bei Beschaffung und Betrieb von Komponenten zum Souveränen Arbeitsplatz sind grundsätzlich die Aspekte der Wirtschaftlichkeit und weitere Aspekte wie Nachhaltigkeit, Barrierefreiheit und so fort zu berücksichtigen. Steht der erwartete Nutzen in einem unverhältnismäßig geringen Verhältnis zu den Kosten, dann ist nach Alternativen zu suchen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Die Entwicklung und Bereitstellung des SouvAP muss die finanziellen Möglichkeiten der öffentlichen Verwaltung berücksichtigen. Gleichwohl sind übergreifende Prinzipien zu beachten, die einen Weg hin zu nachhaltigem Wirtschaften und gesellschaftlichem Fortschritt in der Sozialen Marktwirtschaft ebnen. Insbesondere stehen Aspekte von Gleichbehandlung, Menschenwürde und gesellschaftlicher Teilhabe im Fokus.</p>
<p>Der Souveräne Arbeitsplatz wird vorrangig auf die Belange hinsichtlich Nachhaltigkeit und Barrierefreiheit ausgerichtet. Eine Umsetzung jeder Idee zu jedem Preis kann und darf es dabei jedoch nicht geben.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Produktentscheidungen sind mit Berücksichtigung von Wirtschaftlichkeitsbetrachtungen durchzuführen. Liegen keine Wirtschaftlichkeitsbetrachtungen vor, dann sind diese vorab zu erstellen.</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Unterbrechungsfreier Geschäftsbetrieb wenn nötig</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die Anforderung „Schutzbedarf hoch“ ist nur dann als Anforderung an unterbrechungsfreien Geschäftsbetrieb einzustufen, wenn das Schutzziel Verfügbarkeit mit hoch oder sehr hoch eingestuft wird.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Der Schutzbedarf wird in der Regel als Maximum über die Schutzziele Verfügbarkeit, Vertraulichkeit und Integrität festgestellt. Eine pauschale Zuweisung dieses Maximums auf alle Schutzziele kann einen unnötig hohen Aufwand für Schutzziele auslösen, die unterhalb dieses Maximums eingestuft wurden.</p>
<p>Besonders hohe Auswirkungen hat dies auf die notwendigen Maßnahmen zur Befriedigung einer überhöhten Verfügbarkeit. Das gelingt mehrheitlich nur über redundante technische Komponenten (Systeme und Kommunikationspfade) und zusätzliche Fähigkeiten, die bei der Softwareentwicklung auszugestalten sind.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Die bedarfsseitig zugeordneten Schutzziele sind in jedem Fall differenziert zu erörtern und zu hinterfragen. Im Zweifelsfall ist der gewünschte Schutzbedarf zum Schutzziel Verfügbarkeit über eine Gegenüberstellung der Kosten für Aufbau und Betrieb unterschiedlicher Schutzstufen zu bestätigen. Ist der unterbrechungsfreie Geschäftsbetrieb unabdinglich, dann ist die Verfügbarkeit „hoch“ bis in die Anwendung hinein zu implementieren.</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Orientierung an internationalen Verwaltungsstandards</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Standards für den SouvAP dürfen keine Abgrenzung gegenüber internationalen Verwaltungsstandards auslösen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Standardisierte Prozesse, Schnittstellen und Datenaustauschformate stellen die Weichen hin zu medienbruchfreier Zusammenarbeit zwischen Behörden, zwischen Verwaltung und Wirtschaft und zwischen Verwaltung und Bürgern sowie Bürgerinnen. In einem vereinten Europa und insgesamt einer globalisierten Welt müssen grenzübergreifende Standards zum Einsatz kommen, wenn eine vergleichbare Effizienz auch in internationalen Verwaltungsprozessen erreicht werden soll.</p>
<p>Mit Gründung der FITKO hat der IT-Planungsrat die nationale Standardisierung an diese neue Organisation übertragen. Den Anschluss Deutschlands an elektronisch unterstützte öffentliche Vergabeverfahren innerhalb der Europäischen Union (Pan-European Public Procurement OnLine PEPPOL) treibt die Koordinierungsstelle für IT-Standards voran. Des Weiteren will GAIA-X eine sichere und transparente Dateninfrastruktur nach europäischen Standards schaffen, in der gesicherter Datenaustausch zwischen Institutionen und auch zwischen Staaten gelingt.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Aktivitäten rund um internationale Verwaltungsstandards sind beständig zu beobachten. Dabei aufgedeckte Ergebnisse sind im Zuge der Ausgestaltung und der späteren zyklischen Überarbeitung des SouvAP zu berücksichtigen .</td>
</tr>
</tbody>
</table>

### Informationsprinzipien

-   Einsatz offener Standards für Protokolle und Dateiformate

-   Medienbruchfreie Interoperabilität

-   Geregeltes Teilen von Informationen

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Einsatz offener Standards für Protokolle und Dateiformate</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Für den Transport und die Ablage von Daten sind Protokolle und Dateiformate gemäß offener Standards einzusetzen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Offene Standards unterliegen keiner patentrechtlichen Regelung und sind daher insbesondere bei Herstellern von Produkten beliebt, die auf nahtlose Integration in ein größeres Servicegeflecht ausgerichtet sind. Der Einsatz offener Standards maximiert auf Seite des Anwenders die Wahlfreiheit in der Produktauswahl zu einem SouvAP-Modul und ist dabei ein idealer Wegbereiter hin zu Interoperabilität. Des Weiteren fördert der Einsatz von offenen Standards den Abbau von Herstellerbindungen.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Um die Wechselfähigkeit aufrecht zu halten, sind offene Standards zu wählen, die die Mehrheit der Marktbegleiter ohne herstellerspezifische Erweiterungen bedienen.</td>
</tr>
</tbody>
</table>

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Medienbruchfreie Interoperabilität</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Der Datenaustausch von Stammdaten zwischen verschiedenen Ebenen der Verwaltungstätigkeiten muss ohne Medienbrüche gelingen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>In der Informationsverarbeitung liegt ein Medienbruch immer dann vor, wenn der Datentransport von einem auf einen anderen Verarbeitungsprozess einen Medienwechsel erfordert. Dies behindert die zügige Abwicklung komplexer Prozessketten und ist außerdem eine mögliche Fehlerquelle.</p>
<p>Die medienbruchfreie Interoperabilität nimmt eine Schlüsselstellung in der Digitalisierung der Verwaltung ein. Speziell die ebenen-übergreifende Bewältigung komplexer Verwaltungsvorgänge profitiert von medienbruchfreier Interoperabilität. Für die Automatisierung verketteter Prozesse ist dies eine Voraussetzung.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Alle untereinander in Beziehung stehenden Werkzeuge zur IT-Unterstützung der Verwaltungsprozesse müssen aufeinander abgestimmte Datenformate und Übertragungsprotokolle benutzen.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Geregeltes Teilen von Informationen</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Das Teilen von Informationen ist auf dienstliche Belange hin zu beschränken.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die Zusammenarbeit ist eine Kernanforderung an den Souveränen Arbeitsplatz. Voraussetzung dafür ist ein Teilen von Informationen zwischen Individuen bzw. Gruppen. Informationen haben aber mehrheitlich einen gesetzlich geregelten Schutzbedarf und daher muss das Teilen von Informationen die Sicherheit von Vertraulichkeit, Verfügbarkeit und Integrität gewährleisten können.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Der Informationszugriff ist über restriktive Zugriffsrechteschemen zu regulieren und die Einhaltung der Zugriffsrechte ist beständig zu überwachen.</td>
</tr>
</tbody>
</table>

### Anwendungsprinzipien

-   Austauschbarkeit von Werkzeugen

-   Fokus auf Open Source

-   Einheitliches Bedienkonzept über alle Werkzeuge

-   Container-nativ für Infrastrukturen nach DVS-Standard

-   Barrierefreie Bedienung

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Austauschbarkeit von Werkzeugen</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Der Souveräne Arbeitsplatz soll nach Möglichkeit ausschließlich solche Softwarekomponenten enthalten, für die funktionell vergleichbare Produkte verfügbar sind.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die im Souveränen Arbeitsplatz gebündelten Werkzeuge sollen so zusammengestellt sein, dass Produkt- bzw. Herstellerbindungen ausgeschlossen sind. Aufgrund veränderter Rahmenbedingungen, z.B. Wandlung der produktspezifischen Lizenzbedingungen oder unzureichende Fortentwicklung (nicht geeignet für neue Anforderungen, fehlender Zuschnitt auf neue Regularien) muss ein Wechsel auf ein Alternativprodukt möglich sein. Alternativprodukte müssen nahtlos in die vorhandene Umgebung integrierbar sein.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Vor der Produktauswahl ist eine Marktübersicht anzufertigen, die infrage kommende Werkzeuge betreffend vorhandener Anforderungen vergleichend bewertet. Die Produktauswahl darf keine Entscheidungen aufgrund von Alleinstellungsmerkmalen aussprechen.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Fokus auf Open Source</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Es sind Open Source Alternativen zu aktuell im Einsatz befindlichen proprietären Produkten zu identifizieren und zu einem Bestandteil eines übergreifenden souveränen Produktportfolios zu ertüchtigen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die Strategie zur Stärkung der Digitalen Souveränität von Bund, Ländern und Kommunen hat die Diversifizierung und Schaffung von Alternativen als zentrales Element auf dem Weg hin zur Stärkung der Kontrolle über die eigene IT herausgestellt. Dies ist erforderlich, um der Gefahr entgegenzuwirken, dass die öffentliche Verwaltung die Fähigkeit zu selbstbestimmtem Handeln verliert Die Auflösung des drohenden Konflikts soll über den Wechsel hin zu Open Source Software gelingen.<a href="#fn1" id="fnref1" class="footnote-ref">1</a></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Die Erörterung von Implikationen und die daraus resultierenden Aufgaben sind Gegenstand dieser Architekturarbeit.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Einheitliches Bedienkonzept über alle Werkzeuge</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Werkzeugseitiger Datenzugriff und Funktionen zur Datenverarbeitung müssen werkzeugübergreifend einheitliche Konzepte befolgen. Dies ist auf alle vorgesehenen Interaktionsformen mit den Komponenten des SouvAP auszudehnen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Ein einheitliches Bedienkonzept erleichtert den Zugang zu neuen Werkzeugen und den Umgang mit Werkzeugen, die eher selten benutzt werden. Dies ist jeweils für die unterschiedlichen Formen vorgesehener Endgeräte durchzusetzen. Dabei ist besonders darauf zu achten, dass ein erfahrener Anwender seine gewohnte Arbeitsweise auch nach einem Wechsel auf ein andersartiges Endgerät wiederfindet. Der Umstieg von klassischer Tastatur/Maus-Bedienung hin zu einem Touch-Display oder der Wechsel von händischer zu sprachgestützter Steuerung dürfen keine signifikanten negativen Auswirkungen auf die Effizienz der verwalterischen Geschäftsprozessbewältigung entfalten.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Es ist ein übergreifendes Bedienkonzept zu entwickeln und alle Wergzeuge des SouvAP sind sukzessive auf das übergreifende Bedienkonzept zu migrieren.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Container-nativ für Infrastrukturen nach DVS-Standard</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Alle Werkzeuge des SouvAP sind als portable Systemabbilder zu fertigen. Sie müssen mit Infrastrukturen nach DVS-Standard kompatibel sein.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Container Infrastrukturen enthalten ein komplexes Bündel aus Container-Host, Container Registry und Container-Orchestrierung. Container-Images müssen mit der Container-Engine des Container-Hosts kompatibel sein, die dort enthaltene Anwendung muss einschließlich etwa vorhandener Middleware mit der Container-Engine harmonieren und das Image-Format muss zur Orchestrierung und der Host-Engine passen.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Keine.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Barrierefreie Bedienung</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die Werkzeuge des SouvAP sind für barrierefreie Bedienung auszulegen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die <a href="https://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html"><span class="underline">Barrierefreie-Informationstechnik-Verordnung BITV 2.0</span></a> schreibt vor, dass digitale Inhalte der öffentlichen Stellen seit 2019 barrierefrei sein müssen. Dies ist anzuwenden auf Websites (einschließlich Webseiten in einem Extranet oder Intranet), mobile Anwendungen, elektronisch unterstützte Verwaltungsabläufe, Verfahren zur elektronischen Aktenführung bzw. Vorgangsbearbeitung und grafische Programmoberflächen.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Bei der Umsetzung sind die Inhalte der <a href="https://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html"><span class="underline">Verordnung zur Schaffung barrierefreier Informationstechnik nach dem Behindertengleichstellungsgesetz (Barrierefreie-Informationstechnik-Verordnung - BITV 2.0)</span></a> zu berücksichtigen.</td>
</tr>
</tbody>
</table>

### Infrastrukturprinzipien

-   Stabile Infrastruktur

-   Langfristiger Herstellersupport

-   Standards statt Herstellerbindung

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Stabile Infrastruktur</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die erforderliche Infrastrukturtechnik ist entsprechend den aktuellen Standards ausfallsicher aufzubauen.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die zugrundeliegende Infrastrukturtechnik muss einen sicheren und verlässlichen Betrieb ermöglichen. Die angebotenen Dienste müssen den Bedarfsträgern dauerhaft im Zugriff stehen. Ausfälle aufgrund von Serviceunterbrechungen, etwa bedingt durch technische Fehler, sind mit technischen Mitteln auf minimale Auswirkungen hin zu optimieren.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Die Infrastrukturservices sind mehrfach instanziiert auf redundanter Hardware aufzubauen und die Servicezugänge sind über multiple Netzwerkpfade bereit zu stellen.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Langfristiger Herstellersupport</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Es ist bevorzugt technische Hardware einzusetzen, deren Hersteller langfristigen Support anbieten.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Rechenzentren sind üblicherweise auf einen langfristigen Betrieb hin konzipiert. Aufgrund des nach wie vor exponentiell wachsenden Bedarfs an Rechenleistung und Speicherkapazität sind im Laufe der Nutzungszeit in der Regel Nachbeschaffungen erforderlich. Zusätzlich begründen etwaige technische Ausfälle Ersatzbeschaffungen. Die Erfahrungen der letzten Jahre haben gezeigt, dass zuverlässige Hersteller mit hinreichender Produktqualität und Kapazität diese Bedürfnisse über zugesicherte Produktlebenszyklen bedienen wollen und dies auch mehrheitlich leisten.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Technische Hardware ist von Markenherstellern zu beziehen, die eine Nachkaufgarantie über ein festzulegendes Zeitintervall zusichern.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Standards statt Herstellerbindung</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die IT-Infrastruktur ist aus Produkten zu assemblieren, die technische Standards umsetzen. Alleinstellungsmerkmale und daraus resultierende Herstellerbindungen sind zu vermeiden.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Die IT-Infrastruktur stellt den technischen Rahmen bereit, oberhalb dessen der SouvAP die verwalterische Geschäftsprozessunterstützung leisten soll. Dieser muss auf herstellerübergreifenden technischen Standards aufsetzen, damit auch in diesem Punkt eine Wechselfähigkeit gegeben ist.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Die Spezifikationen zur IT-Infrastruktur dürfen nur offene Standards enthalten.</td>
</tr>
</tbody>
</table>

### Sicherheitsprinzipien

-   Konformität zu Grundschutz, Datenschutz und Geheimschutz

-   Risikoorientiert

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Konformität zu Grundschutz, Datenschutz und Geheimschutz</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Bei der Planung, der Bereitstellung, dem Betrieb und der Aussonderung von Komponenten des SouvAP sind die Maßnahmen gemäß BSI-Grundschutz und C5 anzuwenden.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td><p>Die Arbeitsgruppe IT-Sicherheit des IT-Planungsrats in der <a href="https://www.it-planungsrat.de/fileadmin/beschluesse/2019/Beschluss2019-04_TOP12_Anlage_Leitlinie.pdf"><span class="underline">Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung</span></a> eine verpflichtende Umsetzung von BSI-Grundschutz zur Absicherung von IT-Netzinfrastrukturen der öffentlichen Verwaltung und zur Vorbeugung von Notfällen und Krisen als Mindeststandard festgeschrieben. Für ebenen-übergreifende Verwaltungsprozesse ist BSI-Grundschutz als einheitlicher Sicherheitsstandard zu etablieren.</p>
<p>Des Weiteren ist der SouvAP auf die Verarbeitung sensibler personenbezogener Daten sowie digitaler Verschlusssachen auszurichten. Dazu sind die gesetzlich verankerten technischen und organisatorischen Vorgaben zu Datenschutz und Geheimschutz umzusetzen.</p></td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Der gesamte Infrastrukturverbund zum SouvAP muss BSI-Grundschutz umsetzen.</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>Titel</td>
<td>Risikoorientiert</td>
</tr>
<tr class="even">
<td>Kurzbeschreibung</td>
<td>Die Sicherheitsarchitektur zum SouvAP muss Methoden der Risikoanalyse und der Risikokompensation anwenden.</td>
</tr>
<tr class="odd">
<td>Begründung</td>
<td>Der SouvAP wird in erster Iteration zentral als Bündel von Cloud Services aufbereitet. Ergänzend zur Umsetzung von BSI-Grundschutz fordert die Leistungsbeschreibung die Einhaltung des Cloud Computing Compliance Criteria Catalogs C5. Beide Dokumente verweisen darauf, dass die Verwundbarkeit der Schutzziele Verfügbarkeit, Vertraulichkeit und Integrität mit Risikoanalysen zu bewerten ist und Maßnahmen zur Risikokompensation umzusetzen sind.</td>
</tr>
<tr class="even">
<td>Implikationen</td>
<td>Die erzeugten Softwareschichten sind bezüglich etwaiger Verwundbarkeiten zu bewerten und es sind Härtungsmaßnahmen umzusetzen, die die Risiken auf ein tragbares Niveau beschränken.</td>
</tr>
</tbody>
</table>

Architekturvision
-----------------

Die Architekturvision skizziert ein Idealbild zu einem Ergebnis der hier
beworbenen Architekturarbeit. Sie berücksichtigt alle Anforderungen und
Geschäftsziele aller Anspruchsgruppen und skizziert darauf basierend ein
Zielbild aus eher konservativer Sicht.

Das hier erwartete Zielbild reflektiert ein gewünschtes und nach
Einschätzung der Architekten auch realisierbares Arbeitsergebnis. Es ist
als Aufzählung von Kriterien aufbereitet, die verbal als Liste von
Antworten auf die Anforderungen und Ziele der Anspruchsgruppen
aufbereitet ist:

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><p>Der SouvAP realisiert ein zukunftsorientiertes Ökosystem aus Open Source Softwareprodukten für die öffentlichen Verwaltung.</p></li>
<li><p>Der SouvAP ist ein Bündel aus digital souveränen Services für die Bewältigung aktueller und zukünftiger digitaler Verwaltungsprozesse.</p></li>
<li><p>Der SouvAP enthält Werkzeuge für fachübergreifende barriere- und medienbruchfreie verwalterische Produktion und Kollaboration.</p></li>
<li><p>Der SouvAP setzt alle Anforderungen an eine sichere und datenschutzkonforme Datenverarbeitung in der öffentlichen Verwaltung um.</p></li>
<li><p>Der SouvAP ist mit der Deutschen Verwaltungscloud und dem Bundesclient kompatibel</p></li>
</ul></td>
</tr>
</tbody>
</table>

Mögliche Risiken, Maßnahmen zur Schadensbegrenzung
--------------------------------------------------

Das Ergebnis der Architekturarbeit wird in einen Austausch von bisher
genutzten Werkzeugen zur Bewältigung von verwalterischen
Geschäftsprozessen münden. In Kapitel <span class="underline">4.5</span>
<span class="underline">Prognose zu zukünftigen Fähigkeiten</span> wurde
bereits herausgestellt, dass zumindest in einer Übergangsphase bei zwei
Geschäftszielen negative Auswirkungen zu erwarten sind:

**Maximieren der Wirtschaftlichkeit der Geschäftstätigkeiten der ÖV**

-   Nach aktueller Einschätzung ist in einer Übergangsphase ein
    Parallelbetrieb mit aktuellen und neuen Werkzeugen erforderlich.
    Dies wird mindestens operativen Mehraufwand begründen.

-   In der Übergangsphase werden Lizenzgebühren für die traditionellen
    Werkzeuge fällig. Diese Investition ist als notwendig und sinnvoll
    einzustufen, da damit eine Option für einen Rücksprung offensteht.
    Diese Option ist zu nutzen, wenn ein zeitkritischer Geschäftsprozess
    aufgrund von unzureichender Routine mit den neuen Werkzeugen nicht
    termingerecht umsetzbar ist.

-   Zusätzlich wird erwartet, dass im Parallelbetrieb gegenüber
    Bestandssoftware Lücken im Bereich Formatkonvertierung offenkundig
    werden. Daraus entstehende Medienbrüche sind mittels neu zu
    erstellender Formatwandler zu eliminieren.

**Maximieren von Kundenorientierung/Kundenzufriedenheit**

-   Ein Werkzeugwechsel erfordert unabdingbar eine Eingewöhnungsphase.
    Diese ist proaktiv mit der Entwicklung von Schulungsprogrammen
    vorzubereiten und dem Personal in der Übergangsphase anzubieten.

-   Der Werkzeugwechsel hat gegebenenfalls auch eine Änderung in der
    Unternehmenskultur zur Folge. Daraufhin mögliche negative Effekte
    sind systematisch über Aufklärungen zur Notwendigkeit der
    Transformation und positive Motivation zu „anders arbeiten“
    abzufedern.

Zusätzlich sind Risiken aufgrund externer Effekte zu identifizieren und
zu bewerten. Dazu zählen:

**Änderungen an der Lizenz zu einer Open Source Komponente**

-   In der Vergangenheit haben Hersteller die Lizenzbedingungen von
    Komponenten nach der Markteinführung verändert. Hierbei wird oft von
    Open Source auf Nicht-Open Source Lizenzen übergegangen, etwa um
    Verbreitungsrechte oder Nutzungsmuster zu beschneiden, oder auf neue
    Unternehmensstrategien, etwa nach Übernahme, zu reagieren. Da eine
    Open Source Lizenzierung dauerhaft ist, können solche Änderungen nur
    Folgeversionen einer Software betreffen und nicht rückwirkend
    angewendet werden.

-   Bei Lizenzänderung bleibt die Lizenzbedingung für das Produkt vor
    Lizenzänderung gültig. Die Pflege bzw. Weiterentwicklung durch eine
    Community ist weiterhin erlaubt.

**Abkündigungen von Softwarewartung / Weiterentwicklung**

-   Dieser Sachverhalt ist im Bereich kommerzieller Software ein
    planbares Ereignis. Professionelle kommerzielle Hersteller verkünden
    zu ihren Produkten öffentlich Lebenszyklen, die zu einem dort
    ausgewiesenen Termin enden.

-   Eine ähnliche Planbarkeit ist auch im Bereich Open Source Software
    typischerweise nur in Kombination mit Wartungsverträgen gegeben.

-   Die Verfügbarkeit einer Software innerhalb des Lebenszyklus der
    übergeordneten Distribution ist keine Garantie für dessen
    Weiterentwicklung, Pflege oder Verfügbarkeit in einer späteren, als
    Nachfolger deklarierten Distribution.

Aufgrund dieser Risiken wird eventuell die Entscheidung gefällt, ein im
Einsatz befindliches Produkt gegen ein anderes mit gleichem oder
vergleichbarem Funktionsvorrat einzusetzen. Daraus entstehen
Wechselrisiken:

**Austausch eines Werkzeugs**

-   Nach dem Austausch ist eine Eingewöhnungsphase unumgänglich.
    Abweichende Bedienkonzepte reduzieren die Arbeitsleistung und
    beeinträchtigen unter Umständen die Nutzerzufriedenheit.
    Alternativwerkzeuge sollten daher optisch und funktionell möglichst
    ähnlich sein.

-   Das neue Werkzeug verwendet ein anderes Speicherformat. Es sind
    Formatkonvertierungen erforderlich und die sind nicht fehlerfrei.

Dieser Abschnitt hat ausgewählte Beispiele für Risiken illustriert.
TOGAF empfiehlt, die Architekturarbeit mit einem Risikomanagement zu
begleiten und im Laufe der Arbeit erkannte Risiken und darauf
zugeschnittene Schutzmaßnahmen im Zuge der Governance (TOGAF Phase G) zu
bewerten.

------------------------------------------------------------------------

1.  Siehe
    [Beschluss2021-09\_Strategie\_zur\_Staerkung\_der\_digitalen\_Souveraenitaet.pdf
    (it-planungsrat.de)](https://www.it-planungsrat.de/fileadmin/beschluesse/2021/Beschluss2021-09_Strategie_zur_Staerkung_der_digitalen_Souveraenitaet.pdf)<a href="#fnref1" class="footnote-back">↩︎</a>
