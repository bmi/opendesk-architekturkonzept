**Impressum**

Bundesministerium des Innern und für Heimat

PG ZenDiS „Projektgruppe für Aufbau ZenDiS“

Postanschrift: Alt-Moabit 140, 10557 Berlin

Hausanschrift: Salzufer 1 (Zugang Englische Straße), 10587 Berlin

PGZenDiS@bmi.bund.de

www.bmi.bund.de

**Stand:**  
<html>
<body>
<p><span id="datetime"></span></p>
<script>
var dt = new Date();
document.getElementById("datetime").innerHTML=dt.toLocaleString("de-DE", {month: 'long', year: 'numeric'});
</script>
</body>
</html>


## Lizenz

!!! info "CC-BY-4.0 Lizenzierung"
    [![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://gitlab.opencode.de/bmi/opendesk-architekturkonzept/-/blob/main/LICENSE?ref_type=heads)
    Der Dokumentationsinhalt des Architekturkonzepts ist CC-BY-4.0 lizenziert.
