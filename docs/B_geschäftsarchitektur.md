Phase B: Geschäftsarchitektur
=============================

Überblick
---------

Die Geschäftsarchitektur im Rahmen des Architekturkonzeptes zum SouvAP
hat die vorrangige Aufgabe, den wechselseitigen Einfluss der
angestrebten IT-Lösung auf die dafür wesentlichen geschäftlichen
Prozesse der Beteiligten zu erfassen und zu
berücksichtigen.<a href="#fn1" id="fnref1" class="footnote-ref">1</a>

Die Beschreibung der Geschäftsarchitektur folgt einer an TOGAF
angelehnten Adaption, bestehend aus den Teilen

-   Geschäftsstrategie  
    einschließlich der Geschäftsprinzipien, der organisatorischen
    Rahmenbedingungen, Motivation der Beteiligten, digitale Trends und
    Trends der Verwaltungsarbeit, dem zu erwartenden Betriebsmodells
    sowie der Lösungsidee

-   Geschäfts-Design  
    einschließlich der Beschreibung der zur Verfügung zu stellenden
    Fähigkeiten der zu entwickelnden IT-Lösung, basierend auf den zu
    entwickelnden Business-Capabilities, einer Beschreibung der
    Werteketten der geschäftlichen Beteiligten und ihrer Lieferungen,
    sowie einer Betrachtung der beteiligten Informationsdomänen

-   Geschäfts-Transformation  
    einschließlich der Betrachtung der zu priorisierenden Capabilities
    und Funktionen, dem zu erwartenden Einfluss auf die beteiligten
    Geschäftsorganisationen und Risiken

Geschäftsstrategie
------------------

### Geschäftsprinzipien

In 3.1.3 wurden bereits einige Geschäftsprinzipien benannt.

Die für die Entwicklung des SouvAP geeigneten Organisationen, Funktionen
und Prozesse sollen sich nachfolgenden übergeordneten Architektur- und
Geschäftsprinzipien richten:

Tabelle : Geschäftsprinzipien

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Übergeordnetes Architekturprinzip</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Sicherheit First<br />
(u.a. Security by Design)</td>
<td><p>Alle für die Nutzung des SouvAP notwendigen Sicherheitsstandards, Datenschutz und Geheimschutz sind Grundlage für eine vertrauensvolle Nutzung der Lösung durch die Verwaltungen.</p>
<p>Speziell sind solcherart Anforderungen bereits im Design der Lösungen zu berücksichtigen, da solche Standards i.d.R. nur mit sehr hohem Aufwand nachimplementiert werden können oder ohnehin von Anfang an verbindlich sind.</p></td>
</tr>
<tr class="even">
<td>Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit basierend auf Anwenderfeedback</td>
<td><p>Im Mittelpunkt des SouvAP steht der konkrete Nutzen für die Verwaltungsmitarbeiter und damit für die Verwaltungen.</p>
<p>Direktes Feedback durch den Kunden erhöht die Akzeptanz und Anwenderzufriedenheit.</p></td>
</tr>
<tr class="odd">
<td>Ökologisches Denken und Nachhaltigkeit</td>
<td><p>Der SouvAP bewegt sich in einem Ökosystem, das weit über die Verwaltungsarbeit hinausreicht. Er hat sich an den gesellschaftlichen Zielen zu mehr Nachhaltigkeit zu orientieren.</p>
<p>Teil dieser Nachhaltigkeit ist die Ausrichtung auf ein souveränes Handeln durch Selbstbestimmung, welche Softwareentwicklungen für die Verwaltungsarbeit angemessen sind.</p></td>
</tr>
<tr class="even">
<td><strong>Geschäftsprinzip</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Partnerschaftliche und faire Geschäftsbeziehungen</td>
<td>Obwohl, oder grade weil mehrere Partner mit zum Teil divergierenden geschäftlichen Interessen das Ökosystem des SouvAP bilden, sind faire Geschäftsbeziehungen und ein größtmöglicher Nutzen für alle Geschäftspartner gleichermaßen zu berücksichtigen, um die Ziele des SouvAP zu erreichen.</td>
</tr>
<tr class="even">
<td>Wirtschaftlichkeit</td>
<td>Die Entwicklung und Bereitstellung des SouvAP muss die finanziellen Möglichkeiten der Verwaltung berücksichtigen. Das bedeutet, dass es keine Umsetzung jeder Idee zu jedem Preis geben kann.</td>
</tr>
<tr class="odd">
<td>Unterbrechungsfreier Geschäftsbetrieb wenn nötig</td>
<td>Der SouvAP setzt als eine Internetlösung eine hohe Zuverlässigkeit voraus, ohne die eine Akzeptanz in der Verwaltung nicht erreicht werden kann.</td>
</tr>
<tr class="even">
<td>Orientierung an internationalen Verwaltungsstandards</td>
<td>Grade die Arbeit der Verwaltung ist geprägt von der Notwendigkeit, sich an Gesetze, Normen und Standards penibel zu halten. Das gilt selbstverständlich auch für die eingesetzten Werkzeuge.</td>
</tr>
<tr class="odd">
<td>Lieferung von Services anstatt Anwendungen an die Verwaltungskunden</td>
<td><p>Eine der Grundideen des SouvAP besteht darin, keine installierbaren Anwendungen in den Mittelpunkt zu stellen, sondern Funktionen, die über Services zur Verfügung gestellt werden.</p>
<p>Dies schließt einfache und transparente Lizenz- und Abrechnungsmodelle mit ein.</p></td>
</tr>
<tr class="even">
<td>Prosumer-Lösung</td>
<td><p>Der Begriff Prosumer bezeichnet die Fähigkeit, den Kunden in den Herstellungsprozess direkt mit einzubeziehen und ihn bereits im Produktionsprozess zum Teil der Wertschöpfungskette zu machen.</p>
<p>Im Kontext des SouvAP bedeutet das, angepasste und flexible Lösungen für spezifische Kundengruppen durch Modularität zu erreichen.</p>
<p>Nicht jede Verwaltung ist gleich. Eine der Fähigkeiten des SouvAP muss es sein, spezifische Kundengruppen durch maßgeschneiderte Konfektionen zu erreichen.</p></td>
</tr>
<tr class="odd">
<td>Anforderungsbasierte und demokratische Roadmap</td>
<td>Die Weiterentwicklung des SouvAP entlang der Wertschöpfungskette soll anforderungsbasiert und nach Möglichkeit transparent und gleichberechtigt erfolgen.</td>
</tr>
<tr class="even">
<td>Fail Fast Entwicklungs- und Lieferprozess</td>
<td><p>Um die Akzeptanz und Kundenbindung praktisch zu unterstützen ist ein Entwicklungs- und Lieferprozess über die gesamte Wertschöpfungskette zu definieren, der das Kundenfeedback direkt mit einbezieht.</p>
<p>Fehler lassen sich kaum vermeiden, aber ein zeitnah behobener Fehler wird von den Nutzern nachweislich eher toleriert.</p></td>
</tr>
</tbody>
</table>

### Treiber für die Entwicklung des SouvAP

Die Entwicklung einer Lösung für einen SouvAP wird getrieben durch eine
Reihe von gesellschaftlichen Trends und Weichenstellungen der Politik.

Dazu gehören u.a.:

-   Digitale Trends in der IT

-   Trends in der Entwicklung der Arbeitswelt

-   Gesellschaftspolitische Trends

#### Zu berücksichtigende digitale Trends in der IT

In den letzten Jahren haben in der IT-Industrie einige Entwicklungen
stattgefunden, die selbstverständlich auch die IT in der
Verwaltungsarbeit direkt beeinflussen:

Tabelle : Digitale Trends in der IT

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Digitaler Trend</strong></th>
<th><strong>Ausprägung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Mobiles als neues Web</td>
<td><ul>
<li><blockquote>
<p>Große Smartphone- und Tabletverfügbarkeit</p>
</blockquote></li>
<li><blockquote>
<p>Benutzung eigener Geräte neben dienstlichen, zunehmende Verschmelzung (BYOD)</p>
</blockquote></li>
<li><blockquote>
<p>Nutzererfahrung zunehmend von Mobiles (Tablets, Smartphones) bestimmt</p>
</blockquote></li>
<li><blockquote>
<p>Erwartete zeitliche und örtliche Entkopplung von digitalen Dienstleistungen</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Domain Driven Design und Microservices Architekturen</td>
<td><ul>
<li><blockquote>
<p>Modularisierung der Anwendungen nach Geschäftsabhängigkeiten</p>
</blockquote></li>
<li><blockquote>
<p>Nutzung von wiederverwendbaren und voneinander entkoppelten Services</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Vielfalt der Informationskanäle und Vernetzung untereinander</td>
<td><ul>
<li><blockquote>
<p>Kanalübergreifende Prozesse erwartet</p>
</blockquote></li>
<li><blockquote>
<p>Integration wird zunehmend bedeutsam</p>
</blockquote></li>
<li><blockquote>
<p>Erwartung kürzerer Antwortzeiten</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Personalisierung</td>
<td><ul>
<li><blockquote>
<p>Personalisierte Kommunikation und Nutzererfahrung</p>
</blockquote></li>
<li><blockquote>
<p>Zunehmendes Wissen über die einzelnen Kunden</p>
</blockquote></li>
<li><blockquote>
<p>Personalisierte Datenhaltung</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Nutzung von Plattformen</td>
<td><ul>
<li><blockquote>
<p>Skalierungseffekte und effektivere Kosten</p>
</blockquote></li>
<li><blockquote>
<p>Skalierung der zur Verfügung zu stellenden Ressourcen</p>
</blockquote></li>
<li><blockquote>
<p>Hardware und Software Plattformen (Cloud; Anwendungsplattformen)</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Offene Ökosysteme</td>
<td><ul>
<li><blockquote>
<p>Einbindung von Partnern</p>
</blockquote></li>
<li><blockquote>
<p>Erschließung neuer Dienstleistungsangebote</p>
</blockquote></li>
<li><blockquote>
<p>Open Source</p>
</blockquote></li>
<li><blockquote>
<p>OpenData</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Künstliche Intelligenz</td>
<td><ul>
<li><blockquote>
<p>Maschinelles Lernen und Entscheidungsfindung</p>
</blockquote></li>
<li><blockquote>
<p>Daten als Grundlage für Entscheidungen</p>
</blockquote></li>
<li><blockquote>
<p>Robotik und Prozessautomatisierung</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Größere Risiken durch Hacking und Datenmissbrauch</td>
<td><ul>
<li><blockquote>
<p>Angriffe auf Datendienstleister</p>
</blockquote></li>
<li><blockquote>
<p>Darknet und Internetkriminalität</p>
</blockquote></li>
<li><blockquote>
<p>Lahmlegung ganzer Infrastrukturen</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Agile Methoden und DevOps</td>
<td><ul>
<li><blockquote>
<p>Outcome statt Output</p>
</blockquote></li>
<li><blockquote>
<p>Konkrete Nutzenorientierung</p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

#### Zu berücksichtigende Trends in der Verwaltungsarbeit

Der SouvAP beschreibt einen Teil eines Digitalen Arbeitsplatzes, der
wiederum Teil eines übergreifenden Arbeitsplatzkonzeptes ist, wie
Verwaltungsmitarbeiter zukünftig arbeiten werden.

![](media/file3.png)

Der Erfolg jeglicher Veränderung der Arbeitsweise der Mitarbeiter hängt
von allen diesen Faktoren ab, die sich immer wechselseitig bedingen. Es
ist also nicht einfach eine rein technische Fragestellung.

Auf die Frage, wie ein moderner digitaler Arbeitsplatz aussehen muss,
noch dazu in der Verwaltung, gibt es keine eindeutige Antwort, aber
Hinweise.

Die Beratungsgesellschaft Gartner beschreibt in ihrem Hype Cycle mehrere
solcher für den SouvAP relevante
Trends<a href="#fn2" id="fnref2" class="footnote-ref">2</a>:

Tabelle : Digitale Trends im Arbeitsplatz

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Trend</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Der neue „Arbeitskern“</td>
<td>Der „Neue Arbeitskern“ bezieht sich auf eine Sammlung von SaaS-basierten persönlichen Produktivitäts-, Kollaborations- und Kommunikationstools, kombiniert in einem Cloud-Office-Produkt. Es umfasst im Allgemeinen E-Mail, Instant Messaging, Dateifreigabe, Konferenzen, Dokumentenverwaltung und -bearbeitung, Suche und Entdeckung sowie Zusammenarbeit.</td>
</tr>
<tr class="even">
<td>BYOT</td>
<td>Einzelpersonen beginnen, persönliche IoT-Geräte (Internet of Things) oder Wearables bei der Arbeit zu verwenden, in einem Trend, der als Bring Your Own Thing (BYOT) bekannt ist. BYOT umfasst eine breite Palette von Objekten, wie z. B. Fitnessbänder, intelligente Lampen, Luftfilter, Sprachassistenten, intelligente Ohrhörer und Virtual-Reality-Headsets (VR) für Verbraucher. In Zukunft werden es hochentwickelte Geräte wie Roboter und Drohnen sein.</td>
</tr>
<tr class="odd">
<td>Die Fernökonomie</td>
<td>Persönliche Veranstaltungen und Meetings waren einst die Norm und virtuelle Meetings die Ausnahme, aber COVID-19 hat diese Szenarien umgedreht. Die Pandemie beeinflusste die Entstehung der Fernwirtschaft oder Geschäftsaktivitäten, die nicht auf persönlichen Aktivitäten beruhen. Organisationen mit Betriebsmodellen, die von Erstanbieter- oder gehosteten Veranstaltungen abhängen, sind schnell auf virtuelle Alternativen umgestiegen.</td>
</tr>
<tr class="even">
<td>Smarter Arbeitsplatz</td>
<td>Ein Smarter Arbeitsplatz nutzt die zunehmende Digitalisierung physischer Objekte, um neue Arbeitsweisen zu ermöglichen und die Effizienz der Mitarbeiter zu verbessern. Beispiele für Smart-Workspace-Technologien sind IoT, Digital Signage, integrierte Workplace-Management-Systeme, virtuelle Workspaces, Bewegungssensoren und Gesichtserkennung. Jeder Ort, an dem Menschen arbeiten, kann ein intelligenter Arbeitsplatz sein, z. B. Bürogebäude, Schreibtische, Konferenzräume und sogar Wohnräume.</td>
</tr>
<tr class="odd">
<td>Desktop-as-a-service</td>
<td>Desktop as a Service (DaaS) bietet Benutzern ein virtualisiertes Desktop-Erlebnis auf Abruf, das von einem remote gehosteten Standort bereitgestellt wird. Es umfasst die Bereitstellung, das Patchen und die Wartung der Verwaltungsebene und der Ressourcen zum Hosten von Workloads.</td>
</tr>
<tr class="even">
<td>Demokratisierte Technologiedienste</td>
<td><p>Technologiedienstleistungen der Zukunft werden von den Menschen zusammengestellt und zusammengestellt, die sie tatsächlich nutzen. Einige Beispiele für demokratisierte Technologiedienste sind:</p>
<p>Bürgerentwickler oder Mitarbeiter, die neue Geschäftsanwendungen mit von der Unternehmens-IT genehmigten Entwicklungstools und Laufzeitumgebungen erstellen. Bürgerentwickler werden durch die Verfügbarkeit und Leistungsfähigkeit von Low-Code- und „No-Code“-Entwicklungstools gestärkt.</p>
<p>Citizen Integrator-Tools, die es erfahrenen Geschäftsanwendern mit minimalen IT-Kenntnissen ermöglichen, relativ einfache Anwendungs-, Daten- und Prozessintegrationsaufgaben selbst durch sehr intuitive No-Code-Entwicklungsumgebungen zu bewältigen.</p>
<p>Citizen Data Science, eine neu entstehende Reihe von Funktionen, die es Benutzern ermöglichen, erweiterte analytische Erkenntnisse aus Daten zu extrahieren, ohne dass umfassende Data-Science-Kenntnisse erforderlich sind.</p></td>
</tr>
</tbody>
</table>

Zusätzlich wird nach der Pandemie ein Trend zum "Erhalt" von Home Office
/ "Distributed Office" Arbeitskulturen wahrgenommen, sowie damit
einhergehend eine selbstorganisierte und dadurch asynchronere
Arbeitsweise.

#### Gesellschaftspolitische Ableitungen

Einige dieser allgemeinen Trends aus der Digitalisierung und Veränderung
der Arbeit selbst sind direkt oder indirekt in die Digitalstrategien des
Bundes und der Länder eingeflossen.

z.B. werden in der Digitalstrategie des
Bundes<a href="#fn3" id="fnref3" class="footnote-ref">3</a> 3
Handlungsfelder benannt:

-   Vernetzte und digital souveräne Gesellschaft

-   Innovative Wirtschaft, Arbeitswelt, Wissenschaft und Forschung

-   Lernender, digitaler Staat

In der Digitalstrategie des
Bundes<a href="#fn4" id="fnref4" class="footnote-ref">4</a> werden
einige Themen, den SouvAP betreffend herausgestellt:

-   Wir gestalten neue Modelle des Arbeitens

Um die digitale Wirtschaft zu stärken, unterstützen wir flexible
Arbeitsmodelle, modernen Datenschutz und den Einsatz Künstlicher
Intelligenz.

-   Wir wollen Technologien „made in Germany“

Wer wichtige Technologien selbst entwickelt, bleibt unabhängig. Darum
wollen wir Künstliche Intelligenz, Chips und Quantenrechner aus eigener
Hand.

-   Wir wollen die Verwaltung vereinfachen

Alle Behörden und ihre Dienstleistungen sollen auf ein Smartphone
passen. Dafür schaffen wir die Voraussetzungen – etwa mit der digitalen
ID.

-   Wir sichern Behörden im digitalen Raum ab

Damit Daten der Verwaltungen geschützt sind, fördern wir verschlüsselte
Kommunikation. Zudem werden wir unabhängiger von einzelnen Herstellern.

-   Wir machen Daten sicher für Behörden und Unternehmen

Das Bedürfnis nach Cybersicherheit wächst. Netze und Software müssen
widerstandsfähig sein. Deshalb stärken wir unsere Institutionen.

> Beispielhaft für die Digitalstrategien der Länder sei auf die DVH
> (Digitale Verwaltungsstrategie
> Hessen)<a href="#fn5" id="fnref5" class="footnote-ref">5</a>
> verwiesen. U.a. werden dort verschiedene Ziele benannt:

-   In einem zentralen Verwaltungsportal wollen wir alle behördlichen
    > Angebote der Kommunen, Landes- und Bundesbehörden bündeln und sie
    > allen Bürgerinnen und Bürgern, Unternehmen und Institutionen
    > jederzeit service- und nutzenorientiert online zugänglich machen.

-   Wir wollen Verwaltungsleistungen digitalisieren und online auf allen
    > Ebenen anbieten. Dazu treiben wir die notwendige enge
    > Zusammenarbeit zwischen Bund, Ländern und Kommunen voran. Wir
    > teilen unsere guten Lösungen mit allen, implementieren erprobte
    > Verfahren anderer und stehen insbesondere unseren Kommunen zur
    > Seite.

-   Wir wollen die Möglichkeiten moderner IT-Infrastruktur, innovativer
    > Technologien und optimierter Prozesse ausschöpfen, um die
    > Landesverwaltung stetig zu modernisieren und die vielfältigen
    > Aufgaben effizient und bürgernah zu erfüllen.

#### Schlussfolgerungen und Akzeptanzkriterien für den SouvAP

Der SouvAP soll nicht einfach eine weitere Adaption einer Office-Lösung
mit Schwerpunkt auf das Thema „Souveränität“ sein. Der Erfolg eines
SouvAP wird sich erst einstellen, wenn er eine ganze Reihe
Akzeptanzkriterien berücksichtigt:

-   Im Mittelpunkt steht die Akzeptanz durch die Nutzenden selbst

    -   Die Verwaltungsmitarbeiter haben i.d.R. bereits eine Lösung für
        den digitalen Arbeitsplatz im Einsatz. Der SouvAP darf in seiner
        Funktionalität und Nutzungserlebnis nicht schlechter sein als
        dessen bisherige Lösung. Das heißt insbesondere, dass die
        Vorteile einer neuen, auf die konkrete und unmittelbare
        Verwaltungsarbeit zugeschnittene Lösung etwaige Nachteile durch
        z.B. fehlende Features wettgemacht werden sollten.  
        Wobei die Bewertung sowohl objektiv gemessen werden kann, z.B.
        in der Anzahl der notwendigen Aktionen wie Mausklicks, als auch
        sehr subjektive und nur über Umfragen zu ermittelnde Aspekte
        umfasst.

    -   Die Lösung muss die neuen Anforderungen aus der Digitalisierung
        und der Veränderung der Arbeitswelt der Verwaltung direkt
        berücksichtigen.

    -   Die weitere Konzeption und Entwicklung erfolgt entsprechend der
        Nutzungserfahrung und im Hinblick auf eine moderne Verwaltung

-   Der SouvAP muss die gesellschaftlichen Rahmenbedingungen und
    Zielstellungen erfüllen

    -   Das Thema Souveränität steht im Mittelpunkt und ist ein
        Haupttreiber für die Lösung

    -   Dies schließt nicht nur die bereitgestellte Software selbst mit
        ein, sondern den gesamten Wertschöpfungsprozess einschließlich
        einer Bereitstellung in einer souveränen Cloud-Umgebung (siehe
        DVS) und die Entwicklung der Lösung.

-   Die Lösung muss in seiner Wertschöpfungskette für alle beteiligten
    Partner einen Gewinn darstellen

    -   Der SouvAP wird ähnlich zu dem im Rahmen des OZG vereinbarten
        EfA Prinzip als eine Gemeinschaftslösung betrachtet, die eine
        spezifische, entsprechend den unterschiedlichen Verwaltungen
        angemessene Ausprägung erfährt

    -   Die Vergütung der Leistungen innerhalb der Wertschöpfungskette
        erfolgt nach wirtschaftlichen und fairen Modellen

    -   Die Lösung und deren Entstehung darf den bestehenden
        Verwaltungsprozessen nicht zuwiderlaufen.

siehe hierzu die entsprechenden komplementären Geschäftsprinzipien 49:

-   Ökologisches Denken und Nachhaltigkeit

-   Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit
    basierend auf Anwenderfeedback

-   Sicherheit First (u.a. Security by Design)

-   Partnerschaftliche und faire Geschäftsbeziehungen

### Organisationssicht

#### Beteiligte Organisationen, Akteure

Im Rahmen der Zurverfügungstellung des SouvAP sind folgende
Organisationen unmittelbar beteiligt und bilden eine virtuelle
Organisation eigentlich formal und rechtlich unabhängiger Akteure:

![](media/file4.png)

Abbildung : Beteiligte Organisationen, Akteure

-   BMI/ZENDIS ZenDiS  
    Auftraggeber und Koordinator des zu entwickelnden SouvAP

-   Deutsche Verwaltung

In ihren verschiedenen Ausprägungen als Verwaltung des Bundes, der
Länder und Kommunen sowie Verwaltungskooperationen ist die ÖV der
zentrale Nutzer des SouvAP

-   Kunden der Verwaltung

Die Kunden der Verwaltung sind in erster Linie die Bürger,
verwaltungsnahe Organisationen, Unternehmen und Communities.

Sie sind von der Nutzung des SouvAP nicht ausgeschlossen, falls es
entsprechende Angebote der IT-Dienstleister dazu geben sollte. Der
Source Code steht allen Bürgern und Unternehmen gleichermaßen (über das
Repository Open CoDE) zur Verfügung.

-   IT-Dienstleister des Bundes, der Länder und Kommunen

Sie handeln im gesetzlichen Auftrag der ÖV und stellen ihr zentrale
IT-Dienstleistungen zur Verfügung, so auch den SouvAP.

-   Software-Anbieter der Privatwirtschaft und die Community,
    Softwareanbieter

Viele der zu entwickelnden SouvAP Module entstammen der Entwicklung
privater IT-Firmen und der freien IT-Community

#### Beteiligte Rollen im Rahmen des SouvAP

Neben den Beteiligten am SouvAP im Rahmen der Entwicklung,
Zurverfügungstellung, Betrieb und Nutzung lassen sich die Rollen etwas
differenzieren (siehe auch die Definitionen der Rollen in der DVS):

![](media/file5.png)

Abbildung 6: Beteiligte Rollen im Rahmen des SouvAP

Zu beachten ist, dass die in 4.2.1 genannten Anspruchsgruppen im realen
Wertschöpfungsprozess dabei verschiedene und auch parallele Rollen
einnehmen.  
So sind die Bundes- und Landesbehörden sowohl Auftraggeber der
Kundenseite als auch selbst Nutzer der Software, die IT-Dienstleister
des Bundes, der Länder und Kommunen können auch selbst
Softwarelieferanten sein.

Ebenfalls zu beachten ist, dass die Nutzungsbedingungen der Software in
der Öffentlichen Verwaltung vor allem über Subskriptionen organisiert
werden und nicht über reine Softwarelizenzen.

Erläuterung Konfektionierung: Der Erstellungsprozess des SouvAP erfolgt
mehrstufig (siehe Wertschöpfungskette später). Ein
Software-Konfektionierer stellt durch Auswahl und Integration aus der
Gesamtheit der zur Verfügung stehenden Softwaremodule ein spezifisches
Produkt zusammen.

#### Motivation der Beteiligten Akteure

Innerhalb der virtuellen Gesamtorganisation müssen die unmittelbar der
Verwaltung unterworfenen Beteiligten, also sowohl die
Verwaltungsorganisation selbst als auch ihre IT-Dienstleister von den
externen Partnern, Softwarelieferanten unterschieden werden. Diese
teilen zwar im Rahmen des SouvAP die meisten Interessen, aber es gibt
auch zu berücksichtigende Unterschiede.

Siehe 4.2.2 für eine Auflistung der verschiedenen zu berücksichtigenden
Interessen.

Auch die Interessen innerhalb der Verwaltungsorganisationen sind nicht
identisch. Grade aus den zum Teil sehr spezifischen Anforderungen wird
der Bedarf für speziell konfektionierte Lösungen des SouvAP abgeleitet,
die trotzdem auf demselben Konzept und gemeinsamen Komponenten beruhen
und damit abgestimmt werden müssen.

Siehe hier auch die in 3.1.4 genannten Rahmenbedingungen und
Geschäftsziele der Beteiligten.

### Betriebsmodell

Der Begriff Betriebsmodell wird in der Strategie auf Unternehmensebene
verwendet, um zu beschreiben, wie eine Organisation in Geschäftsbereiche
strukturiert ist, welche Aktivitäten zentralisiert oder dezentralisiert
sind und wie viel Integration zwischen den Geschäftsbereichen
erforderlich ist

Die beteiligten Partner bei der Erstellung, Bereitstellung und Betrieb
des SouvAP haben zwar im Hinblick auf den Erfolg des SouvAP ähnliche
Zielstellungen, aber unterschiedliche organisatorische Voraussetzungen.
So unterliegen alle Entscheidungsträger im öffentlichen Bereich einigen
Beschränkungen, an denen nicht zu rütteln ist.

-   Die Ressorthoheit

-   Die Autonomierechte von Gebietskörperschaften

-   Regeln des Beschaffungsrechts

-   uva.

Privatrechtliche Partner haben hier grundsätzlich eine größere
Flexibilität, sowohl was die Geschäftsmodelle anbelangt, als auch die
Umsetzung in ihren jeweiligen Bereichen und Ebenen.

Wesentlich im Rahmen der Entwicklung des SouvAP ist die Beziehung
zwischen dem Auftraggeber des SouvAP, der gleichzeitig die Steuerung
übernimmt (BMI/ ZenDiS) und den verwaltungsnahen IT-Dienstleistern
(gemeinsames Anforderungs- und Releasemanagement) sowie die Beziehung
dieser IT-Dienstleister untereinander (Bereitstellung und
Referenzimplementierungen).

Die Softwarelieferanten der Open Source Software sind strategisch
wichtig, da deren langfristige Planung der Module mit der Roadmap des
SouvAP abgeglichen werden muss. Für die Steuerungsprozesse der
virtuellen Organisation selbst spielt dieser Aspekt eine eher
untergeordnete Rolle.

![Ein Bild, das Text, Objekt, Erste Hilfe-Kasten, Uhr enthält.
Automatisch generierte Beschreibung](media/file6.png)

Abbildung 7: Integration/ Lokale Verantwortung

Der SouvAP wird der Idee nach als dezentrale Lösung im Sinne von
Referenzimplementierungen eines gemeinsamen Standards entwickelt und als
konfektionierte Lösungen den tatsächlichen Verwaltungskunden zur
Verfügung gestellt. Der unmittelbare Integrationsdruck der Beteiligten
ist in dieser Lösungsidee eher niedrig.

Die Verantwortung für die Leistungserbringung an die Verwaltungskunden
selbst liegt beinahe vollständig bei den IT-Dienstleistern des Bundes,
der Länder und Kommunen, allerdings ist das gemeinsame Releasemanagement
der Lösung in zentraler Verantwortung. Die Entwicklung der
Softwaremodule erfolgt lokal bei den Softwarelieferanten, die Lieferung
dieser Softwaremodule wird allerdings zentral gesteuert. Es kann also
davon ausgegangen werden, dass die Zusammenarbeit als **koordinierter
Verbund** zwischen diesen Hauptbeteiligten erfolgt.

Gegen ein kooperatives Netzwerk bzw. ein zentralisierter Verbund spricht
die Tatsache, dass zumindest momentan die IT-Dienstleister des Bundes,
der Länder und Kommunen verwaltungsrechtlich autonom agieren.

Dies bedeutet insbesondere die Verpflichtung der IT-Dienstleister, die
gemeinsamen Standardisierungsvorgaben zu beachten, um einen gemeinsamen
SouvAP zu ermöglichen und nicht in auseinanderdriftende Landeslösungen
zu zerfallen.

### Lösungsidee

Die Lösungsidee des SouvAP lässt sich folgendermaßen beschreiben:

Der SouvAP ist technologisch ein Desktop-as-a-Service (DaaS). Basierend
auf einer in der DVS beschriebenen Cloud-Infrastruktur wird den
Verwaltungen eine für sie maßgeschneiderte Office-Lösung angeboten.

Die Lösung basiert auf

-   einem einheitlichen fachlichen und technischen Konzept, das vom BMI/
    ZenDiS verwaltet wird und einen einheitlichen Souveränen
    Arbeitsplatz mit einheitlicher Nutzererfahrung ermöglicht

-   von Software-Lieferanten entwickelte OpenSource Module

-   die von IT-Dienstleistern zu spezifischen Lösungen konfektioniert
    werden

-   und die von IT-Dienstleistern des Bundes, der Länder und Kommunen
    den Verwaltungen zur Verfügung gestellt werden

### Legale und kommerzielle Geschäftsaspekte

#### Gesetze und Verwaltungsvorschriften

Bei Softwareausschreibungen in der öffentlichen Verwaltung in
Deutschland sind mehrere Gesetze und Vorschriften zu berücksichtigen.
Einige der wichtigsten sind:

-   **Vergaberecht:** Die Vergabe von Softwareprojekten in der
    öffentlichen Verwaltung unterliegt dem deutschen Vergaberecht. Dies
    umfasst das Gesetz gegen Wettbewerbsbeschränkungen (GWB) und die
    Vergabeverordnung (VgV), die Vorschriften für die Vergabe von
    öffentlichen Aufträgen festlegen.

-   **Informations- und Kommunikationstechnik
    (IKT)-Beschaffungsrichtlinie:** Die IKT-Beschaffungsrichtlinie ist
    eine EU-Richtlinie, die in Deutschland umgesetzt wurde und die
    Beschaffung von IKT-Produkten und -Dienstleistungen in der
    öffentlichen Verwaltung regelt. Sie enthält Vorschriften zur Vergabe
    von IKT-Aufträgen und zur Offenheit für Open Source-Software.

-   **Urheberrecht:** Das Urheberrechtsgesetz (UrhG) regelt die Rechte
    an geistigem Eigentum, einschließlich Software. Bei
    Softwareausschreibungen müssen die rechtlichen Aspekte des
    Urheberrechts und der Lizenzbedingungen berücksichtigt werden.

-   **Datenschutzrecht:** Das Datenschutzrecht, insbesondere die
    Datenschutz-Grundverordnung (DSGVO), legt fest, wie personenbezogene
    Daten verarbeitet werden dürfen. Bei Softwareausschreibungen müssen
    die Anforderungen des Datenschutzrechts berücksichtigt werden,
    insbesondere wenn personenbezogene Daten verarbeitet oder
    gespeichert werden.

-   **IT-Sicherheit:** Die öffentliche Verwaltung hat die Pflicht, die
    Sicherheit ihrer IT-Systeme und Daten zu gewährleisten. Bei
    Softwareausschreibungen müssen daher die Anforderungen an die
    IT-Sicherheit berücksichtigt werden, um sicherzustellen, dass die
    ausgewählte Software den erforderlichen Sicherheitsstandards
    entspricht.

Es empfiehlt sich, die Unterstützung von juristischen Experten und
Fachleuten in öffentlicher Beschaffung hinzuzuziehen, um
sicherzustellen, dass alle rechtlichen Anforderungen erfüllt werden.

#### Software-Nutzungsbedingungen

*Während in der Architekturvision auf die inhaltlichen Aspekte der
Nutzungsbedingungen der Produktbestandteile eingegangen wurde und die
Prinzipien der Nutzung von OSI konformen Lizenzen abgeleitet wurden
müssen die Verwaltungsprozesse dazu als solche designt werden. Einige
Durchführungsregeln:*

-   *Im Rahmen des SouvAP werden ausschließlich OSD konforme Open Source
    Lizenzen für die Softwaremodule berücksichtigt, idealerweise mit
    bestehender Freigabe auf OpenCoDE*

-   *Der zentrale Auftraggeber des SouvAP wird entsprechend seiner
    Gestaltungsmacht auf die Nutzung in diesem Sinne erlaubter Lizenzen
    und erforderlicher Subskriptionsmodelle hinwirken*

-   *Jeder Lieferant, sei es der Hersteller der OSS-Module, als auch die
    IT-Bereitstellung des SouvAP hat seine Nutzungsbedingungen
    hinsichtlich ihrer inhaltlichen Ausrichtung und Wirkung auf
    eventuelle Abrechnungen transparent zu machen*

-   *Jeder Kunde einer Lieferung hat im Gegenzug die Nutzung der
    entsprechenden Subskriptionen zu dokumentieren*

#### Abrechnungsmodelle

*Die gesamte Wertschöpfungskette stellt die Erbringung unterschiedlicher
Leistungen dar, die gegenfinanziert und fair abgerechnet werden sollen.*

*Dafür sollten einige generelle Regeln maßgeblich sein:*

-   *Die öffentlichen Verwaltungen haben einen längeren Planungshorizont
    und sind (Stand heute) sehr CAPEX orientiert, d.h. sie folgen i.d.R.
    lieber Einmalbudgetierungen mit vorhersagbaren Kosten. Variable OPEX
    Leistungen sind dagegen eher die Ausnahme.  
    Mit Blick auf die Zurverfügungstellung in einer Cloud mit einem
    Pay-per-use Ansatz wird es hier erst eines Lernprozesses bedürfen.
    Das Standard-Abrechnungsmodell wird auf ‚herkömmlichen‘ Leistungs-
    und Entgeldkatalogen aufgebaut sein müssen.  
    *

-   *Die Abrechnungsmodelle der verschiedenen Leistungserbringer müssen
    ihrerseits aber so transparent sein, dass ein einfaches
    Abrechnungsmodell für die Verwaltungsorganisation des Endkunden
    möglich wird.*

-   *Es wird angestrebt, dass die Abrechnungsmodelle für verschiedene
    SouvAP Instanzen ähnlich sind, um Vergleichbarkeit zu
    gewährleisten.*

Geschäftsdesign
---------------

### Beschreibung der zu entwickelnden Business Capabilities

Um die im Rahmen des SouvAP zu entwickelnden Architekturbausteine zu
bestimmen, werden die grundlegenden Funktionen, Prozesse, Organisationen
und technischen Lösungen in Business Capabilities abstrahiert. Zugrunde
liegt die oben beschriebene virtuelle Organisation des SouvAP.

Diese Business Capabilities sollen einerseits einen Bezugspunkt für eine
fähigkeitsbasierte Planung schaffen, andererseits auch strukturelle
Abhängigkeiten sichtbar machen.

Für die Entwicklung des SouvAP sind im Wesentlichen drei verschiedene
Gruppen von Business Capabilities maßgeblich:

-   Capabilities den SouvAP aus der Nutzungsperspektive betreffend  
    (Fähigkeiten, die sich aus der unmittelbaren Nutzung und damit
    benötigten Funktionalitäten des SouvAP ergeben)

-   Capabilities aus der Anbieter- bzw. Bereitstellungsperspektive  
    (Fähigkeiten, die der SouvAP aus der Perspektive der Bereitstellung
    und damit sowohl der Konfektionierung als auch der
    Zurverfügungstellung, einschließlich benötigter Infrastrukturdienste
    bieten muss)

-   Capabilities zur Entwicklung und der Governance des SouvAP  
    (Fähigkeiten, die benötigt werden, um den SouvAP zu entwickeln und
    verwalten zu können)

**Anmerkung:** Neben der nutzerorientierten Betrachtung der erwarteten
Fähigkeiten der Lösung selbst sind sowohl die
Bereitstellungsperspektive, als auch die Steuerungs- und
Entwicklungsfähigkeiten der virtuellen Organisation von Interesse. Zur
Vereinfachung und nötigen Abstraktion kann auf das It4IT Referenzmodell
der OpenGroup zurückgegriffen werden, das ebenfalls eine virtuelle
Organisation in ihrer Erbringung von IT-Dienstleistungen in ihrer
Wertschöpfungskette beschreibt.

Die nachfolgend genannten Capabilities sind dabei in drei Ebenen
benannt, was ein in der Literatur und Architekturpraxis übliches
Vorgehen ist.

#### Capabilities, den SouvAP selbst betreffend

(Nutzungsperspektive)

In der nachfolgenden Abbildung 65, werden die von einem Nutzer
abverlangten wesentlichen Fähigkeiten des SouvAP aufgelistet.  
Die Capabilities der Ebene 2 (Kommunikation, Netzwerken, Organisation
und Planung, Management und Zugriff auf Inhalte, Durchführung von
Businessaufgaben) wurden initial der Literatur über die Gestaltung eines
Digitalen Arbeitsplatzes entnommen und entsprechend der hier
adressierten Verwaltungen diskutiert und angepasst.

![](media/file7.png)

Abbildung 8: Capabilities, den SouvAP selbst betreffend

Im Einzelnen lassen sich diese Capabilities folgendermaßen beschreiben:

Tabelle : Capabilities den SouvAP selbst betreffend

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Capability<br />
(Nutzerperspektive)</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Kommunikation</strong></p>
<ul>
<li><blockquote>
<p>Erreichen des gewünschten Publikums</p>
</blockquote></li>
<li><blockquote>
<p>Auswahl des geeigneten Kanals</p>
</blockquote></li>
<li><blockquote>
<p>Interaktion mit dem Publikum</p>
</blockquote></li>
<li><blockquote>
<p>Vertrauen und Sicherheit in die Kommunikation</p>
</blockquote></li>
<li><blockquote>
<p>Kommunikationserfahrung</p>
</blockquote></li>
</ul></td>
<td>Die Verwaltungsmitarbeiter sollen im Rahmen ihrer Zusammenarbeit miteinander und sicher kommunizieren können. Die Kommunikation soll über verschiedene Kanäle und in verschiedenen Ausprägungen möglich sein. Dazu gehören synchrone und asynchrone Kommunikation Mit Schrift, Audio und Video, direkte Kommunikation und Broadcasting, aber auch die Überlegungen, welches Zielpublikum überhaupt erreicht werden soll.</td>
</tr>
<tr class="even">
<td><p><strong>Netzwerken</strong></p>
<ul>
<li><blockquote>
<p>Verwaltung eigener Informationen</p>
</blockquote></li>
<li><blockquote>
<p>Suchen und Entdecken</p>
</blockquote></li>
<li><blockquote>
<p>Verbinden und Interaktion</p>
</blockquote></li>
<li><blockquote>
<p>Informationen und Wissen teilen</p>
</blockquote></li>
<li><blockquote>
<p>Förderung von Fähigkeiten und Themen</p>
</blockquote></li>
</ul></td>
<td>Moderne Verwaltungsarbeit basiert mehr und mehr auf der Vernetzung der Mitarbeiter untereinander über die verschiedenen Ebenen hinweg. Beginnend mit der Selbstorganisation über das Finden relevanter Ansprechpartner und Informationen bis hin zur gezielten Organisation der Verteilung der relevanten Informationen und des Wissens.</td>
</tr>
<tr class="odd">
<td><p><strong>Organisation und Planung</strong></p>
<ul>
<li><blockquote>
<p>Zeitorganisation</p>
</blockquote></li>
<li><blockquote>
<p>Management von Aktivitäten</p>
</blockquote></li>
<li><blockquote>
<p>Planung von Mitarbeitern und Ressourcen</p>
</blockquote></li>
</ul></td>
<td>Die Fähigkeit sich selbst und Aktivitäten anderer zu planen, rückt grade in der immer integrativeren modernen Verwaltungsarbeit zunehmend in den Vordergrund.</td>
</tr>
<tr class="even">
<td><p><strong>Management und Zugriff auf Inhalte</strong></p>
<ul>
<li><blockquote>
<p>Erstellung und Erfassung</p>
</blockquote></li>
<li><blockquote>
<p>Editierung und Zusammenarbeit</p>
</blockquote></li>
<li><blockquote>
<p>Veröffentlichung und Live-Cycle-Management der Inhalte</p>
</blockquote></li>
<li><blockquote>
<p>Suche und Navigation</p>
</blockquote></li>
<li><blockquote>
<p>Feedback und Empfehlungen</p>
</blockquote></li>
</ul></td>
<td>Im Verwaltungskontext hatten die Tätigkeiten des Erzeugens von schriftlichen Inhalten schon immer eine herausragende Bedeutung. Dazu kommen jetzt immer mehr Anforderungen, die Art der Inhalte betreffend, also nicht nur Textdokumente, sondern auch z.B. Multimedia usw. Ebenfalls zunehmen werden die integrativen Erfordernisse, z.B. das gemeinsame direkte Arbeiten an Inhalten.</td>
</tr>
<tr class="odd">
<td><p><strong>Durchführung von Geschäftsaufgaben</strong></p>
<ul>
<li><blockquote>
<p>Durchführung Fachverfahren</p>
</blockquote></li>
<li><blockquote>
<p>Verwaltung ERP-Informationen</p>
</blockquote></li>
<li><blockquote>
<p>Zugriff auf Anwendungen</p>
</blockquote></li>
<li><blockquote>
<p>Arbeitsplatzerfahrung</p>
</blockquote></li>
<li><blockquote>
<p>Persönliche Produktivität</p>
</blockquote></li>
</ul></td>
<td><p>Einen wesentlichen Beitrag zu einer durch Digitalisierung besseren Verwaltungsarbeit liefert die Erhöhung sowohl der persönlichen Produktivität durch geeignete Arbeitsweisen und Toolunterstützung als auch der digitalen Verknüpfung der Verwaltungsprozesse.</p>
<p>Der digitale Arbeitsplatz muss auf die moderne Verwaltung hin ausgerichtet sein.</p></td>
</tr>
</tbody>
</table>

#### Capabilities, die Bereitstellung des SouvAP betreffend

(Bereitstellungsperspektive)

Die in Abbildung 67 aufgelisteten Capabilities entstammen einer Analyse
der Arbeitsweise der IT-Dienstleister des Bundes, der Länder und
Kommunen und beschreiben deren Tätigkeiten als Fähigkeiten im Kontext
der Bereitstellung des SouvAP:

![](media/file8.png)

Abbildung 9: Capabilities, die Bereitstellung des SouvAP betreffend

Im Folgenden werden diese Capabilities beschrieben:

Tabelle : Capabilities, die Bereitstellung des SouvAP betreffend

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Capability<br />
(Bereitstellungsperspektive)</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Infrastruktur und Betrieb</strong></p>
<ul>
<li><blockquote>
<p>Hosting und Infrastruktur</p>
</blockquote></li>
<li><blockquote>
<p>Cloud-Dienste</p>
</blockquote></li>
<li><blockquote>
<p>Plattform-Management</p>
</blockquote></li>
<li><blockquote>
<p>Integration</p>
</blockquote></li>
<li><blockquote>
<p>Umgebungsbereitstellung</p>
</blockquote></li>
<li><blockquote>
<p>Monitoring</p>
</blockquote></li>
</ul></td>
<td><p>Der SouvAP ist als Cloud-Anwendung konzipiert und erfordert von der Bereitstellung die Zurverfügungstellung der entsprechend benötigten Infrastrukturdienste über die verschiedenen IaaS, PaaS und SaaS Ebenen hinweg. Einschließlich der nötigen Basisdienste wie Monitoring usw.</p>
<p>Ebenso werden die Integrationen in das SouvAP Ökosystem erwartet.</p></td>
</tr>
<tr class="even">
<td><p><strong>IT Management</strong></p>
<ul>
<li><blockquote>
<p>Gerätemanagement</p>
</blockquote></li>
<li><blockquote>
<p>Portale und Publizierung</p>
</blockquote></li>
<li><blockquote>
<p>Identitäts- und Zugriffsmanagement</p>
</blockquote></li>
<li><blockquote>
<p>Anwendungsmanagement SLM</p>
</blockquote></li>
<li><blockquote>
<p>IT-Support</p>
</blockquote></li>
<li><blockquote>
<p>Schulung und Fortbildung</p>
</blockquote></li>
<li><blockquote>
<p>Projektmanagement</p>
</blockquote></li>
</ul></td>
<td><p>Der SouvAP wird nicht nur technisch bereitgestellt, sondern muss verwaltet werden. Das betrifft die per Cloud zur Verfügung gestellte Lösung selbst, insbesondere bedarf es eines Portals und der Einbindung in das Rechtemanagement des Kunden.</p>
<p>Es sind aber auch Dienstleistungen für die Verwaltungskunden rund um den SouvAP nötig, wie das Management der Geräte der Kunden, der IT-Support; Schulung, Dokumentation bis hin zu Projektmanagement.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Zentrale Dienste</strong></p>
<ul>
<li><blockquote>
<p>Bedarf und Kapazitäten</p>
</blockquote></li>
<li><blockquote>
<p>Verwaltung von Lizenzen</p>
</blockquote></li>
<li><blockquote>
<p>Beschaffung und Abrechnung</p>
</blockquote></li>
<li><blockquote>
<p>Angebotsmanagement</p>
</blockquote></li>
<li><blockquote>
<p>Vertragsmanagement</p>
</blockquote></li>
<li><blockquote>
<p>Informationssicherheit</p>
</blockquote></li>
<li><blockquote>
<p>Geschäftsreporting</p>
</blockquote></li>
</ul></td>
<td><p>Im Verwaltungskontext ist es sinnvoll die organisatorischen Prozesse von den Managementprozessen der Lösung selbst zu trennen und zu vereinheitlichen.</p>
<p>Das betrifft alle Fragen der Vertragsgestaltungen, einschließlich Angebots- und Beschaffungsprozesse sowie Abrechnung und Geschäftsreporting, die Verwaltung der Lizenzen und ein zentrales Anforderungsmanagement.</p>
<p>Ebenfalls werden i.d.R. die Informationssicherheit und das Kapazitätsmanagement zentralisiert.</p></td>
</tr>
</tbody>
</table>

#### Capabilities der Steuerung und Produktentwicklung

(Steuerungs- und Entwicklungsperspektive)

Die in 68 erfassten Capabilities bilden sowohl die softwaretechnische,
als auch strategische Entwicklungssicht ab. Hinweise dazu gibt es in der
Literatur, u.a. zur IT4IT Referenzarchitektur.

![](media/file9.png)

Abbildung 10: Capabilities der Steuerung und Produktentwicklung

Diese Capabilities lassen sich im Einzelnen folgendermaßen beschreiben:

Tabelle : Capabilities der Steuerung und Produktentwicklung

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Capability<br />
(Steuerung und Enwicklung)</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Strategische Entwicklung</strong></p>
<ul>
<li><blockquote>
<p>Architektur und Standards</p>
</blockquote></li>
<li><blockquote>
<p>Produktbeschreibung</p>
</blockquote></li>
<li><blockquote>
<p>Kooperation und Partnerschaft</p>
</blockquote></li>
<li><blockquote>
<p>Marketing</p>
</blockquote></li>
<li><blockquote>
<p>Projektkoordination</p>
</blockquote></li>
<li><blockquote>
<p>Projektgovernance</p>
</blockquote></li>
<li><blockquote>
<p>Vendormanagement</p>
</blockquote></li>
<li><blockquote>
<p>Projektberatung</p>
</blockquote></li>
</ul></td>
<td><p>Im It4IT Referenzmodell wird die Planung und Steuerung als eigener Wertestrom in ihren Funktionen und ausführenden Komponenten benannt, aus denen sich die entsprechenden Fähigkeiten ableiten lassen:</p>
<p>U.a. die Architektur und die Fähigkeit, zentrale Roadmaps zu erstellen und die strategische Produktentwicklung voranzutreiben. Aber auch die Durchführung des SouvAP im Sinne eines Projektes einschließlich der Produktbeschreibung sind hier genannt. Nicht zu vergessen, das Maketing und Beratung rund um den SouvAP.</p>
<p>Dazu kommen das Management der Softwarelieferanten der OpenSource Module und die Koordination der Partnerschaften mit den IT-Dienstleistern des Bundes, der Länder und Kommunen.</p></td>
</tr>
<tr class="even">
<td><p><strong>Produktentwicklung</strong></p>
<ul>
<li><blockquote>
<p>Anforderungsmanagement</p>
</blockquote></li>
<li><blockquote>
<p>Anwendungsdesign und Modulauswahl</p>
</blockquote></li>
<li><blockquote>
<p>Anwendungsentwicklung und Customizing</p>
</blockquote></li>
<li><blockquote>
<p>Konfigurationsmanagement</p>
</blockquote></li>
<li><blockquote>
<p>Changemanagement</p>
</blockquote></li>
<li><blockquote>
<p>Releasemanagement</p>
</blockquote></li>
<li><blockquote>
<p>Qualitätsmanagement</p>
</blockquote></li>
</ul></td>
<td><p>Eine der Kernfähigkeiten entsprechend des Wertestrommodells nach IT4IT bildet die Entwicklung des SouvAP, basierend auf einem zentralen Anforderungsmanagement.</p>
<p>Die eigentliche Entwicklung erfolgt dabei in zwei verschiedenen Schritten:</p>
<ul>
<li><blockquote>
<p>Zum einen erfolgt die Entwicklung der OpenSource Module, einschließlich der geforderten interoperablen Schnittstellen</p>
</blockquote></li>
<li><blockquote>
<p>Zum anderen wird das den Verwaltungskunden tatsächlich angebotene SouvAP-Produkt entwickelt (Customizing, Modulkonfektion)</p>
</blockquote></li>
</ul>
<blockquote>
<p>Beide Entwicklungsstränge benötigen die Fähigkeiten normaler Softwareentwicklung bis hin zu Change- und Releasemanagement gleichermaßen.</p>
</blockquote></td>
</tr>
</tbody>
</table>

### Zuordnung der Rollen zu den Akteuren und Capabilities

Die Beziehungen der verschiedenen Akteure zu den Rollen bzw.
Capabilities lässt sich dann folgendermaßen beschreiben:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Akteur</strong></th>
<th><strong>Zuordnung zur Rolle</strong></th>
<th><strong>Zuordnung zu Capabilities</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>BMI/ ZenDiS</td>
<td><ul>
<li><p>Übernimmt die Steuerung und Governance des SouvAP</p></li>
</ul></td>
<td><ul>
<li><p>Koordiniert das Anforderungsmanagement</p></li>
<li><p>Eigner der SouvAP strategischen Entwicklung und Roadmap</p></li>
<li><p>Koordiniert das Life Cycle Management und die Produktentwicklung</p></li>
<li><p>Koordiniert die Lizenzen</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Öffentliche Verwaltung</td>
<td><ul>
<li><p>Ist Auftraggeber für die IT-Dienstleister</p></li>
<li><p>Nutzt den SouvAP</p></li>
<li><p>Betreibt und administriert die Software</p></li>
</ul></td>
<td><ul>
<li><p>Nutzt die Fähigkeiten des SouvAP</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Kunden der ÖV<br />
(Bürger, Unternehmen, ..)</td>
<td><ul>
<li><p>Können ggf. den SouvAP nutzen, falls er von den IT-Dienstleistern angeboten wird</p></li>
<li><p>Können die Software nutzen</p></li>
</ul></td>
<td><ul>
<li><p>Ggf. Nutzung der Fähigkeiten des SouvAP</p></li>
</ul></td>
</tr>
<tr class="even">
<td>IT-Dienstleister des Bundes, der Länder und Kommunen</td>
<td><ul>
<li><p>Stellt die Software entsprechend Referenzarchitektur zur Verfügung</p></li>
<li><p>Betreibt die nötige Infrastruktur der Cloud</p></li>
<li><p>Entwickelt ggf. selbst mit am SouvAP</p></li>
</ul></td>
<td><ul>
<li><p>Ist an allen Capabilities der Bereitstellung beteiligt</p></li>
<li><p>Ist an der Produktentwicklung beteiligt (vorrangig Modulauswahl und Konfektionierung)</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Software-Anbieter (Vendoren) und Community</td>
<td><ul>
<li><p>Liefert die Software des SouvAP</p></li>
</ul></td>
<td><ul>
<li><p>Ist an der Produktentwicklung beteiligt (OpenSource Modulentwicklung)</p></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle 7: Zuordnung Akteure, Rollen Capabilities

### Beschreibung der Liefergegenstände durch die beteiligten Partner

Bevor die Werteströme der Entwicklung und Bereitstellung des SouvAP
genauer beschrieben werden, werden hier die eigentlichen
Liefergegenstände des SouvAP aufgeführt, die zur Bereitstellung der
Funktionalität benötigt werden.

![](media/file10.png)

Abbildung Liefergegenstände

Die Entwicklung der Funktionalität des SouvAP erfolgt in mehreren
Lieferungen, die aufeinander aufbauen:

1.  Der SouvAP muss in seiner Funktionalität konzipiert werden

\- Es wird eine Architektur festgelegt, einschließlich der vorgesehenen
Module

\- Diese zunächst abstrakten Module werden mit den Marktteilnehmern
abgesprochen

\- Die Lösung wird entsprechend der abstrakten Module spezifiziert

\- Die Lösung erhält ein Spezifikationsrelease.

1.  Die eigentliche Entwicklung der Module übernehmen Software-Anbieter
    des Marktes. Es entstehen ein oder mehrere konkrete Module, die der
    festgelegten Spezifikation folgen

\- Die Module sind i.d.R. nicht exklusiv für die ÖV

\- Sie folgen einem Vendor spezifischen Release

\- Sie werden von den Vendoren in das OpenCOde Source Repository
geliefert

1.  Aus den zur Verfügung stehenden konkreten Modulen werden
    (verwaltungs-)kundenspezifische Gesamtlösungen eines SouvAP
    konfektioniert und zu einer Produkt-Suite zusammengefasst. Eine
    solche Produkt-Suite ist also eine organisationsspezifische Instanz
    des SouvAP. Die Phönix-Suite stellt ein Beispiel eines solchen
    implementierten Produktes dar.

\- Auswahl der für die Suite relevanten konkreten Module der
Software-Anbieter

\- Ergänzung der Gesamtlösung um eigene Funktionalitäten außerhalb der
Module und Anpassungen (z.B. eigene Integrationen in Fachverfahren)

\- Erstellung Release und Release-Beschreibung des gesamten Produktes

1.  Teil der Lieferung des SouvAP ist eine komplette
    Beispielimplementierung als Source-Code, also einer direkt in einer
    entsprechenden Laufzeitumgebung lauffähigen SouvAP Lösung  
    - Die Beispielimplementierung wird in das Open COde Source
    Repository als Source geliefert  
    - Es werden nicht nur die Module, sondern auch die Integrations- und
    Deploymentbestandteile  
    zur Verfügung gestellt  
    - Diese Implementierung ist nicht für den produktiven Betrieb
    vorgesehen

2.  Die erstellte Software-Suite (Produkt) muss noch in geeigneter Form
    den eigentlichen Kunden zur Verfügung gestellt werden,
    einschließlich der dazu nötigen Infrastruktur.

Die Lösung wird also um die tatsächlichen IT-Service ergänzt

\- Betrieb auf Betreiber-Infrastruktur (DVS-konforme Cloud)

\- Anbindung an die Basisdienste (z.B. IAM, Device Management, usw.)

\- Etablierung Abrechnungsservices

\- Etablierung IT-Support Services

### Werteströme des SouvAP

#### Überblick über die Werteströme

Der Erfolg des bereitzustellenden SouvAP hängt wesentlich vom Nutzen für
alle Beteiligten ab. (siehe Zuordnung der Rollen zu den Akteuren und
Capabilities):

-   Strategische Entwicklung und Bereitstellung der Module

Der souveräne digitale Arbeitsplatz wird in einem zweistufigen Prozess
den eigentlichen Nutzern in der öffentlichen Verwaltung zur Verfügung
gestellt:

-   Entwicklung von interoperablen Modulen und deren
    Zurverfügungstellung im Sourcecode in einem zentralen Repository
    i.d.R. durch OSS Hersteller in deren eigenen Entwicklungsprozessen

-   Aus diesen Modulen werden anschließend im Rahmen der Bereitstellung
    konkrete und passgenaue Produkte durch die IT-Dienstleister des
    Bundes, der Länder und Kommunen konfektioniert und den Kunden zur
    Nutzung angeboten

Der Wertschöpfungsprozess in seiner Konzeption und der zur
Verfügungstellung der Open Source Module wird dabei zentral verwaltet.

-   Produkt- und IT-Service Bereitstellung

Die digitalen Funktionalitäten des SouvAP werden vorrangig von
IT-Unternehmen der öffentlichen Verwaltung in ihren Rechenzentren und
auf ihren Plattformen bereitgestellt. Die IT-Dienstleister stellen die
Funktionalitäten geeignet aus den zentral zur Verfügung gestellten
Modulen zusammen und konfektionieren ein passgenaues Produkt für ihre
Verwaltungskunden.

-   Produkt-Nutzung

Der tatsächliche Nutzen für die Mitarbeiter der öffentlichen Verwaltung
erfolgt in der Auswahl und Nutzung der mit dem SouvAP zur Verfügung
gestellten Funktionalitäten

Im Folgenden werden die einzelnen Werteströme im Einzelnen beschrieben:

Tabelle : Werteströme: Strategische Entwicklung und Bereitstellung der
Module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Strategische Entwicklung und Bereitstellung der Module</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SouvAP Modul-Roadmap</td>
<td><p>Der SouvAP soll als einheitliches Branding den Softwarenutzern aus der Verwaltung zur Verfügung gestellt werden. Dazu gehört eine einheitliche Roadmap der benötigten OpenSource Module.</p>
<p>Die Roadmap setzt auf einem auf Kundenfeedback basierenden Prozess des Anforderungsmanagements auf, unter Beteiligung der Partner aus den IT-Dienstleistern und der Softwarelieferanten.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Anforderungsbasierte und demokratische Roadmap</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Wirtschaftlichkeit</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Orientierung an internationalen Verwaltungsstandards</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>SouvAP Modul-Konzept und Architektur</td>
<td><p>Die Softwaremodule müssen entsprechend der Vorgaben zur Entwicklung der gewünschten Fähigkeiten der Gesamtlösung zu einer einheitlichen Gesamtarchitektur zusammengefasst werden.</p>
<p>In diesem Prozess entsteht eine logische konzeptionelle Lösung, die dem Release des SouvAP entspricht.</p>
<p>Das Konzept selbst ist ein lizenzierbares Eigentum des Auftraggebers des SouvAP.<br />
(siehe auch Anmerkung zu Nutzungsbedingungen)</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit basierend auf Anwenderfeedback</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Modulentwicklung</td>
<td><p>Die Softwarelieferanten erstellen die Open Source Module in eigenen Entwicklungsprozessen als Teil ihrer eigenen Geschäftsprozesse.</p>
<p>Der Auftraggeber des SouvAP nimmt durch seine Modul-Roadmap Einfluss auf die Produktstrategien der Softwarelieferanten.</p>
<p>Die Module unterliegen den OSI-konformen Nutzungsbedingungen und es werden Subskriptionen angeboten</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Fail Fast Entwicklungs- und Lieferprozess</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Sicherheit First</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Modulbereitstellung</td>
<td><p>Die OpenSource Module der Softwarelieferanten (Hersteller) werden in einem vom Auftraggeber des SouvAP definierten Verfahren den Partnern der Bereitstellung in einem zentralen Repository (Open CoDE) zur Verfügung gestellt.</p>
<p>Für einen stabilen Betrieb bieten alle Hersteller i.d.R. Maintenance-Verträge an, auf deren Basis Betreiber Zugriff auf aktuelle Patches und 3rd Party Support erhalten</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Beispielimplementierung</td>
<td><p>Neben den Softwaremodulen wird eine Beispielimplementierung als Source Code im OpenCode-Repository bereitgestellt. Diese enthält eine vollständige Implementierung eines SouvAP, die nicht für den Produktivbetrieb vorgesehen ist</p>
<p>Die Erstellung der Beispielimplementierung erfolgt entsprechend Steuerung durch den Auftraggeber durch einen der IT-Dienstleister des Bundes, der Länder und Gemeinden.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Fail Fast Entwicklungs- und Lieferprozess</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle : Werteströme: Produkt und IT-Service Bereitstellung

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Produkt und IT-Service Bereitstellung</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SouvAP Produkt Konfektion</td>
<td><p>Die IT-Dienstleister des Bundes, der Länder und Kommunen stellen ihren Kunden aus der Verwaltung spezielle auf deren Bedürfnisse zugeschnittene Lösungen aus den entsprechend der Roadmap gelieferten Open Source Modulen zur Verfügung. Diese erfordern ein Customizing des SouvAP (Herstellung einer organisationsspezifischen Instanz des SouvAP)</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Wirtschaftlichkeit</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Orientierung an internationalen Verwaltungsstandards</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Prosumer-Lösung</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Fail Fast Entwicklungs- und Lieferprozess</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Sicherheit First</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Produkt Marketing und Vertrieb</td>
<td><p>Die IT-Dienstleister des Bundes, der Länder und Kommunen können SouvAP Lösungen unter eigener Marke ihren Kunden in der Verwaltung anbieten</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Wirtschaftlichkeit</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit basierend auf Anwenderfeedback</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Produktbereitstellung</td>
<td><p>Die IT-Dienstleister des Bundes, der Länder und Kommunen bieten ihren Kunden in der Verwaltung als Produkt zusammengefasste IT-Services an.<br />
Diese beinhalten nicht nur die Funktionalität der konfektionierten Open Source Module, sondern umfassen auch die für eine erfolgreiche Nutzung notwendige Integration in die IT-Landschaft der Verwaltungen, z.B. Identitätsmanagement, Virenschutz usw.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Lieferung von Services anstatt Anwendungen an die Verwaltungskunden</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Infrastrukturbereitstellung</td>
<td><p>Da die Software als Cloud-Lösung konzipiert ist, müssen die entsprechenden Infrastrukturen und der IT-Betrieb zur Verfügung gestellt werden.<br />
Dies geschieht durch die Plattformbetreiber.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Unterbrechungsfreier Geschäftsbetrieb wenn nötig</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Ökologisches Denken und Nachhaltigkeit</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Produktsubskriptionen und Abrechnung</td>
<td><p>Die IT-Dienstleister des Bundes, der Länder und Kommunen werden ihre IT-Leistungen direkt mit ihren Kunden der Verwaltung entsprechend der geltenden Entgeldvereinbarungen abrechnen. Dabei sind die verschiedenen Subskriptionen und Zusatzdienstleistungen zu berücksichtigen.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle : Werteströme: Produkt Nutzung

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Produkt Nutzung</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SouvAP Produktauswahl und Einkauf</td>
<td><p>Der SouvAP ist für die Verwaltungen und deren Anforderungen konzipiert. Entsprechend dieser Vorgehensweise werden die Verwaltungen mit ihren IT-Dienstleistern die für sie vernünftigsten Lösungen vereinbaren. Praktisch bedeutet das i.d.R. die von den IT-Dienstleistern vorkonfigurierten Produkte als Dienstleistung zu erwerben.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Wirtschaftlichkeit</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Orientierung an internationalen Verwaltungsstandards</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Prosumer-Lösung</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Anforderungsbasierte und demokratische Roadmap</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Produktnutzung</td>
<td><p>Die Mitarbeiter der Verwaltung nutzen die für sie zugeschnittene Lösung des SouvAP.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit basierend auf Anwenderfeedback</em></p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Produktfeedback und Verbesserung</td>
<td><p>Das Anwenderfeedback ist wichtig sowohl für die Arbeitsumgebung der Verwaltungsmitarbeiter selbst, die ihnen durch ihre IT-Dienstleister zur Verfügung gestellt werden, als auch für die Weiterentwicklung des SouvAP als Ganzes.</p>
<p>Hier ist sicherzustellen, dass dieses Feedback allen Partnern gleichermaßen zur Verfügung gestellt wird.</p>
<p><em>Siehe GP:</em></p>
<ul>
<li><blockquote>
<p><em>Ausrichtung auf Verwaltungsnutzen und Anwenderzufriedenheit basierend auf Anwenderfeedback</em></p>
</blockquote></li>
<li><blockquote>
<p><em>Partnerschaftliche und faire Geschäftsbeziehungen</em></p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

#### Zuordnung der Werteströme zu den beteiligten Rollen

Die in obiger Tabelle beschriebenen Werteströme werden durch die in 58
beschriebenen Rollen realisiert. Diese Realisierung wird durch die
gestrichelten Linien ausgedrückt.

Die Werteströme selbst sind in Aktivitäten geordnet (durch Pfeile
verbunden)

![](media/file11.png)

Abbildung Zuordnung Werteströme zu den Rollen

#### Zuordnung der Werteströme zu den Liefergegenständen

Die oben beschriebenen Liefergegenstände ordnen sich in die Kette der
Werteströme folgendermaßen ein:

![](media/file12.png)

Abbildung Wertestrom und Liefergegenstände

### Geschäftsmodell und Lieferketten

#### Liefergegenstände und Schnittstellen

Entsprechend der Werteströme werden die folgenden zu produzierenden
Liefergegenstände jeweils beauftragt sowie geliefert und haben dabei die
angegebenen Schnittstellen bzw. Interaktionen:

Tabelle : Liefergegenstände

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Liefergegenstände</strong></th>
<th><strong>Schnittstellen</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SouvAP Konzept</td>
<td><p>Die Steuerungsorganisation des SouvAP (BMI/ ZenDiS) ist gleichzeitig der Besitzer des Konzeptes des SouvAP. Mit Konzept ist die abstrakte Lösung des SouvAP in Form einer Blaupause gemeint, die später in konkreten Lösungen des SouvAP mündet.</p>
<p>Das Konzept wird den entsprechenden Lieferanten der tatsächlichen Software zur Verfügung gestellt</p></td>
</tr>
<tr class="even">
<td>SouvAP Module</td>
<td><p>Die konkreten Softwaremodule des SouvAP werden von den Software-Lieferanten (Vendoren) entsprechend den im Konzept abstrakt definierten Modulen entwickelt</p>
<p>Im Rahmen des SouvAP werden die Software-Lieferanten dazu von der Steuerungsorganisation beauftragt</p></td>
</tr>
<tr class="odd">
<td>SouvAP Beispielimplementierung</td>
<td><p>Aus den Softwaremodulen wird eine Beispielimplementierung konfektioniert und als Source Code auf Open Code bereitgestellt, die neben den konfektionierten Modulen auch die entsprechenden Integrations- und Deploymentanteile enthält.</p>
<p>Die Beispielimplementierung wird vom Auftraggeber des SouvAP gesteuert und von einem der IT-Dienstleister entwickelt</p></td>
</tr>
<tr class="even">
<td>SouvAP Produkt</td>
<td><p>Aus den Softwaremodulen der Vendoren werden durch die Software-Konfektionierer (i.d.R. IT-Dienstleister) spezifische Produkte zusammengestellt und zu einheitlichen Referenzlösungen entwickelt.</p>
<p>Die Auftraggeber sind i.d.R. die Verwaltungen des Bundes, der Länder und Kommunen<br />
Eine bereits vorhandene Lösung ist die Phönix-Suite des IT-Landesdienstleisters Dataport.</p></td>
</tr>
<tr class="odd">
<td>SouvAP Bereitstellung</td>
<td><p>Die jeweilige Referenzlösung wird den Verwaltungen von den Software- und Plattformbetreibern zur Verfügung gestellt. Die konkreten Lösungen können im Rahmen der Referenzlösung durchaus kundenspezifisch zugeschnitten sein.<br />
So sind spezifische Lösungen für spezifische Kundenanforderungen möglich.</p>
<p>Die Verwaltungsorganisationen nutzen den SouvAP im Rahmen eines Service, der die Software selbst, aber auch die nötigen Basisleistungen wie Integrationen der Fachverfahren beinhalten.</p>
<p>Auftraggeber des Services ist i.d.R. wiederum die entsprechende Verwaltung des Bundes, der Länder und Kommunen</p></td>
</tr>
</tbody>
</table>

Das entsprechende Organisationsdesign hat neben den formalen Aspekten
der Wertschöpfungskette auch die geschäftlichen Rahmenbedingungen im
Auge zu behalten.

Es wird davon ausgegangen, dass sämtliche Software OSD-konform ist.
Dennoch können Subskriptionsmodelle explizit vereinbart werden, z.B. um
Gewährleistungen und Support, oder noch leistungsstärkere
Enterprise-Editionen zu verwenden.

Eine Sonderrolle spielt das Konzept des SouvAP selbst, da es als
Branding den Nutzungsbedingungen des Auftraggebers unterliegt.

![](media/file13.png)

Abbildung Liefergegenstände - Schnittstellen

Die diesem Lieferprozess zugrundeliegenden Geschäftsprinzipien sind:

-   *Partnerschaftliche und faire Geschäftsbeziehungen*

-   *Wirtschaftlichkeit*

-   *Lieferung von Services anstatt Anwendungen an die
    Verwaltungskunden*

-   *Prosumer-Lösung*

-   *Anforderungsbasierte und demokratische Roadmap*

#### Geschäftsmodelle der Beteiligten

Ein Geschäftsmodell erfasst im Wesentlichen, wie Werte in der
Organisation geschaffen werden. Die Modelle können z.B. in Form von
Business Model Canvases beschrieben werden.

**Die Elemente eines Geschäftsmodells:**

-   Management - Wie wird die Organisation geführt?

-   Kosten – Wie ist die Kostenstruktur der Organisation?

-   Produkte und Services – Welche Leistungen stellt die Organisation
    her?

-   Erlöse – Welche Erlöse generiert die Organisation?

-   Kompetenzen – Welche Kompetenzen werden benötigt und über welche
    Kompetenzen verfügt die Organisation?

-   Kunden – Wer sind die Kunden der Organisation?

-   Lieferungen – Wie werden die Leistungen geliefert?

Diese Elemente sind nun für die verschiedenen Rollen im Kontext des
SouvAP zu erfassen und dahingehend zu vergleichen, ob es Überdeckungen
und Konflikte gibt.

Diese Untersuchung würde allein für die Rollen der beteiligten Akteure
der Öffentlichen Verwaltung unvollständig bleiben müssen, da allein
diese bereits sehr diversifiziert sind.

So sind einige IT-Dienstleister des Bundes, der Länder und Kommunen
sowohl Betriebe öffentlichen Rechts, als auch selbst Behörden und damit
in ihrer wirtschaftlichen Handlungsfreiheit anderen Bedingungen
unterworfen.

Die Geschäftsmodelle der beteiligten Partner hängen jetzt in hohem Maße
vom eigenen bzw. zugewiesenem und i.d.R. nicht sehr flexiblen
Rollenverständnis ab.

Um eine Gegenüberstellung zu ermöglichen, werden sowohl die Elemente auf
das in diesem Kontext wesentliche limitiert, als auch Rollen
zusammengefasst und vereinfacht:

-   Auftraggeber (Auftraggeber des SouvAP)

-   Verwaltungen/Kunde (Auftraggeber für die IT-Dienstleister, Kunde)

-   IT-Dienstleister (Software Konfektionierer, Software Betreiber,
    Plattform Betreiber)

-   Software Lieferant (Software Lieferanten)

Vereinfachte Gegenüberstellung:

Tabelle : Geschäftsmodelle

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Rollen</strong></th>
<th><strong>Kosten</strong></th>
<th><strong>Produkte</strong></th>
<th><strong>Erlöse</strong></th>
<th><strong>Kunden</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><strong>Auftraggeber</strong></td>
<td><p>Eigene Organisation</p>
<p>Lizenzierung der Module</p>
<p>Code-Repository</p></td>
<td>Konzept und Branding des SouvAP</td>
<td>Vertrieb des Konzeptes</td>
<td>IT-Dienstleister</td>
</tr>
<tr class="even">
<td><strong>Verwaltungen/Kunde</strong></td>
<td><p>Lizenzkosten</p>
<p>SouvAP Produktentwicklung</p>
<p>SouvAP Bereitstellung</p>
<p>Finanzierung Auftraggeber</p></td>
<td>-</td>
<td>-</td>
<td>Eigene Organisation</td>
</tr>
<tr class="odd">
<td><strong>IT-Dienstleister</strong></td>
<td><p>Eigene Organisation</p>
<p>Finanzierung Auftraggeber</p>
<p>Finanzierung Konzept</p>
<p>Support für die Module</p></td>
<td><p>SouvAP Produkt</p>
<p>SouvAP Bereitstellung</p></td>
<td>Entsprechend Leistungskatalog für die SouvAPProdukte</td>
<td><p>Verwaltungen</p>
<p>ggf. Kunden außerhalb der Verwaltung</p></td>
</tr>
<tr class="even">
<td><strong>Software Lieferant</strong></td>
<td>Eigene Organisation</td>
<td>Module des SouvAP</td>
<td><p>Subskriptionen der Module (Gewährleistung, Support)</p>
<p>Spezielle Anpassungen/Features</p>
<p>Support für die Module</p></td>
<td><p>Auftraggeber</p>
<p>IT-Dienstleister</p></td>
</tr>
</tbody>
</table>

#### Implikationen aus den Geschäftsmodellen

Die Gesamtorganisation des SouvAP ist ein weitgehend geschlossenes
System in dem Sinne, dass alle Dienstleistungen letztendlich von den
Kunden, also der Öffentlichen Verwaltung gezahlt werden. Das beginnt mit
den unmittelbaren Funktionen des SouvAP, erstreckt sich aber auch auf
die Finanzierung des Konzeptes und Brandings des SouvAP.

Es besteht jetzt eine gewisse Varianz, wie die Kostenflüsse innerhalb
der komplexen Organisation gesteuert werden könnten., in der die
unterschiedlichen Rollen durchaus eigene Kosten und Erlöse haben.

Es wird deutlich, dass der SouvAP verwaltungsübergreifend in allen
seinen Aspekten damit nur funktionieren kann, wenn der
Wertschöpfungsprozess dieser unterschiedlichen Rollenverteilung durch
ein hohes Maß an Standardisierung und verbindlichen Festlegungen
Rechnung trägt.

Das bedeutet insbesondere:

Tabelle : Implikationen aus den Geschäftsmodellen

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Festlegung</strong></th>
<th><strong>Beschreibung, Implikation</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Zentrale Governance bei lokaler Adaptierbarkeit</td>
<td><p>BMI/Zendis erstellen die zentralen Vorgaben und übernehmen die Vermittlung und zentrale Vermarktung, sorgen also für das Vertrauen in die konzeptionelle Lösung.</p>
<p>Es wird allerdings lokale SouvAP Produkte geben, die individuell auf spezielle Kunden zugeschnitten sind.</p></td>
</tr>
<tr class="even">
<td>Vereinheitlichung der Beschaffungsprozesse</td>
<td>Um der Komplexität des Gesamtgeschäftsmodells Herr zu werden, muss der Beschaffungs- und Vertriebsweg radikal vereinfacht werden.<br />
Vorbild könnten hier die im Rahmen des OZG entwickelten EfA Prozesse sein</td>
</tr>
<tr class="odd">
<td>Standardisierung der Leistungsabrechnung</td>
<td>Die Leistungs- und Entgeldkataloge der Beteiligten muss auf ein einfach zu verstehendes Modell reduziert werden</td>
</tr>
<tr class="even">
<td>Einheitliche Nutzungsbedingungen</td>
<td>Um die praktische Bereitstellung für die verschiedenen Kunden überhaupt gewährleisten zu können, sind die Regeln z.B. des Nutzungsrechts in der gesamten Wertschöpfungskette radikal zu vereinfachen. Es werden z.B. nur entwickelte Module akzeptiert, die dem OSI Lizenzmodell umfänglich entsprechen. Subskriptionsmodelle sind transparent zu machen.</td>
</tr>
<tr class="odd">
<td>Flexible Module und Komponenten</td>
<td>Die zu entwickelnden und bereitzustellenden Komponenten müssen funktional hochgradig austauschbar sein, um sowohl in einer generischen als auch sehr spezifischen Lösung benutzt werden zu können</td>
</tr>
<tr class="even">
<td>Interoperabilität</td>
<td>Die Komponenten müssen auch technisch denselben Standards für die Integration genügen. Das heißt, dass insbesondere die Cloud-fähigen Komponenten den selben technischen Regeln genügen müssen</td>
</tr>
</tbody>
</table>

### Abgeleitete Prozesse und Beispielworkflows

Ähnlich zu den beschriebenen Business Capabilities und den ebenfalls
beschriebenen Wertschöpfungsketten im Kontext des SouvAP wird ein
Prozessmodell ebenfalls in verschiedenen Ebenen beschrieben:

-   Szenarien der Nutzer des SouvAP

-   Szenarien der beteiligten Akteure an der Entwicklung, Bereitstellung
    und Verwaltung des SouvAP

Die Beschreibung beschränkt sich dabei auf wesentliche
Prozessbestandteile und liefert kein vollständiges Prozessmodell im
Sinne eines BPMN.

#### Nutzungsszenarien der SouvAP Nutzer

Um die relevanten Szenarien beschreiben zu können, sei zuerst auf die
wesentlichen Aspekte der Verwaltungsarbeit selbst verwiesen, in den der
SouvAP genutzt werden wird:

Der Nutzen der Softwarelösung des souveränen digitalen Arbeitsplatzes
ergibt sich primär aus der Nutzung durch die Mitarbeiter der Verwaltung.
Das betrifft:

-   Alle Aspekte der Unterstützung der Arbeitsvorgänge der
    Verwaltungsmitarbeiter durch die Softwarelösung selbst, also die
    Digitalisierung der bestehenden Verwaltungsvorgänge, einschließlich
    der Erhöhung der persönlichen Produktivität

-   Die erfolgreiche Integration der neuen Lösung in die bereits
    bestehende Prozess- und IT-Landschaft

-   Die Ermöglichung von sogar neuen Formen der Lösung von
    Verwaltungsaufgaben durch die Software

Nicht vergessen werden dürfen andere Aspekte der Nutzung von Software,
um sie erfolgreich zu machen und den Nutzen zu generieren:

-   Die Software muss verstanden werden, also z.B. in den Funktionen
    leicht nachzuvollziehen sein und bedarf einer
    einführungsbegleitenden Schulung

-   Die Nutzererfahrung muss insgesamt positiv sein, einschließlich
    Performance

-   Die ‚Form‘, also Modulfunktionalität und Nutzerinterface muss der
    Funktion, also den Verwaltungsaufgaben folgen. Die Anwendungen
    müssen z.B. eine logische Einheit bilden

Der hier zu beschreibende digitale Arbeitsplatz ist vorrangig für die
Mitarbeiter der Öffentlichen Verwaltung gedacht. Die Arbeit selbst
könnte in 3 Tätigkeitsaspekte unterteilt werden:

1.  <span class="underline">Allgemeine Büro- und
    Verwaltungstätigkeiten:</span> Diese Tätigkeiten dienen der
    unmittelbaren Verwaltungstätigkeit, also beispielsweise die Arbeit
    mit den Fach- und Querschnittsverfahren, die Bürgerkommunikation,
    die Veraktung usw. Das ist die klassische Verwaltungsarbeit.

2.  <span class="underline">Projektartige Tätigkeiten und Tätigkeiten in
    Teams:</span> In der Industrie, ebenso in der Öffentlichen
    Verwaltung findet die Arbeit, damit auch die digitale Arbeit immer
    mehr in zeitlich begrenzten Projekten und in virtuellen Teams statt.
    Diese Projektarbeit kann dabei innerhalb der eigenen
    Verwaltungsorganisation, aber auch organisationsübergreifend
    organisiert sein.

3.  <span class="underline">Übergeordnete Tätigkeiten in der
    Verwaltungsorganisation:</span> Die Öffentliche Verwaltung besteht
    aus Verwaltungsorganisationen, die ihrerseits ganz eigene
    Tätigkeiten erfordern. Hier z.B. die Information aller Mitglieder
    der Organisation über ein schwarzes Brett im Intranet.

Diesen Tätigkeitsaspekten können folgende Tätigkeiten zugeordnet werden,
aus denen sich die verschiedenen Nutzungsszenarien ableiten lassen:

![](media/file14.png)

Abbildung : Nutzungsszenarien

Die Mitarbeiter der Öffentlichen Verwaltung sind in diesen Tätigkeiten
auf die Unterstützung durch den SouvAP angewiesen. Um die dazu
benötigten Komponenten zu bestimmen und optimieren zu können, bietet es
sich an, beispielhafte Workflows zu entwerfen und auf ihre
Toolunterstützung bezüglich verschiedener Nutzerprofile zu überprüfen.

Bereits jetzt sind einige dieser Szenarien bereits hinreichend bekannt
und mit Beispielworkflows beschrieben (siehe Anhang), neuere werden
dazukommen. Die Methodik dabei ist, möglichst Szenarien zu finden, die
ein oder mehrere Tätigkeiten einschließen, um Wiederverwendbarkeit
bereits in der Prozessebene zu gewährleisten.

#### Nutzungsszenarien der Entwicklung und Bereitstellung

Nicht nur die Nutzung des Souveränen Arbeitsplatzes durch die
Endanwender selbst erfolgt in typischen Workflows, sondern auch die
Entwicklung und Bereitstellung des Souveränen Arbeitsplatzes durch die
beteiligten Organisationen lassen sich nach typischen Aktivitäten
entsprechend Lieferungen entlang der (oben) bereits beschriebenen
Werteströme detaillierter beschreiben und gruppieren:

![Ein Bild, das Text enthält. Automatisch generierte
Beschreibung](media/file15.png)

Abbildung Workflows der Entwicklung und Bereitstellung

Der jeweilige Wertschöpfungsprozess wurde bereits im Überblick
beschrieben. Einige Tätigkeitsaspekte sind im Sinne des Verständnisses
der Geschäftsprozesse zu detaillieren:

*siehe dazu auch die Capabilities der Steuerung und Produktentwicklung,
sowie Capabilities, die Bereitstellung des SouvAP betreffend*

*ebenso die Werteströme des SouvAP*

Eine tabellarische Übersicht der entsprechenden die Prozesse
repräsentierenden Workflows sieht dann so aus:

Tabelle : Workflows

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Workflow</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Architektur und Standards</td>
<td><p>Die Architektur des SouvAP wird zentral entwickelt und durchläuft einem Genehmigungsprozess der durch den zentralen Auftraggeber organisiert wird. Sie enthält sowohl das Architekturkonzept, als auch eine zentrale Referenzarchitektur des SouvAP.</p>
<p>Zu den jeweiligen Liefergegenständen werden separate Architekturdokumente erwartet, die für ihren jeweiligen Lieferzweck bindend sind und nicht im Widerspruch zu den zentralen Architekturdokumenten stehen dürfen<br />
Das betrifft insbesondere:</p>
<ul>
<li><blockquote>
<p>Die Software-Architekturen der Module der OSS-Hersteller</p>
</blockquote></li>
<li><blockquote>
<p>Die Beispielimplementierung</p>
</blockquote></li>
<li><blockquote>
<p>Die konfektionierten Produkt-Suiten (SouvAP-Instanzen)</p>
</blockquote></li>
<li><blockquote>
<p>Die betreiberspezifischen Bereitstellungsservices</p>
</blockquote></li>
</ul>
<p>Das zentrale Architekturboard des SouvAP zeichnet hier für die kontinuierliche Weiterentwicklung der zentralen Architekturdokumente verantwortlich.</p></td>
</tr>
<tr class="even">
<td>Anforderungsmanagement</td>
<td><p>Es wird ein zentrales Anforderungsmanagement über den SouvAP von der Auftraggeberorganisation bereitgestellt und verwaltet.</p>
<p>In ihm werden die zentralen Vorgaben gesammelt, die die operationale und strategische Weiterentwicklung des SouvAP beschreiben und treiben.</p>
<p>Input wird dabei von allen am Gesamtprozess beteiligten Organisationen erwartet und berücksichtigt, einschließlich eines direkten Kanals der Endanwender der von den Betreibern zur Verfügung gestellten Produkte und Services.</p></td>
</tr>
<tr class="odd">
<td>Anwendungsdesign und Modulauswahl</td>
<td><p>Das Anwendungsdesign ergibt sich zum einen aus dem Anforderungsmanagement des SouvAP, zum anderen aus den von den OSS-Herstellern zur Verfügung gestellten Modulen und deren Fähigkeiten.</p>
<p>Beide Prozesse verlaufen erst einmal unabhängig, soweit sie nicht direkt vom zentralen Auftraggeber beauftragt werden.</p>
<p>Die Modulauswahl erfolgt letztendlich durch den zentralen Auftraggeber unter Berücksichtigung der Anwendungsroadmap.</p></td>
</tr>
<tr class="even">
<td>Kooperation und Partnerschaft</td>
<td><p>Die Kooperation zwischen den verschiedenen Leistungserbringern untereinander und mit dem zentralen Auftraggeber wird über den zentralen Auftraggeber organisiert und in Form von technischen Kanälen ermöglicht.</p>
<p>Diese Kanäle können sein:</p>
<ul>
<li><p>Das zentrale OpenCOde Repository</p></li>
<li><p>Funktionspostfächer</p></li>
<li><p>Social-Media Kanäle</p></li>
<li><p>…</p></li>
</ul>
<p>Die Kooperation erfolgt als Service mit entsprechendem Qualitätslevel entsprechend vertraglicher Bindung der Beteiligten.</p></td>
</tr>
<tr class="odd">
<td>Lizenzmanagement</td>
<td><p>Entsprechend der vereinbarten Geschäftsprinzipien kommen nur OSI konforme Lizenzen in Frage, die ihrerseits auch technisch dadurch gefordert werden, dass ohne eine solche Lizenz das OpenCOde Source-Code Repository nicht befüllt werden darf.</p>
<p>Alle Nutzungsbedingungen und subskriptionsabhängige Bestandteile des SouvAP sind grundsätzlich zentral zu erfassen. Etwaige Konsequenzen für die Bereitstellung der Dienstleistungen des SouvAP sind von den Betreibern mit den Endkunden der Lösung auszuhandeln, als insbesondere die Verrechnung basierend auf den individuellen Leistungskatalogen der SouvAP-Produkte (SouvAP-Instanzen)</p></td>
</tr>
<tr class="even">
<td>Bestell- und Lieferung</td>
<td><p>Bestell- und Lieferprozesse sind entsprechend des Organisationsdesigns hierarchisch organisiert. Die entsprechenden Workflows folgen diesem Design.</p>
<p>Konzeptionsebene:</p>
<ul>
<li><p>Der zentrale Auftraggeber beauftragt die zu liefernden Module</p></li>
<li><p>Die OSS-Hersteller liefern sie</p></li>
<li><p>Der zentrale Auftraggeber beauftragt eine Beispielarchitektur</p></li>
<li><p>Ein Software-Konfektionierer erstellt die Beispielarchitektur</p></li>
<li><p>Die Software-Konfektionierer entwickeln Produkte des SouvAP, u.a. durch Modulauswahl</p></li>
</ul>
<p>Bereitstellungsebene</p>
<ul>
<li><p>Die Verwaltungsorganisation des Endkunden bestellt ein konkretes SouvAP Produkt bei einem der Software-Konfektionierer</p></li>
<li><p>Die Lieferung des SouvAP-Produktes erfolgt i.d.R. an einen Software-Betreiber.</p></li>
<li><p>Der Software-Betreiber liefert sein fertiges SouvAP Produkt an einen Plattform-Betreiber, der das SouvAP Produkt letztendlich als Service an den Endkunden in der Verwaltung liefert</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Abrechnung</td>
<td><p>Entsprechend der Bestell- und Lieferprozesse existieren verschiedene Abrechnungsmodelle und darauf basierende Workflows.</p>
<p>Allgemein gilt, dass die Abrechnung immer zwischen den tatsächlichen Vertragspartnern stattfindet, die ihrerseits eventuelle Zulieferungen mit in die eigenen Leistungskataloge einrechnen müssen.</p></td>
</tr>
<tr class="even">
<td>Betrieb Software Repository</td>
<td><p>Der SouvAP verwendet das zentrale OpenCode GitLab Software Repository. Es werden die üblichen Entwicklungsprozesse angenommen.</p>
<p>Es wird angestrebt, dass auch die Teile außerhalb der Software-Module selbst entwickelt werden, also z.B. Identitätsmanagement oder auch Portalanwendungen im OpenCode Repository verwaltet werden</p></td>
</tr>
<tr class="odd">
<td>LCM der Bestandteile des SouvAP</td>
<td><p>Das Konzept und das Branding des SouvAP wird zentral durch den Auftraggeber des SouvAP in einem LCM-Prozess verwaltet.</p>
<p>Für das Life Cycle Management aller Bestandteile des SouvAP, also auch der spezifisch konfektionierten Produkte, sind die entsprechenden Lieferanten zuständig.</p></td>
</tr>
</tbody>
</table>

### Lösungsdesign

Welche Designvorgaben lassen sich aus einer Geschäftsperspektive nun
bezüglich der bereitzustellenden SouvAP Lösung treffen, die sowohl für
die Anwendungs-, als auch Technologie- und Infrastrukturarchitektur
relevant sind?

Eine funktionale Betrachtung hat zu klären:

-   Welche Nutzer werden mit dem SouvAP später arbeiten, welche
    Nutzerprofile sind in der öffentlichen Verwaltung üblich?

-   Was sind die Rahmenbedingungen der Nutzung, z.B. mit welchen Geräten
    in welchen Umgebungen werden die Mitarbeiter der öffentlichen
    Verwaltung aktiv sein?

-   Welche Anwendungsfeatures werden die Nutzer benötigen, um die
    geforderten Business-Capabilities besser zu unterstützen?

Eine Betrachtung der Entwicklung und Bereitstellung hat zu klären:

-   Welche Komponenten werden prinzipiell die benötigten
    Anwendungsfeatures bereitstellen?

-   Welche dieser Komponenten sind typischerweise bei den beteiligten
    Partnern bereits vorhanden oder müssen neu entwickelt werden?

-   Gibt es spezielle Anforderungen an diese Komponenten aus einer
    Geschäftsperspektive der Beteiligten und wie werden diese
    Komponenten geliefert und bereitgestellt?

-   Wie muss das Gesamtsystem des SouvAP aussehen und wie wird es
    gebildet?

-   Wie wird dieses Gesamtsystem den Mitarbeitern der öffentlichen
    Verwaltung aus zugänglich gemacht?

#### Geräteklassen und Nutzungsprofile der IT-Lösung

Der Rolle ‚Software Nutzer‘ lassen sich verschiedene in der öffentlichen
Verwaltung und deren Kunden übliche Geräteklassen zuordnen, mit denen
die Mitarbeiter den SouvAP nutzen könnten.

Daraus wiederum lassen sich Nutzerprofile ableiten, die für die
Referenzarchitektur und die entsprechenden Nutzungsszenarien
berücksichtigt werden müssen.

![](media/file16.png)

Abbildung 17: Nutzerprofile und Geräteklassen

Die verschiedenen relevanten Kategorien:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Kategorie/Attribute</strong></th>
<th><strong>Ausprägung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Geräteklasse</td>
<td><ul>
<li><blockquote>
<p>Stationärer PC-Nutzer (PC in der Verwaltung)</p>
</blockquote></li>
<li><blockquote>
<p>Notebook Nutzer (Laptop der Verwaltung)</p>
</blockquote></li>
<li><blockquote>
<p>Thin-Client (PC/Laptop ohne eigenes OS)</p>
</blockquote></li>
<li><blockquote>
<p>Tablet, Smartphone (Android, iOS Geräte)</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Gerätemanagement</td>
<td><ul>
<li><blockquote>
<p>Managt (werden von der IT-Organisation verwaltet)</p>
</blockquote></li>
<li><blockquote>
<p>Unmanagt (nicht verwaltete Geräte, z.B. private)</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Desktop Hosting</td>
<td><ul>
<li><blockquote>
<p>Lokal (lokales OS)</p>
</blockquote></li>
<li><blockquote>
<p>Lokal virtualisiert (OS in einer lokalen VM)</p>
</blockquote></li>
<li><blockquote>
<p>Gehostet (OS entfernt gehostet, z.B. WTS, VDI)</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Hosting<br />
Digitalpaket/SouvAP</td>
<td><ul>
<li><blockquote>
<p>Lokal installierte Module</p>
</blockquote></li>
<li><blockquote>
<p>Entfernt installierte Module mit Portalzugang</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Hosting Fachverfahren</td>
<td><ul>
<li><blockquote>
<p>Lokal genutzte Fachverfahren</p>
</blockquote></li>
<li><blockquote>
<p>Gehostete Fachverfahren</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td>Zugang</td>
<td><ul>
<li><blockquote>
<p>Verwaltungsnetz (interner Zugang, geschützte Zugänge)</p>
</blockquote></li>
<li><blockquote>
<p>Öffentliches Netz (normales Internet)</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td>Schutzbedarf</td>
<td><ul>
<li><blockquote>
<p>Normal</p>
</blockquote></li>
<li><blockquote>
<p>Hoch</p>
</blockquote></li>
<li><blockquote>
<p>Sehr hoch</p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle 15: Kategorien/ Attribute

Anmerkung: Der SouvAP ist auf eine webbasierte Lösung ausgerichtet.
Lokale Installationen des SouvAP sind demnach nicht Stand der
Betrachtungen und sind nicht Teil der Referenzarchitektur.

Für die zu entwickelnden Anwendungsszenarien lassen sich u.a. folgende,
durch Betrachtung bei den IT-Dienstleistern gewonnene in der
öffentlichen Verwaltung typische Nutzerprofile ableiten.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Nutzerprofil</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Verwaltungsmitarbeiter<br />
Minimaler Arbeitsplatz</td>
<td><p>Mitarbeiter, der mit Thin-Clients arbeitet und minimale Office-Aktivitäten durchführt</p>
<ul>
<li><p>Nutzung einiger SouvAP Module</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Verwaltungsmitarbeiter<br />
Standardarbeitsplatz</td>
<td><p>Mitarbeiter der Verwaltung mit eigenem stationären Arbeitsplatz und PC</p>
<ul>
<li><p>Normale Nutzung des SouvAP mit allen Modulen</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Verwaltungsmitarbeiter<br />
Erweiterter Arbeitsplatz</td>
<td><p>Mitarbeiter, der neben dem SouvAP auch andere, z.B. Entwickleraufgaben übernimmt</p>
<ul>
<li><p>Normale Nutzung des SouvAP in unterschiedlichen Umgebungen</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Mobiler<br />
Verwaltungsmitarbeiter</td>
<td><p>Mitarbeiter, der vorrangig mobile Endgeräte nutzt, Smartphone und Tablett; diese werden vom IT-Betrieb gemanaged</p>
<ul>
<li><p>Nutzung des SouvAP in allen angebotenen Modulen</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Verwaltungsmitarbeiter<br />
Privat-PC</td>
<td><p>Nutzung eines Privat-PC</p>
<ul>
<li><p>Nutzung des SouvAP in allen angebotenen Modulen</p></li>
</ul></td>
</tr>
<tr class="even">
<td>Verwaltungsmitarbeiter<br />
BYOD</td>
<td><p>Ungemanagte Nutzung von mobilen Endgeräten</p>
<ul>
<li><p>Nutzung ausgewählter Module des SouvAP</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Nutzer außerhalb der Verwaltung (u.a. Mitarbeiter von Unternehmen, mit denen Informationen geteilt werden)</td>
<td><p>Ungemanagte Nutzung von mobilen Endgeräten sowie Privat-PC</p>
<ul>
<li><p>Nutzung ausgewählter Module (z.B. Videokonferenzen)</p></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle 16: Nutzerprofile

#### Zuordnung der Nutzerprofile zu den Geräteklassen

Eine detaillierte Zuordnung der Nutzerprofile zu den Geräteklassen sieht
dann so aus:

<table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th></th>
<th><strong>Geräte<br />
klasse</strong></th>
<th><strong>Geräte Management</strong></th>
<th><strong>Desktop Hosting</strong></th>
<th><strong>Hosting<br />
Digital<br />
paket</strong></th>
<th><strong>Hosting Fachverfahren</strong></th>
<th><strong>Zugang</strong></th>
<th><strong>Schutzbedarf</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><strong>VMa<br />
minimal</strong></td>
<td><ul>
<li><blockquote>
<p>Thin Client</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>managt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted (WTS, VDI)</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>intern</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
<li><blockquote>
<p>hoch</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td><strong>VMa<br />
standard</strong></td>
<td><ul>
<li><blockquote>
<p>Stationärer PC</p>
</blockquote></li>
<li><blockquote>
<p>Notebook</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>managt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted (WTS, VDI)</p>
</blockquote></li>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>intern</p>
</blockquote></li>
<li><blockquote>
<p>öffentlich</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
<li><blockquote>
<p>hoch</p>
</blockquote></li>
<li><blockquote>
<p>sehr hoch</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td><strong>VMa<br />
erweitert</strong></td>
<td><ul>
<li><blockquote>
<p>Stationärer PC</p>
</blockquote></li>
<li><blockquote>
<p>Notebook</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>managt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted (WTS, VDI)</p>
</blockquote></li>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Lokal virtualisiert</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>intern</p>
</blockquote></li>
<li><blockquote>
<p>öffentlich</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
<li><blockquote>
<p>hoch</p>
</blockquote></li>
<li><blockquote>
<p>sehr hoch</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td><strong>VMa<br />
mobil</strong></td>
<td><ul>
<li><blockquote>
<p>Tablett, Smartphone</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>managt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><blockquote>
<p>-</p>
</blockquote></td>
<td><ul>
<li><blockquote>
<p>intern</p>
</blockquote></li>
<li><blockquote>
<p>öffentlich</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
<li><blockquote>
<p>hoch</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="odd">
<td><strong>VMa<br />
privat-PC</strong></td>
<td><ul>
<li><blockquote>
<p>Notebook</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>unmanagt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Gehosted (WTS, VDI)</p>
</blockquote></li>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><blockquote>
<p>Gehosted</p>
</blockquote></td>
<td><ul>
<li><blockquote>
<p>öffentlich</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
</ul></td>
</tr>
<tr class="even">
<td><strong>VMa<br />
mobil BYOD</strong></td>
<td><ul>
<li><blockquote>
<p>Tablett, Smartphone</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>unmanagt</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>Lokal</p>
</blockquote></li>
<li><blockquote>
<p>Gehosted</p>
</blockquote></li>
</ul></td>
<td><blockquote>
<p>-</p>
</blockquote></td>
<td><ul>
<li><blockquote>
<p>öffentlich</p>
</blockquote></li>
</ul></td>
<td><ul>
<li><blockquote>
<p>normal</p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

Tabelle 17: Zuordnung der Nutzerprofile zu den Geräteklassen

<span class="underline">Anmerkung:</span> Nicht alle Nutzerprofile
werden in den ersten Versionen des SouvAP gleichermaßen Berücksichtigung
finden. Der Fokus liegt hier auf dem Standard-Verwaltungsmitarbeiter mit
seinem stationären PC oder Laptop. Zusätzlich wird die mobile Nutzung
des SouvAP sichergestellt mit einer speziellen Betrachtung der dort
angebotenen Module.

Anmerkung: Da die Situation der Bereitstellung von IT-Dienstliestungen
in den unterschiedlichen Verwaltungen unterschiedlich erfolgt, werden
keine Annahmen zur Bereitstellung der Geräteklassen getroffen und sind
nicht Teil des Umfangs der Architekturbetrachtungen.

### Übersicht der zu liefernden Funktionalitäten

Für die Roadmap der Module und zu liefernden Funktionalitäten des SouvAP
sind entsprechend der Geschäftsstrategie die gewünschten
Geschäfts-Capabilities zu entwickeln.

Diese wiederum haben sowohl die definierten Treiber, Geschäftsziele,
Geschäftsprinzipien als auch die zu erwartenden Anforderungen aus der
Arbeitsweise der Verwaltungen zu berücksichtigen.

Üblicherweise werden die Geschäfts-Capabilities in einem Reifemodell
entwickelt, in dem vom erhobenen Ist-Reifegrad ausgehend, die Zielreife
der jeweiligen Geschäfts-Capabilities bestimmt wird.

Da für den SouvAP weder eine umfassende Ist-Aufnahme möglich ist, noch
eine exakte Ziel-Reife bestimmt werden kann, ist es sinnvoll, zumindest
die besonders maßgeblichen Geschäfts-Capabilities aus
Nutzungsperspektive entsprechend Nutzungsszenarien der Verwaltungsarbeit
hervorzuheben, aus denen die funktionalen Module abgeleitet werden
können. Eine Gruppierung nach Verbindlichkeit und Priorität folgt dieser
Methodik.

Annahme:

-   Es werden entsprechend des Verwaltungsauftrages die essenziellen
    **Basis-Fähigkeiten** beschrieben, die unbedingt zu berücksichtigen
    sind. Das schließt die Berücksichtigung neuerer Trends in der
    Verwaltungsarbeit mit ein

-   Es werden die **erweiterten Fähigkeiten** benannt, die eine zu
    erwartende Nachfrage widerspiegeln

#### Priorisierung Tätigkeitsspektrum der Verwaltung

Ausgehend von Beobachtungen innerhalb der öffentlichen Verwaltung kann
eine Annahme bezüglich einer Priorisierung der Tätigkeiten vorgenommen
werden, bei der die Nutzung des SouvAP besonders nützlich und dringend
erscheint:

![](media/file17.png)

Abbildung Priorisierte Tätigkeiten

Es ist hier zu beachten, dass die Priorisierung und Verbindlichkeit
ausschließlich aus den ausgeübten Tätigkeiten abgeleitet werden und
keine Implementierungsroadmap bedeuten, die auch von anderen Faktoren
abhängt (siehe u.a. Kap-E).

Des Weitern ist jede Vorgabe mit einer Verbindlichkeit gemäß [<span
class="underline">RFC 2119</span>](https://www.ietf.org/rfc/rfc2119.txt)
geprägt, wird also mit einem Verbindlichkeitsgrad muss, soll, kann oder
darf nicht assoziiert.

Es gilt die Annahme, dass eine hohe Verbindlichkeit eine hohe
Priorisierung nahelegt.

Tabelle : Verbindlichkeit nach Tätigkeitsspektrum

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><strong>Allgemeine Büro- und Verwaltungstätigkeiten</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusAct-01</td>
<td>Vereinbarung und Überwachung von Terminen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusAct-02</td>
<td>Verwaltung von Ressourcen (z.B. physische Konferenzräume, Dienstautos, ..)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusAct-03</td>
<td>Verwaltungstechnischer Schriftverkehr</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusAct-04</td>
<td>Veraktung der Verwaltungsvorgänge</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusAct-05</td>
<td>Erstellung von Angeboten und Aufträgen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusAct-06</td>
<td>Durchführung von IT-Fachverfahren</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Projektartige- und Teamtätigkeiten</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>BusAct-07</td>
<td>Durchführung von Telefon- und Webkonferenzen in virtuellen Teams</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusAct-08</td>
<td>Umfragen und Abstimmungen in virtuellen Teams</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>BusAct-09</td>
<td>Gemeinsame Erstellung von Content im Team</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusAct-10</td>
<td>Projektmanagement</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>BusAct-11</td>
<td>Instand Messaging</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusAct-12</td>
<td>Gemeinsame Team-Nutzung von Dateien, Dokumenten und Content</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Übergeordnete Tätigkeiten in der Verwaltungsorganisation</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusAct-13</td>
<td>Messaging und E-Mail in den Verwaltungsorganisationen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusAct-14</td>
<td>Intranet und Verwaltungsportale</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusAct-15</td>
<td>Finden und Vernetzen der Mitarbeiter</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusAct-16</td>
<td>Self-Service (HR, ERP, CRM)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusAct-17</td>
<td>Mitarbeit in Foren und Ideenplattformen</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusAct-18</td>
<td>Verwaltungs- und Organisationsmitteilungen</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusAct-19</td>
<td>Blogs und Microblogging</td>
<td></td>
</tr>
</tbody>
</table>

#### Priorisierung der Business-Capabilities

Welche der prinzipiell mit einem SouvAP assoziierten Business
Capabilities werden mit den Funktionen und Features der zu entwickelnden
Module besonders unterstützt werden?

Es wird im folgenden, basierend auf Beobachtungen in der Öffentlichen
Verwaltung eine Annahme dazu getroffen. Diese ist später durch ein
fundiertes Anforderungsmanagement des Auftraggebers zu überarbeiten.

Business Capabilities die Funktionen des SouvAP selbst betreffend (rote
Markierung) bedeutet, dass die Unterstützung ein Muss ist.

![](media/file18.png)

Abbildung Priorisierung Business Capabilities

Tabelle : Verbindlichkeit nach Capabilities - Nutzung

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><strong>Kommunikation</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Fkt-01</td>
<td>Erreichen des gewünschten Publikums</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-02</td>
<td>Auswahl eines geeigneten Kanals</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-03</td>
<td>Interaktion mit dem Publikum</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-04</td>
<td>Vertrauen und Sicherheit in der Kommunikation</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-05</td>
<td>(persönliche) Kommunikationserfahrung</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Netzwerken</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Fkt-06</td>
<td>Verwaltung eigener Informationen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-07</td>
<td>Suchen und Entdecken</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-08</td>
<td>Verbinden und Interaktion</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-09</td>
<td>Informationen und Wissen Teilen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-10</td>
<td>Förderung von (persönlichen) Fähigkeiten und Themen</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Organisation und Planung</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Fkt-11</td>
<td>Zeitorganisation</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-12</td>
<td>Management von Aktivitäten</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-13</td>
<td>Planung der Mitarbeiter und Ressourcen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Management und Zugriff auf Inhalte</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Fkt-14</td>
<td>Erstellung und Erfassung von Inhalten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-15</td>
<td>Editieren und Zusammenarbeit</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-16</td>
<td>Veröffentlichung und LCM</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-17</td>
<td>Suche und Navigation</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-18</td>
<td>Feedback und Empfehlungen</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Durchführung von Businessaufgaben</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Fkt-19</td>
<td>Durchführung von Fachverfahren</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-20</td>
<td>Verwaltung von ERP Informationen</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-21</td>
<td>Zugriff auf Anwendungen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Fkt-22</td>
<td>Arbeitsplatzerfahrung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Fkt-23</td>
<td>Persönliche Produktivität</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Business Capabilities die Bereitstellung des SouvAP selbst betreffend:

Die Verwaltungsvorschriften bezüglich der Bereitstellung der IT der
Kommunen, der Länder und des Bundes sind über verschiedene Regelwerke
normiert. Die entsprechenden abgeleiteten Business Capabilities sind
dementsprechend i.d.R. Muss-Kriterien in ihrer Verbindlichkeit und damit
keiner gesonderten Priorisierung unterworfen.

Tabelle : Verbindlichkeit nach Capabilities - Bereitstellung

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><strong>Infrastruktur und Betrieb</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Del-01</td>
<td>Housing und Infrastruktur</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-02</td>
<td>Cloud Dienste</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-03</td>
<td>Plattform Management</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-04</td>
<td>Integration</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-05</td>
<td>Umgebungsbereitstellung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-06</td>
<td>Monitoring</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>IT Management</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>BusCap-Del-07</td>
<td>Geräte Management</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-08</td>
<td>Portale und Publizierung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-09</td>
<td>Identitäts- und Zugriffsmanagement</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-10</td>
<td>Anwendungsmanagement SLM</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-11</td>
<td>IT Support</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-12</td>
<td>Schulung und Fortbildung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-13</td>
<td>Projekt Management</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Zentrale Dienste</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>BusCap-Del-14</td>
<td>Bedarf und Kapazitäten</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-15</td>
<td>Verwaltung von Lizenzen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-16</td>
<td>Beschaffung und Abrechnung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-17</td>
<td>Angebots Management</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-18</td>
<td>Vertrags Management</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Del-19</td>
<td>Informations Sicherheit</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Del-20</td>
<td>Geschäfts Reporting</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Business Capabilities der Steuerung und Produktentwicklung betreffend:

Tabelle : Verbindlichkeit nach Capabilities - Steuerung und Entwicklung

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><strong>Strategische Entwicklung</strong></td>
<td></td>
</tr>
<tr class="even">
<td>BusCap-Ent-01</td>
<td>Architektur und Standards</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-02</td>
<td>Produkt Beschreibung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-03</td>
<td>Kooperation und Partnerschaft</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-04</td>
<td>Marketing</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-05</td>
<td>Projekt Koordination</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-06</td>
<td>Projekt Governance</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-07</td>
<td>Vendor Management</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-08</td>
<td>Projekt Beratung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Produkt Entwicklung</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>BusCap-Ent-09</td>
<td>Anforderungs Management</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-10</td>
<td>Anwendungsdesign und Modulauswahl</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-11</td>
<td>Anwendungsentwicklung Customizing</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-12</td>
<td>Konfigurations Management</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-13</td>
<td>Change Management</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>BusCap-Ent-01</td>
<td>Release Management</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>BusCap-Ent-01</td>
<td>Qualitäts Management</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Ableitung der bereitzustellenden Lösungskomponenten

Die aus den Verwaltungstätigkeiten und Business Capabilities des SouvAP
abgeleiteten Funktionalitäten lassen sich als abstrakte
Lösungskomponenten beschreiben:

![](media/file19.png)

Abbildung Funktionale Komponenten

Abgeleitete funktionale Komponenten des SouvAP sind demnach:

**Strategische Entwicklung und Bereitstellung der Module**

Tabelle : Verbindlichkeit - Strategische Entwicklung und Bereitstellung
der Module

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Func-Stg-01</td>
<td>Modul-Repositories</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Stg-02</td>
<td>Dokumentationen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Stg-03</td>
<td>Beispielimplementierung</td>
<td>Muss</td>
</tr>
</tbody>
</table>

**Produkt- und IT-Service Bereitstellung**

Tabelle : Verbindlichkeit - Produkt- und IT-Service Bereitstellung

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Func-Prd-01</td>
<td>IAM Identitäts und Zugriffsmanagement</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Prd-02</td>
<td>Single- Sign- On</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Prd-03</td>
<td>Infrastruktur (Cloud)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Prd-04</td>
<td>IT-Services</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Prd-05</td>
<td>Finanzen und Abrechnung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Prd-06</td>
<td>Lizenzverwaltung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Prd-07</td>
<td>Software Verteilung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Prd-08</td>
<td>Software Entwicklung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Prd-09</td>
<td>IT-Fachverfahren</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Prd-10</td>
<td>ERP</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Prd-11</td>
<td>Unternehmenssuche</td>
<td>Muss</td>
</tr>
</tbody>
</table>

**Produktnutzung**

Tabelle : Verbindlichkeit - Produktnutzung

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Func-Usr-01</td>
<td>Portal und UX (Nutzererfahrung)</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Web-Office und File-Sharing</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Func-Usr-02</td>
<td>Textverarbeitung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Usr-03</td>
<td>Tabellenkalkulation</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Usr-04</td>
<td>Präsentation</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Usr-05</td>
<td>Gemeinsame Dateien</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td></td>
<td><strong>Kommunikation</strong></td>
<td></td>
</tr>
<tr class="even">
<td>Func-Usr-06</td>
<td>Audio- und Videokonferenzen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Usr-07</td>
<td>Kurznachrichten</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-08</td>
<td>News-Stream</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-09</td>
<td>Live-Stream</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-10</td>
<td>Foren und Mailinglisten</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>Func-Usr-11</td>
<td>Micro-Blogging</td>
<td>Soll</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Groupware</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Func-Usr-12</td>
<td>E-Mail</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Usr-13</td>
<td>Persönlicher Kalender</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Usr-14</td>
<td>Gruppenkalender</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Usr-15</td>
<td>Aufgaben</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Usr-16</td>
<td>Addressbücher</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Persönliche Produktivität</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Func-Usr-17</td>
<td>Medienwiedergabe</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>Func-Usr-18</td>
<td>Dateikompression</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>Func-Usr-19</td>
<td>Anti-Virus</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-20</td>
<td>Kennwortverwaltung (*)</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-21</td>
<td>PDF-Anzeige</td>
<td>Muss</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Content- und Dokumentenmanagement</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Func-Usr-22</td>
<td>Content Management</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-23</td>
<td>Lernmanagement</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-24</td>
<td>Wissensmanagement</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-25</td>
<td>Video-Contentmanagement</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-26</td>
<td>Dokumentenmanagement</td>
<td>Soll</td>
</tr>
<tr class="even">
<td></td>
<td><strong>Zusammenarbeit</strong></td>
<td></td>
</tr>
<tr class="odd">
<td>Func-Usr-27</td>
<td>Umfragen, Abstimmungen</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-28</td>
<td>Whiteboards</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-29</td>
<td>Visualisierungen</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-30</td>
<td>Feedback</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>Func-Usr-31</td>
<td>Schwarze Bretter</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>Func-Usr-32</td>
<td>Projektmanagement</td>
<td>Soll</td>
</tr>
</tbody>
</table>

(\*) derzeit unklar

### Beschreibung der relevanten Informationsdomänen

Das Informationsmodell des SouvAP soll einen Überblick verschaffen über
die wesentlichen Infor-mationen, die in der Organisation verarbeitet
werden.

Allgemein lassen sich die Informationen, die innerhalb der öffentlichen
Verwaltung verarbeitet werden in verschiedene Kategorien,
Informationsdomänen zusammenfassen:

![](media/file20.png)

(Quelle: Architekturteam)

Tabelle : Informationsdomänen

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Informationsdomäne</strong></th>
<th><strong>Beschreibung</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Steuerung der Verwaltung</td>
<td><blockquote>
<p>Alle Informationen der Steuerung der jeweiligen Verwaltungsorganisation selbst, z.B. Pläne, Strategien, usw.</p>
</blockquote></td>
</tr>
<tr class="even">
<td>Gesetze und Regularien</td>
<td><blockquote>
<p>Alle Gesetze und Regularien, die die Arbeit der Verwaltung berühren</p>
</blockquote></td>
</tr>
<tr class="odd">
<td>Verwaltungsorganisation</td>
<td><blockquote>
<p>Alle Informationen, die die Struktur und Arbeitsweise der Verwaltung zum Inhalt haben</p>
</blockquote></td>
</tr>
<tr class="even">
<td>Mitarbeiter der Verwaltung</td>
<td><blockquote>
<p>Alle Mitarbeiterinformationen, i.d.R. persönliche Daten</p>
</blockquote></td>
</tr>
<tr class="odd">
<td>Finanzen</td>
<td><blockquote>
<p>Alle Informationen der Finanzen und Abrechnung der Verwaltungseinheiten, z.B. Buchhaltung, Bestellungen, usw.</p>
</blockquote></td>
</tr>
<tr class="even">
<td>IT</td>
<td><blockquote>
<p>Alle im Rahmen der Digitalisierung und des IT Betriebes anfallende Informationen</p>
</blockquote></td>
</tr>
<tr class="odd">
<td>Verwaltungskunden</td>
<td><blockquote>
<p>Kundeninformationen der Verwaltung, z.B. Bürger und Betriebe</p>
</blockquote></td>
</tr>
<tr class="even">
<td>Bestandsdaten</td>
<td><blockquote>
<p>Alle Daten über die Liegenschaften, Geografie usw., die Verwaltung betreffend</p>
</blockquote></td>
</tr>
<tr class="odd">
<td>Fachinformationen der Ressorts</td>
<td><blockquote>
<p>Die Verwaltungsvorgänge der Ressorts auf jeder Verwaltungsebene betreffend</p>
</blockquote></td>
</tr>
</tbody>
</table>

Der SouvAP betrifft alle Formen der Kommunikation, Zusammenarbeit und
Schriftverkehr der öffentlichen Verwaltung. Es sind also Informationen
aller Informationsdomänen grundsätzlich betroffen und es werden
dementsprechend Daten aller Domänen erhoben. Eine spezifische
Gruppierung ist nicht möglich, bzw. es müssen alle Informationen und
daher die Konsequenzen der Verwendung von vornherein mitgedacht werden.

**Informationsklassifizierung** entsprechend BSI-Grundschutz:

Es gelten für alle Phasen der Entwicklung, Bereitstellung und Betrieb
des SouvAP grundsätzlich die entsprechenden BSI Vorschriften:

-   BSI Standard 200-2, Kapitel 8.2 Schutzbedarfsfeststellung  
    (Klassen: normal, hoch, sehr hoch)

-   BSI Standard 200-2, Kapitel 5.1 Klassifikation von Informationen  
    (Klassifizierung: öffentlich, intern, vertraulich, streng
    vertraulich)

Es gibt keine dedizierten Vorgaben, welche SouvAP Produkte welchen
konkreten Klassen und Klassifizierungen genügen müssen, sondern sind mit
den jeweiligen Produktkunden abzustimmen, bzw. gesondert zu
zertifizieren.

Neben den Sicherheitsbetrachtungen sind auch andere Regularien auf die
Entwicklung, Bereitstellung und Betrieb anwendbar, z.B. aus dem
Wettbewerbsrecht, dem Persönlichkeitsrecht usw.

Was die Nutzung des SouvAP anbelangt, liegt es in der Natur der Sache,
dass ein SouvAP Produkt nicht allen Regularien immer vollumfänglich von
vornherein und für alle Anwendungsfälle genügen kann. Hier sind ggf.
spezielle Kundenlösungen mit speziellem Bedarf von den Beteiligten zu
konzipieren und zu entwickeln.

Geschäftstransformation
-----------------------

### Priorisierung der Aktivitäten

Die Priorisierung der zu entwickelnden Business Capabilities wurde im
Abschnitt der zu liefernden Funktionalitäten vorgestellt und folgt den
in der Strategie festgelegten Kernfunktionalitäten, die mit dem SouvAP
verbunden sind.

Siehe 5.3.8.2

### Implikationen auf die bestehenden Geschäftsmodelle der Beteiligten

Mit der Einführung des SouvAP entsteht für eine Reihe der beschriebenen
Capabilities eine fachliche und technische Umsetzung, die die Arbeit in
der öffentlichen Verwaltung nachhaltig verändern soll.

Diese neue Lösung bedeutet zumindest eine mehr oder weniger große
Veränderung in den bestehenden Geschäftsprozessen der beteiligten
Akteure:

Tabelle : Implikationen auf die Geschäftsmodelle

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>Akteure</strong></th>
<th><strong>Veränderung und Implikationen</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>BMI/Zendis</p>
<p>(Rolle: Auftraggeber des SouvAP)</p></td>
<td><p>Mit dem Zendis entsteht eine eigene Rechtsform, die den SouvAP zentral steuern und vermarkten wird.</p>
<p>Die Lösung entsteht in Wettbewerb zu sehr etablierten Marken mit ähnlicher Zielgruppe, d,h. das Zendis wird von Anfang an in einer Position sein, die sich an den am Markt etablierten Alternativen messen lassen muss, mit einer erheblichen Erwartungshaltung.</p>
<p>Das betrifft nicht nur die Funktionalität, sondern auch sämtliche Steuerungsprozesse und ebenfalls auch die technische Implementierung in allen Aspekten, also auch UX, Performance und Sicherheit.</p>
<p>Über eine im Konzept und Basisausführung einheitliche Lösung wird die Standardisierung der IT in der gesamten öffentlichen Verwaltung souverän vorangetrieben.</p></td>
</tr>
<tr class="even">
<td><p>Deutsche Verwaltung</p>
<p>(Rollen: Software Nutzer, Auftraggeber Kunden)</p></td>
<td><p>Die Nutzer der Lösung haben eine erhebliche Erwartungshaltung gegenüber der Lösung, bei gleichzeitiger Erfahrung mit bestehenden Lösungen der Marktalternativen.</p>
<p>Sie erhalten sowohl eine neue Lösung entsprechend der beschriebenen Funktionalitäten, als auch die Art und Weise der Arbeit ändert sich für viele, z.B. durch ein Bereitstellungsmodell mittels Cloud-Services.</p>
<p>Die angestrebten Lösungen werden flexibler durch die spezielle Konfektionierung zugeschnittener Module als auch durch standardisierte Schnittstellen, die eine bessere Integration in bestehende und zukünftige IT-Fachverfahren erlauben.</p>
<p>Das macht die Lösung u.U. für die tradierte Beschaffung komplexer aber auch flexibler und moderner in der Abrechnung.</p>
<p>Durch den Anspruch des souveränen Arbeitsplatzes ergeben sich neue Einsatzszenarien entsprechend Risikomanagement der Verwaltungen und Erreichung von Schutzzielen</p></td>
</tr>
<tr class="odd">
<td>Kunden der Verwaltung<br />
(Rolle: sekundärer Software Nutzer)</td>
<td><p>Die Kunden profitieren i.d.R. nicht direkt von der neuen Lösung, aber durch die mit dem Souveränitätsgedanken einhergehende Fokussierung auf Open Source ist auch damit zu rechnen, dass der kommunikative Austausch mit der Verwaltung auch jenseits bestehender closed Varianten möglich wird.</p>
<p>Andere eher implizite Veränderungen ergeben sich aus der damit möglichen Nutzung und damit Vereinheitlichung der Softwarelösungen auch durch die privaten Anwender.</p></td>
</tr>
<tr class="even">
<td><p>IT-Dienstleister des Bundes, der Länder und Kommunen</p>
<p>(Rollen: Software Betreiber, Plattform Betreiber, Software Konfektionierer, Software Lieferant)</p></td>
<td><p>Die IT-Dienstleister integrieren ihre eigenen Serviceleistungen zunehmend in übergeordnete Strukturen, wie z.B. die DVS, EfA Leistungen usw., sowohl organisatorisch, prozessual als auch technisch-architektonisch und selbst kulturell.<br />
Dies zieht Veränderungen in allen diesen Handlungsebenen nach sich.<br />
Prominente Beispiele sind u.a.</p>
<ul>
<li><blockquote>
<p>Veränderte und komplexere Leistungskataloge, die zukünftig evtl. auch pay-per-use berücksichtigen müssen</p>
</blockquote></li>
<li><blockquote>
<p>Komplexere Strukturen der Leistungserbringung durch zunehmende Verflechtung der Herstellungs- und Bereitstellungsprozesse<br />
(siehe u.a. gemeinsames Code-Repository, gemeinsame CI/CD Pipeline)</p>
</blockquote></li>
<li><blockquote>
<p>asynchrone und kürzere Innovationszyklen der beteiligten Komponenten und Softwaremodule</p>
</blockquote></li>
</ul>
<p>Die Leistungserbringung erfolgt mit dem Fokus auf digitale Souveränität. Dies bedeutet ebenfalls auf der einen Seite direkte Vorteile von z.B. hauptsächlich genutzten OpenSource Lizenzen, erfordert aber einen vermutlich höheren Aufwand im Management der Lieferantenbeziehungen und der eigenen Supportmodelle, die nicht immer auf feste Geschäftspartner setzen kann, sondern volatile Communities beinhalten wird.</p></td>
</tr>
<tr class="odd">
<td><p>IT-Dienstleister der Privatwirtschaft und Communities</p>
<p>(Rolle: Software Hersteller/Lieferant)</p></td>
<td><p>Die Einführung des SouvAP bedeutet für die Software Hersteller und Lieferanten, den Bedingungen des zentralen Auftraggebers genügen zu müssen. Diese sind sowohl technischer Natur:</p>
<ul>
<li><blockquote>
<p>Cloud-native Lauffähigkeit der Module in der in Entwicklung begriffenen DVC (ehem. DVS, Deutsche Verwaltungscloud)</p>
</blockquote></li>
<li><blockquote>
<p>Austauschbarkeit und Interoperabilität zu anderen Modulen</p>
</blockquote></li>
<li><blockquote>
<p>Erweiterte Sicherheitsanforderungen an die Module entsprechend BSI-Grundschutz, NIST und vorhandener Best Practises, z.B. in der Containerbereitstellung</p>
</blockquote></li>
<li><blockquote>
<p>Fähigkeit zur Skalierung für große Verwaltungsorganisationen</p>
</blockquote></li>
<li><blockquote>
<p>Offenlegung der Softwareentwicklungsprozesse</p>
</blockquote></li>
</ul>
<p>oder haben einen prozessual-organisatorischen Hintergrund, z.B. die Einhaltung gesetzlicher Vorschriften und Richtlinien</p>
<ul>
<li><blockquote>
<p>Vergabe- und Wettbewerbsrecht</p>
</blockquote></li>
<li><blockquote>
<p>ITK Beschaffungsrichtlinie</p>
</blockquote></li>
<li><blockquote>
<p>Datenschutzrecht</p>
</blockquote></li>
<li><blockquote>
<p>Urheberrecht</p>
</blockquote></li>
<li><blockquote>
<p>Berücksichtigung der Erfordernisse zur Barrierefreiheit</p>
</blockquote></li>
<li><blockquote>
<p>Die komplexe Struktur der öffentlichen Verwaltung</p>
</blockquote></li>
</ul>
<p>Daneben gilt es, Voraussetzungen zu schaffen, dass die OSI konforme Software auch tatsächlich unter den vorherrschenden Bedingungen laufen kann und darf</p>
<ul>
<li><blockquote>
<p>Vollständige Produkte, einschließlich der notwendigen Produkt-, Service und Schulungsdokumentationen</p>
</blockquote></li>
<li><blockquote>
<p>Anbieten geeigneter Service- und Supportmodelle, mit klaren SLA</p>
</blockquote></li>
<li><blockquote>
<p>Mitwirkungspflichten zu Zertifizierungen der Produkte</p>
</blockquote></li>
</ul></td>
</tr>
</tbody>
</table>

### Geschäftsrisiken

Die Konzeption, Entwicklung, Einführung und der Betrieb des SouvAP ist
mit einer Reihe von Risiken verbunden

#### Einführung des SouvAP als Teil der Digitalisierung

Der SouvAP ist mehr als die Einführung eines abgegrenzten
IT-Fachverfahren, sondern ist als Teil der Digitalisierung der
öffentlichen Verwaltung als Ganzes zu verstehen. Dies geht mit einer
Reihe Risiken einher:

-   **Datenschutz und Datensicherheit:** Mit der zunehmenden
    Digitalisierung und Verknüpfung von Arbeitsvorgängen sammelt und
    verarbeitet die öffentliche Verwaltung immer mehr Daten. Das Risiko
    von Datenschutzverletzungen und Datenlecks steigt entsprechend. Es
    ist wichtig, angemessene Sicherheitsmaßnahmen zu implementieren und
    den Schutz personenbezogener Daten sicherzustellen.

-   **Cybersecurity:** Die öffentliche Verwaltung ist ein attraktives
    Ziel für Cyberangriffe, da sie eine Vielzahl sensibler Informationen
    und kritischer Infrastrukturen verwaltet. Die zunehmende
    Digitalisierung, wie die hier adressierte Zurverfügungstellung einer
    ganzen Produktklasse als SouvAP erhöht das Risiko von
    Cyberangriffen, wie z.B. Datenmanipulation, Ransomware oder
    Identitätsdiebstahl. Ein effektives Cybersecurity-Framework ist
    entscheidend, um solche Angriffe zu verhindern und angemessen darauf
    zu reagieren.

-   **Mangelnde IT-Kompetenz:** Die Entwicklung, Bereitstellung und
    Betrieb des SouvAP erfordert spezifisches technisches Know-how und
    Fähigkeiten, die zwar in der Regel bei den Softwareherstellern und
    IT-Dienstleistern, aber möglicherweise nicht in ausreichendem Maße
    in allen Teilen der öffentlichen Verwaltung vorhanden sind. Ein
    Mangel an qualifizierten IT-Fachkräften und die Notwendigkeit einer
    kontinuierlichen Weiterbildung stellen ein Risiko für eine
    erfolgreiche Einführung des SouvAP in der öffentlichen Verwaltung
    dar.

-   **Widerstand gegen Veränderungen:** Die Einführung neuer digitaler
    Prozesse und Technologien erfordert Veränderungen in der
    Arbeitsweise der Mitarbeiter und der Organisation. Widerstand gegen
    Veränderungen und mangelnde Akzeptanz können die Umsetzung des
    SouvAP behindern. Eine umfassende Change-Management-Strategie ist
    erforderlich, um die Akzeptanz der Mitarbeiter zu fördern und sie
    auf den mit dem SouvAP angestrebten digitalen Wandel vorzubereiten.

-   **Digitale Kluft:** Die Einführung des SouvAP in der komplexen
    Verwaltungslandschaft kann dazu führen, dass bestimmte Teile der
    Verwaltung von den Vorteilen ausgeschlossen werden. Es besteht das
    Risiko einer digitalen Kluft, in der Teile der
    Verwaltungsmitarbeiter von der Nutzung des SouvAP ausgeschlossen
    sind, oder zumindest Schwierigkeiten haben, diesen zu nutzen. Es ist
    wichtig, sicherzustellen, dass der SouvAP möglichst viele
    Verwaltungsmitarbeiter erreicht, inklusiv ist und niemanden
    benachteiligt.

-   **Beherrschung der Komplexität:  
    **Der SouvAP wird als ein gebrandetes Produkt in mehreren
    Produktausprägungen, mit vielen Partnern in einer ohnehin bereits
    komplexen Verwaltungslandschaft zur Verfügung gestellt. Das stellt
    eine besondere Herausforderung dar, die nur durch klare Prozesse und
    Lieferbedingungen gemeistert werden kann.

#### Risiken im Kontext der Nutzung von Software

Eines der Hauptziele des SouvAP ist die Durchsetzung der eigenen
Datensouveränität in der öffentlichen Verwaltung. Somit sind die mit
diesem Ziel verbundenen Technologien und Methoden noch einmal gesondert
zu betrachten. Speziell der Einsatz von Open Source-Software als Basis
des SouvAP ist dahingehend zu prüfen, ob es erhöhte oder gar neue
Risiken gegenüber klassischer Software gibt.

-   **Sicherheit:** Es ist wichtig, dass Sicherheitsaspekte während des
    gesamten Softwareentwicklungszyklus berücksichtigt werden. Dazu
    gehören Sicherheitsbewertungen, Sicherheitstests, die Einhaltung
    bewährter Sicherheitspraktiken und die kontinuierliche Überwachung
    der Sicherheitslage. Durch eine proaktive Herangehensweise an die
    Sicherheit können viele potenzielle Sicherheitsrisiken minimiert
    oder vermieden werden. Darüber hinaus enthält eine große Anzahl
    Nicht-Open Source bereits Open Source Bestandteile.  
    In diesem Kontext hervorzuheben ist eine automatisierte Codeanalyse
    zur Schadcodeerkennung. Bei Open Source finden Teile des
    Softwareentwicklungszyklus jetzt allerdings öffentlich statt und
    sind damit auch transparent und vor allem überprüfbar. Z.B. kann
    geprüft werden, ob Kompilate zu den Sources passen.

-   **Unterstützung:** Open Source profitiert in der Regel von
    gemeinschaftlichen Entwicklungen, die nicht unbedingt die in der
    öffentlichen Verwaltung zwingend notwendige
    IT-Unterstützungsleistung umfassen muss. Um diese
    Unterstützungsleistungen, einschließlich z.B. Schulungsangeboten
    trotzdem sicherzustellen, werden darauf spezialisierte Firmen
    betraut, welche oft zusätzliche Enterprise Editionen mit erweiterten
    Funktionen zur Verfügung stellen.

-   **Kompatibilität und Standards:** Um eine hohe Verbreitung des
    SouvAP zu garantieren, müssen die dazugehörigen Module sowohl
    untereinander kompatibel sein, vor allem müssen aber die vorhandenen
    Schnittstellen in andere Ökosysteme der Kunden auf tatsächlich
    verwendeten Standards beruhen. Dies führt zu Herausforderungen, da
    die in der öffentlichen Verwaltung genutzten IT-Verfahren häufig
    proprietär entwickelt wurden. Hinzu kommt, dass neben freien
    IT-Standards ebenfalls verbreitete firmenabhängige Pseudostandards
    im Einsatz sind.  
    Die Kompatibilität und Interoperabilität müssen in jedem Fall
    sorgfältig geprüft und möglicherweise angepasst werden, um
    reibungslose Abläufe sicherzustellen. Open Source hilft hier
    insofern sogar weiter, weil zumindest die Chance besteht, Standards
    einsehen und Anpassungen vornehmen zu können.

-   **Abhängigkeiten:** Der Erfolg und die Weiterentwicklung von Open
    Source-Software hängen häufig von der Unterstützung und der
    Aktivität einer Community ab. Es besteht das Risiko, dass ein Open
    Source-Projekt nicht mehr aktiv gepflegt wird oder die
    Entwicklergemeinschaft sich auflöst. Dies kann zu Herausforderungen
    bei der langfristigen Wartung und Unterstützung führen. Auf der
    anderen Seite können natürlich auch klassische Firmen ihren
    Geschäftsbetrieb einstellen, ohne dass ein Nachfolger bereitsteht.
    Bei Open Source-Software kann die Verantwortung auf eine breitere
    Gemeinschaft verteilt sein, was die Abhängigkeit von einem einzelnen
    Anbieter zu verringern hilft.  
    Darüber hinaus sind freiwillige Entwicklergemeinschaften i.d.R.
    schneller in ihren Reaktionszeiten zu Sicherheitslücken. Benutzer
    müssen nicht darauf vertrauen, dass der Closed Source Anbieter
    schnell auf Sicherheitsbedrohungen reagiert und die notwendigen
    Updates bereitstellt.

-   **Lizenzierung:** Die Einführung des SouvAP in der öffentlichen
    Verwaltung muss sicherstellen, dass die Lizenzbedingungen der
    verwendeten Open Source-Software richtig verstanden und eingehalten
    werden, um mögliche rechtliche Konflikte zu vermeiden.

#### Allgemeine Risiken in der Einführung des SouvAP

Neben eher allgemeinen Risiken, die jede größere Produktklasse im Rahmen
der Digitalisierung betreffen existieren eine Reihe von spezifischen
Risiken bei der Einführung des SouvAP:

-   **Gesellschaftliche Akzeptanz (Anspruch):**  
    Der SouvAP hat ein erhebliches Aufmerksamkeitspotenzial, da er in
    direktem Wettbewerb zu den weit verbreiteten kommerziellen Anbietern
    steht. Damit geht ein sehr hoher Anspruch an die Lösung einher, der
    erst einmal noch eingelöst werden muss.  
    Dies umfasst z.B. die Erkenntnis, dass es keine ‚schwachen‘,
    unausgereiften Teile in der Lösung geben darf, oder es geht mit
    einer offenen Kommunikationsstrategie einher, die die Adressierung
    dieses Risiko zum Teil des Einführungsprozesses selbst macht.

-   **Nutzerspezifische Akzeptanz:**  
    Der individuelle Nutzer in der öffentlichen Verwaltung bekommt mit
    dem SouvAP eine Produktklasse, die er mit hoher Wahrscheinlichkeit
    in Form kommerzieller Produkte bereits kennt. Dadurch existiert eine
    individuelle Erwartungshaltung, was den Funktionsumfang, als auch
    die Laufzeitumgebung, Performance usw. des SouvAP betrifft.  
    Es müssen geeignete Maßnahmen, wie z.B. Schulungen usw. durchgeführt
    werden, die die Akzeptanz zumindest nicht durch Unkenntnis
    schmälern.

-   **Akzeptanz durch die Leistungserbringer:**  
    Die Leistungserbringung durch die Softwarehersteller und -betreiber
    muss sich für sie selbst lohnen. Das umfasst nicht nur die
    Wirtschaftlichkeit, sondern auch weichere Faktoren wie
    Bekanntheitsgrad, Zuverlässigkeit, Renommee usw.  
    Nur in einer tatsächlichen Partnerschaft der Beteiligten wird diese
    Akzeptanz zu erreichen sein.

-   **Wirtschaftlichkeit:**  
    Die Einführung des SouvAP wird mit erheblichen Kosten und komplexen
    Kostenstrukturen verbunden sein, einschließlich der Lizenzgebühren,
    der Schulung von Mitarbeitern, der Anpassung oder Integration in
    bestehende Systeme und der laufenden Wartung und Aktualisierung. Es
    besteht das Risiko von Kostenüberschreitungen, insbesondere wenn die
    Teilprojekte nicht gut geplant sind oder das Budget nicht angemessen
    festgelegt wurde.  
    Hinzu kommt, dass mit dieser Klasse von Arbeitsmitteln die Messung
    tatsächlicher Produktivitätsgewinne und damit die Gründe für
    Investitionen sehr schwierig zu ermitteln sind.

#### Spezifische Risiken mit der Einführung des SouvAP

-   **Technische Probleme:**  
    Bei der Einführung des SouvAP besteht das Risiko technischer
    Probleme wie fehlerhafter Installation, nicht gegebener
    Interoperabilität, Inkompatibilität mit bestehender Infrastruktur,
    Softwarefehlern oder Performance-Problemen. Solche Probleme können
    zu Betriebsunterbrechungen, Datenverlust und Frustration bei den
    Nutzern führen.

-   **Datenmigration und -integrität:**  
    Die Übertragung von Daten von alten Systemen auf den neuen SouvAP
    kann ein komplexer Prozess sein. Es besteht das Risiko von
    Datenverlust oder -beschädigung während der Migrationsphase. Darüber
    hinaus müssen die Daten in der neuen Software korrekt und konsistent
    integriert werden, um die Datenintegrität sicherzustellen.  
    Insbesondere in der Öffentlichen Verwaltung existiert eine Vielzahl
    von Bestandsdokumenten, die nicht nur der Anzahl wegen
    Aufmerksamkeit verlangen, sondern auch wiedergabetreu sein müssen.

-   **Performance:**  
    Der SouvAP ist als reine Cloud-Lösung geplant und damit insbesondere
    von der jeweiligen Netzinfrastruktur abhängig, um performant zu
    sein.  
    Hier gilt es von vornherein Offline-Fähigkeiten für den Fall
    zeitweiser Netzwerkunterbrechungen zu implementieren.

Es ist wichtig, dass alle Partner in der Implementierung des SouvAP
diese Risiken kennen und angemessene Maßnahmen ergreifen, um sie zu
adressieren und zu minimieren. Dies kann die Durchführung einer
sorgfältigen Risikobewertung, die Implementierung von
Sicherheitsmaßnahmen und die gesonderte Einbindung von Fachleuten
umfassen, um den sicheren und effektiven Einsatz des SouvAP in der
öffentlichen Verwaltung zu gewährleisten.

------------------------------------------------------------------------

1.  Vgl. [<span
    class="underline">https://pubs.opengroup.org/architecture/togaf9-doc/arch/chap07.html</span>](https://pubs.opengroup.org/architecture/togaf9-doc/arch/chap07.html)<a href="#fnref1" class="footnote-back">↩︎</a>

2.  Vgl. [<span class="underline">6 Digital Workplace Trends on the
    Gartner Hype Cycle for the Digital Workplace,
    2020</span>](https://www.gartner.com/smarterwithgartner/6-trends-on-the-gartner-hype-cycle-for-the-digital-workplace-2020)<a href="#fnref2" class="footnote-back">↩︎</a>

3.  Vgl. [<span class="underline">Digitalstrategie: Digitaler Aufbruch |
    Bundesregierung</span>](https://www.bundesregierung.de/breg-de/themen/digitaler-aufbruch/digitalstrategie-2072884)<a href="#fnref3" class="footnote-back">↩︎</a>

4.  Vgl. [<span class="underline">Digitalstrategie Deutschland |
    Digitalstrategie Deutschland
    (digitalstrategie-deutschland.de)</span>](https://www.digitalstrategie-deutschland.de/)<a href="#fnref4" class="footnote-back">↩︎</a>

5.  Vgl. [<span
    class="underline">https://digitales.hessen.de/moderne-verwaltung/strategie-digitale-verwaltung</span>](https://digitales.hessen.de/moderne-verwaltung/strategie-digitale-verwaltung)<a href="#fnref5" class="footnote-back">↩︎</a>
