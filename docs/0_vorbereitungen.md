Vorbereitungen
==============

Ausgangslage
------------

### Verfügbare Vorgehensmodelle für die Architekturarbeit

Die Architektur zum SouvAP behandelt die drei Phasen

-   Planung – Sammlung von Anforderungen, was der SouvAP leisten muss,

-   Aufbau – Transformation der Anforderungen auf
    Service-Bereitstellungen – und

-   Betrieb – Erbringung der Services gemäß geltender
    Dienstgütevereinbarungen.

Das Vorgehen in der Planung soll die Architekturdomänen

-   Geschäftsarchitektur,

-   Informationssystem- und Datenarchitektur und

-   Infrastrukturarchitektur

differenzieren.

Zu jeder dieser drei Architekturdomänen wurden in den letzten Jahren
unterschiedliche Vorgehensmodelle entwickelt, die im Kern die
Transformation eines Ist-Zustands zu einem Soll-Zustand adressierten.
Aufgrund der fortschreitenden IT-Durchdringung in praktisch allen
Lebensbereichen hat sich in den letzten Jahren eine ganzheitliche
Behandlung aller drei Architekturdomänen in einem übergreifenden
Vorgehensmodell durchgesetzt. Aktuell wird international die
Architektur-Entwicklungsmethode gemäß TOGAF ([<span
class="underline">The Open Group Architecture
Framework</span>](https://pubs.opengroup.org/architecture/togaf9-doc/arch/index.html))
präferiert. Dort sind die zuvor genannten Phasen Planung, Aufbau und
Betrieb mitberücksichtigt.

Aufgrund der hohen Anforderungen der verwalterischen Datenverarbeitung
an regulatorische Vorgaben deckt die Vorgehensweise gemäß TOGAF jedoch
nicht alle benötigten Aspekte einer Architektur eines SouvAP für die
öffentliche Verwaltung ab. Zum einen behandelt TOGAF selbst in keiner
Weise Sicherheitsaspekte, zum anderen macht TOGAF keine Vorgaben zu
Betriebsprozessen. Zu diesen zwei Aufgabenstellungen kann sich die
Architektur zum SouvAP an den folgenden Rahmenwerken bedienen:

-   [<span class="underline">IT Grundschutz
    (BSI)</span>](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Standards-und-Zertifizierung/IT-Grundschutz/it-grundschutz_node.html)
    diskutiert Gefahren bei der Datenverarbeitung aus Sicht des
    Anlagenbetreibers. Ein umfangreicher Katalog an Maßnahmen adressiert
    den Schutz des kompletten IT-Infrastrukturverbunds über alle Phasen
    seines Lebenszyklus, also ab Planung über Beschaffung und Betrieb
    bis zur Dekommisionierung.

-   [<span class="underline">ITIL</span>](https://www.itlibrary.org/)
    bündelt Leitlinien, die auf die Bereitstellung von IT-Services in
    hoher Qualität fokussieren. ITIL beschreibt Vorgehensweisen, mit
    denen ein IT-Dienstleister komplexe Aufgaben des IT-Betriebs ab
    Planung über Inbetriebnahme, Betrieb, Wartung und Aussonderung
    effizient und mit geringem Fehlerrisiko umsetzen kann.

Aspekte zur Sicherheit sind als Querschnittsdomäne zu betrachten,
faktisch also in die Phasen Planung, Aufbau und Betrieb sowie in die
Ausgestaltung der Architekturdomänen Geschäftsarchitektur, Informations-
und Datenarchitektur sowie Infrastrukturarchitektur einzubeziehen. Für
die zuletzt genannten Architekturdomänen hat die [<span
class="underline">Sherwood Applied Business Security
Architecture</span>](https://sabsa.org/) SABSA ein Vorgehensmodell
entwickelt, das in TOGAF integriert werden kann.

### Einflussnehmende strategische Dokumente

Die folgenden strategischen Dokumente beeinflussen die Ausgestaltung der
Architekturarbeit:

-   [<span class="underline">Nationale E-Government
    Strategie</span>](https://www.it-planungsrat.de/fileadmin/it-planungsrat/der-it-planungsrat/nationale-e-government-strategie/NEGS_Fortschreibung.pdf),

-   [<span class="underline">Deutsche Verwaltungscloud
    Strategie</span>](https://www.it-planungsrat.de/fileadmin/beschluesse/2021/Beschluss2021-46_Deutsche_Verwaltungscloud-Strategie_AL1.pdf),

-   [<span class="underline">IT-Strategie des
    Bundes</span>](https://www.cio.bund.de/SharedDocs/downloads/Webs/CIO/DE/digitaler-wandel/it-strategie/version-der-it-strategie.pdf?__blob=publicationFile&v=1),

-   IT-Strategien/Digitalstrategien der Länder (z.B. [<span
    class="underline">Freie und Hansestadt
    Hamburg</span>](https://www.hamburg.de/contentblob/13508768/703cff94b7cc86a2a12815e52835accf/data/download-digitalstrategie-2020.pdf),
    [<span class="underline">Hansestadt
    Bremen</span>](https://www.transparenz.bremen.de/metainformationen/verwaltung-4-1-eine-e-government-und-digitalisierungsstrategie-fuer-die-freie-hansestadt-bremen-111440),
    [<span
    class="underline">Sachsen-Anhalt</span>](https://www.investieren-in-sachsen-anhalt.de/fileadmin/SOM/SOM_Uebergreifend/Printprodukte/Studien_und_Co/IKT-_Strategie2020_Sachsen_Anhalt_2012.pdf),
    [<span
    class="underline">Schleswig-Holstein</span>](https://www.landtag.ltsh.de/infothek/wahl19/umdrucke/03300/umdruck-19-03354.pdf)),

-   Zielbilder der öffentlichen RZ-Dienstleister.

### Geschäftsprinzipien, Geschäftliche Ziele

An dieser Stelle sind geschäftliche Prinzipien und Ziele herausgestellt,
die übergreifend Einfluss auf jede Architekturdomäne nehmen sollen:

-   Risikobewusstsein,

-   Wirtschaftlichkeit,

-   Investitionssicherheit,

-   Zukunftsfähigkeit,

-   Robustheit,

-   Effektivität,

-   Qualität,

-   Kundenzufriedenheit,

-   Informationssicherheit,

-   Datenschutz und

-   Geheimschutz.

Die genannten Schlagwörter sind in den zuvor genannten IT-Strategien und
in den Zielbildern der öffentlichen Verwaltungen als strategische Ziele
herausgestellt. Die Auswahl wurde auf Ziele konzentriert, die für den
SouvAP als relevant eingestuft werden.

### Übergreifende Rahmenbedingungen

Des Weiteren wird die Architekturarbeit durch die folgenden
unternehmerischen und rechtlichen Rahmenbedingungen flankiert:

-   Verwaltungsvorschriften,

-   betriebliche Richtlinien und Standards der beteiligten
    Organisationen,

-   geltende Gesetze, z.B. [<span
    class="underline">Datenschutzgrundverordnung</span>](https://dsgvo-gesetz.de/),
    [<span
    class="underline">IT-Sicherheitsgesetz</span>](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl121s1122.pdf#__bgbl__%2F%2F*%5B@attr_id%3D'bgbl121s1122.pdf'%5D__1678095052054).

### Anvisierter Geschäftsnutzen der Architekturarbeit

Die Architekturarbeit zum SouvAP will Bund, Länder und Kommunen in die
Lage versetzen, ebenen-übergreifend standardisiert medienbruchfrei und
souverän verwalterische Geschäftsprozesse mit digitalen Werkzeugen zu
bewältigen. Sie will damit einen wesentlichen Beitrag zur Modernisierung
des Staates liefern. Der SouvAP will dabei die Voraussetzungen für eine
digitale Autonomie schaffen und somit die digitale Souveränität des
Staates fördern.

### Beteiligte Unternehmen und Partner

Der SouvAP wird in einer kollaborativen, offenen und effektiven
Zusammenarbeit innerhalb der öffentlichen Verwaltung (ÖV) erstellt. Die
Softwarebasis wird aus vorhandenen Open Source (OS) Ökosystemen
assembliert. Dazu zählen initial die Nextcloud GmbH, die OpenXchange AG,
Univention und Element. Die technische Konzeption, die Umsetzung und die
Erprobung des SouvAP sollen der IT-Dienstleister Dataport und die
beteiligten OS-Hersteller vorantreiben.

### Organisationsmodell für Architekturarbeit

Die Leistungsbeschreibung zum SouvAP sieht die Einrichtung eines
Architekturboards für die Spezifikation der grundlegenden technischen
Architektur des SouvAP vor. Dort sind das BMI, Dataport und weitere
Teilnehmende aus der ÖV beteiligt. Die OS-Hersteller werden durch
Univention vertreten. Das Architekturboard soll Architektur-Empfehlungen
bzw. Entscheidungsvorlagen an die Projektsteuerung und die technische
Entwicklung liefern.

Im technischen Beirat (Produktstrategieboard) konzentrierte Aufgaben
sollen Vertreter des BMI, von Dataport und von OS-Herstellern umsetzen.
Hier ist der Austausch mit dem OS-Ökosystem zur Umsetzung von
Anforderungen, dem Einsatz und der Weiterentwicklung von OS-Produkten zu
betreiben und es sind Produkte und Weiterentwicklungspotentiale zu
diskutieren bzw. deren Umsetzungspläne im Detail auszuarbeiten.

Ein User Experience-Board soll (strategische) Anforderungen der ÖV
identifizieren und die Erprobung des SouvAP durchführen. Im User
Experience-Board sind insbesondere Teilnehmende der ÖV vertreten (z. B.
Betreiber und Nutzende der ÖV).

Architekturvorhaben
-------------------

### Abgrenzung beteiligter Organisationseinheiten

Der SouvAP wurde vom BMI initiiert mit dem Ziel, den Bediensteten der
öffentlichen Verwaltung einen umfänglichen digitalen Werkzeugkasten für
Aufgaben der Produktion, Kommunikation, Kollaboration und (übergreifend)
die Anbindung ausgewählter Fachverfahren (z.B. E-Akte) zur Verfügung zu
stellen. Damit steht das BMI in der Rolle des Auftraggebers und die
Bediensteten der öffentlichen Verwaltung nehmen die Rolle der Nutzenden
ein.

Im Zuge der Architekturarbeit übernimmt das Architekturboard die
Architektenrolle. Der technische Beirat übernimmt die Entwicklerrolle
und wird dabei durch das OS-Ökosystem unterstützt. Produktentscheidungen
obliegen dem Auftraggeber.

Dataport soll die Bereitstellung und den Betrieb des SouvAP in einer
ersten Version wahrnehmen. In der Folge sollen der Betrieb, der Support
und die Weiterentwicklung des SouvAP auf ein Partnernetzwerk innerhalb
der öffentlichen Verwaltung ausgedehnt werden. Ergänzende Entwicklungs-
und Supportverträge mit Anbietern auch aus dem Open Source Umfeld sollen
die professionelle und nachhaltige Nutzung gewährleisten. Das Open
Source Umfeld wird hier im weiteren Sinne dem OS-Ökosystem zugeordnet.
Das Diagramm illustriert das Organisationsmodell und den
Kommunikationsfluss zwischen den Organisationen.

![](media/file0.png)

Abbildung : Organisationsmodell und Kommunikationsfluss zwischen
Organisationen

Anmerkung: Die Leistungsbeschreibung SouvAP stellt die grundsätzliche
Rahmenbedingung „DVS-Konformität“. Die Digitale
Verwaltungscloud-Strategie DVS unterscheidet Plattformbetreiber und
Softwarebetreiber. Die Architekturentwicklung SouvAP trifft diese
Unterscheidung zunächst nicht.

### Einzusetzende Rahmenwerke und Steuerung der Architekturarbeit

Die Architekturarbeit folgt den Empfehlungen des Rahmenmodells TOGAF.
Die gestaltenden Architekturphasen B bis D gemäß TOGAF werden durch eine
Domäne Sicherheitsarchitektur ergänzt. Die Sicherheitsarchitektur greift
die Empfehlungen des Rahmenwerks SABSA auf und wird als
Querschnittsdomäne geführt.

Sämtliche Tätigkeiten betreffend der Vermittlung der Bedarfe und
Anforderungen zwischen der ÖV und dem OS-Ökosystem sowie der Umsetzung
der in der Leistungsbeschreibung genannten Teilprojekte und Leistungen
zum SouvAP werden von einer Projektsteuerungsstruktur koordiniert und
überwacht. Die übergreifende Projektsteuerung leisten ein
Projektfortschrittsmonitoring, das Finanzcontrolling und das
strategische Produktmanagement. Diese Aufgaben liegen in der
Verantwortung des Auftraggebers.

### Architekturteam

Die Architekturarbeit gemäß TOGAF ist als zyklisch iteratives Vorgehen
konzipiert. TOGAF gruppiert die Einzelschritte in die Teilbereiche
Planung, Transition und Betrieb (der neuen Architektur). Im Betrieb
aufgedeckte Abweichungen vom geplanten Ziel und dort aufgedeckter
Anpassungsbedarf können eine neue Iteration begründen. Diese ist formell
über Änderungsanforderungen einzuleiten.

Das Design der Architektur (Vision, Geschäftsarchitektur,
Informationssystemarchitektur, Infrastrukturarchitektur) ist durch das
Architekturboard zu steuern und durch seine Mitglieder oder von den
Mitgliedern beauftragten Organisationen auszugestalten. Ergebnisse der
Designarbeit sind Entscheidungsvorschläge für eine technische
Architektur, die durch den Auftraggeber zu bestätigen ist.

Die anschließenden Planungsphasen (Chancen und Produkte,
Migrationsplanung) steuert der Technische Beirat. Hier ist ein enger
Dialog mit dem OS-Ökosystem erforderlich. Das Ergebnis ist eine Liste
von Produkten, die das Gremium zur Umsetzung der technischen Architektur
empfiehlt. Die Produktentscheidung trifft der Auftraggeber.

Die Transition (Implementierungs-Governance) muss unter Einbindung und
im Einklang mit den Erkenntnissen, Empfehlungen und
Unterstützungsleistungen von Architekturboard, Technischem Beirat und
User Experience-Board erfolgen.

Der abschließende Betrieb dieser ersten Iteration der
Architekturentwicklung gemäß TOGAF-Vorgehensmodell ist die zuvor
genannte Erprobung des SouvAP. Seine Durchführung steuert das User
Experience-Board, also Betreiber und Nutzende der ÖV.

### Architekturprinzipien

Die Leistungsbeschreibung verweist auf die Architekturrichtlinie des
Bundes, die Standards der Deutschen Verwaltungscloud-Strategie und die
Föderalen Architekturrichtlinien für die IT. Dazu fordert die
Leistungsbeschreibung eine Prüfung auf Verwendbarkeit im SouvAP.

### Architekturrichtlinie des Bundes

Im Dokument [<span class="underline">Architekturrichtlinie des
Bundes</span>](https://www.cio.bund.de/SharedDocs/Publikationen/DE/Strategische%5B1%5DThemen/it_strategie_der_bundesverwaltung_download.pdf?__blob=publicationFile)
sind 18 übergreifende Architekturvorgaben ausgearbeitet. Jeder Vorgabe
ist eines oder mehrere strategische Ziele gemäß der IT-Strategie der
Bundesverwaltung als Verweis zugeordnet. Des Weiteren ist jede Vorgabe
mit einer Verbindlichkeit gemäß [<span class="underline">RFC
2119</span>](https://www.ietf.org/rfc/rfc2119.txt) geprägt, wird also
mit einem Verbindlichkeitsgrad muss, soll, kann oder darf nicht
assoziiert.

Der SouvAP übernimmt die übergreifenden Architekturvorgaben aus der
Architekturrichtlinie des Bundes als Architekturprinzipien mit ihren
Verbindlichkeiten wie folgt (Abweichungen der Verbindlichkeitsgrade
gegenüber der Architekturrichtlinie des Bundes sind mit \*
gekennzeichnet):

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>ÜBAV-01</td>
<td>Einhaltung der Architekturvorgaben</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ÜBAV-02</td>
<td>Rechtliche Rahmenbedingungen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ÜBAV-03</td>
<td>Digitale Souveränität</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ÜBAV-04</td>
<td>Standards und einheitliche Methoden</td>
<td>Muss*</td>
</tr>
<tr class="odd">
<td>ÜBAV-05</td>
<td>Informationssicherheit, Datenschutz und Geheimschutz</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ÜBAV-06</td>
<td>Sichere Systemgrundkonfiguration („Security-by-Default“)</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ÜBAV-07</td>
<td>Referenzarchitekturen</td>
<td>Soll*</td>
</tr>
<tr class="even">
<td>ÜBAV-08</td>
<td>Benutzerfreundlichkeit und Barrierefreiheit</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>ÜBAV-09</td>
<td>Hersteller- und Anbieterunabhängigkeit</td>
<td>Muss*</td>
</tr>
<tr class="even">
<td>ÜBAV-10</td>
<td>Interoperabilität</td>
<td>Muss*</td>
</tr>
<tr class="odd">
<td>ÜBAV-11</td>
<td>Lose Kopplung</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>ÜBAV-12</td>
<td>Nachhaltigkeit</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>ÜBAV-13</td>
<td>Komplexität</td>
<td>Soll</td>
</tr>
<tr class="even">
<td>ÜBAV-14</td>
<td>Modularität und Wiederverwendbarkeit</td>
<td>Muss*</td>
</tr>
<tr class="odd">
<td>ÜBAV-15</td>
<td>Cloud Computing</td>
<td>Muss*</td>
</tr>
<tr class="even">
<td>ÜBAV-16</td>
<td>Digitale Kollaboration</td>
<td>Muss*</td>
</tr>
<tr class="odd">
<td>ÜBAV-17</td>
<td>Open Source</td>
<td>Muss*</td>
</tr>
<tr class="even">
<td>ÜBAV-18</td>
<td>Daten</td>
<td>Muss</td>
</tr>
</tbody>
</table>

#### Standards der Deutschen Verwaltungscloud-Strategie

Die Deutsche Verwaltungscloud-Strategie hat zum Ziel, gemeinsame
Standards für die föderale Cloud-Infrastruktur der ÖV und deren
Standorte zu definieren. Diese Standards sollen übergreifend für Bund,
Länder und Kommunen sowie für deren IT-Dienstleister gültig werden. Für
Teilnehmer an der Deutschen Verwaltungscloud ist die Umsetzung dieser
Standards verpflichtend. Ausnahmen sind in begründeten Fällen möglich.

Die Standards der Deutschen Verwaltungscloud-Strategie richten sich an
die Bereiche

1.  Entwicklung und Entwicklungsplattform,

2.  Anwendungsbereitstellung und -management,

3.  Code Repository,

4.  Infrastruktur-Service und technologischer Stack sowie

5.  Betriebsstandards und Betriebsmodell.

Die Deutsche Verwaltungscloud-Strategie listet insgesamt 20 Standards
nebst Verbindlichkeitsstufen gemäß RFC 2119. Der SouvAP übernimmt die
folgenden Standards:

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>DVS-001-R01</td>
<td>Anwendbares Recht</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-002-R01</td>
<td>Vorgaben für Produktion, Service und Subunternehmer</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-003-R01</td>
<td>Hoheit über Hard- und Software</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-004-R01</td>
<td>Standards für Softwarekomponenten</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-005-R01</td>
<td>Zertifizierung nach IT-Grundschutz des BSI</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-006-R01</td>
<td>Erfüllung des Kriterienkatalogs C5</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-007-R01</td>
<td>Hoheit über Krypto-Module und Schlüssel</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-008-R01</td>
<td>Bereitstellung von Containerumgebung (CaaS) und Container Cluster</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-009-R01</td>
<td>Anlieferung von Containerlösungen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-010-R01</td>
<td>Angebot von Erweiterungen zur Containerumgebung</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>DVS-011-R01</td>
<td>Erreichbarkeit der Standorte</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-012-R01</td>
<td>Umsetzung des Zonenmodells</td>
<td>Soll</td>
</tr>
<tr class="odd">
<td>DVS-013-R01</td>
<td>Einrichtung einer Schnittstelle zum Cloud-Service-Portal</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-014-R01</td>
<td>Betreiberwechsel</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-015-R01</td>
<td>Bereitstellung notwendiger Dokumentationen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-016-R01</td>
<td>Bereitstellung von Entwicklungsbereichen</td>
<td>Kann</td>
</tr>
<tr class="odd">
<td>DVS-017-R01</td>
<td>Anbindung an die zentrale OS-Plattform</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>DVS-018-R01</td>
<td>Unterstützung von DevOps-Ansätzen</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>DVS-019-R01</td>
<td>Angebot von Software-as-a-Service (SaaS)</td>
<td>Kann</td>
</tr>
<tr class="even">
<td>DVS-020-R01</td>
<td>Funktion eines Ausweichrechenzentrums</td>
<td>Kann</td>
</tr>
</tbody>
</table>

Die angegebenen Standards liegen aktuell in einer ersten Version vor
(Kennung R01). Nach einer Fortschreibung auf der Seite der Deutschen
Verwaltungscloud-Strategie ist die jeweils aktuelle Version gültig.

#### Föderale IT-Richtlinien

Der IT-Planungsrat hat 13 Föderale Architekturrichtlinien verfasst und
diese passend zu einer additiven Betrachtung von Vorgaben entwickelt.
Die Vorgaben wurden in die Dimensionen strategische Ziele (SR),
normative Vorgaben (R), föderal relevant (Fö) und fachübergreifend (Fu)
gruppiert. Die den daraus abgeleiteten Architekturrichtlinien jeweils
zugeordneten Verbindlichkeiten folgen den Empfehlungen von RFC 2119 mit
der Einschränkung, dass auf die Option „SOLL“ verzichtet wurde, da auch
für „MUSS“-Anforderungen begründete Abweichungen und
Ausnahmegenehmigungen zulässig sind.

Der SouvAP übernimmt die übergreifenden Architekturvorgaben aus den
Föderalen Architekturrichtlinien mit ihren Verbindlichkeiten wie folgt:

<table>
<thead>
<tr class="header">
<th>Kennung</th>
<th>Titel</th>
<th>Verbindlichkeit</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SR1</td>
<td>Verwendung von Standards</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR2</td>
<td>Sicherstellung von Wiederverwendung</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>SR3</td>
<td>Bestehende Marktstandards verwenden</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR4</td>
<td>Sichere Systemgrundkonfiguration („Security-by-Default“ und “Privacy-by-Default”)</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>SR5</td>
<td>API-First Ansatz</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR6</td>
<td>Sicherstellung der Nutzereinbindung („Usability by Design“)</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>SR7</td>
<td>Sicherstellung der Herstellerunabhängigkeit</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR8</td>
<td>Einsatz von Open Source</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>SR9</td>
<td>Gewährleistung der Interoperabilität von IT-Lösungen</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR10</td>
<td>Sicherstellung von loser Kopplung/Modularität</td>
<td>Muss</td>
</tr>
<tr class="odd">
<td>SR11</td>
<td>Gewährleistung einer umweltfreundlichen und nachhaltigen Nutzung von Informationstechnik</td>
<td>Muss</td>
</tr>
<tr class="even">
<td>SR13</td>
<td>Open Data by Design</td>
<td>Muss</td>
</tr>
</tbody>
</table>

Die Föderale Architekturrichtlinie SR12 Umsetzung des „Once Only“
Prinzips wird hier nicht übernommen, da ihr Fokus auf den Dialog von
Bürgerinnen und Bürgern mir der Verwaltung liegt. Der SouvAP ist auf die
verwalterische Geschäftsprozessunterstützung für Bedienstete der
öffentlichen Verwaltung ausgerichtet.

### Konfektionierung von Architektur-Frameworks für SouvAP

Für das Projekt SouvAP werden Architekturrichtlinien mit Bezug auf die
Architekturrichtlinie des Bundes, die Deutsche
Verwaltungscloud-Strategie und die Föderalen IT-Architekturrichtlinien
zugrunde gelegt.

Die Architekturrichtlinie des Bundes hat für ihre Darstellungen die
Formatvorlagen des TOGAF Frameworks adaptiert. Die Föderalen
IT-Architekturrichtlinien verweisen auf die Notwendigkeit, die
Gestaltung der föderalen IT-Architektur für Deutschland auf eine
ganzheitliche Betrachtung auszurichten und orientiert sich dabei am
Architekturframework TOGAF.

Die Deutsche Verwaltungscloud-Strategie trifft keine Vorgaben, nach
welchem Schema Architekturarbeiten im Rahmen der Realisierung der
Deutschen Verwaltungscloud durchzuführen sind. Die Vorlage der dort
festgelegten Standards wurde an die der Architekturrichtlinie für die IT
des Bundes angelehnt, ist also gewissermaßen TOGAF-konform.

Es bietet sich an, die Architekturarbeit zum SouvAP mit der der Methode
gemäß TOGAF umzusetzen, zumal die auf das Zielbild einflussnehmenden
Behörden des Auftragnehmers offensichtlich selbst bereits die
Herangehensweise von TOGAF favorisieren und zumindest in Teilen
adaptieren. Auch das in Deutschland für technische Sicherheitsstandards
verantwortliche Bundesamt für Sicherheit in der Informationstechnik BSI
weiß TOGAF zu schätzen und wendet den Optimierungsansatz „Architecture
Development Method“ ADM in seinem [<span
class="underline">Hochverfügbarkeits-Kompendium Band
G</span>](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Hochverfuegbarkeit/HVKompendium/BandG/HVKompendium_Band_G_node.html)
an. Das Vorgehen: rekursive Verbesserung von IT-Ressourcen mit Blick auf
die Qualitätsanforderungen der Geschäftsprozesse, um eine
Hochverfügbarkeits-Konzeption betreffend Rest-Risiken zu optimieren.

Aufgrund der sicherheitsrelevanten Rahmenbedingungen ist die Kopplung
mit einer Sicherheitsarchitektur erforderlich. Diese muss die
systematische Entwicklung der Architekturdomänen Geschäftsarchitektur,
Informationsarchitektur und Infrastrukturarchitektur begleiten. Die
anschließende Implementierung bzw. Migration kann dadurch direkt auf
sicherheitsrelevante Anforderungen zugreifen und es liegen auch bereits
logische und technische Umsetzungshinweise vor. Nach aktueller
Eischätzung ist hierfür das Vorgehensmodell SABSA zu favorisieren, da
TOGAF dafür eine [<span class="underline">nahtlose
Ergänzung</span>](https://sabsa.org/sabsa-togaf-integration-white-paper-download-request/)
dokumentiert hat.
