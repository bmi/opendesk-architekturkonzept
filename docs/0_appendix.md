Appendix
========

Bei Verwendung offener Schnittstellen und Standards wird die Art und
Weise der Kommunikation innerhalb einer heterogenen Systemlandschaft
(hier die Komponenten des Souveränen Arbeitsplatzes) standardisiert und
Transparenz geschaffen, auf welche Art und Weise Informationen
ausgetauscht werden. Dabei können offene Schnittstellen und Standards
Aufgaben auf vier Ebenen übernehmen.

Offene Schnittstellen und Standards auf **struktureller Ebene** dienen
der Vereinheitlichung davon, wie Daten von A nach B fließen (und ggf.
zurück). Beispiele hierfür sind u.a. REST oder TLS/SSL.

Auf **syntaktischer Ebene** übernehmen offene Schnittstellen und
Standards die Aufgabe, bestimmte Strukturen im Datenstrom vorzugeben.
Beispiel hierfür sind u.a. XML oder JSON.

Auf der **semantischen Ebene** geht es darum, Inhalte oder Funktionen
systemübergreifend zu verstehen (z.B. bei Abgleich von Verfügbarkeiten
in Kalendern, der Darstellung des Anwesenheitsstatus in Chats oder der
Darstellung des Datumsformats in der Tabellenkalkulation). Beispielhaft
sind hier iCal, XMPP, ISO 8601 zu nennen. Standards dieser Ebene können
auf syntaktische Standards zurückgreifen, z.B. XMPP nutzt XML, oder eine
eigene syntaktische Ebene implementieren, z.B. PDF.

Auf der **organisatorischen Ebene** setzen Schnittstellen und Standards
oft einen semantischen oder syntaktischen Teil voraus und adressieren
zudem anwendungsübergreifende Anforderungen. So können durch Verwendung
solcher Schnittstellen und Standards beispielsweise Rechte und Rollen
einheitlich über diverse Anwendungen und Nutzungsweisen hinweg
Verwendung finden, z.B. durch eine Modellierung der Unternehmensstruktur
innerhalb LDAP, welches durch die organisatorische Notwendigkeit einer
zentralen Rechte und Rollenmodellierung in nahezu jeder Anwendung
verankert ist.

Beim Souveränen Arbeitsplatz werden offene Schnittstellen auf allen vier
benannten Ebenen zum Einsatz kommen.

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
<col style="width: 25%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Standard</th>
<th>Ebene</th>
<th>Aufgabe/Beschreibung</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><em>LDAP</em></td>
<td>IETF RFC 4511</td>
<td><em>Organisatorisch</em></td>
<td><em>Rechteverwaltung, Adressbuch, Benutzer-Identitäten</em></td>
</tr>
<tr class="even">
<td>Matrix</td>
<td></td>
<td>Semantisch</td>
<td>Kommunikation</td>
</tr>
<tr class="odd">
<td><p>ODF,</p>
<p>OOXML</p></td>
<td><p>OASIS ODF</p>
<p>ISO/IEC 29500</p></td>
<td>Semantisch</td>
<td><p>Office-Dokument-Auszeichnungsformat zur</p>
<p>Definition von Office-Dokumentinhalten</p></td>
</tr>
<tr class="even">
<td>OpenID Connect</td>
<td>RFC 6750</td>
<td>Semantisch</td>
<td>Identity Management</td>
</tr>
<tr class="odd">
<td>PDF/A-3</td>
<td>ISO/IEC 32000-1</td>
<td>Semantisch</td>
<td>Dokumentaustausch</td>
</tr>
<tr class="even">
<td>SAMLv2</td>
<td>RFC 7522</td>
<td>Semantisch</td>
<td>Identity Management</td>
</tr>
<tr class="odd">
<td>Sieve</td>
<td>IETF RFC 5183</td>
<td>Semantisch</td>
<td>Mailfilter</td>
</tr>
<tr class="even">
<td>SIP</td>
<td></td>
<td>Semantisch</td>
<td>Telefonie</td>
</tr>
<tr class="odd">
<td><em>CardDAV</em></td>
<td>IETF RFC 6352</td>
<td><em>Semantisch</em></td>
<td><em>Kontaktdaten­austausch</em></td>
</tr>
<tr class="even">
<td><em>CalDAV</em></td>
<td>IETF RFC 6638</td>
<td><em>Semantisch</em></td>
<td><em>Kalender, Kollaboration</em></td>
</tr>
<tr class="odd">
<td><em>WebDAV<br />
</em></td>
<td>IETF RFC 2616 / IETF RFC 4918</td>
<td><em>Semantisch</em></td>
<td><em>Verfügbarkeit Dateien und Dokumente, Reservierung und exklusiver Zugriff, Versionierung</em></td>
</tr>
<tr class="even">
<td>CMIS</td>
<td>OASIS CMIS</td>
<td><em>Semantisch</em></td>
<td><em>Verfügbarkeit Dateien und Dokumente, Reservierung und exklusiver Zugriff, Versionierung</em></td>
</tr>
<tr class="odd">
<td>IMAP</td>
<td>IETF RFC 3501</td>
<td>Semantisch</td>
<td>E-Mail, Kollaboration</td>
</tr>
<tr class="even">
<td>TLS / SSL</td>
<td>RFC 8446</td>
<td>Strukturell</td>
<td>Verschlüsselte Kommunikation</td>
</tr>
<tr class="odd">
<td>WebRTC</td>
<td>W3C WebRTC 1.0</td>
<td>Semantisch</td>
<td>Video Meeting</td>
</tr>
</tbody>
</table>
