--from https://github.com/jgm/pandoc/issues/6317#issuecomment-977150938
function RawBlock(raw)
    if raw.format:match 'html' and raw.text:match '%<table' then
      return pandoc.read(raw.text, raw.format).blocks
    end
  end