# openDesk Architekturkonzept

Der Inhalt dieser Website und des Architekturkonzepts wird entsprechend des TOGAF Frameworks in einem iterativen Prozess geändert. 

## Das Projekt openDesk

Der openDesk ist wesentlicher Bestandteil für eine selbstbestimmte, sichere und zukunftsfähige Informationstechnik (IT) für die gesamte Öffentliche Verwaltung (ÖV). Mittels openDesks wird Mitarbeitenden, IT-Administratoren und Betreibern der ÖV zukünftig eine wirksame Open Source (OS)-basierte Alternative im Bereich Arbeitsplatzumgebung zur Verfügung stehen. openDesk stellt dabei eine Maßnahme im Rahmen der gemeinsamen Strategie zur Stärkung der Digitalen Souveränität von Bund, Ländern und Kommunen sowie einen wesentlichen Schritt zur Auflösung kritischer Abhängigkeiten in der IT der ÖV dar. Weiterführende Informationen können in unseren FAQs gefunden werden.


## Zielsetzung

Ziel des Projektes ist der Aufbau der Basisvariante von openDesk bis Ende 2023. Die Basisfunktionen umfassen alle notwendigen Anwendungen für die Mitarbeitenden der ÖV in den Bereichen Produktivität, Kollaboration und Kommunikation. Alle Anwendungen basieren auf bestehenden und bewährten OS-Produkten.
Das Projekt unterteilt sich grob in die technische Entwicklung (z.B. Architektur, Plattform- und Anwendungsdienste), die Einbettung in die Strukturen der ÖV (z.B. Deutsche Verwaltungscloud-Strategie (DVS)-konforme Bereitstellung) und die Erprobung der Anwendungen, Fachverfahren und des Betriebs.


## So können Sie im Architekturkonzept kooperieren
    
Wir freuen uns auf Ihre Teilnahme!
Das Architekturkonzept wird in seinen einzelnen Kapitel als Markdown .md Dateien gepflegt. Änderungen können per Merge Request eingereicht werden. Die eingereichten Merge Request werden durch den Projektkreis reviewed.
Diese GitLab Page hostet immer den letzten Stand der Kapitel Inhalte. Die Markdown Kapitel werden als Release ebenfalls als konvertierte .docx bereitgestellt.


## Rückmeldungen und Beteiligung

Wir befinden uns mitten in der Entwicklung und freuen uns über Verbesserungsvorschläge und Feedback.
Die Feedbackkanäle sind in unserem [Informationsrepository unter dem Abschnitt "Mitwirkung und Beteiligung"](https://gitlab.opencode.de/bmi/souveraener_arbeitsplatz/info/-/blob/main/OVERVIEW.md#rückmeldungen-und-beteiligung) zu finden.

## Lizenz

Der Dokumentationsinhalt des Architekturkonzepts ist CC-BY-4.0 lizenziert.